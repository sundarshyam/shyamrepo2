/* Formatted on 2014/01/24 15:17 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_qa_cmp_qaevaluation.bdy";

CREATE OR REPLACE PACKAGE BODY it_pkg_qa_cmp_qaevaluation
IS
--
	/******************************************************************
	* Description : This procedure is used to update type QA evaluation and update RA partnumber details  
	****************************************************************/
	 PROCEDURE gm_upd_ra_qaevaluation
  	(
  		p_help_desk_id	IN	t914_helpdesk_log.c914_helpdesk_id%TYPE,
 		p_rma_id    IN t506_returns.C506_RMA_ID%TYPE
  	)
	AS
		v_type 	   	   t506_returns.c506_type%TYPE;
		v_reason	   t506_returns.c506_reason%TYPE;		
		v_user_id	   t506_returns.c506_last_updated_by%TYPE;
	--
	CURSOR item_cursor IS
		SELECT T507.c506_rma_id, T507.c205_part_number_id, T507.c507_item_qty , T506.c506_created_by 
		  FROM t507_returns_item T507, t506_returns T506
		   WHERE T507.c506_rma_id IN (p_rma_id)
		   AND T507.c507_status_fl = 'Q'
		   AND T507.c506_rma_id = T506.c506_rma_id
		   AND T506.c506_void_fl IS NULL;  
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Correcting RA ' || p_rma_id);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		BEGIN
			
		   SELECT c506_type,c506_reason 
			  INTO v_type,v_reason
			   FROM t506_returns WHERE c506_rma_id = p_rma_id 
			   AND c506_void_fl IS NULL;
		       
		EXCEPTION WHEN NO_DATA_FOUND THEN
		     DBMS_OUTPUT.put_line ('Invalid RA - ' || p_rma_id );
		END;
		IF (v_type <> '3301' AND v_type <> '3302') --3301 - Consignment - Sets   3302 - Consignment - Items
		THEN
			raise_application_error ('-20999' , 'Invalid RA Type - ' || p_rma_id );
		END IF;
		
		IF (v_reason = '3253') --3253-QA Evaluation
		THEN
			raise_application_error ('-20999' , 'RA already in QA Evaluation - ' || p_rma_id );
		END IF;		
		
		UPDATE t506_returns  
			SET c506_reason = 3253 --QA Evaluation
			, c506_last_updated_by = v_user_id
			, c506_last_updated_date = SYSDATE
			WHERE c506_rma_id = p_rma_id AND c506_void_fl IS NULL;

		    
			 BEGIN 
			    FOR currindex IN item_cursor
			     LOOP 			        
			         gm_pkg_qa_com_rsr_txn.gm_sav_ra_incident_link(currindex.c506_rma_id, currindex.c205_part_number_id, 
			         	currindex.c507_item_qty , currindex.c506_created_by );			         
			     END LOOP;
			 END;

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id
											 , p_rma_id || ' Successfully changed to QAEvaluation'
											  );
		DBMS_OUTPUT.put_line (p_rma_id || ' Successfully changed to QAEvaluation')
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing QA evaluation: ' || SQLERRM);
			ROLLBACK;
	END gm_upd_ra_qaevaluation;

	
END it_pkg_qa_cmp_qaevaluation;
/
