/* Formatted on 2011/01/14 15:08 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_mfg.bdy"

CREATE OR REPLACE PACKAGE BODY it_pkg_mfg
IS
--

/*********************************************************
	* Description : This procedure is used to updated 
	* control number for specific part in Manufacture
	*********************************************************/
	PROCEDURE gm_sav_control_num (
		p_help_desk_id		t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_mr_id 			t304_material_request_item.c303_material_request_id%TYPE
	  , p_part_num			t304_material_request_item.c205_part_number_id%TYPE
	  , p_source_ctrl_num	t304_material_request_item.c304_control_number%TYPE
	  , p_target_ctrl_num	t304_material_request_item.c304_control_number%TYPE
	)
	AS
		v_user_id	   NUMBER;
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		UPDATE t304_material_request_item
		   SET c304_control_number = p_target_ctrl_num
		 WHERE c303_material_request_id = p_mr_id
		   AND c205_part_number_id = p_part_num
		   AND c304_control_number = p_source_ctrl_num
		   AND c304_delete_fl IS NULL;

		--
		IF (SQL%ROWCOUNT = 0)
		THEN
			ROLLBACK;
			raise_application_error ('-20085', 'No record is found to be updated.');
		END IF;

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'MR ID: ' || p_mr_id);
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'New Control Number: ' || p_target_ctrl_num);
		DBMS_OUTPUT.put_line (SQL%ROWCOUNT || ' Row(s) are updated. ');
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_control_num;
END it_pkg_mfg;
/
