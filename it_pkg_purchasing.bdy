/* Formatted on 2011/03/04 15:17 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_purchasing.bdy"

CREATE OR REPLACE PACKAGE BODY it_pkg_purchasing
IS
--
	/******************************************************************
	* Description : This procedure is used to  add countries
	* to the available REGIONS/INHOUSE list
	****************************************************************/
	PROCEDURE gm_sav_region (
		p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_region_name	 t9350_hierarchy.c9350_hierarchy_nm%TYPE
	)
	AS
		v_count 	   NUMBER;
		v_user_id	   NUMBER;
		v_seq		   NUMBER;
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		---Check if the region already existed in the T9350_HIERARCHY table
		SELECT COUNT (1)
		  INTO v_count
		  FROM t9350_hierarchy t9350
		 WHERE t9350.c9350_hierarchy_nm = p_region_name;

		SELECT MAX (c9350_hierarchy_id) + 1
		  INTO v_seq
		  FROM t9350_hierarchy;

		IF (v_count > 0)
		THEN
			raise_application_error ('-20085'
								   , 'Region ' || p_region_name || ' already existed in the HIERARCHY table.');
		ELSE
			INSERT INTO t9350_hierarchy
						(c9350_hierarchy_id, c9350_hierarchy_nm, c9350_void_fl, c9350_due_days, c9350_created_by
					   , c9350_created_date, c9350_last_updated_by, c9350_last_updated_date
						)
				 VALUES (v_seq, p_region_name, NULL, NULL, v_user_id
					   , SYSDATE, NULL, NULL
						);
		END IF;

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id
											 , 'Region: ' || p_region_name || ' added to HIERARCHY tablen'
											  );
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_region;

	  /********************************************************************************
    * Description : This Procedure is used to map region with all US DEMAND SHEETS.
    *********************************************************************************/
PROCEDURE gm_map_region_to_demandsheet (
        p_user_id IN T4021_DEMAND_MAPPING.C4021_LAST_UPDATED_BY%TYPE,
        p_jira_id IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
        p_demand_types IN VARCHAR2,
        p_ref_types IN VARCHAR2)
AS
    CURSOR dm_cur
    IS
         SELECT     c4020_demand_master_id v_master_id, C4020_DEMAND_NM v_mater_nm
               FROM t4020_demand_master
              WHERE C4020_VOID_FL IS NULL
                AND C4020_INACTIVE_FL IS NULL
                AND c901_demand_type IN
                (SELECT token FROM v_in_list WHERE token IS NOT NULL
                )
            AND c4020_demand_master_id IN
            (SELECT     c9352_ref_id
                   FROM t9352_hierarchy_flow
                  WHERE c901_ref_type = 60260              --Demand sheet type
                    AND c9350_hierarchy_id NOT IN (27, 28) --Excluding OUS Direct Entities,OUS Distributors regions.
            ) ;
    CURSOR new_region_cur
    IS
         SELECT token FROM v_double_in_list WHERE token IS NOT NULL;         
    v_cnt      NUMBER;
    v_mater_nm VARCHAR (200) ;    
BEGIN
    my_context.set_my_inlist_ctx (p_demand_types) ;
    my_context.set_my_double_inlist_ctx (p_ref_types, p_demand_types) ;
    FOR master_id IN dm_cur
    LOOP
        FOR region_id IN new_region_cur
        LOOP
             --Checking already region is mapped or not. 
             SELECT     COUNT (1)
                   INTO v_cnt
                   FROM t4021_demand_mapping t4021
                  WHERE t4021.c901_ref_type = '40032'    --t901 Region reference
                    AND t4021.c4020_demand_master_id = master_id.v_master_id
                    AND t4021.c4021_ref_id = region_id.token ;
            --mapping with new region        
            IF v_cnt = 0 THEN                 
                 INSERT
                       INTO t4021_demand_mapping
                        (
                            c4021_demand_mapping_id, c4020_demand_master_id, c901_ref_type
                          , c4021_ref_id           , C4021_CREATED_BY      , C4021_CREATED_DATE
                        )
                        VALUES
                        (
                            s4021_demand_mapping.NEXTVAL, master_id.v_master_id, 40032
                          , region_id.token             , p_user_id            , SYSDATE
                        ) ;
                -- create log after the new region is mapped.
                it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_jira_id, 'REGION ' || get_code_name (region_id.token) || ' IS MAPPED  WITH '|| master_id.v_mater_nm ||' DEMAND SHEET') ;
            END IF;
        END LOOP;
    END LOOP;
    COMMIT;
EXCEPTION
WHEN OTHERS THEN
    DBMS_OUTPUT.put_line ('Error Occurred while executing: ' || SQLERRM) ;
    ROLLBACK;
    --
    --  END;
END gm_map_region_to_demandsheet;


	  /********************************************************************************
    * Description : This Procedure is used to map region with all US DEMAND SHEETS.
    *********************************************************************************/
PROCEDURE gm_sav_po_cutplan 
AS
	v_vendid	t401_purchase_order.c301_vendor_id%TYPE;
	v_pototal	t401_purchase_order.c401_po_total_amount%TYPE;
	v_userid	t401_purchase_order.c401_created_by%TYPE :='305029' ;
	v_type		t401_purchase_order.c401_type%TYPE :='3100';
	v_po_id		VARCHAR2(100);
	v_message	VARCHAR2(1000);
	v_pnum         t402_work_order.c205_part_number_id%TYPE;
	v_rev		    t402_work_order.c402_rev_num%TYPE;
	v_qty               t402_work_order.c402_qty_ordered%TYPE;
	v_cost              t402_work_order.c402_cost_price%TYPE;
	v_critfl            t402_work_order.c402_critical_fl%TYPE :='';
	v_dhrid             t402_work_order.c408_dhr_id%TYPE :='';
	v_farfl             t402_work_order.c402_far_fl%TYPE :='';
	v_validateFl        t402_work_order.c402_validation_fl%TYPE :='N';
	v_lotCount          NUMBER :=0;
	v_priceid           t402_work_order.c405_pricing_id%TYPE;
	v_substring         VARCHAR2(199);


	CURSOR cur_part
	IS
		--As we are having qty in decimal, we are rounding it, as the Portal is not handled to have decimal qty in WO.
		SELECT DISTINCT C205_PART_NUMBER_ID pnum, ROUND(SUM(C205_QTY),0) qty
		FROM MY_TEMP_PART_LIST
		WHERE C205_QTY >0
		GROUP BY C205_PART_NUMBER_ID;
BEGIN
	BEGIN
		SELECT C906_RULE_VALUE INTO v_vendid 
		FROM T906_RULES 
		WHERE C906_RULE_ID='DEFAULT_VENDOR' 
		AND C906_RULE_GRP_ID='CUT_PLAN_LOAD'
		AND C906_VOID_FL IS NULL;
	EXCEPTION WHEN OTHERS
	THEN
		v_vendid := '103'; -- Texas Human Biologics
	END;
	
	--As we do not know the part numbers and qty while creating the PO, setting the po total to 0 ,will update the po total and the end of this procedure.
	v_pototal := 0;

	GM_PLACE_PO(v_vendid,v_pototal,v_userid,v_type,NULL,v_po_id,v_message);

	IF v_po_id IS NOT NULL
	THEN
		FOR cur_val IN cur_part
		LOOP
			v_pnum := cur_val.pnum;
			v_qty  := cur_val.qty;
			
			IF v_qty IS NOT NULL
			THEN
				v_qty := TO_NUMBER(v_qty);
			END IF;
			
			--Proceed with WO Creation only when the part quantity is >0;
			IF v_qty >0
			THEN
				SELECT C205_REV_NUM INTO v_rev 
				FROM  T205_PART_NUMBER
				WHERE C205_PART_NUMBER_ID = v_pnum;
				
				BEGIN
					SELECT C405_COST_PRICE,C405_PRICING_ID INTO v_cost , v_priceid	
					FROM T405_VENDOR_PRICING_DETAILS
					WHERE 
					C405_COST_PRICE IS NOT NULL 
					AND C405_ACTIVE_FL = 'Y' 
					AND C205_PART_NUMBER_ID = v_pnum;
				EXCEPTION
				WHEN OTHERS
				THEN
					v_cost :='-1';
					v_priceid :='-1';
				END ;
				
				-- As Zero Vendor Price is loaded in BBA, we have to use >=0, as we do not want to filter out $0 parts.
				IF v_cost >=0 
				THEN
					GM_PLACE_WO(v_po_id,v_pnum,v_rev,v_vendid,v_qty,v_cost,v_critfl,v_dhrid,v_farfl,v_validateFl,v_lotCount,v_priceid,v_type,v_userid,v_message);
				ELSE
					GM_UPDATE_LOG(v_po_id,'WO Not Created due to missing Vendor Price.  '|| v_pnum,4000726,v_userid,v_message);
				END IF;

				v_pototal := v_pototal+(v_cost*v_qty);
				v_priceid := '';
				v_cost := '';
			END IF;
			
		END LOOP;

	END IF;
	UPDATE T401_PURCHASE_ORDER 
	SET C401_PO_TOTAL_AMOUNT =v_pototal
	    , C401_PO_NOTES= 'PO Created From Cut Plan Load'
	WHERE C401_PURCHASE_ORD_ID = v_po_id;

	DBMS_OUTPUT.PUT_LINE('v_pototal' ||v_pototal||'.. v_po_id'||v_po_id);


END gm_sav_po_cutplan;

 /*********************************************************************************
  * Description : This Procedure is used to void the workorder.
  **********************************************************************************/
PROCEDURE gm_void_work_order (
		p_help_desk_id IN	 t914_helpdesk_log.c914_helpdesk_id%TYPE,
	    p_wo_id	 	   IN	 t402_work_order.c402_work_order_id%TYPE,
	    p_user_id 	   IN    t402_work_order.C402_LAST_UPDATED_BY%TYPE
	)
	AS
		v_count  NUMBER;
		v_status_cnt  NUMBER;
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		
		SELECT COUNT (1) INTO v_status_cnt
			FROM t402_work_order
			WHERE c402_status_fl  < 3
			AND c402_work_order_id= p_wo_id
			AND c402_void_fl     IS NULL;

		---Check if the DHR is already existed in the T408_DHR table
		SELECT COUNT (1)
		  INTO v_count
		  FROM T408_DHR
		 WHERE C402_WORK_ORDER_ID = p_wo_id AND C408_VOID_FL IS NULL;

		IF (v_count = 0 AND v_status_cnt = 0)
		THEN
			  UPDATE T402_WORK_ORDER 
		       SET C402_VOID_FL = 'Y' 
		        ,C402_LAST_UPDATED_DATE = SYSDATE 
		        ,C402_LAST_UPDATED_BY   = p_user_id 
		       WHERE C402_WORK_ORDER_ID = p_wo_id;
		ELSE
			DBMS_OUTPUT.put_line ('WO# <'|| p_wo_id || '> cannot be void');
		END IF;

		-- logs
		 it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'WO ID: ' || p_wo_id);
		  DBMS_OUTPUT.put_line ('WO# <'|| p_wo_id || '> voided');
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_void_work_order;
	
END it_pkg_purchasing;
/
