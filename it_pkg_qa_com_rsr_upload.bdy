
--@"C:\Data Correction\Script\it_pkg_qa_com_rsr_upload.bdy"

/***********************************************************************
* Description : This procedure is used to insert/update COM-RSR record *
***********************************************************************/
CREATE OR REPLACE
	PACKAGE BODY it_pkg_qa_com_rsr_upload AS

  	PROCEDURE gm_com_rsr_data_upload (

          p_incident_id 			      IN 	t3100_incident.c3100_incident_id%TYPE,
          p_ra_id 				          IN 	t3101_incident_part_detail.c506_rma_id%TYPE,
          p_issue_type 			          IN 	t3100_incident.c901_issue_type%TYPE,
          p_complaint_nm 			      IN 	t3100_incident.c3100_complainant_id%TYPE,
          p_status 				          IN 	t3100_incident.c901_status%TYPE,	
          p_company                 	  IN 	t3100_incident.c3100_company%TYPE,
          p_area_direct 			      IN 	t3100_incident.c101_ad_id%TYPE,
          p_inc_date 				      IN 	t3100_incident.c3100_incident_dt%TYPE,
          p_inc_cat 				      IN 	t3100_incident.c901_incident_category%TYPE,
          p_compl_recv_by 		          IN 	t3100_incident.c101_complaint_received_by%TYPE,
          p_compl_recv_dt 		          IN 	t3100_incident.c3100_received_dt%TYPE,
          p_originator 			          IN 	t3100_incident.c101_originator%TYPE,
          p_mdrreportble 			      IN 	t3100_incident.c3100_mdr_reportable%TYPE,
          p_details_desc_event 	          IN 	t3100_incident.c3100_detail_event_desc%TYPE,
          p_part_number   		          IN 	t3101_incident_part_detail.c205_part_number_id%TYPE,
          p_gem 					      IN 	t3101_incident_part_detail.c101_gem_id%TYPE,
          p_type 					      IN 	t3101_incident_part_detail.c901_product_family%TYPE,
          p_lot_number 			          IN 	t3101_incident_part_detail.c3101_lot_number%TYPE,
          p_quantity 				      IN 	t3101_incident_part_detail.c3101_qty%TYPE,
          p_surgeon_nm 			          IN 	t3102_incident_detail.c3102_surgeon_nm%TYPE,
          p_part_avail_eval_fl 	          IN 	t3102_incident_detail.c3102_part_eval_fl%TYPE,
          p_other_details 		      	  IN 	t3102_incident_detail.c3102_other_details%TYPE,
          p_part_rec   		              IN 	t3103_incident_qa_eval.c3103_part_receive_dt%TYPE,
          p_eval_send  		              IN 	t3103_incident_qa_eval.c3103_sent_to_eval_dt%TYPE,
          p_eval_rec_dt             	  IN 	t3104_incident_eng_eval.c3104_evaluator_rec_dt%TYPE,
          p_Eval_Res                	  IN 	t3104_incident_eng_eval.c3104_evaluation_result%TYPE,
          p_Eval_Id                 	  IN 	t3104_incident_eng_eval.c101_evaluator_id%TYPE,
          p_Com_Rsr_Disp            	  IN 	t3104_incident_eng_eval.c901_com_rsr_disp_type%TYPE,
          p_Prod_Disp               	  IN 	t3104_incident_eng_eval.c901_product_disp_type%TYPE,
          p_closed_by   	          	  IN 	t3105_incident_closure_dtl.c101_closed_by%TYPE,
          p_closed_date  	          	  IN 	t3105_incident_closure_dtl.c3105_closed_dt%TYPE,
          p_prev_comments    	      	  IN 	t3105_incident_closure_dtl.c3105_corrective_action%TYPE,
          p_void_fl                 	  IN 	t3105_incident_closure_dtl.c3105_void_fl%TYPE
  	) AS
 
  		  v_user_id 							NUMBER;
  		  v_incident_id 						t3100_incident.c3100_incident_id%TYPE;
  		  v_complainant_type 					t3100_incident.c901_complainant_type%TYPE;
  		  v_phone_number 						t3100_incident.c3100_phone_number%TYPE;
  		  v_address 							t3100_incident.c3100_address%TYPE;
  		  v_part_number 						t3101_incident_part_detail.c205_part_number_id%TYPE;
  		  v_part_count 							NUMBER;
  		  v_project_id 							t3101_incident_part_detail.c202_project_id%TYPE;
  		  v_part_desc 							t3101_incident_part_detail.c205_part_desc%TYPE;
  		  v_prod_famy 							t3101_incident_part_detail.c901_product_family%TYPE;
  		  v_ra_id 								t3101_incident_part_detail.c506_rma_id%TYPE;
  		  v_count 								NUMBER;
 
  BEGIN  
	  	  -- To get the user id
    	  v_user_id := it_pkg_cm_user.get_valid_user;
    BEGIN
    
    -- To select the Complainant type.  102811 - In-House,  102812 - Sales Reps
      SELECT comp_type 
      	INTO v_complainant_type 
      	FROM (SELECT '102811' comp_type FROM t101_user
       WHERE c901_user_type IN (300) 
         AND c101_user_id = p_complaint_nm
    UNION
      SELECT '102812' comp_type
        FROM t703_sales_rep t703 ,t101_party t101
       WHERE t703.c101_party_id = t101.c101_party_id 
       	 AND t703.c703_void_fl  IS NULL
         AND t703.c703_sales_rep_id = p_complaint_nm) 
       WHERE ROWNUM=1;
       
    EXCEPTION
    	WHEN NO_DATA_FOUND
		THEN
			v_complainant_type:='';
  	END;
  
           
   IF v_complainant_type = '102812'
     THEN
       BEGIN
      	
	     -- To get the Sales Reps Phone number and Address
         SELECT c703_phone_number,c703_ship_add1 ||','|| c703_ship_city || ' ' || get_code_name_alt(c703_ship_state) || ' ' || c703_ship_zip_code
           INTO v_phone_number,v_address
           FROM t703_sales_rep 
          WHERE c703_sales_rep_id = p_complaint_nm
		    AND c703_void_fl IS NULL;
			  
       EXCEPTION
           WHEN NO_DATA_FOUND
           THEN
             v_phone_number:='';
             v_address:='';
       END;
   END IF;
  
    -- To check the part is present in the t205_part_number table.
    SELECT count(1) 
    	INTO v_part_count
      	FROM t205_part_number 
       WHERE c205_part_number_id=p_part_number;
       
    IF v_part_count != 0  
		THEN
		  -- To get the part number, project, part number desc and product family of the part.
		  SELECT c205_part_number_id
		  		,c202_project_id
		  		,c205_part_num_desc
		  		,c205_product_family
		   INTO v_part_number
		   		,v_project_id
		   		,v_part_desc
		   		,v_prod_famy
		   FROM t205_part_number 
		  WHERE c205_part_number_id = p_part_number;
	ELSE
		v_part_number:='';
    END IF;    
  
  BEGIN
  	
	-- To get the RA id of the COM/RSR
    SELECT c506_rma_id 
       INTO v_ra_id  
       FROM t506_returns 
      WHERE c506_rma_id = p_ra_id;
    
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
        v_ra_id:='';
  END;
  
  BEGIN
  
	-- To get the count value of the COM/RSR Id to insert or to be update the COM/RSR
    SELECT count(1) 
      INTO v_count 
      FROM t3100_incident 
     WHERE c3100_incident_id = p_incident_id;
         
        IF v_count = 0
          THEN
          
            INSERT INTO t3100_incident
                (c3100_incident_id,c901_issue_type , c901_complainant_type , c3100_complainant_id , c901_status 
                 , c101_ad_id, c3100_incident_dt,c901_incident_category, c101_complaint_received_by , c3100_received_dt                  
                 , c101_originator, c3100_mdr_reportable, c3100_detail_event_desc, c3100_created_by 			                           
                 , c3100_created_date,c3100_address,c3100_phone_number,c3100_company 
                )
            VALUES
                (p_incident_id,p_issue_type, v_complainant_type, p_complaint_nm, p_status
                 , p_area_direct,p_inc_date,p_inc_cat,p_compl_recv_by, p_compl_recv_dt
                 ,p_originator,p_mdrreportble, p_details_desc_event, v_user_id 
                 , SYSDATE,v_address, v_phone_number, p_company
                );
                
            INSERT INTO t3101_incident_part_detail
                (c3100_incident_id,c205_part_number_id , c202_project_id , c205_part_desc , c901_product_family 
                 , c101_gem_id, c3101_lot_number,c3101_qty, c506_rma_id, c3101_created_by , c3101_created_date
                )
            VALUES
                (p_incident_id,v_part_number,v_project_id, v_part_desc, v_prod_famy
                 ,p_gem,p_lot_number,p_quantity, v_ra_id, v_user_id , SYSDATE
                );	
            
            INSERT INTO t3102_incident_detail
                (c3100_incident_id, c3102_surgeon_nm , c3102_part_eval_fl, c3102_other_details, c3102_created_by, c3102_created_date         
                )
            VALUES
                (p_incident_id ,p_surgeon_nm ,p_part_avail_eval_fl ,p_other_details , v_user_id , SYSDATE
                );
          
            INSERT INTO t3103_incident_qa_eval 
              (c3100_incident_id, c3103_part_receive_dt, c3103_sent_to_eval_dt,c3103_created_by, c3103_created_date
              )
            VALUES 
              (p_incident_id, p_part_rec,p_eval_send, v_user_id, SYSDATE
              );
               
		    INSERT INTO t3104_incident_eng_eval
              (c3100_incident_id,c3104_evaluator_rec_dt ,c3104_evaluation_result,c101_evaluator_id,c901_com_rsr_disp_type
              ,c901_product_disp_type,c3104_created_by,c3104_created_date
              )
	        VALUES 
              (p_incident_id,p_eval_rec_dt,p_Eval_Res,p_Eval_Id,p_Com_Rsr_Disp 
               ,p_Prod_Disp,v_user_id, SYSDATE
              );
                  
            INSERT INTO t3105_incident_closure_dtl
              (c3100_incident_id, c101_closed_by, c3105_closed_dt
               , c3105_corrective_action , c3105_created_by, c3105_created_date
              )
            VALUES
              (p_incident_id, p_closed_by,p_closed_date
               , p_prev_comments, v_user_id, SYSDATE
              ) ;
              
        ELSE
        
        	-- To update the Incident Details.
         	UPDATE t3100_incident 
         	   SET c901_issue_type 				= p_issue_type
         	   	 , c901_complainant_type 		= v_complainant_type
         	   	 , c3100_complainant_id 		= p_complaint_nm
         	   	 , c901_status 					= p_status
         	   	 , c101_ad_id 					= p_area_direct
         	   	 , c3100_incident_dt 			= p_inc_date
         	   	 , c901_incident_category 		= p_inc_cat
         	   	 , c101_complaint_received_by 	= p_compl_recv_by
         	   	 , c3100_received_dt 			= p_compl_recv_dt
         	   	 , c101_originator 				= p_originator
         	   	 , c3100_mdr_reportable 		= p_mdrreportble
         	   	 , c3100_detail_event_desc 		= p_details_desc_event
         	   	 , c3100_last_updated_by 		= v_user_id
         	   	 , c3100_last_updated_date 		= SYSDATE
         	   	 , c3100_address 				= v_address
         	   	 , c3100_phone_number 			= v_phone_number
         	   	 , c3100_company 				= p_company 
         	 WHERE c3100_incident_id 			= p_incident_id;
            
         	-- To update the Incident Part details
          	UPDATE t3101_incident_part_detail 
          	   SET c205_part_number_id 			= v_part_number
          	     , c202_project_id 				= v_project_id
          	     , c205_part_desc 				= v_part_desc
          	     , c901_product_family 			= v_prod_famy
          	     , c101_gem_id 					= p_gem
          	     , c3101_lot_number 			= p_lot_number
          	     , c3101_qty 					= p_quantity
          	     , c506_rma_id 					= v_ra_id
          	     , c3101_last_updated_by 		= v_user_id
          	     , c3101_last_updated_date 		= SYSDATE
             WHERE c3100_incident_id 			= p_incident_id;
           
           -- To update the Incident Details.
           UPDATE t3102_incident_detail
           	  SET c3102_surgeon_nm 				= p_surgeon_nm
           	  	, c3102_part_eval_fl 			= p_part_avail_eval_fl
           	  	, c3102_other_details 			= p_other_details
           	  	, c3102_last_updated_by 		= v_user_id
           	  	, c3102_last_updated_date 		= SYSDATE  
           	WHERE c3100_incident_id 			= p_incident_id;
          
          -- To update the QA evaluation details
          UPDATE t3103_incident_qa_eval 
             SET c3103_part_receive_dt 			= p_part_rec
               , c3103_sent_to_eval_dt 			= p_eval_send
               , c3103_last_updated_by 			= v_user_id
               , c3103_last_updated_date 		= SYSDATE
           WHERE c3100_incident_id 				= p_incident_id;
          
          -- To update the Engineering evaluation details
          UPDATE t3104_incident_eng_eval 
             SET c3104_evaluator_rec_dt 		= p_eval_rec_dt
               , c3104_evaluation_result 		= p_eval_res
               , c101_evaluator_id 				= p_eval_id
               , c901_com_rsr_disp_type 		= p_com_rsr_disp
               , c901_product_disp_type 		= p_prod_disp
               , c3104_last_updated_by 			= v_user_id
               , c3104_last_updated_date 		= SYSDATE 
           WHERE c3100_incident_id 				= p_incident_id;
          
          -- To update the Closure details
          UPDATE t3105_incident_closure_dtl 
          	 SET c101_closed_by					= p_closed_by
          	   , c3105_closed_dt 				= p_closed_date
          	   , c3105_corrective_action 		= p_prev_comments
          	   , c3105_last_updated_by 			= v_user_id
          	   , c3105_last_updated_date 		= SYSDATE 
           WHERE c3100_incident_id 				= p_incident_id;
           
      END IF;
      
    END;
  
  END gm_com_rsr_data_upload;

END it_pkg_qa_com_rsr_upload;
/
