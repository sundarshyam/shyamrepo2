/* Formatted on 2011/01/05 18:29 (Formatter Plus v4.8.0) */
-- @"C:\Data Correction\Script\it_pkg_cm_user.bdy"
--T914_HELPDESK_LOG

CREATE OR REPLACE PACKAGE BODY it_pkg_cm_user
IS
--
	/*********************************************************
	* Description : This procedure used to fetch login user info
	*********************************************************/
	FUNCTION get_valid_user
		RETURN NUMBER
	IS
		v_login_user   VARCHAR2 (500);
	BEGIN
		SELECT USER
		  INTO v_login_user
		  FROM DUAL;


		IF v_login_user = 'GLOBUS_APP'
		THEN
			RETURN 303043;
		ELSIF  v_login_user = 'RICHARDK'
		THEN
			RETURN 303043;
		ELSIF v_login_user = 'DJAMES'
		THEN
			RETURN 303013;
		ELSIF v_login_user = 'SUCHITRA'
		THEN
			RETURN 303150;
		ELSIF v_login_user = 'RVARATHARAJAN'
		THEN
			RETURN 303510;
		ELSIF v_login_user = 'VPRASATH'
		THEN
			RETURN 303149;	
		ELSE
			raise_application_error ('-20085', 'Not an authorised user to execute the procedure');
		END IF;

	END get_valid_user;

	/*********************************************************
	* Description : This procedure is used to store information
	* into helpdesk_log table
	*********************************************************/
	PROCEDURE gm_cm_sav_helpdesk_log (
		p_help_desk_id		 IN   t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_914_helpdesk_log	 IN   t914_helpdesk_log.c914_helpdesk_log%TYPE
	)
	AS
		v_help_desk_log t914_helpdesk_log.c914_helpdesk_log%TYPE;
	BEGIN
		IF (p_914_helpdesk_log IS NULL)
		THEN
			SELECT USER || ' Executed the log '
			  INTO v_help_desk_log
			  FROM DUAL;
		ELSE
			v_help_desk_log := p_914_helpdesk_log;
		END IF;

		INSERT INTO t914_helpdesk_log
					(c914_helpdesk_id, c914_helpdesk_log
					)
			 VALUES (p_help_desk_id, v_help_desk_log
					);
	END gm_cm_sav_helpdesk_log;

	/*********************************************************
	* Description : This procedure is used to copy user access
	* from one user to other user
	*********************************************************/
	PROCEDURE gm_sav_copy_user_access (
		p_help_desk_id	 t102_user_login.c102_login_username%TYPE
	  , p_source_user	 t102_user_login.c102_login_username%TYPE
	  , p_target_user	 t102_user_login.c102_login_username%TYPE
	)
	AS
		v_user_dept_id t101_user.c901_dept_id%TYPE;
		v_party_group_id t101_party.c1500_group_id%TYPE;
		v_user_id	   NUMBER;
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		SELECT t101.c901_dept_id, t101p.c1500_group_id
		  INTO v_user_dept_id, v_party_group_id
		  FROM t101_user t101, t101_party t101p
		 WHERE t101.c101_user_id IN (SELECT t102.c101_user_id
									   FROM t102_user_login t102
									  WHERE t102.c102_login_username = p_source_user)
		   AND t101p.c101_party_id = t101.c101_party_id;

		gm_cm_sav_helpdesk_log (p_help_desk_id, 'Coping Data from Source User *** ' || p_source_user);
		gm_cm_sav_helpdesk_log (p_help_desk_id, 'Source Department ID  *****' || v_user_dept_id);
		gm_cm_sav_helpdesk_log (p_help_desk_id, 'Party Group ID ******' || v_party_group_id);

		UPDATE t101_user t101
		   SET t101.c901_dept_id = v_user_dept_id
			 , c101_last_updated_date = SYSDATE
			 , c101_last_updated_by = v_user_id
		 WHERE t101.c101_user_id IN (SELECT t102.c101_user_id
									   FROM t102_user_login t102
									  WHERE t102.c102_login_username = p_target_user);

		--
		IF (SQL%ROWCOUNT = 0)
		THEN
			ROLLBACK;
			raise_application_error ('-20085', 'New user Not available in the system');
		END IF;

		--
		UPDATE t101_party t101p
		   SET t101p.c1500_group_id = v_party_group_id
			 , t101p.c101_last_updated_by = v_user_id
			 , t101p.c101_last_updated_date = SYSDATE
		 WHERE t101p.c101_party_id IN (
								SELECT t101.c101_party_id
								  FROM t101_user t101, t102_user_login t102
								 WHERE t102.c102_login_username = p_target_user
									   AND t102.c101_user_id = t101.c101_user_id);

		IF (SQL%ROWCOUNT = 0)
		THEN
			ROLLBACK;
			raise_application_error ('-20085', 'New Party not available in the system ');
		END IF;

		--
		gm_cm_sav_helpdesk_log (p_help_desk_id, 'Target user ' || p_target_user || ' Completed sucessfully !.... ');
		DBMS_OUTPUT.put_line ('Target user ' || p_target_user || ' Completed sucessfully !.... ');
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_copy_user_access;
	
	/**************************************************************************************************
    *  Description: Procedure to update Inactive users to Active users
    *  Author: Bala
    **************************************************************************************************/
	 PROCEDURE gm_upd_inactive_to_active_user (
         p_jira_id   IN t914_helpdesk_log.c914_helpdesk_id%TYPE
       , p_toactive_user_id   IN   t102_user_login.c101_user_id%TYPE    
       , p_toactive_user_name IN   t102_user_login.c102_login_username%TYPE
   )
   AS
      v_party_id         t101_party.c101_party_id%TYPE;
      v_distributor_id   t701_distributor.c701_distributor_id%TYPE;
      v_usertype         t101_user.c901_user_type%TYPE;
      v_user_loginname   t102_user_login.c102_login_username%TYPE;
      v_index            NUMBER;
      v_user_id	         NUMBER;
   BEGIN
	   
	-- Mandatory helpdesk to log executed user information
    IF p_jira_id IS NULL THEN
        raise_application_error ( - 6502, 'Jira Id cannot be null') ;
    ELSE
        gm_cm_sav_helpdesk_log (p_jira_id, NULL) ;
    END IF;
	   
	IF p_toactive_user_id IS NULL THEN
        raise_application_error ( - 6502, 'To make active User Id cannot be null') ;
    ELSE
        gm_cm_sav_helpdesk_log (p_jira_id,p_toactive_user_id || ' :To make active User Id') ;
    END IF;
        
    IF p_toactive_user_name IS NULL THEN
        raise_application_error ( - 6502, 'To make active User Name cannot be null') ;
    ELSE
        gm_cm_sav_helpdesk_log (p_jira_id,p_toactive_user_name || ' :To make active User Name') ;
    END IF;
    
    v_user_id	:= get_valid_user;
    
    IF p_toactive_user_name IS NOT NULL
    THEN           
        UPDATE t102_user_login
            SET c102_login_username = p_toactive_user_name
             ,c102_user_lock_fl = null
             ,c901_auth_type = 320
         	 ,c102_last_updated_by = v_user_id
             ,c102_last_updated_date = SYSDATE
       WHERE c101_user_id = p_toactive_user_id;    
       -- To update the user status as Active
       UPDATE t101_user
			SET c901_user_status     = 311 --Active
    		 ,c101_last_updated_by = v_user_id
    		 ,c101_last_updated_date = sysdate
  		WHERE c101_user_id     = p_toactive_user_id;
--

    END IF;  
   
    gm_cm_sav_helpdesk_log (p_jira_id, 'User id ' || p_toactive_user_id || ' ,user name ' || p_toactive_user_name || ' Activated sucessfully !.... ');
	DBMS_OUTPUT.put_line ('User id ' || p_toactive_user_id || ' ,user name ' || p_toactive_user_name || ' Activated sucessfully !.... ');
	COMMIT;
     
     EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing ' || SQLERRM);
			ROLLBACK;   
     
    END gm_upd_inactive_to_active_user;
    
	/**************************************************************************************************
	*  Description: Procedure to update user last name change
	*  Author: Mani
	**************************************************************************************************/
	PROCEDURE gm_upd_user_last_name (
	        p_jira_id        IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
	        p_user_id        IN t101_user.c101_user_id%TYPE,
	        p_user_last_name IN t101_user.c101_user_l_name%TYPE,
	        p_login_name     IN t102_user_login.c102_login_username%TYPE)
	AS
	    v_user_id  NUMBER;
	    v_party_id NUMBER;
	    v_user_cnt NUMBER;
	    v_login_name t102_user_login.c102_login_username%TYPE := TRIM (p_login_name) ;
	    v_last_name t101_user.c101_user_l_name%TYPE           := TRIM (p_user_last_name) ;
	BEGIN
	    -- Mandatory helpdesk to log executed user information
	    IF p_jira_id IS NULL THEN
	        raise_application_error ( '-20999', 'Jira Id cannot be null') ;
	    ELSE
	        gm_cm_sav_helpdesk_log (p_jira_id, NULL) ;
	    END IF;
	    -- validate user id
	    SELECT COUNT (1) INTO v_user_cnt FROM T101_USER WHERE C101_USER_ID = p_user_id;
	    IF v_user_cnt = 0 THEN
	        raise_application_error ( '-20999', 'User id not available - Please provide the correct user id') ;
	    END IF;
	    -- validate  last name
	    IF v_last_name IS NULL THEN
	        raise_application_error ( '-20999', 'Last name cannot be null') ;
	    END IF;
	    -- validate login user name
	    IF v_login_name IS NULL THEN
	        raise_application_error ( '-20999', 'Login name cannot be null') ;
	    END IF;
	    -- IT user id
	    v_user_id := get_valid_user;
	    -- to get the party id
	     SELECT c101_party_id
	       INTO v_party_id
	       FROM t101_user
	      WHERE c101_user_id = p_user_id;
	    -- to update the party - last name
	     UPDATE t101_party
	    SET c101_LAST_NM      = v_last_name, c101_last_updated_date = sysdate, c101_last_updated_by = v_user_id
	      WHERE c101_party_id = v_party_id;
	    -- to update the email id and last name  
	     UPDATE T101_USER
	    SET c101_Email_Id        = v_login_name ||'@globusmedical.com', c101_user_L_name = v_last_name,
	        c101_last_updated_by = v_user_id, c101_last_updated_date = sysdate
	      WHERE c101_user_id     = p_user_id;
	    -- to update the user login name
	     UPDATE t102_user_login
	    SET c102_login_username = v_login_name, c102_last_updated_date = sysdate, c102_last_updated_by = v_user_id
	      WHERE c101_user_id    = p_user_id;
	END gm_upd_user_last_name;

END it_pkg_cm_user;
/
