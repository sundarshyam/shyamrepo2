/* Formatted on 2011/02/01 15:54 (Formatter Plus v4.8.0) */
-- @"C:\database\Data Correction\Script\it_pkg_ac_cycle_count.bdy"
CREATE OR REPLACE PACKAGE BODY it_pkg_ac_cycle_count
IS
    /*********************************************************
    * Description : This procedure is used to validate the cycle count id
    *********************************************************/
PROCEDURE gm_validate_cycle_count (
        p_cycle_count_id IN t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%TYPE)
AS
    v_cnt NUMBER := 0;
BEGIN
	
     SELECT COUNT (1)
       INTO v_cnt
       FROM t2705_cycle_count_lock
      WHERE c2705_cyc_cnt_lock_id = p_cycle_count_id
        AND c2705_void_fl        IS NULL;
        
        
    IF v_cnt                      = 0 THEN
        raise_application_error ('-20999', 'Cycle count ID ' ||p_cycle_count_id ||
        ' is invalid/voided, Please enterd the correct Cycle count ID.') ;
    END IF;
    
END gm_validate_cycle_count;

/*********************************************************
* Description : This procedure is used to update the cycle count status
*********************************************************/
PROCEDURE gm_upd_cycle_count_status (
        p_help_desk_id   IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
        p_cycle_count_id IN t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%TYPE,
        p_status         IN t2705_cycle_count_lock.c901_lock_status%TYPE)
AS
    v_user_id NUMBER;
BEGIN
    --
    IF TRIM (p_help_desk_id) IS NULL THEN
        raise_application_error ('-20999', 'Please enter the help desk id') ;
    END IF;
    
    -- to validate the cycle count id
    
    it_pkg_ac_cycle_count.gm_validate_cycle_count (p_cycle_count_id) ;
    
    
    -- Mandatory helpdesk to log executed user information
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (TRIM (p_help_desk_id), NULL) ;
    
    -- get the user id
    v_user_id := it_pkg_cm_user.get_valid_user;
    
    
    -- update the cycle count id
     UPDATE T2705_CYCLE_COUNT_LOCK
    SET C901_LOCK_STATUS          = p_status, C2705_LAST_UPDATED_BY = v_user_id, C2705_LAST_UPDATED_DATE = SYSDATE
      WHERE C2705_CYC_CNT_LOCK_ID = p_cycle_count_id
        AND C2705_VOID_FL        IS NULL;
        
END gm_upd_cycle_count_status;
/*********************************************************
* Description : This procedure is used to void the cycle count id
*********************************************************/
PROCEDURE gm_void_cycle_count (
        p_help_desk_id   IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
        p_cycle_count_id IN t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%TYPE)
AS
    v_user_id NUMBER;
    
BEGIN
    --
    IF TRIM (p_help_desk_id) IS NULL THEN
        raise_application_error ('-20999', 'Please enter the help desk id') ;
    END IF;
    
    -- validate the cycle count
    it_pkg_ac_cycle_count.gm_validate_cycle_count (p_cycle_count_id) ;
    
    -- Mandatory helpdesk to log executed user information
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (TRIM (p_help_desk_id), NULL) ;
    
    v_user_id := it_pkg_cm_user.get_valid_user;
    
    
    -- update the cycle count id
     UPDATE T2705_CYCLE_COUNT_LOCK
    SET C2705_VOID_FL             = 'Y', C2705_LAST_UPDATED_BY = v_user_id, C2705_LAST_UPDATED_DATE = SYSDATE
      WHERE C2705_CYC_CNT_LOCK_ID = p_cycle_count_id
        AND C2705_VOID_FL        IS NULL;
     
     -- to void the cycle count details
      UPDATE T2706_CYCLE_COUNT_LOCK_DTLS
    SET c2706_void_fl        = 'Y'
      , C2706_LAST_UPDATED_BY = v_user_id
      , C2706_LAST_UPDATED_DATE   = SYSDATE
      WHERE C2705_CYC_CNT_LOCK_ID = p_cycle_count_id
        AND c2706_void_fl IS NULL;
        
END gm_void_cycle_count;
/*********************************************************
* Description : This procedure is used to remove the override Qty
*********************************************************/
PROCEDURE gm_remove_override_qty (
        p_help_desk_id   IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
        p_cycle_count_id IN t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%TYPE,
        p_part_number    IN t2706_cycle_count_lock_dtls.c205_part_number_id%TYPE) 

AS
    v_user_id NUMBER;
    v_cnt NUMBER;
BEGIN
    v_user_id := it_pkg_cm_user.get_valid_user;
    --
    it_pkg_ac_cycle_count.gm_validate_cycle_count (p_cycle_count_id) ;
    
     --
    IF TRIM (p_help_desk_id) IS NULL THEN
        raise_application_error ('-20999', 'Please enter the help desk id') ;
    END IF;
    
    SELECT count (1) INTO v_cnt
   FROM T2706_CYCLE_COUNT_LOCK_DTLS
  WHERE C2705_CYC_CNT_LOCK_ID = p_cycle_count_id
    AND (C205_PART_NUMBER_ID  = p_part_number
    OR C504_CONSIGNMENT_ID    = p_part_number)
    AND C2706_OVERRIDE_QTY IS NOT NULL
    AND C2706_VOID_FL        IS NULL;
    
    IF v_cnt = 0 THEN
        raise_application_error ('-20999', 'Invalid Transactions/Already remove the Override Qty, Please verify') ;
    END IF;
    
    -- Mandatory helpdesk to log executed user information
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (TRIM (p_help_desk_id), NULL) ;
    
     UPDATE T2706_CYCLE_COUNT_LOCK_DTLS
    SET C2706_OVERRIDE_QTY        = NULL, C2706_OVERRIDE_QTY_FL = NULL, C2706_COMMENTS = NULL
      , C2706_LAST_UPDATED_BY = v_user_id
      , C2706_LAST_UPDATED_DATE   = SYSDATE
      WHERE C2705_CYC_CNT_LOCK_ID = p_cycle_count_id
        AND (C205_PART_NUMBER_ID   = p_part_number OR C504_CONSIGNMENT_ID = p_part_number)
        AND c2706_void_fl IS NULL;
        
     -- to remove the approved by/date
     
         UPDATE T2705_CYCLE_COUNT_LOCK
    SET C2705_APPROVED_BY = NULL, C2705_APPROVED_DATE = NULL, C2705_LAST_UPDATED_BY = v_user_id, C2705_LAST_UPDATED_DATE = SYSDATE
      WHERE C2705_CYC_CNT_LOCK_ID = p_cycle_count_id
        AND C2705_VOID_FL        IS NULL
        AND C2705_APPROVED_BY IS NOT NULL;
        
END gm_remove_override_qty;

/*********************************************************
* Description : This procedure is used to insert the parts/CN to scheduler
*********************************************************/
PROCEDURE gm_sav_cyc_cnt_scheduler (
        p_help_desk_id        IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
        p_cyc_cnt_id          IN T2704_CYCLE_COUNT_SCHEDULE.C2700_CYCLE_COUNT_ID%TYPE,
        p_cyc_cnt_location_id IN T2704_CYCLE_COUNT_SCHEDULE.C2701_CYC_CNT_LOCATION_ID%TYPE,
        p_part_number         IN T2704_CYCLE_COUNT_SCHEDULE.c205_part_number_id%TYPE,
        p_consignment_id      IN T2704_CYCLE_COUNT_SCHEDULE.c504_consignment_id%TYPE,
        p_scheduler_date IN VARCHAR2,
        p_user_id             IN T2704_CYCLE_COUNT_SCHEDULE.c2704_last_updated_by%TYPE)
AS
    v_date_fmt VARCHAR2 (20) ;
BEGIN
    --
     SELECT NVL (get_compdtfmt_frm_cntx (), 'MM/dd/yyyy')
       INTO v_date_fmt
       FROM dual;
    --
     UPDATE T2704_CYCLE_COUNT_SCHEDULE
    SET c2704_schedule_date                = to_date (p_scheduler_date, v_date_fmt), c2704_last_updated_by = p_user_id,
        c2704_last_updated_date            = CURRENT_DATE
      WHERE C2700_CYCLE_COUNT_ID           = p_cyc_cnt_id
        AND C2701_CYC_CNT_LOCATION_ID      = p_cyc_cnt_location_id
        AND c205_part_number_id            = p_part_number
        AND NVL (c504_consignment_id, '0') = NVL (p_consignment_id, '0')
        -- to avoid the duplicate records
        AND c2704_schedule_date            = TRUNC (to_date (p_scheduler_date, v_date_fmt))
        AND C2704_VOID_FL                 IS NULL;
    IF (SQL%ROWCOUNT                       = 0) THEN
         INSERT
           INTO T2704_CYCLE_COUNT_SCHEDULE
            (
                C2704_CYC_CNT_SCHEDULE_DTLS_ID, C2700_CYCLE_COUNT_ID, C2701_CYC_CNT_LOCATION_ID
              , c205_part_number_id, c504_consignment_id, c2704_schedule_date
              , c2704_last_updated_by, c2704_last_updated_date
            )
            VALUES
            (
                s2704_CYCLE_COUNT_SCHEDULE.nextval, p_cyc_cnt_id, p_cyc_cnt_location_id
              , p_part_number, p_consignment_id, to_date (p_scheduler_date, v_date_fmt)
              , p_user_id, CURRENT_DATE
            ) ;
    END IF;
    --
END gm_sav_cyc_cnt_scheduler;

/*********************************************************
* Description : This procedure is used to update the cycle count scheduler date
*********************************************************/
PROCEDURE gm_upd_cyc_cnt_scheduler_date (
        p_help_desk_id   IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
        p_cc_lock_id     IN t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%TYPE,
        p_scheduler_date IN VARCHAR2)
AS
    v_user_id  NUMBER;
    v_cnt      NUMBER;
    v_date_fmt VARCHAR2 (20) ;
BEGIN
    --
    IF TRIM (p_help_desk_id) IS NULL THEN
        raise_application_error ('-20999', 'Please enter the help desk id') ;
    END IF;
    
    -- to validate the cycle count id
    
    it_pkg_ac_cycle_count.gm_validate_cycle_count (p_cc_lock_id) ;
    
    -- Mandatory helpdesk to log executed user information
    
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (TRIM (p_help_desk_id), NULL) ;
    
    -- get the user id
    
    v_user_id := it_pkg_cm_user.get_valid_user;
    
    -- to get the date format
    
     SELECT NVL (get_compdtfmt_frm_cntx (), 'MM/dd/yyyy')
       INTO v_date_fmt
       FROM dual;
       
    -- to update the scheduler date
    
     UPDATE t2705_cycle_count_lock
    SET c2705_schedule_date      = to_date (p_scheduler_date, v_date_fmt), C2705_LAST_UPDATED_BY = v_user_id,
        c2705_last_updated_date   = SYSDATE
      WHERE c2705_cyc_cnt_lock_id = p_cc_lock_id
        AND c2705_void_fl        IS NULL;
        
	--
	        
END gm_upd_cyc_cnt_scheduler_date; 

/*********************************************************
* Description : This procedure is used to void the non wip loaner CN
*********************************************************/
PROCEDURE gm_void_wip_loaner_cn (
        p_help_desk_id IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
        p_cc_lock_id   IN t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%TYPE)
AS
    v_user_id        NUMBER;
    v_warehouse_type NUMBER;
    v_cnt            NUMBER := 0;
    v_plant_id       NUMBER;
    --
    CURSOR remove_wip_loaner_cn_cur
    IS
         SELECT t2706.c504_consignment_id cn_id, t2706.c2706_cyc_cnt_lock_dtls_id cc_lock_dtls_id
           FROM t2705_cycle_count_lock t2705, t2706_cycle_count_lock_dtls t2706, t504_consignment t504
          , t504a_consignment_loaner t504a
          WHERE t2705.c2705_cyc_cnt_lock_id     = t2706.c2705_cyc_cnt_lock_id
            AND t504.c504_consignment_id        = t2706.c504_consignment_id
            AND t504.c504_consignment_id        = t504a.c504_consignment_id
            AND t2705.c2705_cyc_cnt_lock_id     = p_cc_lock_id
            AND t2705.c2705_void_fl            IS NULL
            AND t2706.c2706_void_fl            IS NULL
            AND t504.c504_type                  = 4127
            AND t504.c207_set_id               IS NOT NULL
            AND NVL (T504a.c504a_status_fl, 0) <> 30 -- WIP Loaner
            AND t504.c504_status_fl             = 4
            AND t504.c504_verify_fl             = 1
            AND t504.c504_void_fl              IS NULL;
BEGIN
    --
    IF TRIM (p_help_desk_id) IS NULL THEN
        raise_application_error ('-20999', 'Please enter the help desk id') ;
    END IF;
    
    -- to validate the cycle count id
    it_pkg_ac_cycle_count.gm_validate_cycle_count (p_cc_lock_id) ;
    
    -- Mandatory helpdesk to log executed user information
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (TRIM (p_help_desk_id), NULL) ;
    
    -- get the user id
    v_user_id := it_pkg_cm_user.get_valid_user;
    
    -- to validate the warehouse
     SELECT t2701.c901_warehouse_location
       INTO v_warehouse_type
       FROM t2705_cycle_count_lock t2705, t2701_cycle_count_location t2701
      WHERE t2701.c2701_cyc_cnt_location_id = t2705.c2701_cyc_cnt_location_id
        AND t2705.c2705_cyc_cnt_lock_id     = p_cc_lock_id
        AND t2701.c2701_void_fl            IS NULL
        AND t2705.c2705_void_fl            IS NULL;
    --
    DBMS_OUTPUT.PUT_LINE (' Warehouse type  '|| v_warehouse_type) ;
    
    IF v_warehouse_type <> 106811 -- WIP Loaner
        THEN
        raise_application_error ('-20999', 'Please provide the valide WIP-Loaner Lock id') ;
    END IF;
    --
    
    FOR remove_wip_loaner IN remove_wip_loaner_cn_cur
    LOOP
        v_cnt := v_cnt + 1;
        --
        DBMS_OUTPUT.PUT_LINE (v_cnt ||' Consignment to Void '|| remove_wip_loaner.cn_id) ;
        --
        
        it_pkg_ac_cycle_count.gm_void_cc_lock_dtls (remove_wip_loaner.cc_lock_dtls_id, 'Y', v_user_id) ;
        
        --
        
    END LOOP;
END gm_void_wip_loaner_cn;

/*********************************************************
* Description : This procedure is used to void cycle count lock details table
*********************************************************/
PROCEDURE gm_void_cc_lock_dtls (
        p_cc_lock_dtls_id IN t2706_cycle_count_lock_dtls.c2706_cyc_cnt_lock_dtls_id%TYPE,
        p_void_fl         IN t2706_cycle_count_lock_dtls.c2706_void_fl%TYPE,
        p_user_id         IN t2706_cycle_count_lock_dtls.c2706_last_updated_by%TYPE)
AS
BEGIN
     UPDATE t2706_cycle_count_lock_dtls
    SET c2706_void_fl                  = p_void_fl, c2706_last_updated_by = p_user_id, c2706_last_updated_date = CURRENT_DATE
      WHERE c2706_cyc_cnt_lock_dtls_id = p_cc_lock_dtls_id;
END gm_void_cc_lock_dtls;


/*********************************************************
* Description : This procedure is used to update the scheduler date values
*********************************************************/

PROCEDURE gm_upd_scheduler_date (
        p_help_desk_id         IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
        p_cc_scheduler_dtls_id IN t2704_cycle_count_schedule.c2704_cyc_cnt_schedule_dtls_id%TYPE,
        p_scheduler_date       IN VARCHAR2)
        
AS

    v_date_fmt VARCHAR2 (20) ;
    v_user_id  NUMBER := 30301;
    
BEGIN
    -- to get the date format
     SELECT NVL (get_compdtfmt_frm_cntx (), 'MM/dd/yyyy')
       INTO v_date_fmt
       FROM dual;
       
    --
    IF TRIM (p_help_desk_id) IS NULL THEN
        raise_application_error ('-20999', 'Please enter the help desk id') ;
    END IF;
    
    -- Mandatory helpdesk to log executed user information
    
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (TRIM (p_help_desk_id), NULL) ;
    
    
     UPDATE t2704_cycle_count_schedule
    SET c2704_schedule_date = to_date(p_scheduler_date, v_date_fmt), c2704_last_updated_by = v_user_id
    , c2704_last_updated_date = CURRENT_DATE
      WHERE c2704_cyc_cnt_schedule_dtls_id = p_cc_scheduler_dtls_id;
      
    --  
    dbms_output.put_line ('Row Updated details '|| SQL%ROWCOUNT) ;
    --
    
END gm_upd_scheduler_date;
END it_pkg_ac_cycle_count;
/
