/* Formatted on 2012/02/17 14:48 (Formatter Plus v4.8.0) */


-- @"C:\Data Correction\Script\it_pkg_system.bdy"

CREATE OR REPLACE PACKAGE BODY it_pkg_system
--
IS
/*****************************************************************************************
 * Description : This procedure is used to    save new system
 * std set string format : '978.901,N|978.902,Y'
 * set name format: 'REVLOK Implant and Instrument Set^REVLOK Disposable Instruments Set'
 * addtional set format : '978.903,978.904'
 * locaner set format : '978.901LN,978.902LN'
*******************************************************************************************/
   PROCEDURE gm_sav_system (
      p_help_desk_id          IN   t914_helpdesk_log.c914_helpdesk_id%TYPE,
      p_system_name           IN   VARCHAR2,
      p_std_set_ids           IN   VARCHAR2,
      p_addl_set_ids          IN   VARCHAR2,
      p_loaner_std_set_ids    IN   VARCHAR2,
      p_loaner_addl_set_ids   IN   VARCHAR2
   --  , p_exist_sys_fl    IN VARCHAR2
   )
   AS
      v_user_id        t205_part_number.c205_last_updated_by%TYPE;
      v_exist_sys_fl   VARCHAR2 (1)                                 := 'N';
      v_set_name       t207_set_master.c207_set_nm%TYPE;
      v_max_seq        NUMBER;
      v_max_set        VARCHAR2 (5);
      v_baseline       VARCHAR2 (1);
      v_system_id      t207_set_master.c207_set_id%TYPE;
   BEGIN
      g_help_desk_id := p_help_desk_id;
      -- Mandatory helpdesk to log executed user information
      it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
      v_user_id := it_pkg_cm_user.get_valid_user;

      --my_context.set_my_inlist_ctx (p_set_names);
      SELECT DECODE (COUNT (1), 0, 'N', 1, 'Y')
        INTO v_exist_sys_fl
        FROM t207_set_master
       WHERE UPPER (regexp_replace (c207_set_nm, '.?trade;|.?reg;|.?\(.*\)')) LIKE
                                                         UPPER (p_system_name)
         AND c901_set_grp_type = 1600
         AND c207_void_fl IS NULL;

      IF (v_exist_sys_fl = 'Y')
      THEN
         SELECT SUBSTR (c207_set_id, 5)
           INTO v_max_set
           FROM t207_set_master
          WHERE UPPER (regexp_replace (c207_set_nm,
                                       '.?trade;|.?reg;|.?\(.*\)')
                      ) LIKE UPPER (p_system_name)
            AND c207_set_id LIKE '300%'
            AND c901_set_grp_type = 1600
            AND c207_void_fl IS NULL;

         it_pkg_cm_user.gm_cm_sav_helpdesk_log (g_help_desk_id,
                                                   'System exists and id is '
                                                || v_max_set
                                               );
      ELSE
         SELECT s207_system_id.NEXTVAL
           INTO v_max_set
           FROM DUAL;        /*t207_set_master
                WHERE c207_set_id LIKE '300%' AND c901_set_grp_type = 1600; */

         SELECT MAX (c207_seq_no) + 1
           INTO v_max_seq
           FROM t207_set_master
          WHERE c207_set_id LIKE '300%' AND c207_set_id != '300.026';

         --save the four sets ('300.xxx', 'SET.xxx', 'AAE.xxx', 'AA.xxx')
         gm_sav_set_master ('300.' || TO_CHAR (v_max_set),
                            p_system_name,
                            p_system_name,
                            v_user_id,
                            4060,
                            1600,
                            v_max_seq,
                            NULL,
                            NULL
                           );
         gm_sav_set_master ('SET.' || TO_CHAR (v_max_set),
                            p_system_name || ' - Additional Inventory',
                            'Additional Inventory details for'
                            || p_system_name,
                            v_user_id,
                            4060,
                            1604,
                            999,
                            20101,
                            20701
                           );
         gm_sav_set_master ('AAE.' || TO_CHAR (v_max_set),
                            p_system_name || ' - Additional Items',
                            p_system_name || ' - Additional Items',
                            v_user_id,
                            NULL,
                            1604,
                            299,
                            NULL,
                            20706
                           );
         gm_sav_set_master ('AA.' || TO_CHAR (v_max_set),
                            p_system_name || ' - Items',
                            p_system_name || ' - Items',
                            v_user_id,
                            NULL,
                            1605,
                            199,
                            NULL,
                            20703
                           );
         /****create mapping records in t207a**********/
         gm_insert_set_link ('300.' || v_max_set, 'SET.' || v_max_set, 20002);
         gm_insert_set_link ('SET.' || v_max_set, 'AAE.' || v_max_set, 20003);
         gm_insert_set_link ('AAE.' || v_max_set, 'AA.' || v_max_set, 20005);
         it_pkg_cm_user.gm_cm_sav_helpdesk_log (g_help_desk_id,
                                                   'Created new system '
                                                || '300.'
                                                || v_max_set
                                                || 'With SET ID '
                                                || 'SET.'
                                                || v_max_set
                                                || 'With AAE '
                                                || 'AAE.'
                                                || v_max_set
                                               );
      END IF;

      v_system_id := '300.' || v_max_set;
      /****create mapping records in t207a**********/
      gm_link_std_set (p_std_set_ids, 4070, v_user_id, v_system_id);
      gm_link_additional_set (p_addl_set_ids, 4070, v_user_id, v_system_id);
      gm_link_std_set (p_loaner_std_set_ids, 4071, v_user_id, v_system_id);
      gm_link_additional_set (p_loaner_addl_set_ids,
                              4071,
                              v_user_id,
                              v_system_id
                             );
      it_pkg_cm_user.gm_cm_sav_helpdesk_log (g_help_desk_id,
                                                'standard sets: '
                                             || p_std_set_ids
                                             || ' - additional sets'
                                             || p_addl_set_ids
                                            );
      DBMS_OUTPUT.put_line ('Save system Completed.....');
      --COMMIT;
      dbms_mview.REFRESH ('V207A_SET_CONSIGN_LINK');
      dbms_mview.REFRESH ('V240A_SALES_TARGET_LIST');
      dbms_mview.REFRESH ('V207B_SET_PART_DETAILS');
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line ('Error Occurred while executing SP: '
                               || SQLERRM
                              );
         ROLLBACK;
   END gm_sav_system;

/*****************************************************************************************
 * Description : This procedure is creat links for Standard sets
*******************************************************************************************/
   PROCEDURE gm_link_std_set (
      p_std_set_ids   IN   VARCHAR2,
      p_set_type      IN   t207_set_master.c207_type%TYPE,
      p_user_id       IN   t101_user.c101_user_id%TYPE,
      p_system_id     IN   t207_set_master.c207_set_id%TYPE
   )
   AS
      v_std_setid_len   NUMBER             := NVL (LENGTH (p_std_set_ids), 0);
      v_std_setid_str   VARCHAR2 (4000)                    := p_std_set_ids;
      v_substring       VARCHAR2 (4000);
      v_set_id          t207_set_master.c207_set_id%TYPE;
      v_baseline        VARCHAR2 (2);
      v_system_id       t207_set_master.c207_set_id%TYPE;
   BEGIN
      IF v_std_setid_len > 0
      THEN
         WHILE INSTR (v_std_setid_str, '|') <> 0
         LOOP
            --
            v_set_id := NULL;
            v_baseline := NULL;
            v_substring :=
                 SUBSTR (v_std_setid_str, 1, INSTR (v_std_setid_str, '|') - 1);
            v_std_setid_str :=
                    SUBSTR (v_std_setid_str, INSTR (v_std_setid_str, '|') + 1);
            v_set_id := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
            v_baseline := v_substring;
            --update std sets
            DBMS_OUTPUT.put_line (   ' About to update STD set '
                                  || v_set_id
                                  || ' for system '
                                  || p_system_id
                                  || ' with baseline flag '
                                  || v_baseline
                                 );
            gm_upt_set_master (v_set_id,
                               p_user_id,
                               p_system_id,
                               20731,
                               20700,
                               v_baseline
                              );
            gm_sav_set_link (p_system_id, v_set_id, 20002, p_set_type);
            gm_upt_set_details (p_system_id, v_set_id, p_user_id);
            DBMS_OUTPUT.put_line (   ' remaining v_std_setid_str '
                                  || v_std_setid_str
                                 );
         END LOOP;
      -- DBMS_OUTPUT.put_line (' Linked p_std_set_ids ids  '|| p_std_set_ids );
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'gm_link_std_set '
                               || SQLERRM
                               || 'v_set_id '
                               || v_set_id
                               || ' v_baseline '
                               || v_baseline
                               || ' v_std_setid_str '
                               || v_std_setid_str
                              );
         ROLLBACK;
   END gm_link_std_set;

/*****************************************************************************************
 * Description : This procedure is creat links for Additional available sets
*******************************************************************************************/
   PROCEDURE gm_link_additional_set (
      p_additional_set_ids   IN   VARCHAR2,
      p_set_type             IN   t207_set_master.c207_type%TYPE,
      p_user_id              IN   t101_user.c101_user_id%TYPE,
      p_system_id            IN   t207_set_master.c207_set_id%TYPE
   )
   AS
      CURSOR cur_details
      IS
         SELECT token
           FROM v_in_list
          WHERE token IS NOT NULL;

      v_set_id   t207_set_master.c207_set_id%TYPE;
   BEGIN
      -- Remove from context first
      my_context.set_my_inlist_ctx ('');
      --additional avail set
      my_context.set_my_inlist_ctx (p_additional_set_ids);

      FOR var_detail IN cur_details
      LOOP
         --
         v_set_id := var_detail.token;

     DBMS_OUTPUT.put_line (   ' About to update Additional set '
		  || v_set_id
		  || ' for system '
		  || p_system_id );

         gm_sav_set_link ('SET.' || SUBSTR (p_system_id, 5),
                          v_set_id,
                          20003,
                          p_set_type
                         );
         ---update for AAS
         gm_upt_set_master (v_set_id,
                            p_user_id,
                            p_system_id,
                            20731,
                            20702,
                            20101
                           );
         gm_upt_set_details (p_system_id, v_set_id, p_user_id);
      END LOOP;
   -- DBMS_OUTPUT.put_line (' Linked p_additional_set_ids ids  '|| p_additional_set_ids );
   END gm_link_additional_set;

/*****************************************************************************************
 * Description : This procedure is save new set in set master table
*******************************************************************************************/
   PROCEDURE gm_sav_set_master (
      p_set_id         IN   t207_set_master.c207_set_id%TYPE,
      p_set_nm         IN   VARCHAR2,
      p_set_desc       IN   VARCHAR2,
      p_user_id        IN   t205_part_number.c205_last_updated_by%TYPE,
      p_category       IN   t207_set_master.c207_category%TYPE,
      p_set_grp_type   IN   t207_set_master.c901_set_grp_type%TYPE,
      p_seq_no         IN   t207_set_master.c207_seq_no%TYPE,
      p_rpt_id         IN   t207_set_master.c901_cons_rpt_id%TYPE,
      p_hierarchy      IN   t207_set_master.c901_hierarchy%TYPE
   )
   AS
   BEGIN
      INSERT INTO t207_set_master
                  (c207_set_id, c207_set_nm, c207_set_desc,
                   c207_created_date, c207_created_by, c207_category,
                   c901_set_grp_type, c207_seq_no, c901_cons_rpt_id,
                   c901_hierarchy
                  )
           VALUES (p_set_id, p_set_nm, p_set_nm,
                   SYSDATE, p_user_id, p_category,
                   p_set_grp_type, p_seq_no, p_rpt_id,
                   p_hierarchy
                  );
   END gm_sav_set_master;

/*****************************************************************************************
 * Description : This procedure is create links for a standard and additional available set
*******************************************************************************************/
   PROCEDURE gm_sav_set_link (
      p_main_setid   IN   t207a_set_link.c207_main_set_id%TYPE,
      p_set_linkid   IN   t207a_set_link.c207_link_set_id%TYPE,
      p_type         IN   t207a_set_link.c901_type%TYPE,
      p_set_type     IN   t207_set_master.c207_type%TYPE
   )
   AS
      v_main_set_id    t207a_set_link.c207_main_set_id%TYPE;
      v_child_set_id   t207a_set_link.c207_main_set_id%TYPE;
      v_set_name       t207_set_master.c207_set_nm%TYPE;
      v_user_id        t205_part_number.c205_last_updated_by%TYPE;
      v_ai_set_count   NUMBER;
      v_existing_link_fl VARCHAR2(2);
   BEGIN
      v_user_id := it_pkg_cm_user.get_valid_user;
      v_existing_link_fl := 'Y';

      BEGIN
         -- Check if mapping exists
         SELECT t207a.c207_main_set_id
           INTO v_main_set_id
           FROM t207a_set_link t207a
          WHERE t207a.c207_link_set_id = p_set_linkid
            AND t207a.c901_type NOT IN (20004)
            AND t207a.c901_shared_status IS NULL;

         -- If itz not 1601 then it must be an additional stuff. so just return
         -- No Changes in mapping
         IF (v_main_set_id = p_main_setid)
         THEN
            RETURN;
         ELSE
            IF p_set_type = 4070
            THEN
               BEGIN
                  SELECT c207_link_set_id
                    INTO v_child_set_id
                    FROM t207a_set_link
                   WHERE c207_main_set_id = p_set_linkid AND c901_type = 20004;

               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
		  
		 DBMS_OUTPUT.put_line (   ' No Child set available. so creating child set for '
		  || p_set_linkid || ' with type ' || p_type );
                     gm_sav_ai_set (p_set_linkid, p_type, p_set_type);
                     v_child_set_id := 'AI.' || p_set_linkid;
       		     v_existing_link_fl := 'N';
               END;
            END IF;

            DELETE FROM t207a_set_link
                  WHERE c901_type <> 20004
                    AND (   c207_link_set_id = p_set_linkid
                         OR c207_link_set_id = v_child_set_id
                        );

            gm_sav_set_link_mappings (p_main_setid,
                                      p_set_linkid,
                                      v_child_set_id,
                                      p_type,
                                      p_set_type,
                                      v_existing_link_fl
                                     );
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            DBMS_OUTPUT.put_line (   ' No Data found so creating a set '
                                  || p_set_linkid
                                  || ' with main set id '
                                  || p_main_setid
                                 );
            it_pkg_cm_user.gm_cm_sav_helpdesk_log (g_help_desk_id,
                                                      'No links present for  '
                                                   || p_set_linkid
                                                   || ' Will be linked to '
                                                   || p_main_setid
                                                  );
            gm_sav_ai_set (p_set_linkid, p_type, p_set_type);
--   END IF;
               -- Map the main and AI set
            gm_sav_set_link_mappings (p_main_setid,
                                      p_set_linkid,
                                      'AI.' || p_set_linkid,
                                      p_type,
                                      p_set_type,
                                      'N'
                                     );
         --ELSE
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line (   ' Error while saving set link '
                                  || p_set_linkid
                                  || ' with main set id '
                                  || p_main_setid
                                 );
            it_pkg_cm_user.gm_cm_sav_helpdesk_log
                                (g_help_desk_id,
                                    'Error Occurred while saving setlink for '
                                 || p_set_linkid
                                 || ' with main id '
                                 || p_main_setid
                                );
            ROLLBACK;
      END;
   END gm_sav_set_link;

/*****************************************************************************************
 * Description : This procedure is creat mappings for parent and itz AI child sets.
 * Eg: for 965.905 and its Additional Inventory AI.XXXX
*******************************************************************************************/
   PROCEDURE gm_sav_set_link_mappings (
      p_main_setid     IN   t207a_set_link.c207_main_set_id%TYPE,
      p_set_linkid     IN   t207a_set_link.c207_link_set_id%TYPE,
      p_child_set_id   IN   t207a_set_link.c207_link_set_id%TYPE,
      p_type           IN   t207a_set_link.c901_type%TYPE,
      p_set_type       IN   t207_set_master.c207_type%TYPE,
      p_existing_fl    IN   VARCHAR2
   )
   AS
   BEGIN
      gm_insert_set_link (p_main_setid, p_set_linkid, p_type);
      it_pkg_cm_user.gm_cm_sav_helpdesk_log
                                           (g_help_desk_id,
                                               ' Creating links for main set'
                                            || p_main_setid
                                            || ' for link '
                                            || p_set_linkid
                                            || ' with child '
                                            || p_child_set_id
                                           );

      IF p_set_type = 4070
      THEN
         IF p_existing_fl <> 'Y'
         THEN
            gm_insert_set_link (p_set_linkid, p_child_set_id, 20004);
         END IF;

         IF (p_type = 20002)
         THEN
            gm_insert_set_link ('SET.' || (SUBSTR (p_main_setid, 5)),
                                p_child_set_id,
                                20003
                               );
         ELSE
            gm_insert_set_link ('AAE.' || (SUBSTR (p_main_setid, 5)),
                                p_child_set_id,
                                20005
                               );
         END IF;
      END IF;
   END gm_sav_set_link_mappings;

/*****************************************************************************************
 * Description : This procedure is insert mappings in the set link table
*******************************************************************************************/
   PROCEDURE gm_insert_set_link (
      p_main_setid   IN   t207a_set_link.c207_main_set_id%TYPE,
      p_set_linkid   IN   t207a_set_link.c207_link_set_id%TYPE,
      p_type         IN   t207a_set_link.c901_type%TYPE
   )
   AS
   BEGIN
      INSERT INTO t207a_set_link
                  (t207a_set_link_id, c207_main_set_id, c207_link_set_id,
                   c901_type
                  )
           VALUES (s207a_set_link.NEXTVAL, p_main_setid, p_set_linkid,
                   p_type
                  );
   END gm_insert_set_link;

/*****************************************************************************************
 * Description : This procedure is update record in set master table
*******************************************************************************************/
   PROCEDURE gm_upt_set_master (
      p_set_id         IN   t207_set_master.c207_set_id%TYPE,
      p_user_id        IN   t205_part_number.c205_last_updated_by%TYPE,
      p_systemid       IN   t207_set_master.c207_set_sales_system_id%TYPE,
      p_minset_logic   IN   t207_set_master.c901_minset_logic%TYPE,
      p_hierarchy      IN   t207_set_master.c901_hierarchy%TYPE,
      p_baseline_fl    IN   VARCHAR2
   )
   AS
   BEGIN
      UPDATE t207_set_master t207
         SET t207.c901_cons_rpt_id = DECODE (p_baseline_fl,
                                             'Y', 20100,
                                             20101
                                            ),
             t207.c901_hierarchy = p_hierarchy,
             t207.c901_minset_logic = p_minset_logic,
             t207.c207_set_sales_system_id = p_systemid,
             t207.c207_last_updated_date = SYSDATE,
             t207.c207_last_updated_by = p_user_id
       WHERE t207.c207_set_id = p_set_id;
   END gm_upt_set_master;

/*****************************************************************************************
 * Description : This procedure is update record in set detail table
*******************************************************************************************/
   PROCEDURE gm_upt_set_details (
      p_max_set   IN   t208_set_details.c207_set_id%TYPE,
      p_set_id    IN   t208_set_details.c207_set_id%TYPE,
      p_user_id   IN   t205_part_number.c205_last_updated_by%TYPE
   )
   AS
   BEGIN
      UPDATE t208_set_details
         SET c207_set_id = p_max_set,
             c208_last_updated_by = p_user_id,
             c208_last_updated_date = SYSDATE
       WHERE c205_part_number_id IN (SELECT c205_part_number_id
                                       FROM t208_set_details
                                      WHERE c207_set_id = p_set_id)
         AND c207_set_id LIKE '300.026';
   END gm_upt_set_details;

/*****************************************************************************************
* Description : This procedure is to create a new AI Set, if it wasnt there already
*******************************************************************************************/
   PROCEDURE gm_sav_ai_set (
      p_set_linkid   IN   t207a_set_link.c207_link_set_id%TYPE,
      p_type         IN   t207a_set_link.c901_type%TYPE,
      p_set_type     IN   t207_set_master.c207_type%TYPE
   )
   AS
      v_set_name         t207_set_master.c207_set_nm%TYPE;
      v_user_id          t205_part_number.c205_last_updated_by%TYPE;
      v_ai_set_count     NUMBER;
      v_hierarchy_type   t207_set_master.c901_hierarchy%TYPE;
   BEGIN
      v_user_id := it_pkg_cm_user.get_valid_user;

      IF p_set_type = 4070
      THEN
         BEGIN
            SELECT c207_set_nm
              INTO v_set_name
              FROM t207_set_master
             WHERE c207_set_id = p_set_linkid AND c207_void_fl IS NULL;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               DBMS_OUTPUT.put_line (   ' Set '
                                     || p_set_linkid
                                     || ' doesnt exist in set master '
                                    );
         END;
      ELSE -- If this is loaner, dont need to create AI sets
         RETURN;
      END IF;

      -- Since there was no prior mapping, no data found. so insert
                    -- Create an AI Set
      IF (p_type = 20002)
      THEN
         v_hierarchy_type := 20704;
      ELSE
         v_hierarchy_type := 20702;
      END IF;

      SELECT COUNT (1)
        INTO v_ai_set_count
        FROM t207_set_master
       WHERE c207_set_id = 'AI.' || p_set_linkid;

      IF (v_ai_set_count = 0)
      THEN
         gm_sav_set_master ('AI.' || p_set_linkid,
                            v_set_name,
                            v_set_name || ' - Additional Inventory',
                            v_user_id,
                            4060,
                            1603,
                            53,
                            NULL,
                            v_hierarchy_type
                           );
      END IF;

      DBMS_OUTPUT.put_line (   ' Created '
                            || 'AI.'
                            || p_set_linkid
                            || ' in set master '
                           );
   END gm_sav_ai_set;

/*****************************************************************************************
 * Description : This procedure is used to   update tracking implant in t205 table
*******************************************************************************************/
   PROCEDURE gm_sav_tracking_implant (
      p_help_desk_id     IN   t914_helpdesk_log.c914_helpdesk_id%TYPE,
      p_part_numbers     IN   VARCHAR2,
      p_remove_part_fl   IN   VARCHAR2
   )
   AS
      v_user_id   t205_part_number.c205_last_updated_by%TYPE;
   BEGIN
      it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
      v_user_id := it_pkg_cm_user.get_valid_user;
      my_context.set_my_inlist_ctx (p_remove_part_fl);

      UPDATE t205_part_number
         SET c205_tracking_implant_fl = NULL,
             c205_last_updated_by = v_user_id,
             c205_last_updated_date = SYSDATE
       WHERE c205_part_number_id IN (SELECT token
                                       FROM v_in_list
                                      WHERE token IS NOT NULL);

      my_context.set_my_inlist_ctx (p_part_numbers);

      UPDATE t205_part_number
         SET c205_tracking_implant_fl = 'Y',
             c205_last_updated_by = v_user_id,
             c205_last_updated_date = SYSDATE
       WHERE c205_part_number_id IN (SELECT token
                                       FROM v_in_list
                                      WHERE token IS NOT NULL);

      IF (SQL%ROWCOUNT = 0)
      THEN
         ROLLBACK;
         raise_application_error ('-20085',
                                  'No record is found to be updated.'
                                 );
      END IF;

      DBMS_OUTPUT.put_line (SQL%ROWCOUNT || ' Row(s) are updated. ');
      --COMMIT;
      dbms_mview.REFRESH ('V207B_SET_PART_DETAILS');
   --
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line ('Error Occurred while executing SP: '
                               || SQLERRM
                              );
         ROLLBACK;
   END gm_sav_tracking_implant;
END it_pkg_system;
/
