/* Formatted on 2011/01/20 15:10 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_op_returns.bdy"

CREATE OR REPLACE PACKAGE BODY it_pkg_op_returns
IS
--

	/*********************************************************
	* Description : This procedure is used to initiate new return
	* if the return is accidently colsed.
	*********************************************************/
	PROCEDURE gm_sav_reopen_closed_ra (
		p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_ra_id 		 t506_returns.c506_rma_id%TYPE
	)
	AS
		v_user_id	   NUMBER;
		v_string	   VARCHAR2 (20);
		v_distid	   t701_distributor.c701_distributor_id%TYPE;
		v_expdate	   VARCHAR2 (20);

		-- Found all missing items
		CURSOR return_cur
		IS
			SELECT c205_part_number_id pnum, c507_item_qty pending_qty, c507_item_price price
			  FROM t507_returns_item t507
			 WHERE t507.c506_rma_id = p_ra_id AND t507.c507_status_fl = 'M';
	BEGIN
		BEGIN
			SELECT t506.c701_distributor_id, TO_CHAR (t506.c506_expected_date, 'mm/dd/yyyy')
			  INTO v_distid, v_expdate
			  FROM t506_returns t506
			 WHERE t506.c506_rma_id = p_ra_id
			   AND t506.c506_status_fl = '2'
			   AND t506.c506_type = 3306
			   AND c506_void_fl IS NULL;   --'0', open, '1' pending credit, '2' closed. 3306 Consignment - Distributor Closure
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error ('-20085'
									   , 'Return ' || p_ra_id || ' does not exist, or its voided or not closed.'
										);
		END;

		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		SELECT 'GM-RA-' || s506_return.NEXTVAL
		  INTO v_string
		  FROM DUAL;

		--3302 Consignment - Items, 3313 Consignment Return
		INSERT INTO t506_returns
					(c506_rma_id, c701_distributor_id, c506_type, c506_reason, c506_expected_date, c506_status_fl
				   , c506_created_by, c506_created_date
					)
			 VALUES (v_string, v_distid, 3302, 3313, TO_DATE (v_expdate, 'mm/dd/yyyy'), '0'
				   , v_user_id, SYSDATE
					);

		FOR rad_val IN return_cur
		LOOP
			INSERT INTO t507_returns_item
						(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_item_qty, c507_item_price
					   , c507_status_fl
						)
				 VALUES (s507_return_item.NEXTVAL, v_string, rad_val.pnum, rad_val.pending_qty, rad_val.price
					   , 'Q'
						);

			INSERT INTO t507_returns_item
						(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_item_qty, c507_item_price
					   , c507_status_fl
						)
				 VALUES (s507_return_item.NEXTVAL, v_string, rad_val.pnum, rad_val.pending_qty, rad_val.price
					   , 'R'
						);
		END LOOP;

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Old Return ID: ' || p_ra_id);
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'New Return ID: ' || v_string);
		DBMS_OUTPUT.put_line ('New Return ' || v_string || ' is created successfully. ');
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_reopen_closed_ra;
/*********************************************************
	* Description : This procedure is used to update RA status TSK-9157
	*********************************************************/
	PROCEDURE gm_sav_ra_reason (
		p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_ra_id 		 t506_returns.c506_rma_id%TYPE
	  , p_reason         t506_returns.c506_reason%TYPE
	  , p_user_id        t506_returns.  c506_last_updated_by%TYPE  
	)
	AS		
		v_user_id	   NUMBER;
	BEGIN	
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;
		--to update reason
		UPDATE t506_returns 
         SET c506_reason = p_reason 
      	  ,c506_last_updated_by = p_user_id 
	      ,c506_last_updated_date = sysdate 
	     	 WHERE c506_rma_id = p_ra_id;  
		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'RA Reason updated: ' || p_ra_id);
		--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_ra_reason;
END it_pkg_op_returns;
/
