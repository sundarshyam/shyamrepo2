/* Formatted on 2011/02/01 15:54 (Formatter Plus v4.8.0) */
-- @"C:\database\Data Correction\Script\it_pkg_ar_batch.bdy"
CREATE OR REPLACE PACKAGE BODY it_pkg_ar_batch
IS
    --
    /*********************************************************
    * Description : This procedure is used to remove the duplicate orders present in same batch
    *********************************************************/
PROCEDURE gm_remove_dup_order_in_batch (
        p_help_desk_id IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
        p_batch_id     IN T9601_BATCH_DETAILS.C9600_BATCH_ID%TYPE)
AS
    v_batch_detail_id T9601_BATCH_DETAILS.C9601_BATCH_DETAIL_ID%TYPE;
    v_duplicate_row VARCHAR2 (4000) ;
    v_user_id       NUMBER;
    v_batch_id t9600_batch.c9600_batch_id%TYPE;
    v_batch_cnt NUMBER;
    v_cnt       NUMBER := 0;
    --
    CURSOR v_duplicate_entry_cur
    IS
         SELECT C9601_REF_ID ref_id, COUNT (1)
           FROM T9601_BATCH_DETAILS
          WHERE C9600_BATCH_ID = v_batch_id
            AND C901_REF_TYPE  = 18751
            AND c9601_void_fl IS NULL
       GROUP BY C9601_REF_ID
        HAVING COUNT (1) > 1 ;
BEGIN
    v_batch_id := TRIM (p_batch_id) ;
    -- checking whether batch id is present
     SELECT COUNT (1)
       INTO v_batch_cnt
       FROM t9600_batch
      WHERE C9600_BATCH_ID  = v_batch_id
        AND C901_BATCH_TYPE = 18751
        AND C9600_VOID_FL  IS NULL;
    --
    IF v_batch_cnt = 0 THEN
        raise_application_error ('-20999', 'Batch ID ' ||v_batch_id ||
        ' is invalid/voided, Please enterd the correct Batch ID.') ;
    END IF;
    --
    IF TRIM (p_help_desk_id) IS NULL THEN
        raise_application_error ('-20999', 'Please enter the help desk id') ;
    END IF;
    -- Mandatory helpdesk to log executed user information
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (TRIM (p_help_desk_id), NULL) ;
    v_user_id := it_pkg_cm_user.get_valid_user;
    -- 18751 Invoice Batch
    FOR v_duplicate IN v_duplicate_entry_cur
    LOOP
        -- to get the max number id
         SELECT MAX (C9601_BATCH_DETAIL_ID)
           INTO v_batch_detail_id
           FROM T9601_BATCH_DETAILS
          WHERE C9600_BATCH_ID = v_batch_id
            AND C9601_REF_ID   = v_duplicate.ref_id
            AND C901_REF_TYPE  = 18751
            AND c9601_void_fl IS NULL;
        -- to update the void flag
         UPDATE T9601_BATCH_DETAILS
        SET c9601_void_fl              = 'Y', c9601_last_update_by = v_user_id, c9601_last_updated_date = CURRENT_DATE
          WHERE C9600_BATCH_ID         = v_batch_id
            AND C901_REF_TYPE          = 18751
            AND C9601_REF_ID   = v_duplicate.ref_id
            AND C9601_BATCH_DETAIL_ID <> v_batch_detail_id
            AND c9601_void_fl         IS NULL;
        --
        v_duplicate_row := v_duplicate_row || v_duplicate.ref_id || ', ';
        v_cnt           := v_cnt + 1;
    END LOOP;
    --
    IF v_cnt <> 0 THEN
        -- remove the last comma.
        v_duplicate_row := SUBSTR (v_duplicate_row, 0, LENGTH (v_duplicate_row) - 2) ;
        --
        DBMS_OUTPUT.PUT_LINE ('Below Dulicate Orders are removed from batch: ' || v_duplicate_row) ;
    ELSE
        DBMS_OUTPUT.PUT_LINE ('No Duplicate Order ID in this Batch : ' || v_batch_id) ;
    END IF;
END gm_remove_dup_order_in_batch;

/*********************************************************
    * Description : This procedure is used to void invoices 
    *********************************************************/
PROCEDURE gm_void_invoice(
    p_help_desk_id IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
    p_invoice_id   IN T503_INVOICE.C503_INVOICE_ID%TYPE )
AS
  v_void_fl             T503_INVOICE.C503_VOID_FL%TYPE;
  v_payment_amt 		T503_INVOICE.C503_INV_PYMNT_AMT%TYPE;
  v_user_id 			NUMBER;
  v_monthly_comp_id 	t1900_company.c1900_company_id%TYPE;
  v_company_id 			t1900_company.c1900_company_id%TYPE;
  v_invoice_type  		t503_invoice.c901_invoice_type%TYPE;
BEGIN
  -- Mandatory helpdesk to log executed user information
  It_Pkg_Cm_User.Gm_Cm_Sav_Helpdesk_Log (P_Help_Desk_Id, Null);
  
  -- get the user id
  v_user_id := it_pkg_cm_user.get_valid_user;
     
      BEGIN
        SELECT C503_VOID_FL,NVL(C503_INV_PYMNT_AMT,0),c1900_company_id,c901_invoice_type
        INTO v_void_fl,v_payment_amt,v_company_id,v_invoice_type
        FROM T503_INVOICE
        WHERE C503_INVOICE_ID=p_invoice_id;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        raise_application_error ('-20085', 'No record found');
      END;
  
  
	  -- to get monthly invoice bath company id
	    SELECT get_rule_value(v_company_id,'MONTHLY_INV_COMP')
	    INTO v_monthly_comp_id
	    FROM DUAL;
   
   
	    IF v_void_fl ='Y' THEN
	      raise_application_error ('-20085', 'Invoice is already voided');
	    END IF;
  

   	   IF v_payment_amt <> 0 THEN
	     raise_application_error ('-20085', 'Already Payment has been updated ,so invoice cannot be voided');
	   END IF;
  
  
		  UPDATE T503_INVOICE
		  SET C503_Void_Fl        ='Y',
		    C503_Last_Updated_By  =v_user_id,
		    C503_Last_Updated_Date=CURRENT_DATE
		  WHERE C503_Invoice_Id   = p_invoice_id;
  
  
  		--Update Invoice Batch detail void flag to Y
		  UPDATE t9601_batch_details
		  SET c9601_void_fl         = 'Y',
		    c9601_last_update_by    = v_user_id,
		    c9601_last_updated_date = CURRENT_DATE
		  WHERE c9601_ref_id       IN
		    (SELECT c501_order_id
		    FROM t501_order
		    WHERE c503_invoice_id = p_invoice_id
		    AND c501_void_fl     IS NULL
		    )
		  AND c9601_void_fl IS NULL;
  
  
  		-- clear PO only for the Monthly invoice
		  IF v_company_id = v_monthly_comp_id THEN
		   
		    --Update Invoice attribute details void flag to Y
		    UPDATE T503b_Invoice_Attribute
		    SET C503b_Void_Fl         = 'Y',
		      C503b_Last_Updated_By   = v_user_id,
		      C503b_Last_Updated_Date = CURRENT_DATE
		    WHERE C503_Invoice_Id     = p_invoice_id;
		   
		    -- Update the customer PO as null which was generated by system for the Monthly Invoice
		    
		    UPDATE T501_Order
		    SET C501_Customer_Po     = '',
		      C501_Last_Updated_By   = v_user_id,
		      C501_Last_Updated_Date = CURRENT_DATE
		    WHERE C503_Invoice_Id    = p_invoice_id;
		  
		  END IF;
  
  
			  -- Update the Order table to set the INvoice Id as NULL
			  UPDATE t501_order
			  SET c503_invoice_id      = NULL ,
			    c501_last_updated_by   = v_user_id ,
			    c501_last_updated_date = CURRENT_DATE
			  WHERE c503_invoice_id    = p_invoice_id;
 
			  -- Update the file info table void fl
			  UPDATE t903_upload_file_list
			  SET c903_delete_fl       = 'Y' ,
			    c903_last_updated_by   = v_user_id ,
			    c903_last_updated_date = CURRENT_DATE
			  WHERE c903_ref_id        = p_invoice_id
			  AND c901_ref_type        = '90906';
  
		  it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Invoice Id: ' || p_invoice_id);
		  DBMS_OUTPUT.put_line ('Invoice Id '||p_invoice_id || ' voided ');
		 
		EXCEPTION
		WHEN OTHERS THEN
		  DBMS_OUTPUT.put_line ('Error Occurred while executing: ' || SQLERRM);
		  
		  
END gm_void_invoice;

END it_pkg_ar_batch;
/
