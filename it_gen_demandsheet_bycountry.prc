-- @"C:\Database\Datacorrection\Script\it_gen_demandsheet_bycountry.prc";

CREATE OR REPLACE PROCEDURE it_gen_demandsheet_bycountry (
    p_country_value IN t4015_global_sheet_mapping.c901_level_value%TYPE,
    p_user_id       IN t4020_demand_master.c4020_created_by%TYPE)
AS
    v_glbl_map_id t4015_global_sheet_mapping.c4015_global_sheet_map_id%TYPE;
    v_lvl_nm 	  VARCHAR2 (50) ;
    v_salesfl 	  t4015_global_sheet_mapping.c4015_sales_sheet_fl%TYPE;
    v_consignfl   t4015_global_sheet_mapping.c4015_consignment_sheet_fl%TYPE;
    v_inhousfl    t4015_global_sheet_mapping.c4015_inhouse_sheet_fl%TYPE;
    v_outdemsheetid t4020_demand_master.c4020_demand_master_id%TYPE;
    
    --Cursor to get all the active template.
    CURSOR v_cur_glbl_map
    IS
         SELECT c4020_demand_master_id demandmasterid, c4020_demand_nm demandnm, c901_demand_type demandtype
	          , c4020_demand_period demandperiod, c4020_forecast_period fcperiod, c4020_request_period reqperiod
	          , c4020_inactive_fl inactivefl, c901_company_id companyid, c4020_primary_user primaryuser
	          , get_code_name (c901_company_id) companynm, get_user_name (c4020_primary_user) primaryusernm,
	            c901_sheet_status sheetstatusid, get_code_name (c901_sheet_status) sheetstatus
           FROM t4020_demand_master t4020
          WHERE c4020_void_fl IS NULL
            AND c901_demand_type = 4000103 --Demand Sheet Template Type
       ORDER BY UPPER (c4020_demand_nm) ;
BEGIN
	--From the input passed country ID, get the inventory, sheet mapping details.
     SELECT t4015.c4015_global_sheet_map_id, DECODE (t4015.c901_level_id, '102584', 
	        get_rule_value ( t4015.c901_level_value, 'REG_SHRT_CODE'), get_code_name (t4015.c901_level_value)),
	        t4015.c4015_sales_sheet_fl, t4015.c4015_consignment_sheet_fl, t4015.c4015_inhouse_sheet_fl
       INTO v_glbl_map_id, v_lvl_nm, v_salesfl
          , v_consignfl, v_inhousfl
       FROM t4015_global_sheet_mapping t4015, t4016_global_inventory_mapping t4016
      WHERE t4015.c4016_global_inventory_map_id = t4016.c4016_global_inventory_map_id
        AND t4016.c901_level_value              = p_country_value 
        AND t4015.c4015_void_fl                IS NULL
        AND t4016.c4016_void_fl                IS NULL;
        
 	--Loop through each Template and create 3 Type of demand sheets(Sales,Consignment,In-house) and Demand Mapping for each one.    
    FOR v_cur IN v_cur_glbl_map
    LOOP
        IF NVL (v_salesfl, 'N') = 'Y' THEN
            --Create Sales Demand Sheet.
            gm_pkg_oppr_sheet.gm_fc_sav_demsheet (NULL, v_cur.demandnm||' '||v_lvl_nm||' '|| GET_CODE_NAME_ALT (40020),
                                                  v_cur.demandperiod, v_cur.fcperiod, NULL, NULL, v_cur.inactivefl, p_user_id, v_cur.primaryuser, 40020 --sales
                                                , NULL, v_cur.demandmasterid, v_glbl_map_id, v_cur.companyid, NULL, v_outdemsheetid) ;
           
	        --Creating Sales Sheet Mapping, if Sales Demand Sheet is created in above.
	        IF v_outdemsheetid IS NOT NULL THEN
                 DELETE
                   FROM T4021_Demand_Mapping
                  WHERE c4020_demand_master_id = v_outdemsheetid;
                 INSERT
                   INTO t4021_demand_mapping
                    (
                        c4021_demand_mapping_id, c4020_demand_master_id, c901_ref_type
                      , c4021_ref_id, c901_action_type
                    )
                 SELECT s4021_demand_mapping.NEXTVAL, v_outdemsheetid, t4021.c901_ref_type
                  , t4021.c4021_ref_id, t4021.c901_action_type
                   FROM t4020_demand_master t4020, t4021_demand_mapping t4021
                  WHERE t4020.c4020_demand_master_id = v_cur.demandmasterid
                    AND t4020.c4020_demand_master_id = t4021.c4020_demand_master_id
                    AND t4020.c901_demand_type       = 4000103 -- Demand Sheet Template
                    AND t4020.c4020_inactive_fl     IS NULL
                    AND t4020.c4020_void_fl         IS NULL
                    AND t4021.c901_ref_type          = 40030;  --sales type mapping
            END IF;
        END IF;
        
        IF NVL (v_consignfl, 'N') = 'Y' THEN
            --Create Consign Demand Sheet.
            gm_pkg_oppr_sheet.gm_fc_sav_demsheet (NULL, v_cur.demandnm||' '||v_lvl_nm||' '|| GET_CODE_NAME_ALT (40021),
                                                  v_cur.demandperiod, v_cur.fcperiod, NULL, NULL, v_cur.inactivefl, p_user_id, v_cur.primaryuser, 40021 -- consignment
                                                , v_cur.reqperiod, v_cur.demandmasterid, v_glbl_map_id, v_cur.companyid, NULL, v_outdemsheetid) ;
           
	        --Creating Consign Sheet Mapping, if Consign Demand Sheet is created in above.
            IF v_outdemsheetid IS NOT NULL THEN
                 DELETE
                   FROM T4021_Demand_Mapping
                  WHERE c4020_demand_master_id = v_outdemsheetid;
                 INSERT
                   INTO t4021_demand_mapping
                    (
                        c4021_demand_mapping_id, c4020_demand_master_id, c901_ref_type
                      , c4021_ref_id, c901_action_type
                    )
                 SELECT s4021_demand_mapping.NEXTVAL, v_outdemsheetid, t4021.c901_ref_type
                  , t4021.c4021_ref_id, t4021.c901_action_type
                   FROM t4020_demand_master t4020, t4021_demand_mapping t4021
                  WHERE t4020.c4020_demand_master_id = v_cur.demandmasterid
                    AND t4020.c4020_demand_master_id = t4021.c4020_demand_master_id
                    AND t4020.c901_demand_type       = 4000103 -- Demand Sheet Template
                    AND t4020.c4020_inactive_fl     IS NULL
                    AND t4020.c4020_void_fl         IS NULL
                    AND t4021.c901_ref_type          = 40031;  --Consign type mapping
            END IF;
        END IF;
        
        IF NVL (v_inhousfl, 'N') = 'Y' THEN
            --Create inhouse Demand Sheet.
            gm_pkg_oppr_sheet.gm_fc_sav_demsheet (NULL, v_cur.demandnm||' '||v_lvl_nm||' '|| GET_CODE_NAME_ALT (40022),
                                                  v_cur.demandperiod, v_cur.fcperiod, NULL, NULL, v_cur.inactivefl, p_user_id, v_cur.primaryuser, 40022 -- Inhouse
                                                , v_cur.reqperiod, v_cur.demandmasterid, v_glbl_map_id, v_cur.companyid, NULL, v_outdemsheetid) ;
           
	        --Creating inhouse Sheet Mapping, if inhouse Demand Sheet is created in above.
            IF v_outdemsheetid IS NOT NULL THEN
                 DELETE
                   FROM T4021_Demand_Mapping
                  WHERE c4020_demand_master_id = v_outdemsheetid;
                 INSERT
                   INTO t4021_demand_mapping
                    (
                        c4021_demand_mapping_id, c4020_demand_master_id, c901_ref_type
                      , c4021_ref_id, c901_action_type
                    )
                 SELECT s4021_demand_mapping.NEXTVAL, v_outdemsheetid, t4021.c901_ref_type
                  , t4021.c4021_ref_id, t4021.c901_action_type
                   FROM t4020_demand_master t4020, t4021_demand_mapping t4021
                  WHERE t4020.c4020_demand_master_id = v_cur.demandmasterid
                    AND t4020.c4020_demand_master_id = t4021.c4020_demand_master_id
                    AND t4020.c901_demand_type       = 4000103 -- Demand Sheet Template
                    AND t4020.c4020_inactive_fl     IS NULL
                    AND t4020.c4020_void_fl         IS NULL
                    AND t4021.c901_ref_type          = 4000109; --inhouse type mapping
            END IF;
        END IF;
        
    END LOOP;
END it_gen_demandsheet_bycountry;
/