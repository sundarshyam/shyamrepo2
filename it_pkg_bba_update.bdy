CREATE OR REPLACE
PACKAGE BODY it_pkg_bba_update
IS
--

	/*********************************************************
	* Description : This procedure inserts/updates CPC info
	* 
	*********************************************************/
	PROCEDURE gm_upd_cpc_data (
		p_help_desk_id					IN t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_cpc_name						IN t704b_account_cpc_mapping.c704b_cpc_name%TYPE
	  , p_cpc_address					IN t704b_account_cpc_mapping.c704b_cpc_address%TYPE
		, p_cpc_barcode_lbl_ver	IN t704b_account_cpc_mapping.c704b_cpc_barcode_lbl_ver%TYPE
		, p_cpc_final_cpc_ver		IN t704b_account_cpc_mapping.c704b_cpc_final_cpc_ver%TYPE
		, p_active_fl					  IN t704b_account_cpc_mapping.c704b_active_fl%TYPE
	)
	AS
  BEGIN		
		UPDATE t704b_account_cpc_mapping 
    SET c704b_cpc_address = nvl(p_cpc_address,c704b_cpc_address),
    c704b_cpc_barcode_lbl_ver = nvl(p_cpc_barcode_lbl_ver,c704b_cpc_barcode_lbl_ver),
    c704b_cpc_final_cpc_ver = nvl(p_cpc_final_cpc_ver,c704b_cpc_final_cpc_ver),
		c704b_active_fl = p_active_fl
    WHERE c704b_cpc_name = p_cpc_name
    AND  c704b_void_fl IS NULL;
     
     
    IF SQL%rowcount = 0 THEN
			INSERT INTO t704b_account_cpc_mapping
				(c704b_account_cpc_map_id  , c704b_cpc_name, c704b_cpc_address, c704b_cpc_barcode_lbl_ver, c704b_cpc_final_cpc_ver,c704b_active_fl                  
			   , c704b_last_updated_by, c704a_last_updated_date
				)
			VALUES (s704b_account_cpc_mapping.nextval, p_cpc_name, p_cpc_address, p_cpc_barcode_lbl_ver, p_cpc_final_cpc_ver, p_active_fl
        , 30301, SYSDATE);
    END IF;

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'CPC Name : ' || p_cpc_name);

		dbms_output.put_line (SQL%rowcount || ' Row(s) are updated. ');
		COMMIT;
	--
	exception
		WHEN others
		THEN
			dbms_output.put_line ('Error Occurred while executing SP: ' || sqlerrm);
			ROLLBACK;
  END gm_upd_cpc_data;
  
    /*
    * Updating lot qty for the passing control number in warehouse for inventory adjustment.
    */
	PROCEDURE gm_sav_lot_qty (
	    p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE,
	    p_part_number     t205_part_number.c205_part_number_id%TYPE,
    	p_control_number	t5061_control_number_inv_log.c5060_control_number%TYPE,
    	p_warehouse_type  t5060_control_number_inv.c901_warehouse_type%TYPE   	   
	)
	AS		
		v_user_id	   NUMBER; 
		v_count 	   NUMBER;
		v_company_id   t2550_part_control_number.c1900_company_id%TYPE; 
	    v_plant_id	   t2550_part_control_number.c5040_plant_id%TYPE;	  
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;	
		
		SELECT c1900_company_id, c5040_plant_id 
		 INTO v_company_id,v_plant_id
		 FROM t2550_part_control_number
		 WHERE c2550_control_number= p_control_number
		 AND c205_part_number_id   = p_part_number;
		
		SELECT COUNT(1) INTO v_count
		 FROM t5060_control_number_inv t5060
		 WHERE t5060.c5060_control_number = p_control_number
		 AND t5060.c5060_qty                    > 0
		 AND t5060.c205_part_number_id = p_part_number
		 AND t5060.c901_warehouse_type    = p_warehouse_type
		 AND t5060.c1900_company_id = v_company_id 
		 AND t5060.c5040_plant_id = v_plant_id; 
		
		IF(v_count = 1) THEN		
		
			UPDATE t5060_control_number_inv t5060 
			   SET t5060.c5060_qty = 0, 
			   	 t5060.c5060_control_number = 'NOC#',
			     t5060.c5060_last_update_trans_id = 'INV_ADJ', 
			     t5060.c901_last_transaction_type = '4316', --manual adjustment
			     t5060.c5060_last_updated_by = v_user_id, 
			     t5060.c5060_last_updated_date = SYSDATE, 
			     t5060.c1900_company_id = v_company_id , 
			     t5060.c5040_plant_id = v_plant_id 
			   WHERE t5060.c5060_control_number = p_control_number 
			   AND c5060_qty > 0 
			   AND t5060.c205_part_number_id = p_part_number
			   AND t5060.c901_warehouse_type = p_warehouse_type
			   AND t5060.c1900_company_id = v_company_id 
		 	   AND t5060.c5040_plant_id = v_plant_id;
			   
			UPDATE t5061_control_number_inv_log
			    SET c5060_control_number  = 'NOC#'
			    WHERE c5060_control_number = p_control_number
			    AND c205_part_number_id = p_part_number
			    AND c1900_company_id = v_company_id 
			 	AND c5040_plant_id = v_plant_id;
		--logs	   
			it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, p_control_number||' Warehouse -'||p_warehouse_type||' qty-0  updated '); 
		 ELSE
		 --logs
		 	it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, p_control_number||' Warehouse -'||p_warehouse_type||' not found ');  		 
		 END IF;
	    
		
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END GM_SAV_LOT_QTY;
	
	/*
    * Updating missing donor Id in BBA inventory.
    */
	PROCEDURE gm_upd_donor_id (
	    p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE,
	    p_donor_id     T2550_PART_CONTROL_NUMBER.C2540_DONOR_ID%TYPE,
    	p_control_number  T2550_PART_CONTROL_NUMBER.C2550_CONTROL_NUMBER%TYPE,
    	p_user_id  T2550_PART_CONTROL_NUMBER.C2550_LAST_UPDATED_BY%TYPE   	   
	)
	AS
	  v_count 	   NUMBER;
	BEGIN
	  -- Mandatory help desk to log executed user information
     it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
    
        SELECT COUNT(1) INTO v_count FROM T2550_PART_CONTROL_NUMBER 
        WHERE C2550_CONTROL_NUMBER = p_control_number;
        
        IF(v_count = 1) THEN
		    UPDATE T2550_PART_CONTROL_NUMBER
	   		   SET C2540_DONOR_ID       = p_donor_id ,
 			   C2550_LAST_UPDATED_BY    = p_user_id ,
  			   C2550_LAST_UPDATED_DATE  = SYSDATE
			WHERE C2550_CONTROL_NUMBER = p_control_number;
			 -- help desk log	   
			it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'BBA control number ' || p_control_number||' donor Id'|| p_donor_id||'updated '); 
	     ELSE
	        -- help desk log
	        it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'BBA control number ' || p_control_number||' not found');
	     END IF; 
	EXCEPTION
	  WHEN OTHERS
	  THEN
		DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
	 	ROLLBACK;
	END gm_upd_donor_id;
  
 /******************************************************************
	* Description : Procedure is used to update Allograft status to sold.
  * Ref : TSK-10946
	****************************************************************/
	PROCEDURE gm_upd_Status_Sold(
	    p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_cntrl_no     T2550_PART_CONTROL_NUMBER.C2550_CONTROL_NUMBER%TYPE
	  , p_user_id		   T2550_PART_CONTROL_NUMBER.c2550_last_updated_by%TYPE
	)
	AS		
		v_user_id	   NUMBER; 
    v_count      NUMBER;
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;	

    SELECT count(1) INTO v_count FROM t2550_part_control_number WHERE C2550_CONTROL_NUMBER = p_cntrl_no; 
		
    IF (v_count > 0)
		THEN
     
     UPDATE T2550_PART_CONTROL_NUMBER 
      SET C2550_LOT_STATUS = 105006 
      ,C2550_LAST_UPDATED_BY = v_user_id 
      ,C2550_LAST_UPDATED_DATE = SYSDATE 
      WHERE C2550_CONTROL_NUMBER = p_cntrl_no;  
     -- log
  	it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Control No.: ' || p_cntrl_no);
   END IF;
 
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_upd_Status_Sold;
  
END it_pkg_bba_update;