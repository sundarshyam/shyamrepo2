/* Formatted on 2011/01/31 11:30 (Formatter Plus v4.8.0) */
-- @"C:\Data Correction\Script\it_pkg_sm_sales.bdy";

CREATE OR REPLACE
PACKAGE BODY it_pkg_sm_sales
IS

PROCEDURE gm_sav_sheet_region_map (
        p_help_desk_id t914_helpdesk_log.c914_helpdesk_id%TYPE ,
        p_reg_id IN t901_code_lookup.c901_code_id%TYPE)
AS
    CURSOR c_sheetids
    IS
         
         SELECT
            t4020.c4020_demand_master_id ID , t4020.c4020_demand_nm NAME
           FROM
            t4020_demand_master t4020
          WHERE
            t4020.c901_demand_type IN (40020 , 40021)
            -- sales and consignment sheets
            AND t4020.c4020_void_fl IS NULL
            AND t4020.c4020_inactive_fl IS NULL
            AND UPPER (t4020.c4020_demand_nm) NOT LIKE '%EUROPE%'
            AND UPPER (t4020.c4020_demand_nm) NOT LIKE '%INDIA%'
            AND UPPER (t4020.c4020_demand_nm) NOT LIKE '%UK%'
            AND UPPER (t4020.c4020_demand_nm) NOT LIKE '%GERMAN%'
            AND UPPER (t4020.c4020_demand_nm) NOT LIKE '%BELGIUM%'
            AND UPPER (t4020.c4020_demand_nm) NOT LIKE '%MALAYSIA%'
            AND UPPER (t4020.c4020_demand_nm) NOT LIKE '%GREECE%'
            AND UPPER (t4020.c4020_demand_nm) NOT LIKE '%AFRICA%';
    
    CURSOR c_regions
    IS
         
         SELECT
            t901.c901_code_id ID
           FROM
            t901_code_lookup t901
          WHERE
            t901.c901_code_id = p_reg_id;
    
    v_cnt     NUMBER;
    v_sht_cnt NUMBER;
    v_user_id NUMBER;
BEGIN
    -- Mandatory helpdesk to log executed user information
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id , NULL) ;
    v_user_id := it_pkg_cm_user.get_valid_user;
    v_sht_cnt := 0;
    
    FOR sheet IN c_sheetids
    LOOP
        
        FOR region IN c_regions
        LOOP
             
             SELECT
                COUNT (1)
               INTO
                v_cnt
               FROM
                t4021_demand_mapping t4021 , t4020_demand_master t4020
              WHERE
                t4021.c4021_ref_id = TO_CHAR (region.ID)
                AND t4021.c901_ref_type = 40032
                AND t4021.c4020_demand_master_id = sheet.ID
                AND t4020.c4020_demand_master_id = t4021.c4020_demand_master_id
                AND t4020.c4020_void_fl IS NULL
                AND t4020.c4020_inactive_fl IS NULL
                AND t4020.c901_demand_type IN (40020 , 40021) ;
            
            IF v_cnt = 0 THEN
                 
                 INSERT
                   INTO
                    t4021_demand_mapping
                    (
                        c4021_demand_mapping_id , c4020_demand_master_id
                      , c901_ref_type , c4021_ref_id
                      , c901_action_type , c4021_created_by
                      , c4021_created_date
                    )
                    VALUES
                    (
                        s4021_demand_mapping.NEXTVAL , sheet.ID
                      , 40032 , TO_CHAR (region.ID)
                      , NULL , v_user_id
                      , SYSDATE
                    ) ;
                
                v_sht_cnt := v_sht_cnt + 1;
            
            END IF;
        
        END LOOP;
    
    END LOOP;
    it_pkg_cm_user.gm_cm_sav_helpdesk_log ( p_help_desk_id , 'Sheet mapping for  region: ' || get_code_name (p_reg_id))
    ;
    DBMS_OUTPUT.put_line ( 'The mapping of  '||v_sht_cnt || ' sheets is done for the region :' || get_code_name (
    p_reg_id)) ;
    COMMIT;

EXCEPTION

WHEN OTHERS THEN
    DBMS_OUTPUT.put_line ( 'Error Occurred while executing SP: ' || SQLERRM) ;
    ROLLBACK;

END gm_sav_sheet_region_map;
/*********************************************************
* Description : This procedure is used to map and void the accounts for specific rep.
*********************************************************/

PROCEDURE gm_sav_acct_rep_map
    (
        p_help_desk_id IN t914_helpdesk_log.c914_helpdesk_id%TYPE ,
        p_rep_id IN t704a_account_rep_mapping.C703_SALES_REP_ID%TYPE ,
        p_FS_id  IN t701_distributor.C701_DISTRIBUTOR_ID%TYPE ,
        p_acct_ids IN VARCHAR2 ,
        p_rem_acctids IN VARCHAR2
    )
AS
    CURSOR c_accts
    IS
         
         SELECT
            v700.ac_id
           FROM
            v700_territory_mapping_detail v700
          WHERE
            v700.d_id = p_FS_id
	    and v700.ac_id IS NOT NULL;
    
    CURSOR c_tokens
    IS
         
         SELECT token FROM v_in_list WHERE token IS NOT NULL;
    
    v_cnt     NUMBER;
    v_user_id NUMBER;
BEGIN
    v_cnt := 0;
    IF p_rep_id IS NULL THEN
	raise_application_error ('-20999' , 'Please enter valid rep id.') ;
    END IF;
    -- Mandatory helpdesk to log executed user information
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id , NULL) ;
    v_user_id := it_pkg_cm_user.get_valid_user;
    -- IF fields sales is passed then get all his account and update for the new rep.
    
    IF p_FS_id IS NOT NULL AND p_acct_ids IS NULL AND p_rem_acctids IS NULL THEN
        
        FOR rec IN c_accts
        LOOP
             
             INSERT
               INTO
                t704a_account_rep_mapping
                (
                    c704a_account_rep_map_id , c704_account_id
                  , c703_sales_rep_id , c704a_active_fl
                  , c704a_created_by , c704a_created_date
                )
                VALUES
                (
                    s704a_account_rep_mapping.NEXTVAL , rec.ac_id
                  , p_rep_id , 'Y'
                  , v_user_id , SYSDATE
                ) ;
            
            v_cnt := 1;
        
        END LOOP;
    
    END IF;
    -- IF fields sales is not passed and specific accounts are passed then call below if.
    
    IF p_FS_id IS NULL AND p_acct_ids IS NOT NULL THEN
        my_context.set_my_inlist_ctx ( p_acct_ids) ;
        
        FOR rec IN c_tokens
        LOOP
             
             INSERT
               INTO
                t704a_account_rep_mapping
                (
                    c704a_account_rep_map_id , c704_account_id
                  , c703_sales_rep_id , c704a_active_fl
                  , c704a_created_by , c704a_created_date
                )
                VALUES
                (
                    s704a_account_rep_mapping.NEXTVAL , rec.token
                  , p_rep_id , 'Y'
                  , v_user_id , SYSDATE
                ) ;
            
            v_cnt := 1;
        
        END LOOP;
    
    END IF;
    -- IF specific accounts are passed to be removed then call below if.
    
    IF p_FS_id IS NULL AND p_rem_acctids IS NOT NULL THEN
        my_context.set_my_inlist_ctx ( p_rem_acctids) ;
        
        FOR rec IN c_tokens
        LOOP
             
             UPDATE
                t704a_account_rep_mapping
            SET
                C704A_VOID_FL = 'Y' , C704A_LAST_UPDATED_BY = v_user_id
              , C704A_LAST_UPDATED_DATE = sysdate
              WHERE
                c704_account_id = rec.token;
        
        END LOOP;
        v_cnt := 1;
    
    END IF;
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id , 'Account mapping for  rep: ' || p_rep_id) ;
    
    IF v_cnt = 0 THEN
        raise_application_error ('-20999' , 'No mapping change has been done. Please verify again.') ;
    
    END IF;
    DBMS_OUTPUT.put_line ('Account mapping Completed.....') ;
    COMMIT;

EXCEPTION

WHEN OTHERS THEN
    DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM) ;
    ROLLBACK;

END gm_sav_acct_rep_map;

/*********************************************************
* Description : This procedure is used to update sales territory
*********************************************************/

PROCEDURE gm_upd_super_territory (
 p_terr_id IN  t101_user.c101_user_id% TYPE,
 p_in_user_id IN t101_user.c101_user_id% TYPE
 ) 
 AS
	 v_dist VARCHAR(20);
	 v_user_id NUMBER;
BEGIN
	 v_user_id := it_pkg_cm_user.get_valid_user;
	 SELECT c701_distributor_id INTO v_dist FROM t101_user where c101_user_id = p_terr_id;
	   
	 UPDATE t101_user SET c101_access_level_id = 1, c101_last_updated_by = v_user_id, c101_last_updated_date = sysdate
	  WHERE c101_user_id     = p_terr_id;
	  
	 UPDATE t101_user SET c701_Distributor_ID = v_dist, c101_last_updated_by = v_user_id, c101_last_updated_date = sysdate
	  WHERE c101_user_id    = p_in_user_id;
	  
	 commit;

EXCEPTION
	
WHEN OTHERS THEN
	    DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM) ;
	    ROLLBACK;
END gm_upd_super_territory;

 /*********************************************************
* Description : This procedure is used to insert AD(Area Director) 
*********************************************************/

 PROCEDURE gm_upd_new_ad_user
 (
	p_new_ad_user IN t101_user.C101_USER_F_NAME%TYPE,
	p_curr_ad_user_id IN t101_user.c101_user_id%TYPE,
	p_reg_id IN t708_region_asd_mapping.C901_REGION_ID%TYPE
) 
 AS
	 v_user_id NUMBER;
	 v_ad_user NUMBER;
	 v_in_ad_user NUMBER;
	 v_count NUMBER;
  BEGIN
	
	v_user_id := it_pkg_cm_user.get_valid_user;
	  
	select max(c101_user_id)+1 into v_ad_user from t101_user;
	  
	
	SELECT COUNT(t101.c101_user_id) into v_count
	FROM t708_region_asd_mapping t708, t101_user t101
	where t708.c101_user_id = t101.c101_user_id
	AND t101.C101_USER_F_NAME like p_new_ad_user;
	
	IF v_count = 0 THEN
	--- Inactive the Cuurent AD User
	UPDATE t708_region_asd_mapping 
	set c708_delete_fl = 'Y',c708_last_updated_by = v_user_id, c708_last_updated_date = sysdate 
	where c101_user_id = p_curr_ad_user_id;
	
	--2005 S (sales dept)
	-- 311 Active
	-- 300 Employee
	--IF v_in_ad_user IS NOT NULL THEN
	 INSERT
	   INTO t101_user
	    (
	        C101_USER_ID, C101_USER_SH_NAME, C101_USER_F_NAME,
	        C101_USER_L_NAME, C101_EMAIL_ID, C101_DEPT_ID,
	        C101_ACCESS_LEVEL_ID, C101_HIRE_DT, C101_TERMINATION_DATE,
	        C101_WORK_PHONE, C101_CREATED_DATE, C101_CREATED_BY,
	        C101_LAST_UPDATED_DATE, C101_LAST_UPDATED_BY, C101_SSN_NUMBER,
	        C101_OTHER_PHONE, C101_PH_EXTN, C901_DEPT_ID,
	        C701_DISTRIBUTOR_ID, C101_ADDRESS, C901_USER_STATUS,
	        C901_USER_TYPE, C101_PARTY_ID, C101_TITLE,
	        C101_DATE_FORMAT
	    )
	    VALUES
	    (
	        v_ad_user, p_new_ad_user, p_new_ad_user,
	        '-TBH', 'djames@globusmedical.com', NULL,
	        3, Sysdate, NULL,
	        NULL, sysdate, v_user_id,
	        NULL, NULL, NULL,
	        NULL, NULL, 2005,
	        NULL, NULL, 311,
	        300, NULL, NULL,
	        NULL
	    ) ;
	      
	
	--8000 Area Director
	 INSERT
	   INTO t708_region_asd_mapping
	    (
	        C708_REGION_ASD_ID, C101_USER_ID, C901_REGION_ID,
	        C708_START_DT, C708_END_DATE, C708_DELETE_FL,
	        C708_CREATED_BY, C708_CREATED_DATE, C708_LAST_UPDATED_DATE,
	        C708_LAST_UPDATED_BY, C901_USER_ROLE_TYPE, C901_REF_ID
	    )
	    VALUES
	    (
	        s708_region_asd_mapping.NEXTVAL, v_ad_user, p_reg_id,
	        sysdate, NULL, NULL,
	        v_user_id, sysdate, NULL,
	        NULL, 8000, NULL
	    ) ;
	  
	  END IF;
	  
	 IF v_count >0 THEn
	   DBMS_OUTPUT.put_line ('The User : ' || p_new_ad_user || ' is  already exist.' );
	 END IF;
	
	
	EXCEPTION
	
	WHEN OTHERS THEN
	    DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM) ;
	    ROLLBACK;
	    
END gm_upd_new_ad_user;


PROCEDURE gm_upd_new_vp_user (
                p_new_vp_user IN t101_user.C101_USER_F_NAME%TYPE,
                p_curr_vp_user_id IN t101_user.c101_user_id%TYPE
) 
 AS
                v_user_id NUMBER;
                v_vp_user NUMBER;
                v_in_vp_user NUMBER;
                v_count NUMBER;
  CURSOR cur_reg 
                IS
  select c708_region_asd_id reg_asd_id, c901_region_id reg_id from t708_region_asd_mapping WHERE c101_user_id = p_curr_vp_user_id;
  BEGIN
                
                v_user_id := it_pkg_cm_user.get_valid_user;
                  
                select max(c101_user_id)+1 into v_vp_user from t101_user;
                  
                
                SELECT COUNT(t101.c101_user_id) into v_count
                FROM t708_region_asd_mapping t708, t101_user t101
                where t708.c101_user_id = t101.c101_user_id
                AND t101.C101_USER_F_NAME like p_new_vp_user;
  
			IF v_count = 0 THEN
    
                --2005 S (sales dept)
                -- 311 Active
                -- 300 Employee
                --IF v_in_ad_user IS NOT NULL THEN
                INSERT
                   INTO t101_user
                    (
                        C101_USER_ID, C101_USER_SH_NAME, C101_USER_F_NAME,
                        C101_USER_L_NAME, C101_EMAIL_ID, C101_DEPT_ID,
                        C101_ACCESS_LEVEL_ID, C101_HIRE_DT, C101_TERMINATION_DATE,
                        C101_WORK_PHONE, C101_CREATED_DATE, C101_CREATED_BY,
                        C101_LAST_UPDATED_DATE, C101_LAST_UPDATED_BY, C101_SSN_NUMBER,
                        C101_OTHER_PHONE, C101_PH_EXTN, C901_DEPT_ID,
                        C701_DISTRIBUTOR_ID, C101_ADDRESS, C901_USER_STATUS,
                        C901_USER_TYPE, C101_PARTY_ID, C101_TITLE,
                        C101_DATE_FORMAT
                    )
                    VALUES
                    (
                        v_vp_user, p_new_vp_user, p_new_vp_user,
                        '-TBH', 'djames@globusmedical.com', NULL,
                        3, Sysdate, NULL,
                        NULL, sysdate, v_user_id,
                        NULL, NULL, NULL,
                        NULL, NULL, 2005,
                        NULL, NULL, 311,
                        300, NULL, NULL,
                        NULL
                    ) ;
                      

     FOR var_temp IN cur_reg
      LOOP
      BEGIN
  
                --- Inactive the Cuurent VP User
                UPDATE t708_region_asd_mapping 
                set c708_delete_fl = 'Y',c708_last_updated_by = v_user_id, c708_last_updated_date = sysdate 
                where c708_region_asd_id = var_temp.reg_asd_id ;
  
                --VP (8001).
			INSERT
			   INTO t708_region_asd_mapping
			    (
			        C708_REGION_ASD_ID, C101_USER_ID, C901_REGION_ID,
			        C708_START_DT, C708_END_DATE, C708_DELETE_FL,
			        C708_CREATED_BY, C708_CREATED_DATE, C708_LAST_UPDATED_DATE,
			        C708_LAST_UPDATED_BY, C901_USER_ROLE_TYPE, C901_REF_ID
			    )
			    VALUES
			    (
			        s708_region_asd_mapping.NEXTVAL, v_vp_user, var_temp.reg_id,
			        sysdate, NULL, NULL,
			        v_user_id, sysdate, NULL,
			        NULL, 8001, NULL
			    ) ;

                   DBMS_OUTPUT.put_line ('The User : ' || p_new_vp_user || ' is  already exist.' );
                EXCEPTION
                WHEN OTHERS THEN
                    DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM) ;
                    ROLLBACK;
                 END;
                END LOOP; 
  END IF;
                    
END gm_upd_new_vp_user;

/*********************************************************
* Description : This procedure is used to update the product request status
*********************************************************/
PROCEDURE gm_approve_product_request (
        p_help_desk_id t914_helpdesk_log.c914_helpdesk_id%TYPE,
        p_prod_req_id IN T525_PRODUCT_REQUEST.C525_PRODUCT_REQUEST_ID%TYPE)
AS
    v_cnt     NUMBER;
    v_user_id NUMBER;
BEGIN
     SELECT COUNT (1)
       INTO v_cnt
       FROM T525_PRODUCT_REQUEST
      WHERE C525_PRODUCT_REQUEST_ID = p_prod_req_id
        AND C525_VOID_FL           IS NULL;
    --
    IF v_cnt = 0 THEN
        raise_application_error ('-20999', 'Invalid Request ID, Please provide correct Request ID ') ;
    END IF;
    -- Mandatory helpdesk to log executed user information
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL) ;
    v_user_id := it_pkg_cm_user.get_valid_user;
    --
     UPDATE T526_PRODUCT_REQUEST_DETAIL
    SET c526_status_fl              = '5', c526_last_updated_by = v_user_id, c526_last_updated_date = SYSDATE
      WHERE c525_product_request_id = p_prod_req_id
        AND c526_status_fl          = 2
        AND C526_VOID_FL           IS NULL;
    --    
    DBMS_OUTPUT.put_line ('Number of rows to be update: ' || SQL%ROWCOUNT) ;
    --
END gm_approve_product_request;

/*********************************************************
* Description : This procedure is used to update the collector id information
*********************************************************/
PROCEDURE gm_upd_acc_attri_collector_id (
        p_help_desk_id t914_helpdesk_log.c914_helpdesk_id%TYPE,
        p_company_id t1900_company.c1900_company_id%TYPE,
        p_region_id t901_code_lookup.c901_code_id%TYPE,
        p_att_val t704a_account_attribute.c704a_attribute_value%TYPE)
AS
    v_user_id      NUMBER;
    v_comp_reg_cnt NUMBER;
    v_att_val_cnt  NUMBER;
    v_att_type     NUMBER := 103050;
BEGIN
    -- Mandatory helpdesk to log executed user information
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL) ;
    v_user_id := it_pkg_cm_user.get_valid_user;
    -- to validate the company and region id
     SELECT COUNT (1)
       INTO v_comp_reg_cnt
       FROM v700_territory_mapping_detail
      WHERE d_compid  = p_company_id
        AND region_id = p_region_id;
    --
    IF v_comp_reg_cnt = 0 THEN
        raise_application_error ('-20999', 'Invalid Company/Region ID, Please provide correct data ') ;
    END IF;
    -- to validate the attribute value
     SELECT COUNT (1)
       INTO v_att_val_cnt
       FROM t901_code_lookup
      WHERE c901_code_id = p_att_val;
    --
    IF v_att_val_cnt = 0 THEN
        raise_application_error ('-20999', 'Invalid Collector ID, Please provide correct data ') ;
    END IF;
    --
     UPDATE t704a_account_attribute t704a
    SET t704a.c704a_attribute_value   = p_att_val, t704a.c704a_last_updated_by = v_user_id, t704a.c704a_last_updated_date
                                      = CURRENT_DATE
      WHERE t704a.c704a_void_fl      IS NULL
        AND t704a.c901_attribute_type = v_att_type
        AND EXISTS
        (
             SELECT v700.ac_id
               FROM v700_territory_mapping_detail v700
              WHERE v700.d_compid  = p_company_id
                AND v700.region_id = p_region_id
                AND v700.ac_id     = t704a.c704_account_id
        ) ;
    --
    DBMS_OUTPUT.put_line ('Number of rows to be update: ' || SQL%ROWCOUNT) ;
END gm_upd_acc_attri_collector_id;
END it_pkg_sm_sales;
/