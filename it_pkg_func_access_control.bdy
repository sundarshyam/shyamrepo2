/* Formatted on 2011/01/20 15:14 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_func_access_control.bdy"

CREATE OR REPLACE PACKAGE BODY it_pkg_func_access_control
IS
--

	/*********************************************************
	* Description : This procedure is used to updated
	* access right for the specific user to left menu link
	*********************************************************/
	PROCEDURE gm_sav_func_access (
		p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_source_user	 t102_user_login.c102_login_username%TYPE
	  , p_group_id		 t1500_group.c1500_group_id%TYPE
	)
	AS
		v_user_id	   NUMBER;
		v_source_userid NUMBER;
		v_count 	   NUMBER;
	--
	BEGIN
		--check to see if the user name is correct
		BEGIN
			IF (p_source_user IS NOT NULL)
			THEN
				SELECT t102.c101_user_id
				  INTO v_source_userid
				  FROM t102_user_login t102
				 WHERE t102.c102_login_username = p_source_user;
			END IF;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error ('-20085', 'User name is wrong.');
		END;

		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		---Check if the group exist
		SELECT COUNT (1)
		  INTO v_count
		  FROM t1500_group t1500
		 WHERE t1500.c1500_group_id = p_group_id AND t1500.c1500_void_fl IS NULL;

		IF (v_count = 0)
		THEN
			raise_application_error ('-20085', 'Access control group ' || p_group_id || ' does not exist.');
		END IF;

		/*SELECT COUNT (1)
		  INTO v_count
		  FROM t1530_access t1530
		 WHERE t1530.c1500_group_id = p_group_id AND t1530.c1530_void_fl IS NULL;

		IF (v_count = 0)
		THEN
			raise_application_error ('-20085'
								   , 'No access found for the group ' || p_group_id || ' Please contact Tech Team.'
									);
		END IF;
		*/

		SELECT COUNT (1)
		  INTO v_count
		  FROM t1501_group_mapping t1501
		 WHERE t1501.c101_mapped_party_id = v_source_userid AND t1501.c1500_group_id = p_group_id 
		  AND t1501.c1501_void_fl IS NULL;

		IF (v_count > 0)
		THEN
			raise_application_error ('-20085'
								   , 'The user ' || p_group_id || ' already exist in the group ' || p_group_id
									);
		ELSE
			INSERT INTO t1501_group_mapping
						(c1501_group_mapping_id, c101_mapped_party_id, c1500_mapped_group_id, c1500_group_id
					   , c1501_created_by, c1501_created_date
						)
				 VALUES (s1501_group_mapping.NEXTVAL, v_source_userid, NULL, p_group_id
					   , v_user_id, SYSDATE
						);
		END IF;

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'User Name: ' || p_source_user);
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Group ID : ' || p_group_id);
		DBMS_OUTPUT.put_line ('Access right is added to the user ' || p_source_user);
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_func_access;
END it_pkg_func_access_control;
/
