/* Formatted on 2011/01/20 14:53 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_ac_realtime_currency.bdy"

CREATE OR REPLACE PACKAGE BODY it_pkg_ac_realtime_currency
IS
--

	/****************************************************************
	* Description : This procedure is used to Save Realtime Currency values
	****************************************************************/
	PROCEDURE gm_sav_exchange_rate_dtl (
		p_help_desk_id	 IN 	t914_helpdesk_log.c914_helpdesk_id%TYPE,
		p_from_date			IN		DATE,
		p_to_date			IN		DATE,
	   p_base_curr      IN       t907_currency_conv.c907_curr_cd_from%TYPE,
      p_quote_curr     IN       t907_currency_conv.c907_curr_cd_to%TYPE,
      p_curr_value     IN       t907_currency_conv.c907_curr_value%TYPE,
      p_source	       IN       t907_currency_conv.C901_SOURCE%TYPE,
	  p_failure_fl	   IN	    t907_currency_conv.C907_FAILURE_FL%TYPE,
	  p_conv_type	   IN 		t907_currency_conv.c901_conversion_type%TYPE,
	  p_user_id        IN       t907_currency_conv.c907_created_by%TYPE	
    )
	AS
	v_count NUMBER :=0;
	v_curr_value    t907_currency_conv.c907_curr_value%TYPE;
	v_user_id	   NUMBER;
	
	BEGIN
	
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;
		
		v_curr_value := p_curr_value;
		
		BEGIN
			SELECT count(1)
					 INTO v_count
					 FROM t907_currency_conv 
					WHERE c907_void_fl IS NULL
					  AND C901_SOURCE= p_source
					  AND (c907_from_date >= TRUNC(p_from_date) AND c907_to_date <= TRUNC(p_to_date))                  
					  AND c907_curr_cd_from = p_base_curr
					  AND c907_curr_cd_to = p_quote_curr
					  AND c901_conversion_type = p_conv_type;
		 EXCEPTION
			 WHEN NO_DATA_FOUND THEN
				v_count := 0;
			 WHEN OTHERS THEN 
			 	v_count := 0;
		END;
		
		DBMS_OUTPUT.PUT_LINE(' # of Records '||v_count);
		IF v_count >1
		THEN
			raise_application_error ('-20085', 'No record is found to be updated. Error Count: '||v_count);		
		END IF;
		
		IF (v_count = 0)
		THEN
			   INSERT INTO t907_currency_conv
	                     (c907_curr_seq_id, c907_curr_cd_from, c907_curr_cd_to,
	                      c907_from_date, c907_to_date, c907_curr_value, c901_conversion_type,
	                      c901_source,c907_failure_fl,c907_created_by, c907_created_date
	                     )
	              VALUES (s907_curr_conv.NEXTVAL, p_base_curr, p_quote_curr,
	                      TRUNC(p_from_date), TRUNC(p_to_date), v_curr_value, p_conv_type,
	                      p_source,p_failure_fl,p_user_id, CURRENT_DATE 
	                     );
		ELSE
			update T907_CURRENCY_CONV 
				set C907_FAILURE_FL=p_failure_fl,
					C907_LAST_UPDATED_BY=p_user_id,
					C907_LAST_UPDATED_DATE=CURRENT_DATE,
					C907_CURR_VALUE=v_curr_value
			where 
				C901_SOURCE= p_source and 
				C901_CONVERSION_TYPE=p_conv_type and 
				C907_FROM_DATE=p_from_date and 
				C907_TO_DATE=p_to_date and 
				c907_curr_cd_from=p_base_curr and
				c907_void_fl IS NULL
				;
		
		/*
		ELSE
		
			ROLLBACK;
			raise_application_error ('-20085', 'No record is found to be updated. Error Count: '||v_count);		
			*/
		END IF;
		
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Base Currency:'|| GET_CODE_NAME_ALT(p_base_curr)||':p_base_curr : ' || p_base_curr||' :p_from_date:'||p_from_date||' :p_to_date:'||p_to_date||':p_curr_value:'||p_curr_value ||' : Record Count :'||v_count||' p_conv_type :'||p_conv_type);
		
		
		COMMIT;
		DBMS_OUTPUT.PUT_LINE(' CURRENCY SET ');	

	END gm_sav_exchange_rate_dtl;
	
	END it_pkg_ac_realtime_currency;
/
