CREATE OR REPLACE
PACKAGE BODY it_tmp_gm_Inventory_load

IS
  
 /*
* Purpose: Main procedure to be invoked for the Loaner Set building data load migration
* Steps:
*  1. Set Config Qty validation
*  2. IABL initiation
*  3. Set Initiation and Set building
*  4. Loaner Initiate
*  Atec 403 - Set List Data Load Migration. July 2017
*  Author: gpalani. Reviewed by Richard K
*/

PROCEDURE tmp_gm_main_jpn_set_field
AS
   -- p_flag t5054_inv_location_log.C5054_LOT_STATUS_FL%TYPE := 'Y';
 
      v_count NUMBER :='0';
	  p_flag varchar2(10);

    CURSOR cur_set
    IS

        SELECT DISTINCT (C207_SET_ID) sets_id, C5010_tag_id tag_id,    ALLADIN_ACCOUNT_ID ACCT_ID
        FROM globus_app.JP_LOANER_INV_STG JPN
        WHERE 
        JPN.set_loaded_fl IS NULL
        and jpn.c207_set_id IS NOT NULL
        and jpn.alladin_account_id is not null
        order by C207_SET_ID, C5010_tag_id  ;
      
        

       
     BEGIN   
    FOR set_list IN cur_set
	
    LOOP
	    BEGIN 
			tmp_gm_set_validation(set_list.sets_id, set_list.tag_id,set_list.ACCT_ID,p_flag);
		
		IF (p_flag='Y') THEN
				
	            tmp_gm_iabl (set_list.sets_id, set_list.tag_id) ;
	            tmp_gm_set_build (set_list.sets_id, set_list.tag_id) ;
	            
	            dbms_output.put_line ('Set Build Processing competed for the Tag:'||set_list.tag_id) ;
	            
	         
	            IF (set_list.ACCT_ID IS NOT NULL) THEN
	                tmp_gm_Loaner_Initiate (set_list.sets_id, set_list.ACCT_ID, set_list.tag_id) ;
	                dbms_output.put_line ('Loaner Successfully Initiated:'||set_list.sets_id||'And Tag ID'||set_list.tag_id);
	                   
					   UPDATE JP_LOANER_INV_STG
						SET set_loaded_fl    = 'LOANER INITIATED'
						WHERE c207_SET_ID  = set_list.sets_id
						AND c5010_tag_id = set_list.tag_id;           
	            END IF;
	            
         END IF;
    
    EXCEPTION
    
    WHEN OTHERS THEN
        dbms_output.put_line ('Set Build Processing failed for the set:'||set_list.sets_id||'And Tag ID:'||
        set_list.tag_id||SQLERRM);
        
            UPDATE JP_LOANER_INV_STG
            SET set_loaded_fl    = 'Set Build Processing failed'
              WHERE c207_SET_ID  = set_list.sets_id
                AND c5010_tag_id = set_list.tag_id;
            
        ROLLBACK;
    END;
     
     v_count    := v_count + 1;
        IF (v_count = 100) THEN
            COMMIT;
             v_count:='0';
        END IF;
		
 END LOOP;
END tmp_gm_main_jpn_set_field;

  
  
  
  /*
* Purpose: Main procedure to be invoked for the Loaner Set which are in warehouse.
* Purpose: Main procedure to be invoked for the Loaner Set which are in warehouse.
* Steps:
*  1. Set Config Qty validation
*  2. IABL initiation
*  3. Set Initiation and Set building
*  4. Loaner Initiate
*  Atec 403 - Set List Data Load Migration. July 2017
*  Author: gpalani. Reviewed by Richard K
*/

PROCEDURE tmp_gm_main_jpn_set_build

AS


      v_count NUMBER :='0';
   
    CURSOR cur_set
    IS

     SELECT DISTINCT (C207_SET_ID) sets_id, C5010_tag_id tag_id,    ALLADIN_ACCOUNT_ID ACCT_ID
        FROM globus_app.JP_LOANER_INV_STG JPN
        WHERE
        jpn.set_loaded_fl is null
        order by C207_SET_ID, C5010_tag_id;
             
       BEGIN 
      
        
    FOR set_list IN cur_set
    
    LOOP
       Begin
	       
	    	tmp_gm_set_validation(set_list.sets_id, set_list.tag_id,set_list.ACCT_ID,p_flag);

	    IF (p_flag='Y') THEN
	       
            tmp_gm_iabl (set_list.sets_id, set_list.tag_id) ;
            tmp_gm_set_build (set_list.sets_id, set_list.tag_id) ;
            
            dbms_output.put_line ('Set Build Processing competed for the Tag:'||set_list.tag_id) ;
                
            UPDATE JP_LOANER_INV_STG
            SET set_loaded_fl    = 'Set Building Process completed.'
              WHERE c207_SET_ID  = set_list.sets_id
                AND c5010_tag_id = set_list.tag_id;
       	 END IF;         
                
	    EXCEPTION
	    
	    WHEN OTHERS THEN
	        dbms_output.put_line ('Set Build Processing failed for the set:'||set_list.sets_id||'And Tag ID:'||
	        set_list.tag_id||SQLERRM);
	        
	         UPDATE JP_LOANER_INV_STG
            SET set_loaded_fl    = 'Set Build Processing failed'
              WHERE c207_SET_ID  = set_list.sets_id
                AND c5010_tag_id = set_list.tag_id;
	        
	    END;
	     
        v_count    := v_count + 1;
       
        IF (v_count = 100) THEN
            COMMIT;           
            v_count:='0';
        END IF;
        
  END LOOP;
END tmp_gm_main_jpn_set_build;
  
 


/************************************************
*-- The purpose of this procedure is to validate the following pre-requiste to avoid Set building failure
*-- Account Availablity check
*-- Set Type check
*-- Tag Status check      
*-- Tag Attribute check 
*-- Author: Gomathi Palani. Verified by Richard K.
*************************************************/

	PROCEDURE tmp_gm_set_validation
  	   (
        p_set_id  IN t208_set_details.C207_SET_ID%TYPE,
        p_tag_id  IN t5010_tag.C5010_TAG_ID%TYPE,
        p_acct_id IN t704_account.c704_account_id%TYPE,
        p_flag OUT VARCHAR
		)
    AS
        v_part_num T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
        v_source_QTY NUMBER;
        v_set_id T207_SET_MASTER.C207_SET_ID%TYPE;
        V_SET_QTY NUMBER;
        V_SET_PART  T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
        v_set_part_num T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
        v_temp_acc t704_account.c704_account_id%TYPE;
        v_err_dtls varchar2(10000);
        v_set_type T207_SET_MASTER.C207_TYPE%TYPE;
        v_flag VARCHAR2(100);
        v_tag_part_inp_str clob :=null;
        v_count number:='0';
        v_company_id t1900_company.c1900_company_id%TYPE;
        v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
        
        CURSOR cur_source_set
        IS
          
        SELECT JPN.C205_PART_NUMBER_ID PART_NO, JPN.LOT_NUMBER CONTROL_NUMBER, JPN.PHYSICAL_SET_QTY QTY,
             TRIM(get_part_attribute_value(JPN.C205_PART_NUMBER_ID, 92340)) TAG_FLAG
               FROM JP_LOANER_INV_STG JPN
               WHERE JPN.C207_SET_ID=p_set_id
 			   AND JPN.C5010_TAG_ID=p_tag_id
 			   AND DECODE(JPN.alladin_account_id, NULL, -999,JPN.alladin_account_id) = DECODE(p_acct_id, NULL, -999, p_acct_id)
               GROUP BY PART_NO, CONTROL_NUMBER;  
           
    BEGIN
	    
        p_flag :='Y';
        
        
  		  SELECT get_plantid_frm_cntx  INTO v_plant_id FROM dual;
  		  SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
                 
      --- Account Availablity check      
        IF (p_acct_id is not null) THEN

         BEGIN
	    
	          SELECT c704_account_id INTO NVL(v_temp_acc,NULL)	
	          FROM t704_account 
	          WHERE c704_ext_ref_id=p_acct_id
	          and c704_void_fl is null
	          and c1900_company_id=v_company_id;
	          
	       IF (v_temp_acc is null) THEN
	          	v_err_dtls :='SpineIT Account ID was not created for the Alladin Acct ID: '||p_acct_id;
	          
	         	 INSERT INTO LOANER_LOANER_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ALLADIN_ACCT_ID,ERR_DTLS)
	         	 VALUES(p_set_id,p_tag_id,p_acct_id,v_err_dtls);
	          	 p_flag :='N';
	          
	       END IF;
	      
	     END;
	 
	   -- Set type check      
   
	     BEGIN
	         
	   		  SELECT C207_TYPE INTO NVL(v_set_type,NULL) FROM
	   		  T207_SET_MASTER
	   		  WHERE C207_SET_ID=p_set_id
	   		  and c207_void_FL IS NULL;
	          
	          IF (v_set_type NOT IN ('4074')) THEN
	          
		          v_err_dtls :='Set Type is not a Loaner Set. Please change the set type to 4074 (Loaner Set) for the Set ID : '||p_set_id;
		          
		          INSERT INTO LOANER_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
		          p_flag :='N';
		          
	          END IF;
	      END;    
	          
	  --- Tag Status check      
     
	      BEGIN
	         
	   		  SELECT c901_status INTO NVL(v_tag_status,NULL)
	   		  FROM T5010_TAG 
	   		  WHERE C5010_TAG_ID=p_tag_id
	   		  and c5010_void_flag is null;
	          
	          IF (v_tag_status NOT IN ('51011')) THEN
	          
	          v_err_dtls :='Tag Status is not in released status for the Tag id : '||p_tag_id;
	          
	          INSERT INTO LOANER_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
	           p_flag :='N';
	           
	           END IF;
	      END; 
	      
	   BEGIN   	         
        FOR cur_set IN cur_source_set
        
        LOOP
        
            V_PART_NUM    := cur_set.PART_NO;
            V_Source_QTY  := cur_set.QTY;
            v_flag		  := cur_set.TAG_FLAG;
	          
             IF v_tag_flag         	  = 'Y' THEN
	             v_count :=v_count+1;
	             v_tag_part_inp_str :=v_tag_part_inp_str||V_PART_NUM||',';
	             
             END IF;
             
         END LOOP;
       END;
         
         IF (v_count>0) THEN
         
         	  v_err_dtls :='More than 1 tagabble part associated with the Set in the Tag Attribute for the Setid:  '||p_set_id||'Taggable parts are'||v_tag_part_inp_str;
              INSERT INTO LOANER_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
              p_flag :='N';
	           
         END IF;
	      
         IF (p_flag ='N') THEN 
         
            	UPDATE JP_LOANER_INV_STG
				SET set_loaded_fl    = 'LOANER Skipped Check LOANER_STATUS_LOG'
				WHERE c207_SET_ID  = set_list.sets_id
				AND c5010_tag_id = set_list.tag_id; 
	      
		 END IF;
   
    END tmp_gm_set_validation;
 
  
 
/*
 * Purpose: Procedure to invoke the IABL for the Loaner set list data load migraion
 * Atec 403 Data load Migration - API Japan
 * July 2017. Author gpalani, Reviewed by Richard K
 */
PROCEDURE tmp_gm_iabl
		(
        p_set_id IN t208_set_details.C207_SET_ID%TYPE,
        p_tag_id IN t5010_tag.C5010_TAG_ID%TYPE
		)

AS
  v_user t101_user.c101_user_id%TYPE;
  v_initiate_input_str CLOB;
  v_control_input_str CLOB;
  v_verify_input_str CLOB;
  v_part_no t205_part_number.c205_part_number_id%TYPE;
  v_txn_id t504_consignment.c504_consignment_id%TYPE;
  v_cost t820_costing.c820_purchase_amt%TYPE;
  v_purchase_amt t820_costing.c820_purchase_amt%TYPE;
  v_company_cost  t820_costing.C820_LOCAL_COMPANY_COST%TYPE;
  v_message varchar2(20);
  v_reason NUMBER;
  v_txntype NUMBER;
  v_wh_type VARCHAR2(100);
  v_cur_wh  VARCHAR2(50); 
  v_txn_type_name varchar2(50);
  v_source_wh NUMBER;
  v_target_wh NUMBER;
  v_out VARCHAR2(100);
  v_costing_id NUMBER;
  v_company_id NUMBER;
  v_plant_id NUMBER;
  v_portal_com_date_fmt VARCHAR2 (20) ;
  v_bl_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
  V_TRIP_PRICE NUMBER;
  v_qty NUMBER;
  v_control_num VARCHAR2(40);
  v_costing_type NUMBER;
  v_count number :='0';
 
CURSOR cur_set IS

	  SELECT JPN.c205_part_number_id PART_NO, JPN.LOT_NUMBER CONTROL_NUMBER,
	  JPN.physical_set_qty QTY
	  FROM JP_LOANER_INV_STG JPN
	  WHERE JPN.c207_SET_ID=p_set_id
	  AND JPN.c5010_tag_id=p_tag_id;
 
BEGIN

-- 105131 -- dd/mm/yyyy
  SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
  SELECT get_plantid_frm_cntx  INTO v_plant_id FROM dual;
  SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
  -- Below line of code will get the user who is executing the script. This will avoid the need to pass the user id as a parameter
  v_user := IT_PKG_CM_USER.GET_VALID_USER;
  

  -- Create Location for BL
  
  BEGIN
	  
    SELECT c5052_location_id INTO v_bl_location_id
    FROM t5052_location_master WHERE c5052_location_cd='JPN_BL_LOCATION'and c1900_company_id= v_company_id and c5052_void_fl is null;
    
    EXCEPTION WHEN OTHERS THEN
    dbms_output.put_line('*********Location error' || ' Error:' || sqlerrm);
    
  END;
  
FOR  inv_set IN cur_set

  LOOP
  
   BEGIN
	   
      v_part_no :=inv_set.part_no;
      v_qty := inv_set.qty;
      v_control_num :=inv_set.control_number;
      v_txn_type_name:='INVAD-BULK';
      v_txntype:= '400080';
      v_target_wh:='90814';
      v_source_wh:='90814';
      v_cur_WH := 'BL';
      v_wh_type:='90814';
      v_costing_type  :='4900';
      v_reason :='90814';
    
 BEGIN
	    
     select COST, company_cost INTO v_purchase_amt, v_company_cost 
     from jpn_dm_inventory 
     where part=v_part_no 
     and warehouse='FG';     
       

     Exception
    
       WHEN NO_DATA_FOUND THEN
       v_purchase_amt:='0';
       v_company_cost :='0';
 --   dbms_output.put_line('v_purchase_amt=' || v_purchase_amt);

 END;
   	   
	   SELECT NVL(TRIM(GET_PART_PRICE(v_part_no,'E')),0) INTO V_TRIP_PRICE
	    FROM DUAL;
		 
   -- Get Inventory cost or insert if not available
      SELECT get_inventory_cogs_value(v_part_no)  INTO v_cost FROM dual;
      
     IF v_cost IS NULL THEN
        
       INSERT INTO t820_costing
	   ( c820_costing_id,c205_part_number_id,c901_costing_type,c820_part_received_dt,c820_qty_received,c820_purchase_amt,c820_qty_on_hand 
    	,c901_status,c820_delete_fl,c820_created_by,c820_created_date,c1900_company_id,c5040_plant_id,C820_LOCAL_COMPANY_COST
		,C1900_OWNER_COMPANY_ID,c901_owner_currency,C901_LOCAL_COMPANY_CURRENCY       
		) 
	   VALUES ( s820_costing.nextval,v_part_no,v_costing_type,CURRENT_DATE,0,v_purchase_amt,0,'4802','N','2277820',CURRENT_DATE,v_company_id        
			    ,v_plant_id,v_company_cost,'1018','1','4000917');
          
        -- Build input string 
        END IF;
        
        v_initiate_input_str := v_initiate_input_str || v_part_no || ',' || v_qty || ',,,' || V_TRIP_PRICE || '|';         
        v_control_input_str := v_control_input_str || v_part_no || ',' || v_qty || ',' || v_control_num || ',,' || V_TRIP_PRICE || '|';
        v_verify_input_str := NULL;
       
       
      IF v_txn_id IS NULL THEN
      
         SELECT get_next_consign_id(v_txn_type_name) 
         INTO v_txn_id
         FROM dual;
         
      END IF;

   -- Commented the below code since there is no need to update the bulk location qty during IABL
   -- 	gm_pkg_op_inv_field_sales_tran.gm_sav_fs_inv_loc_part_details (v_bl_location_id,v_txn_id, v_part_no,v_qty,'400080','4301',null,current_date, v_user);    
     END;
     
	v_count :=v_count+1;

		if(v_count>20) then 
			
				
		  -- IABL initiate 
		          gm_save_inhouse_item_consign(v_txn_id,v_txntype , v_reason ,0,'','IABL created for the Set ID:'||p_set_id||':Tag ID:'||p_tag_id,v_user,v_initiate_input_str,v_out);
		    
		  -- IABL Control 
		          gm_ls_upd_inloanercsg(v_txn_id,v_txntype,'0','0',NULL,'Japan Set Data Load',v_user,v_control_input_str,null,'ReleaseControl',v_message);
		        
		  -- IABL Verify
		  
		          gm_ls_upd_inloanercsg(v_txn_id,v_txntype,'0','0',NULL,'Japan Set Data load',v_user,v_verify_input_str,null,'Verify',v_message);
		          dbms_output.put_line('IABL Txn=' || v_txn_id || ' Verified');
        
		   	     
		          UPDATE t412_inhouse_transactions SET c412_comments='IABL Inventory Update for JPN Set Building' , c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
			 	  where c412_inhouse_trans_id=v_txn_id;
		        
		        
			      v_txn_id := NULL;
			      v_count :=0;
			      v_initiate_input_str :=null;
			      v_control_input_str :=null;
		END IF;

  END LOOP;
  

          -- Outer Loop
          
   IF (v_initiate_input_str is not null) THEN

	  
		   -- IABL initiate 
		          gm_save_inhouse_item_consign(v_txn_id,v_txntype , v_reason ,0,'','IABL created for the Set ID:'||p_set_id||':Tag ID:'||p_tag_id,v_user,v_initiate_input_str,v_out);
		      
		   -- IABL Control 
		          GM_LS_UPD_INLOANERCSG(v_txn_id,v_txntype,'0','0',NULL,'Japan Set Data Load',v_user,v_control_input_str,null,'ReleaseControl',v_message);
		         
		   -- IABL Verify
		  
		    	  gm_ls_upd_inloanercsg(v_txn_id,v_txntype,'0','0',NULL,'Japan Set Data load',v_user,v_verify_input_str,null,'Verify',v_message);
		          dbms_output.put_line('IABL Txn=' || v_txn_id || ' Verified');
		        
	        
		          UPDATE t412_inhouse_transactions SET c412_comments='IABL Inventory Update for JPN Set Building' , c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
				  where c412_inhouse_trans_id=v_txn_id;
				  v_txn_id := NULL;
			      v_count :=0;
			      v_initiate_input_str :=null;
				  v_control_input_str :=null;
			    
  END IF;

  END tmp_gm_iabl;    

  

 
 /*
 ************************************************
 * The purpose of this procedure is to Initiate a Loaner Set build for the given Set ID
 * Ticket: Atec 403 : Japan Set List Data migration. July 2017 
 * Author: Gomathi Palani. Verified by Richard K.
************************************************* */

	PROCEDURE tmp_gm_set_build
		(
        p_set_id IN t208_set_details.C207_SET_ID%TYPE,
        p_tag_id IN t5010_tag.C5010_TAG_ID%TYPE
		)
AS
	  v_user t101_user.c101_user_id%TYPE;
	  v_initiate_input_str_set_build CLOB;
	  v_part_no t205_part_number.c205_part_number_id%TYPE;
	  v_tag_flag VARCHAR2 (2) ;
	  v_tag_part_number t205_part_number.c205_part_number_id%TYPE;
	  v_tag_control_number VARCHAR2(40);
	  v_input_str_tag_creation CLOB;
	  v_company_id t1900_company.C1900_COMPANY_ID%TYPE;
	  v_plant_id t5040_plant_master.C5040_PLANT_ID%TYPE;
	  v_portal_com_date_fmt VARCHAR2 (100) ;
	  v_qty NUMBER;
	  v_control_num VARCHAR2(40);
	  v_out_request_id VARCHAR2 (20) ;
	  v_out_consign_id VARCHAR2 (20) ;
	  v_errmsg VARCHAR2 (200) ;
	  v_tagable_flag VARCHAR(2);
	  v_input_str_putaway CLOB;
	  v_msg varchar2(10);


CURSOR cur_set IS

	  SELECT JPN.C205_PART_NUMBER_ID PART_NO, JPN.LOT_NUMBER CONTROL_NUMBER,
	  JPN.PHYSICAL_SET_QTY QTY, TRIM(get_part_attribute_value(JPN.C205_PART_NUMBER_ID, 92340)) TAG_FLAG
	  FROM JP_LOANER_INV_STG JPN
	  WHERE JPN.C207_SET_ID=p_set_id
	  AND JPN.C5010_TAG_ID=p_tag_id
	  and jpn.set_loaded_fl is null;
	 
	 
	BEGIN

	  SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
	  v_user := IT_PKG_CM_USER.GET_VALID_USER;  
	 
	FOR  inv_set IN cur_set
	 
	 LOOP
	  
	   BEGIN
		   
		   v_qty         :=inv_set.QTY;
		   v_part_no     :=inv_set.PART_NO;
		   v_control_num :=inv_set.CONTROL_NUMBER;   
					 
		 -- Build Input String for Set Build
		   v_initiate_input_str_set_build := v_initiate_input_str_set_build||v_part_no||'^'||v_qty||'^'||v_control_num||'^|';  
	   
		  -- To fetch the tagable part and control number for the set
		  -- v_tag_flag             := get_part_attribute_value (v_part_no, 92340);
		  
		  v_tag_flag    := inv_set.TAG_FLAG;
			
		IF v_tag_flag         	  = 'Y' THEN
		
			v_tagable_flag 		 :='Y'; 
			v_tag_part_number    := v_part_no;
			v_tag_control_number := v_control_num;
				
		END IF;
		  
	  END;
		
	END LOOP;

			gm_pkg_op_process_request.gm_sav_initiate_set(NULL,to_char(CURRENT_DATE, v_portal_com_date_fmt),'50618',NULL,p_set_id,'4127',
			NULL,'50625',v_user,'4120',NULL,NULL,'15',v_user,NULL,CURRENT_DATE,v_out_request_id,v_out_consign_id);

	  IF (v_tagable_flag ='Y') THEN

			v_input_str_tag_creation:=p_tag_id||'^'||v_tag_control_number||'^'||v_tag_part_number||'^'||p_set_id||'^'||'51000^'||v_out_consign_id||'^'||'40033^|';
			gm_pkg_ac_tag_info.gm_sav_tag_detail(v_input_str_tag_creation,v_user,v_errmsg);

		 
		 ELSE
			dbms_output.put_line('The set ID**'||p_set_id||'and the tag id '||p_tag_id||' does not have a tagable part');
			
	  END IF;
		
		-- Set build Processing Lot Number verification step
			gm_save_set_build(v_out_consign_id, v_initiate_input_str_set_build,'JPN Data Migration tag#'||p_tag_id, '1.20', '0', v_user, v_errmsg,'LOANERPOOL');
		
		-- Moving the Set to Loaner Common Pool
			gm_save_set_build(v_out_consign_id, v_initiate_input_str_set_build,'JPN Data Migration tag#'||p_tag_id, '1.20', '1', v_user, v_errmsg,'LOANERPOOL');
		
			gm_pkg_op_loaner.gm_sav_process_picture(v_out_consign_id,v_user,v_msg);

			gm_pkg_op_set_put_txn.gm_sav_set_put (p_tag_id,'080-J-001-J-1',v_user,v_out_consign_id);
		 
			dbms_output.put_line(v_out_consign_id||' : is Moved to Loaner Common Pool');

		 
			UPDATE t504_consignment
			SET C504_LAST_UPDATED_BY=v_user, C504_LAST_UPDATED_DATE =CURRENT_DATE, c504_comments='JPN Data Migration tag: '||p_tag_id
			WHERE c504_consignment_id=v_out_consign_id;
			
			UPDATE t504a_consignment_loaner
			SET C504a_LAST_UPDATED_BY=v_user, C504a_LAST_UPDATED_DATE =CURRENT_DATE, c504a_etch_id=p_tag_id
			WHERE c504_consignment_id=v_out_consign_id;
		   
			update JP_LOANER_INV_STG
			set NEW_TXN_ID=v_out_consign_id	
			where c207_set_id=p_set_id
			and c5010_tag_id=p_tag_id;
	    
	 
END tmp_gm_set_build;     


/*
 ************************************************
 * The purpose of this procedure is to Initiate a Loaner Set for the given Set ID
 * Ticket: Atec 403 : Japan Loaner Set List Data migration. July 2017 
 * Author: Gomathi Palani. Verified by Richard K.
************************************************* */


PROCEDURE tmp_gm_Loaner_Initiate 
	 (
        p_set_id    IN t208_set_details.C207_SET_ID%TYPE,
        p_acc_id    IN T704_ACCOUNT.C704_EXT_REF_ID%TYPE,
        p_tag_id    IN t5010_tag.C5010_TAG_ID%TYPE
	 )
AS
    -- Loaner request and Approval
    v_req_id         VARCHAR2 (20) ;
    v_reqdet_litpnum VARCHAR2 (20) ;
    v_req_det_id     VARCHAR2 (20) ;
    v_req_dtl_id     VARCHAR2 (20) ;
    v_dist_id v700_territory_mapping_detail.D_ID%TYPE;
    v_rep_id v700_territory_mapping_detail.rep_id%TYPE;
    v_user NUMBER;
    v_input_str_LN_Initiate CLOB;
    v_request_for NUMBER := '4127';
    v_ship_to     NUMBER := '4120';
    v_request_by  NUMBER := '50626';
    V_LN_CONSIGN_ID T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE;
    v_GMNA_Acct_id t704_account.C704_ACCOUNT_ID%TYPE;
    v_loaner_dt VARCHAR2(40);
    v_dateformat varchar2(100);
    v_shipid number;
    v_shipflag varchar2(10);
    v_input_str_pending_pick CLOB;
    v_status_fl number;
        
   
BEGIN
	    
    -- Setting Japan Context
-- gm_pkg_cor_client_context.gm_sav_client_context ('1022', get_code_name ('10306121'), get_code_name ('105131'), '3012');
   select get_compdtfmt_frm_cntx() into v_dateformat from dual;
   v_user := IT_PKG_CM_USER.GET_VALID_USER;
    
      select C704_ACCOUNT_ID INTO v_GMNA_Acct_id from t704_account
      where c704_ext_ref_id=p_acc_id;
    
      SELECT d_id, v_rep_id
      INTO v_dist_id, v_rep_id
      FROM v700_territory_mapping_detail
      WHERE ac_id= v_GMNA_Acct_id;
      
      v_loaner_dt :=to_char(CURRENT_DATE,get_compdtfmt_frm_cntx()); 

      v_input_str_LN_Initiate := p_set_id||'^1'||'^'||v_loaner_dt||'|';

      -- Loaner Initiate
   	  gm_pkg_op_product_requests.gm_sav_product_requests (CURRENT_DATE, v_request_for, v_dist_id,
      v_request_by, v_user, v_ship_to, NULL, v_user, v_input_str_LN_Initiate, v_rep_id, v_GMNA_Acct_id, v_req_id, NULL, NULL) ;
           
     -- Fetching Request ID
      SELECT C526_PRODUCT_REQUEST_DETAIL_ID
      INTO v_req_det_id
      FROM t526_product_request_detail
      WHERE c525_product_request_id = v_req_id;
      
     -- Creating Shipping Record      
      gm_pkg_cm_shipping_trans.gm_sav_shipping(v_req_det_id,'50182','4120',v_dist_id,'5001'
     ,'5004',null,'0',null,v_user,v_shipid,null,null);

 	-- Take the CN id from the created for this Set and Tag combination
	
	   SELECT  DISTINCT(NEW_TXN_ID) INTO V_LN_CONSIGN_ID FROM JP_LOANER_INV_STG
	   where c207_set_id=p_set_id
	   and c5010_tag_id=p_tag_id;
	   
	   dbms_output.put_line('NEW_TXN_ID is:'||V_LN_CONSIGN_ID);
   
	   SELECT c526_status_fl 
	   INTO v_status_fl 
	   FROM t526_product_request_detail 
	   WHERE c526_product_request_detail_id=v_req_det_id;
	    	 
       gm_pkg_sm_loaner_allocation.gm_sav_loaner_req_appv (v_req_det_id, v_user, '10', v_req_dtl_id, v_reqdet_litpnum,v_req_det_id) ;
    
	   SELECT c526_status_fl 
	   INTO v_status_fl 
	   FROM t526_product_request_detail 
	   WHERE c526_product_request_detail_id=v_req_det_id;
	 
       v_input_str_pending_pick :=V_LN_CONSIGN_ID||'^'||null||'^'||p_tag_id||'|';
    
       gm_pkg_op_set_pick_txn.gm_sav_set_pick_batch(v_input_str_pending_pick, v_user);
     
       gm_pkg_cm_shipping_trans.gm_sav_shipout(V_LN_CONSIGN_ID,'50182','4120',v_dist_id,'5001','5004'
           	,V_LN_CONSIGN_ID||':'||p_tag_id,0,0,v_user,0,v_shipid,NULL,v_shipflag);    


END tmp_gm_Loaner_Initiate;

/*
************************************************
* The purpose of this procedure is to Initiate IAFG for the Loose Items
* Ticket: Atec 403 : Japan Loaner Set List Data migration. July 2017
* Author: Gomathi Palani. Verified by Richard K. 
************************************************* */

PROCEDURE tmp_gm_loose_item_iafg
AS
    v_user t101_user.c101_user_id%TYPE := '303043';
    v_initiate_input_str_IAFG CLOB;
    v_control_input_str_IAFG CLOB;
    v_verify_input_str_IAFG CLOB;
    v_part_no t205_part_number.c205_part_number_id%TYPE;
    v_company_id          NUMBER;
    v_plant_id            NUMBER;
    v_portal_com_date_fmt VARCHAR2(20) ;
    v_qty                 NUMBER;
    v_purchase_amt        NUMBER := '0';
    v_lot_number   		  VARCHAR2(40) ;
    v_reason	          NUMBER;
    v_txntype             NUMBER;
    v_wh_type             VARCHAR2(100) ;
    v_cur_wh              VARCHAR2(50) ;
    v_txn_type_name       VARCHAR2(50) ;
    v_message             VARCHAR2(100) ;
    v_source_wh           NUMBER;
    v_target_wh           NUMBER;
    v_costing_type        NUMBER;
    V_TXN_ID              VARCHAR2(20) ;
    V_OUT                 NUMBER;
    v_fg_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
    v_part_price t205_part_number.c205_equity_price%TYPE;
    v_cost t820_costing.c820_purchase_amt%TYPE;
    v_company_cost  t820_costing.C820_LOCAL_COMPANY_COST%TYPE;
    v_count number := '0';
    v_pkey NUMBER;
    
	   
    CURSOR cur_item_iafg
    IS
         SELECT JPN.C205_part_number_id PART_NO, JPN.physical_set_QTY QTY, jpn.lot_number LOT_NUMBER, jpn.pkey pkey
           FROM JP_LOANER_INV_STG JPN
           WHERE JPN.C5010_tag_id is null
           AND JPN.C207_SET_ID IS NULL
           and jpn.set_loaded_fl='IAFG'
           ORDER BY C205_part_number_id;
       
BEGIN
	
   BEGIN
		
	SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
    SELECT get_plantid_frm_cntx  INTO v_plant_id FROM dual;
    SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
        
     -- Create Location for FG and BL
  BEGIN
	  
    SELECT c5052_location_id INTO v_fg_location_id
    FROM t5052_location_master
    WHERE c5052_location_cd='JPN-FG-LOCATION' 
    and c1900_company_id= v_company_id
    AND c5052_void_fl is null;
    
     EXCEPTION WHEN OTHERS THEN
     dbms_output.put_line('*********Location error' || ' Error:' || sqlerrm);
  END;
  
  FOR inv_item IN cur_item_iafg
  
    LOOP
        BEGIN
		
                v_qty     		:= inv_item.QTY;
                v_part_no 		:= inv_item.PART_NO;
                v_lot_number    := inv_item.LOT_NUMBER;
                v_txn_type_name := 'InvAdj-Shelf';
                v_txntype       := '400068';
                v_target_wh     := '90800';
                v_source_wh     := '400069';
                v_cur_WH        := 'FG';
                v_wh_type       := '90800';
                v_costing_type  := '4900';
                v_reason        := '90800';                
                v_pkey 			 := inv_item.pkey;
                
                BEGIN
                     SELECT COST, company_cost
                       INTO v_purchase_amt, v_company_cost
                       FROM jpn_dm_inventory
                       WHERE part      = v_part_no
                       AND warehouse = 'FG';
                EXCEPTION
                WHEN NO_DATA_FOUND THEN
        
                    v_purchase_amt := '0';
                    v_company_cost :='0';
                END;
                
                
      SELECT get_inventory_cogs_value(v_part_no)  INTO v_cost FROM dual;
      
    IF v_cost IS NULL THEN
              
		       INSERT INTO t820_costing (
		           c820_costing_id
		          ,c205_part_number_id
		          ,c901_costing_type
		          ,c820_part_received_dt
		          ,c820_qty_received
		          ,c820_purchase_amt
		          ,c820_qty_on_hand       
		          ,c901_status
		          ,c820_delete_fl
		          ,c820_created_by
		          ,c820_created_date
		          ,c1900_company_id
		          ,c5040_plant_id
		          ,C820_LOCAL_COMPANY_COST  
		          ,C1900_OWNER_COMPANY_ID
		          ,c901_owner_currency
		          ,C901_LOCAL_COMPANY_CURRENCY
		        ) 
		        VALUES 
		        (	 s820_costing.nextval
		          ,v_part_no
		          ,v_costing_type
		          ,CURRENT_DATE
		          ,0
		          ,v_purchase_amt
		          ,0
		          ,4801
		          ,'N'
		          ,V_USER
		          ,CURRENT_DATE
		          ,v_company_id
		          ,v_plant_id
		          ,v_company_cost
		          ,'1018'
		          ,'1'
		          ,'4000917'
		        );
                               
   END IF;               
                                         
    --Get Part price
      BEGIN
	      
		        SELECT	c2052_equity_price price
		        INTO v_part_price
		        FROM t2052_part_price_mapping  
		        WHERE c205_part_number_id = v_part_no
		        AND c1900_company_id =v_company_id;
        
      EXCEPTION  WHEN no_data_found THEN
      
		          dbms_output.put_line('v_part_no=' || v_part_no || ' Price not found');
		          v_part_price := null;
		          dbms_output.put_line(v_part_no ||' - Without Equity Price, Inventory migration cannot be done, skip to next');
		          continue;
          
      END;
      
      	IF (v_txn_id is null) THEN
                
      		   		SELECT get_next_consign_id (v_txn_type_name) INTO v_txn_id FROM dual;
                
      	END IF;
           
	        		INSERT INTO MY_TEMP_KEY_VALUE_IAFG(MY_TEMP_TXN_ID,MY_TEMP_TXN_KEY) values(v_pkey,v_part_no);
	  			
	              
	                v_initiate_input_str_IAFG := v_initiate_input_str_IAFG || v_part_no || ',' || v_qty || ',,,' ||v_part_price || '|';
	                
	                v_control_input_str_IAFG := v_control_input_str_IAFG || v_part_no || ',' || v_qty || ',' ||v_lot_number || ',,' || v_part_price || '|';
	                
	                v_verify_input_str_IAFG := v_verify_input_str_IAFG || v_part_no || ',' || v_qty ||','|| v_lot_number|| ','||v_fg_location_id||',' || v_target_wh || '|';
	                
	                -- Calling FS location procedure to update the part qty
	                gm_pkg_op_inv_field_sales_tran.gm_sav_fs_inv_loc_part_details (v_fg_location_id,v_txn_id, v_part_no,v_qty,'400068',4301,null,current_date, v_user);
                
         v_count :=v_count+1;
                
           IF(v_count=20) then 
                
	                 gm_save_inhouse_item_consign (v_txn_id, v_txntype, v_reason, '0', '', 'IAFG created for the Loose item Adjustment', v_user, v_initiate_input_str_IAFG, v_out) ;
	                 
	                 GM_LS_UPD_INLOANERCSG (v_txn_id, v_txntype, '0', '0', NULL, 'Japan Set Data Load', v_user,v_control_input_str_IAFG, NULL, 'ReleaseControl', v_message) ;
	            
	            	 gm_ls_upd_inloanercsg (v_txn_id, v_txntype, '', '0', NULL, 'Japan -- Manual Data --load', v_user,v_verify_input_str_IAFG, NULL, 'Verify', v_message) ;
	            
	            	 UPDATE MY_TEMP_KEY_VALUE_IAFG SET     MY_TEMP_TXN_VALUE= v_txn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
	             
	             	 UPDATE t412_inhouse_transactions SET c412_comments='IAFG Inventory Update' , c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
				 	 where c412_inhouse_trans_id=v_txn_id;
				 
			-- Re-Setting the flag for the next IAFG creation.
			
		            v_count :='0';
		            v_initiate_input_str_IAFG :=NULL;
		            v_control_input_str_IAFG :=NULL;
		            v_verify_input_str_IAFG :=NULL;
		            v_txn_id := NULL;
            
          END IF;
          
       END;
              
 END LOOP;
            
           IF (v_initiate_input_str_IAFG IS NOT NULL) THEN
            
		              gm_save_inhouse_item_consign (v_txn_id, v_txntype, v_reason, '0', '', 'IAFG created for the Loose item Adjustment', v_user, v_initiate_input_str_IAFG, v_out) ;
		            
		              GM_LS_UPD_INLOANERCSG (v_txn_id, v_txntype, '0', '0', NULL, 'Japan Set Data Load', v_user,
		              v_control_input_str_IAFG, NULL, 'ReleaseControl', v_message) ;
		            
		              gm_ls_upd_inloanercsg (v_txn_id, v_txntype, '', '0', NULL, 'Japan -- Manual Data --load', v_user,
		              v_verify_input_str_IAFG, NULL, 'Verify', v_message) ;
		            
		              UPDATE MY_TEMP_KEY_VALUE_IAFG SET     MY_TEMP_TXN_VALUE= v_txn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
					
		              UPDATE t412_inhouse_transactions SET c412_comments='IAFG Inventory Update' , c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
					  where c412_inhouse_trans_id=v_txn_id;
			 
			          v_count :='0';
			          v_initiate_input_str_IAFG :=NULL;
			          v_control_input_str_IAFG :=NULL;
			          v_verify_input_str_IAFG :=NULL;
			          v_txn_id := NULL;
          
     	   END IF;  
         
		     	        UPDATE JP_LOANER_INV_STG T1 SET set_loaded_fl='IAFG_PROCESSED',
						T1.NEW_TXN_ID = (SELECT T2.MY_TEMP_TXN_VALUE FROM MY_TEMP_KEY_VALUE_IAFG T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
						WHERE T1.PKEY IN (SELECT T2.MY_TEMP_TXN_ID FROM MY_TEMP_KEY_VALUE_IAFG T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
						and set_loaded_fl='IAFG';
			
       	COMMIT;
       	
    EXCEPTION WHEN OTHERS then
    	  DBMS_OUTPUT.PUT_LINE('Loose Item IAFG ERROR for part'||v_part_no||SQLERRM);
    ROLLBACK;
 
  END; 
END tmp_gm_loose_item_iafg;

/*
************************************************
* The purpose of this procedure is to Initiate IAFG for a inspection location
* Ticket: Atec 403 : Japan Loaner Set List Data migration. July 2017
* Author: Gomathi Palani. Verified by Richard K. 
************************************************* */

PROCEDURE tmp_gm_loose_item_iafg_ins
AS
    v_user t101_user.c101_user_id%TYPE := '303043';
    v_initiate_input_str_IAFG CLOB;
    v_control_input_str_IAFG CLOB;
    v_verify_input_str_IAFG CLOB;
    v_part_no t205_part_number.c205_part_number_id%TYPE;
    v_company_id          NUMBER;
    v_plant_id            NUMBER;
    v_portal_com_date_fmt VARCHAR2 (20) ;
    v_qty                 NUMBER;
    v_purchase_amt        NUMBER := '0';
    v_acc_id T704_ACCOUNT.C704_ACCOUNT_ID%TYPE;
    v_lot_number    VARCHAR2 (40) ;
    v_reason        NUMBER;
    v_txntype       NUMBER;
    v_wh_type       VARCHAR2 (100) ;
    v_cur_wh        VARCHAR2 (50) ;
    v_txn_type_name VARCHAR2 (50) ;
    v_message       VARCHAR2 (100) ;
    v_source_wh    NUMBER;
    v_target_wh    NUMBER;
    v_costing_type NUMBER;
    V_TXN_ID       VARCHAR2 (20) ;
    V_OUT          NUMBER;
    v_fg_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
    v_part_price t205_part_number.c205_equity_price%TYPE;
    v_cost t820_costing.c820_purchase_amt%TYPE;
    v_company_cost  t820_costing.C820_LOCAL_COMPANY_COST%TYPE;
    v_count number := '0';
    v_pkey number;
    
    CURSOR cur_item_iafg
    IS
         SELECT JPN.C205_part_number_id PART_NO, JPN.physical_set_QTY QTY, jpn.lot_number LOT_NUMBER, jpn.pkey pkey
          
           FROM JP_LOANER_INV_STG JPN
          WHERE JPN.C5010_tag_id is null
           AND JPN.C207_SET_ID IS NULL
           and jpn.set_loaded_fl='L'
       ORDER BY C205_part_number_id;
       

BEGIN
	 gm_pkg_cor_client_context.gm_sav_client_context ('1025', get_code_name ('10306121'), get_code_name ('105131'), '3016');
    SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
    SELECT get_plantid_frm_cntx  INTO v_plant_id FROM dual;
    SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
    
    
     -- Create Location for FG and BL
  BEGIN
	  
    SELECT c5052_location_id 
    INTO v_fg_location_id
    FROM t5052_location_master
    WHERE c5052_location_cd='JPN_FG_INS_LOCATION' 
    and c1900_company_id= v_company_id AND c5052_void_fl is null;
        
    EXCEPTION WHEN OTHERS THEN
    dbms_output.put_line('*********Location error' || ' Error:' || sqlerrm);
    
  END;
    
  FOR inv_item IN cur_item_iafg
  
        LOOP
            BEGIN
	          	
                v_qty    		:= inv_item.QTY;
                v_part_no 		:= inv_item.PART_NO;
                v_acc_id        := null;
                v_lot_number    := inv_item.LOT_NUMBER;
                v_txn_type_name := 'InvAdj-Shelf';
                v_txntype       := '400068';
                v_target_wh     := '90800';
                v_source_wh     := '400069';
                v_cur_WH        := 'FG';
                v_wh_type       := '90800';
                v_costing_type  := '4900';
                v_reason        := '90800';         
                v_pkey 			:= inv_item.pkey;
                
                BEGIN
	                
                     SELECT COST, company_cost
                     INTO v_purchase_amt, v_company_cost
                     FROM jpn_dm_inventory
                     WHERE part      = v_part_no
                     AND warehouse = 'FG';
                        
                EXCEPTION
                
                WHEN NO_DATA_FOUND THEN
                    v_purchase_amt := '0';
                    v_company_cost :='0';
                END;
                
                
       SELECT get_inventory_cogs_value(v_part_no)  INTO v_cost FROM dual;
      
      IF v_cost IS NULL THEN
              
       INSERT INTO t820_costing (
           c820_costing_id
          ,c205_part_number_id
          ,c901_costing_type
          ,c820_part_received_dt
          ,c820_qty_received
          ,c820_purchase_amt
          ,c820_qty_on_hand       
          ,c901_status
          ,c820_delete_fl
          ,c820_created_by
          ,c820_created_date
          ,c1900_company_id
          ,c5040_plant_id
          ,C820_LOCAL_COMPANY_COST
          ,C1900_OWNER_COMPANY_ID
          ,c901_owner_currency
          ,C901_LOCAL_COMPANY_CURRENCY
        ) 
        VALUES 
        (	 s820_costing.nextval
          ,v_part_no
          ,v_costing_type
          ,CURRENT_DATE
          ,0
          ,v_purchase_amt
          ,0
          ,4801
          ,'N'
          ,V_USER
          ,CURRENT_DATE
          ,v_company_id
          ,v_plant_id
          ,v_company_cost
          ,'1018'
          ,'1'
          ,'4000917'
        );
                               
     END IF;               
                                         
    --Get Part price
      BEGIN
	      
        SELECT	c2052_equity_price price
        INTO v_part_price
        FROM t2052_part_price_mapping  
        WHERE c205_part_number_id = v_part_no
        AND c1900_company_id =v_company_id;
        
      EXCEPTION  WHEN no_data_found THEN
          dbms_output.put_line('v_part_no=' || v_part_no || ' Price not found');
          v_part_price := null;
          dbms_output.put_line(v_part_no ||' - Without Equity Price, Inventory migration cannot be done, skip to next');
          continue;
      END;
      
     IF (v_txn_id is null) THEN
                
	       SELECT get_next_consign_id (v_txn_type_name) 
	       INTO v_txn_id FROM dual;
	                
     END IF;
                INSERT INTO MY_TEMP_KEY_VALUE_IAFG_INS(MY_TEMP_TXN_ID,MY_TEMP_TXN_KEY) values(v_pkey,v_part_no);
                
                v_initiate_input_str_IAFG := v_initiate_input_str_IAFG || v_part_no || ',' || v_qty || ',,,' ||v_part_price || '|';
                
                v_control_input_str_IAFG := v_control_input_str_IAFG || v_part_no || ',' || v_qty || ','
                ||v_lot_number || ',,' || v_part_price || '|';
                
                v_verify_input_str_IAFG := v_verify_input_str_IAFG || v_part_no || ',' || v_qty ||','|| v_lot_number|| ','||v_fg_location_id||',' 
                || v_target_wh || '|';
                
                gm_pkg_op_inv_field_sales_tran.gm_sav_fs_inv_loc_part_details (v_fg_location_id,v_txn_id, v_part_no,v_qty,
                '400068',4301,null,current_date, v_user);
                
                v_count :=v_count+1;
                
         if(v_count=20) THEN 
         
                gm_save_inhouse_item_consign (v_txn_id, v_txntype, v_reason, '0', '', 'IAFG created for the Loose item Adjustment INS',
                v_user, v_initiate_input_str_IAFG, v_out) ;
          
                GM_LS_UPD_INLOANERCSG (v_txn_id, v_txntype, '0', '0', NULL, 'Japan Set Data Load', v_user,v_control_input_str_IAFG,
                NULL, 'ReleaseControl', v_message) ;
            
            	gm_ls_upd_inloanercsg (v_txn_id, v_txntype, '', '0', NULL, 'Japan -- Manual Data --load', v_user,
            	v_verify_input_str_IAFG, NULL, 'Verify', v_message) ;
            	 
            	dbms_output.put_line ('IAFG INS Loc Txn=' || v_txn_id || ' Verified');
            	
            	UPDATE MY_TEMP_KEY_VALUE_IAFG_INS SET     MY_TEMP_TXN_VALUE= v_txn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
			
           
				UPDATE t412_inhouse_transactions SET c412_comments='IAFG Inventory Update for Inspection Location' , 
				c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
				where c412_inhouse_trans_id=v_txn_id;

	            v_count :='0';
	            v_initiate_input_str_IAFG :=NULL;
	            v_control_input_str_IAFG :=NULL;
	            v_verify_input_str_IAFG :=NULL;
	            v_txn_id := NULL;
	            
       END IF;
    END;
END LOOP;
            -- Outer loop
         IF (v_initiate_input_str_IAFG IS NOT NULL) THEN
            
              gm_save_inhouse_item_consign (v_txn_id, v_txntype, v_reason, '0', '', 'IAFG created for the Loose item Adjustment INS', v_user, v_initiate_input_str_IAFG, v_out) ;
              
              GM_LS_UPD_INLOANERCSG (v_txn_id, v_txntype, '0', '0', NULL, 'Japan Set Data Load', v_user,v_control_input_str_IAFG, NULL, 'ReleaseControl', v_message) ;
            
              gm_ls_upd_inloanercsg (v_txn_id, v_txntype, '', '0', NULL, 'Japan -- Manual Data --load', v_user,v_verify_input_str_IAFG, NULL, 'Verify', v_message) ;
              
              dbms_output.put_line ('IAFG INS Loc Txn=' || v_txn_id || ' Verified');
              
              UPDATE MY_TEMP_KEY_VALUE_IAFG_INS SET     MY_TEMP_TXN_VALUE= v_txn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
			
              UPDATE t412_inhouse_transactions SET c412_comments='IAFG Inventory Update for Inspection Location' , c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
			  where c412_inhouse_trans_id=v_txn_id;
	            v_count :='0';
	            v_initiate_input_str_IAFG :=NULL;
	            v_control_input_str_IAFG :=NULL;
	            v_verify_input_str_IAFG :=NULL;
	            v_txn_id := NULL;
	           
          END IF;  
        
  		      UPDATE JP_LOANER_INV_STG T1 SET set_loaded_fl='M',
				T1.NEW_TXN_ID = (SELECT T2.MY_TEMP_TXN_VALUE FROM MY_TEMP_KEY_VALUE_IAFG_INS T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
				WHERE T1.PKEY IN (SELECT T2.MY_TEMP_TXN_ID FROM MY_TEMP_KEY_VALUE_IAFG_INS T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
				AND set_loaded_fl='L';
   COMMIT;
 
    EXCEPTION WHEN OTHERS then
    DBMS_OUTPUT.PUT_LINE('Loose Item IAFG ins ERROR for part'||v_part_no||SQLERRM);
    ROLLBACK;
   
END tmp_gm_loose_item_iafg_ins;


/*
************************************************
* The purpose of this procedure is to Initiate Account Initite for the Loose items
* Ticket: Atec 403 : Japan Loaner Set List Data migration. July 2017
* Author: Gomathi Palani. Verified by Richard K.
************************************************* */

PROCEDURE tmp_gm_loose_item_initiate
AS
    v_user t101_user.c101_user_id%TYPE := '303043';
    v_input_str_item_consign CLOB;
    v_control_str_item_consign CLOB;
    v_part_no t205_part_number.c205_part_number_id%TYPE;
    v_company_id          t1900_company.C1900_COMPANY_ID%TYPE;
    v_plant_id            t5040_plant_master.C5040_PLANT_ID%TYPE;
    v_portal_com_date_fmt VARCHAR2 (20) ;
    v_qty                 NUMBER;
    v_acc_id T704_ACCOUNT.c704_ext_ref_id%TYPE;
    v_lot_number       VARCHAR2 (40) ;
    v_ship_date        VARCHAR2 (100) ;
    v_req_id           VARCHAR2 (20) ;
    v_out_cn_id        VARCHAR2 (20) ;
    V_OUT              NUMBER;
    v_GMNA_Acct_id    t704_account.C704_ACCOUNT_ID%TYPE;
    v_strTxnType       NUMBER        := '4110';
    v_strRelVerFl      VARCHAR2 (10) := 'on';
    v_strLocType       NUMBER        := '93343';
    v_shipid           NUMBER;
    v_shipflag         VARCHAR2 (10) ;
    v_fg_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
    v_pkey number;
    
    CURSOR cur_item_acc
    IS
        SELECT DISTINCT (JPN.ALLADIN_ACCOUNT_ID) acct_id
           FROM JP_LOANER_INV_STG JPN
          WHERE JPN.C5010_tag_id       IS NULL
            AND JPN.ALLADIN_ACCOUNT_ID IS NOT NULL
            AND JPN.C207_SET_ID        IS NULL
            AND jpn.set_loaded_fl       = 'IAFG_PROCESSED'
       ORDER BY ALLADIN_ACCOUNT_ID;
       
    CURSOR cur_item_initiate
    IS
         SELECT JPN.C205_part_number_id PART_NO, JPN.physical_set_QTY QTY, jpn.lot_number LOT_NUMBER, jpn.pkey pkey
           FROM JP_LOANER_INV_STG JPN
          WHERE JPN.C5010_tag_id      IS NULL
            AND JPN.ALLADIN_ACCOUNT_ID = v_acc_id
            AND JPN.C207_SET_ID       IS NULL
            AND jpn.set_loaded_fl      = 'IAFG_PROCESSED'
       ORDER BY C205_part_number_id;
       
BEGIN
	
    
     SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
     SELECT get_plantid_frm_cntx INTO v_plant_id FROM dual;
     SELECT get_compid_frm_cntx () INTO v_company_id FROM dual;
  
 

BEGIN
	
     FOR inv_item_acc IN cur_item_acc
 
        LOOP
             v_acc_id      := inv_item_acc.acct_id;
BEGIN
	            
  	 FOR inv_item_consign IN cur_item_initiate
    
        LOOP
             v_qty        := inv_item_consign.QTY;
             v_part_no    := inv_item_consign.PART_NO;
             v_lot_number := inv_item_consign.LOT_NUMBER;
             v_pkey       := inv_item_consign.pkey;
                
				  SELECT C704_ACCOUNT_ID
				  INTO v_GMNA_Acct_id
				  FROM t704_account
				  WHERE c704_ext_ref_id = v_acc_id;
				  
		
			BEGIN
				
			     SELECT c5052_location_id
			       INTO v_fg_location_id
			       FROM t5052_location_master
			      WHERE c5052_location_CD = 'JPN-FG-LOCATION'
			        AND c1900_company_id  = v_company_id
			        AND c5052_void_fl    IS NULL;
			        
				EXCEPTION
				WHEN OTHERS THEN
			    DBMS_OUTPUT.PUT_LINE ('*********Location error' || ' Error:' || SQLERRM) ;
			END;            
                  
                   
      			INSERT INTO MY_TEMP_KEY_VALUE_loose_item(MY_TEMP_TXN_ID,MY_TEMP_TXN_KEY) values(v_pkey,v_part_no);
                    
                v_input_str_item_consign   := v_input_str_item_consign|| v_part_no||'^'||v_qty||'^'||v_ship_date||'^|';
                
     			v_control_str_item_consign := v_control_str_item_consign||v_part_no||','||v_qty||','||v_lot_number||','||v_fg_location_id||','||'TBE'||','||'90800'||'|';
                
   END LOOP;
            
                v_input_str_item_consign := v_input_str_item_consign||'~5001^5004|';
                 
                gm_pkg_op_account_request.gm_sav_initiate_request(NULL,CURRENT_DATE,50618,NULL,NULL,40025,v_GMNA_Acct_id,50625,V_USER,4120,NULL,NULL,
                15,V_USER,v_input_str_item_consign,50060,'',v_GMNA__Acct_id,'',CURRENT_DATE,'',v_req_id,v_out_cn_id,NULL);
	                
   	            dbms_output.put_line ('v_req_id: '||v_req_id) ;
   	            dbms_output.put_line ('v_out_cn_id: '||v_out_cn_id) ;
   	                  
   	             -- Added the below procedure to enable the Initate RA flag
   	             gm_pkg_op_return_txn.gm_sav_return_fl_for_req(v_req_id,'Y',V_USER);
                      
   	              -- Creating Shipping record    
                 gm_pkg_cm_shipping_trans.gm_sav_shipping (v_req_id, '50184', '4120', v_GMNA_Acct_id, '5001', '5004', NULL,
                '0', NULL, '303201', v_shipid, NULL, NULL) ;
                  	       
                  	                    	  
                -- controlling the Account consignment
               	gm_pkg_op_item_control_txn.gm_sav_item_control_main (v_out_cn_id, v_strTxnType, v_strRelVerFl,
               	v_control_str_item_consign, v_strLocType, V_USER);
                    	
    			-- Ship Out
                gm_pkg_cm_shipping_trans.gm_sav_shipout (v_out_cn_id, '50181', '4120', v_GMNA_Acct_id, '5001', '5004',
                v_out_cn_id|| ':Item', 0, 0, '303201', 0, v_shipid, NULL, v_shipflag) ;
                    	
                -- Account Net Qty by Lot (Net Number Procedure)
                gm_pkg_set_lot_track.GM_PKG_SET_LOT_UPDATE(v_out_cn_id,V_USER,v_GMNA_Acct_id,'50181','5');

                UPDATE t504_consignment
                SET C504_LAST_UPDATED_BY = v_user, C504_LAST_UPDATED_DATE = CURRENT_DATE, c504_comments =
                'JPN Data Migration:Loose Items for Alladin Acc: '||v_acc_id
                WHERE c504_consignment_id = v_out_cn_id;
                      
                UPDATE MY_TEMP_KEY_VALUE_loose_item SET MY_TEMP_TXN_VALUE= v_out_cn_id 
                WHERE MY_TEMP_TXN_VALUE IS NULL;
                    	
                dbms_output.put_line ('Loose Item Initiated for the Acct ID: '||v_GMNA_Acct_id) ;
 
                    
		        UPDATE JP_LOANER_INV_STG T1 SET set_loaded_fl='Loose Item Initiated',
				T1.NEW_TXN_ID = (SELECT T2.MY_TEMP_TXN_VALUE FROM MY_TEMP_KEY_VALUE_loose_item T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
				WHERE T1.PKEY IN (SELECT T2.MY_TEMP_TXN_ID FROM MY_TEMP_KEY_VALUE_loose_item T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
				AND SET_LOADED_FL='IAFG_PROCESSED';
	      
			--	COMMIT;
	  	   		 	   
		    EXCEPTION WHEN OTHERS then
            DBMS_OUTPUT.PUT_LINE('Loose Item CONSIGNMENT ERROR for part '||v_part_no||SQLERRM);
          --  ROLLBACK;
   	      END;
   	      
   
		  
    END LOOP;
END;	
END tmp_gm_loose_item_initiate;

/*
This procedure is created to create the Item consignment for the field sales warehouse.
Author :gpalani
TSK: 8279

*/

PROCEDURE tmp_gm_fswh_load
AS

    v_user t101_user.c101_user_id%TYPE := '303043';
    v_input_str_item_consign CLOB;
    v_control_str_item_consign CLOB;
    v_part_no t205_part_number.c205_part_number_id%TYPE;
    v_company_id          t1900_company.C1900_COMPANY_ID%TYPE;
    v_plant_id            t5040_plant_master.C5040_PLANT_ID%TYPE;
    v_portal_com_date_fmt VARCHAR2 (20) ;
    v_qty                 NUMBER;
    v_dist_id t701_distributor.C701_DISTRIBUTOR_ID%TYPE;
    v_lot_number       VARCHAR2 (40) ;
    v_txntype          NUMBER;
    v_ship_date        VARCHAR2 (100) ;
    v_out_cn_id        VARCHAR2 (20) ;
    v_req_id           VARCHAR2 (20) ;
    v_strTxnType         NUMBER        := '4110';
    v_strRelVerFl        VARCHAR2 (10) := 'on';
    v_strLocType         NUMBER        := '93343';
    v_shipid           NUMBER;
    v_shipflag         VARCHAR2 (10) ;
    v_fg_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
    v_count      NUMBER := 0;
    v_count_item NUMBER := 0;
    v_pkey number;
	
    CURSOR cur_item_acc
    IS
        SELECT DISTINCT (JPN.C701_DISTRIBUTOR_ID) dist_id
        FROM JP_LOANER_INV_STG JPN
        WHERE JPN.C5010_tag_id       IS NULL
   		AND C701_DISTRIBUTOR_ID IS NOT NULL
        AND JPN.C207_SET_ID        IS NULL
		AND JPN.TYPE ='FSWH'
        AND JPN.set_loaded_fl       = 'IAFG Pending'
        ORDER BY C701_DISTRIBUTOR_ID;
	   
    CURSOR cur_item_initiate
    IS
         SELECT JPN.C205_part_number_id PART_NO, JPN.physical_set_QTY QTY, jpn.lot_number LOT_NUMBER, jpn.pkey pkey
         FROM JP_LOANER_INV_STG JPN
         WHERE JPN.C5010_tag_id      IS NULL
		 AND C701_DISTRIBUTOR_ID= v_dist_id
         AND JPN.C207_SET_ID       IS NULL
		 AND JPN.TYPE ='FSWH'
         AND JPN.set_loaded_fl='IAFG Initiated'
         ORDER BY C205_part_number_id;
BEGIN
    -- gm_pkg_cor_client_context.gm_sav_client_context ('1022', get_code_name ('10306121'), get_code_name ('105131'),'3012');
	
        SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
        SELECT get_plantid_frm_cntx INTO v_plant_id FROM dual;
        SELECT get_compid_frm_cntx () INTO v_company_id FROM dual;
    -- Create Location for FG and BL
    BEGIN
         SELECT c5052_location_id
         INTO v_fg_location_id
         FROM t5052_location_master
         WHERE c5052_location_cd = 'JPN_FG_LOCATION'
         AND c1900_company_id  = v_company_id
         AND c5052_void_fl is null;
    
    EXCEPTION
	
		WHEN OTHERS THEN
        dbms_output.put_line ('*********Location error' || ' Error:' || sqlerrm) ;
    END;
    
    BEGIN
    
        FOR inv_item_acc IN cur_item_acc
        
			LOOP
				v_dist_id            := inv_item_acc.dist_id;
				
			    BEGIN
				
				   FOR inv_item_consign IN cur_item_initiate
				
				     LOOP
						v_qty        := inv_item_consign.QTY;
						v_part_no    := inv_item_consign.PART_NO;
						v_lot_number := inv_item_consign.LOT_NUMBER;
						v_pkey 		 := inv_item_consign.pkey;
         
                    INSERT INTO MY_TEMP_KEY_VALUE_loose_item(MY_TEMP_TXN_ID,MY_TEMP_TXN_KEY) values(v_pkey,v_part_no);
                 
					v_input_str_item_consign   := v_input_str_item_consign|| v_part_no||'^'||v_qty||'^'||v_ship_date||'^|';
					v_control_str_item_consign := v_control_str_item_consign||v_part_no||','||v_qty||','||v_lot_number||','||v_fg_location_id||','||'TBE'||','||'90800'||'|';
                
					v_count_item                 := v_count_item + 1;
                
                IF (v_count_item >= 20) THEN
                
                  	  v_input_str_item_consign := v_input_str_item_consign||'~5001^5004|';
                    
                   	   gm_pkg_op_process_prod_request.gm_sav_initiate_item (NULL, CURRENT_DATE, 50618, NULL, 40021, v_dist_id, 50625, V_USER, 4120, NULL, NULL, 15, V_USER, v_input_str_item_consign, 50060, '',CURRENT_DATE, v_req_id, v_out_cn_id, NULL) ;
                    
                 	   gm_pkg_cm_shipping_trans.gm_sav_shipping (v_req_id, '50184', '4120', v_dist_id, '5001', '5004', NULL,'0', NULL, v_user, v_shipid, NULL, NULL);
                  	  
                    -- controlling the Item consignment
                    	gm_pkg_op_item_control_txn.gm_sav_item_control_main (v_out_cn_id, v_strTxnType, v_strRelVerFl,v_control_str_item_consign, v_strLocType, V_USER);
                    	
                    	gm_pkg_cm_shipping_trans.gm_sav_shipout (v_out_cn_id, '50181', '4120', v_dist_id, '5001', '5004',v_out_cn_id|| ':FSWH', 0, 0, '303043', 0, v_shipid, NULL, v_shipflag);
                    	v_count_item               := 0;
                    	v_input_str_item_consign   := NULL;
                    	v_control_str_item_consign := NULL;
                    	v_shipid                   := NULL;
                        
                    	UPDATE t504_consignment SET C504_LAST_UPDATED_BY = v_user, C504_LAST_UPDATED_DATE = CURRENT_DATE, c504_comments ='FSWH inventory update for the distributor: '||v_dist_id
                      	WHERE c504_consignment_id = v_out_cn_id;
                      
                        UPDATE MY_TEMP_KEY_VALUE_loose_item SET    MY_TEMP_TXN_VALUE= v_out_cn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
                    	
                      	dbms_output.put_line ('FSWH inventory update for the Distributor '||v_dist_id);
                END IF;
            END LOOP;
            
            IF (v_input_str_item_consign IS NOT NULL) THEN
            
                v_input_str_item_consign := v_input_str_item_consign||'~5001^5004|';
                
                gm_pkg_op_process_prod_request.gm_sav_initiate_item (NULL, CURRENT_DATE, 50618, NULL, 40021, v_dist_id,50625, V_USER, 4120, NULL, NULL, 15, V_USER, v_input_str_item_consign, 50060, '', CURRENT_DATE,v_req_id, v_out_cn_id, NULL) ;
              				      
                gm_pkg_cm_shipping_trans.gm_sav_shipping (v_req_id, '50184', '4120', v_dist_id, '5001', '5004', NULL, '0', NULL, v_user, v_shipid, NULL, NULL) ;
                
                -- controlling the Item consignment
                gm_pkg_op_item_control_txn.gm_sav_item_control_main (v_out_cn_id, v_strTxnType, v_strRelVerFl,v_control_str_item_consign, v_strLocType, V_USER) ;
                
                gm_pkg_cm_shipping_trans.gm_sav_shipout (v_out_cn_id, '50181', '4120', v_dist_id, '5001', '5004',v_out_cn_id|| ':FSWH', 0, 0, '303043', 0, v_shipid, NULL, v_shipflag) ;
                
                 UPDATE t504_consignment
                 SET C504_LAST_UPDATED_BY = v_user, C504_LAST_UPDATED_DATE = CURRENT_DATE, c504_comments ='FSWH inventory update for the distributor: '||v_dist_id   WHERE c504_consignment_id = v_out_cn_id;
                  
                 UPDATE MY_TEMP_KEY_VALUE_loose_item SET MY_TEMP_TXN_VALUE= v_out_cn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
                    
                 dbms_output.put_line ('FSWH inventory updated for the Distributor: '||v_dist_id);
                 
                 v_dist_id                  := NULL;
                 v_count_item               := 0;
                 v_input_str_item_consign   := NULL;
                 v_control_str_item_consign := NULL;
                 v_shipid                   := NULL;
            
			END IF;
                    
                 UPDATE JP_LOANER_INV_STG T1 SET set_loaded_fl='IAFG Initiated',T1.NEW_TXN_ID = (SELECT T2.MY_TEMP_TXN_VALUE FROM MY_TEMP_KEY_VALUE_loose_item T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
			     WHERE T1.PKEY IN (SELECT T2.MY_TEMP_TXN_ID FROM MY_TEMP_KEY_VALUE_loose_item T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
			     AND SET_LOADED_FL='IAFG Pending';
	      
		-- COMMIT;
	  	   		 	   
		    EXCEPTION WHEN OTHERS then
		    
		    v_dist_id                  := NULL;
            v_count_item               := 0;
            v_input_str_item_consign   := NULL;
            v_control_str_item_consign := NULL;
            v_shipid                   := NULL;
            
            DBMS_OUTPUT.PUT_LINE('FSWH inventory error '||v_part_no||SQLERRM);
           -- ROLLBACK;
   	      end;
   	      
    END LOOP;
END;	
END tmp_gm_fswh_load;

 
 END it_tmp_gm_Inventory_load;
/
