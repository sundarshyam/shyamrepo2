/* Formatted on 2011/08/09 17:27 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_cogs_update.bdy"

CREATE OR REPLACE PACKAGE BODY it_pkg_cogs_update
IS
--

	/*********************************************************
	* Description : This procedure is used to corect COGS Error
	*********************************************************/
	PROCEDURE gm_sav_correct_cogs_err (
		p_help_desk_id	 IN   t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_txn_id		 IN   t822_costing_error_log.c822_txn_id%TYPE
	  , p_pnum			 IN   t822_costing_error_log.c205_part_number_id%TYPE
	  , p_ftype 		 IN   t822_costing_error_log.c822_from_costing_type%TYPE
	  , p_fix_status	 IN   t822_costing_error_log.c822_flag_fix_status%TYPE	 -- Type C, F Cancel/Fix
	)
	AS
		CURSOR errorlog_cur
		IS
			SELECT c822_costing_error_log_id errid, c205_part_number_id pnum, c822_txn_id txnid, c822_qty qty
				 , c822_from_costing_type fctype, c822_to_costing_type tctype, c822_created_by cby
				 , c822_created_date cdate, c822_fixed_by fby, c822_fixed_date fdate, c901_posting_type ptype
				 , c810_party_id partyid
			  FROM t822_costing_error_log
			 WHERE c822_txn_id = p_txn_id
			   AND c822_flag_fix_status IS NULL   -- Type C, F Cancel/Fix
			   AND c205_part_number_id = DECODE (p_pnum, NULL, c205_part_number_id, p_pnum)
			   AND (c822_from_costing_type = NVL (p_ftype, 99999) OR c822_from_costing_type IS NULL);

		v_user_id	   NUMBER;
		v_cnt		   NUMBER := 0;
		v_txn_id	   t822_costing_error_log.c822_txn_id%TYPE;
	--
	BEGIN
		IF p_txn_id IS NULL OR p_fix_status IS NULL OR (p_ftype IS NULL AND p_fix_status != 'C')
		THEN
			raise_application_error ('-20085', 'Pls enter transaction Id/Status Flag/From Costing Type.');
		END IF;

		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		FOR errorlog IN errorlog_cur
		LOOP
			IF (errorlog.ptype IS NOT NULL OR p_fix_status = 'C')
			THEN
				v_txn_id	:= errorlog.txnid;
				DBMS_OUTPUT.put_line ('p_fix_status: ' || p_fix_status);
				gm_pkg_cogs_update.gm_cogs_correction (errorlog.errid, 30301, p_fix_status);
				it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Part Number: ' || errorlog.pnum);
				v_cnt		:= v_cnt + 1;
			END IF;
		END LOOP;

		--

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'TXN ID: ' || v_txn_id);
		DBMS_OUTPUT.put_line ('No. of row(s) updated: ' || v_cnt || ' Txn Id: ' || v_txn_id);

		IF (v_cnt = 0)
		THEN
			ROLLBACK;
			raise_application_error ('-20085', 'No record  found to be updated.');
		ELSE
			COMMIT;
		END IF;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_correct_cogs_err;
	
	/*********************************************************
	* Description : This procedure is used to Close the non costing COGS error
	*********************************************************/
	PROCEDURE gm_close_non_cost_cogs_error (
		p_help_desk_id	 IN   t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_division_id	 IN   t202_project.c1910_division_id%TYPE
	  , p_pnum_str			 IN  VARCHAR2
	)AS
    v_user_id NUMBER;
    v_cnt     NUMBER := 0;
    CURSOR non_costing_cogs_err_cur
    IS
         SELECT t822.c822_costing_error_log_id err_log_id, t202.c1910_division_id div_id, t205.c205_part_number_id pnum
           FROM t205_part_number t205, t202_project t202, t822_costing_error_log t822
          WHERE t202.c202_project_id     = t205.c202_project_id
            AND t205.c205_part_number_id = t822.c205_part_number_id
            AND (t202.c1910_division_id  = p_division_id
            OR t205.C205_PART_NUMBER_ID IN
            (
                 select token from v_in_list
            ))
            AND t822.c901_fix_type IS NULL
            AND t822.c901_error_type = 102941; -- Non costing cogs error
    BEGIN
        -- Mandatory helpdesk to log executed user information
        it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL) ;
        v_user_id := it_pkg_cm_user.get_valid_user;
        -- to set the part number in context
        my_context.set_my_inlist_ctx (p_pnum_str) ;
        --
        FOR non_costing_error IN non_costing_cogs_err_cur
        LOOP
            --
            v_cnt := v_cnt + 1;
            --53007 Cancle
             UPDATE t822_costing_error_log
            SET c901_fix_type                 = 53007, c822_fixed_by = v_user_id, c822_fixed_date = SYSDATE
              WHERE c822_costing_error_log_id = non_costing_error.err_log_id;
            --
            DBMS_OUTPUT.put_line (v_cnt || ' Non Costing type to update '|| non_costing_error.err_log_id ||' Division '||
            non_costing_error.div_id ||' Part # '|| non_costing_error.pnum) ;
        END LOOP;
        --
        DBMS_OUTPUT.put_line (' Total rows to be updated '|| v_cnt) ;
    END gm_close_non_cost_cogs_error;
    
END it_pkg_cogs_update;
/
