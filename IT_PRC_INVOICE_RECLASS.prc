--@"C:\Database\DataCorrection\Script\IT_PRC_INVOICE_RECLASS.prc";

CREATE OR REPLACE PROCEDURE IT_PRC_INVOICE_RECLASS (
	p_invoiceid		  IN		 T503_INVOICE.C503_INVOICE_ID%TYPE
  , p_reclassifyto	  IN		 VARCHAR2
  , p_dateformat      IN		 VARCHAR2
  , p_userid		  IN		 T503_INVOICE.C503_CREATED_BY%TYPE
  )
 AS  
 
    v_void_fl        T503_INVOICE.c503_void_fl%TYPE;
    v_company_id     T503_INVOICE.C1900_COMPANY_ID%TYPE;
      
    BEGIN  
	    
	    IF p_invoiceid is NULL THEN
	   	 	GM_RAISE_APPLICATION_ERROR('-20999','401','Invoice Id');
	    END IF;
	    
	     IF p_reclassifyto is NULL THEN
	   	 	GM_RAISE_APPLICATION_ERROR('-20999','401','Reclassify Date');
	    END IF;
	    
	     IF p_dateformat is NULL THEN
	   	 	GM_RAISE_APPLICATION_ERROR('-20999','401','Date format');
	    END IF;
	    
	     IF p_userid is NULL THEN
	   	 	GM_RAISE_APPLICATION_ERROR('-20999','401','User Id');
	    END IF;
	    
	     IF (to_date(p_reclassifyto,p_dateformat) > CURRENT_DATE) THEN
	     	GM_RAISE_APPLICATION_ERROR('-20999','404','User Id');
	     END IF;
	    
    	BEGIN
	    	Select c503_void_fl,C1900_COMPANY_ID into v_void_fl,v_company_id from T503_INVOICE
	    	where C503_INVOICE_ID=p_invoiceid;
	    	EXCEPTION
		WHEN NO_DATA_FOUND 
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','403',p_invoiceid);
    	END; 
	
       
    	IF v_void_fl IS NULL THEN
    	
    		update T503_INVOICE set C503_LAST_UPDATED_BY=p_userid,C503_LAST_UPDATED_DATE=SYSDATE,C503_INVOICE_DATE= to_date(p_reclassifyto,p_dateformat) 
    		where C503_INVOICE_ID=p_invoiceid;
    
    		update t501_order set C501_ORDER_DATE= to_date(p_reclassifyto,p_dateformat), C501_SHIPPING_DATE= to_date(p_reclassifyto,p_dateformat), 
    		C501_ORDER_DATE_TIME= to_date(p_reclassifyto,p_dateformat) 
    		,C501_LAST_UPDATED_BY=p_userid,C501_LAST_UPDATED_DATE=SYSDATE 
    		where C503_INVOICE_ID=p_invoiceid AND C1900_COMPANY_ID=v_company_id;
        
    	ELSE 
    			GM_RAISE_APPLICATION_ERROR('-20999','402',p_invoiceid);
    	END IF;
    

    END IT_PRC_INVOICE_RECLASS;
/