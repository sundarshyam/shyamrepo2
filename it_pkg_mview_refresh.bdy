/* Formatted on 2011/01/20 14:53 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_mview_refresh.bdy"


CREATE OR REPLACE PACKAGE BODY it_pkg_mview_refresh
IS
--

	/*********************************************************
	* Description : This procedure is used to refresh the v700 view	
	*********************************************************/
	PROCEDURE gm_mview_refresh 
	AS
	BEGIN
		DBMS_MVIEW.REFRESH ('v700_territory_mapping_detail');
	END gm_mview_refresh;
END it_pkg_mview_refresh;	
/
