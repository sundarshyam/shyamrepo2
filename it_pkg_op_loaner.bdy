/* Formatted on 2011/01/31 15:40 (Formatter Plus v4.8.0) */
-- @"C:\Data Correction\Script\it_pkg_op_loaner.bdy";
--T914_HELPDESK_LOG

CREATE OR REPLACE PACKAGE BODY it_pkg_op_loaner
IS
--
/***************************************************
   * Description : This procedure is used call the allocation logic on a setid
   *********************************************************/
	PROCEDURE gm_sav_allocate (
		p_help_desk_id	 t102_user_login.c102_login_username%TYPE
	  , p_set_id		 t208_set_details.c207_set_id%TYPE
	)
	AS
		v_user_dept_id t101_user.c901_dept_id%TYPE;
		v_party_group_id t101_party.c1500_group_id%TYPE;
		v_user_id	   NUMBER;
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id
											 , 'calling loaner allocation logic on set id ' || p_set_id);
		gm_pkg_op_loaner.gm_sav_allocate (p_set_id, 4127, v_user_id);	--product loaner
		   --
		-- gm_cm_sav_helpdesk_log (p_help_desk_id, 'Target user ' || p_target_user || ' Completed sucessfully !.... ');
		--
		DBMS_OUTPUT.put_line ('allocated Completed sucessfully !.... ');
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_allocate;

/***************************************************
   * Description : This procedure is used swap loaner
   *********************************************************/
	PROCEDURE gm_sav_loaner_swap (
		p_help_desk_id	 t102_user_login.c102_login_username%TYPE
	  , p_source_cn 	 t504_consignment.c504_consignment_id%TYPE
	  , p_target_set	 t504_consignment.c207_set_id%TYPE
	)
	AS
		v_target_cn    t504_consignment.c504_consignment_id%TYPE;
		v_updated_by   t504_consignment.c504_last_updated_by%TYPE;
		v_status	   t504a_consignment_loaner.c504a_status_fl%TYPE;
		v_status_fl    t412_inhouse_transactions.c412_status_fl%TYPE;
--
	BEGIN
		-- check to see if the source is inactive, if it is , then error
		SELECT c504a_status_fl
		  INTO v_status
		  FROM t504a_consignment_loaner
		 WHERE c504_consignment_id = p_source_cn
		 for UPDATE;

		IF v_status = 60
		THEN
			raise_application_error ('-20085', 'This CN is inactive, can not swap');
		END IF;

		-- check if CN status changed when do inhouse transaction for inactive CN
		BEGIN
			SELECT c412_status_fl
			  INTO v_status_fl
			  FROM t412_inhouse_transactions
			 WHERE c412_status_fl =
					   (SELECT MIN (c412_status_fl)
						  FROM t412_inhouse_transactions
						 WHERE c412_ref_id = p_source_cn
						   AND c412_type NOT IN (50159, 50160, 50161, 50162, 100062)
						   AND c412_void_fl IS NULL)
			   AND c412_ref_id = p_source_cn
			   AND ROWNUM = 1;
		EXCEPTION	-- even if inhouse trans was not created , status has to be updated
			WHEN NO_DATA_FOUND
			THEN
				v_status_fl := 4;
		END;

		IF v_status_fl < 4
		THEN
			DBMS_OUTPUT.put_line ('Cannot move ' || p_source_cn
								  || ' to pending process as it has with open transactions.'
								 );
			raise_application_error ('-20999'
								   ,	'Cannot move '
									 || p_source_cn
									 || '  to pending process as it has with open transactions.'
									);
		END IF;

		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_updated_by := it_pkg_cm_user.get_valid_user;

		--Step 1: Create a mirror CN
		SELECT 'GM-CN-' || s504_consign.NEXTVAL
		  INTO v_target_cn
		  FROM DUAL;

		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Target Set ' || p_target_set);
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Source CN ' || p_source_cn);
		DBMS_OUTPUT.put_line (' ****Step 1: Create CN  ' || v_target_cn);

		INSERT INTO t504_consignment
					(c504_consignment_id, c701_distributor_id, c504_ship_date, c504_return_date, c504_comments
				   , c504_total_cost, c504_created_date, c504_created_by, c504_last_updated_date, c504_last_updated_by
				   , c504_status_fl, c504_void_fl, c704_account_id, c504_ship_to, c207_set_id, c504_ship_to_id
				   , c504_final_comments, c504_inhouse_purpose, c504_type, c504_delivery_carrier, c504_delivery_mode
				   , c504_tracking_number, c504_ship_req_fl, c504_verify_fl, c504_verified_date, c504_verified_by
				   , c504_update_inv_fl, qb_consignment_txnid, c504_reprocess_id, c504_master_consignment_id
				   , c520_request_id, c504_ship_cost, c704_location_account_id, c703_location_sales_rep_id, c504_hold_fl
					)
			 VALUES (v_target_cn, NULL, NULL, NULL, 'Converted to LN Set List from ' || p_source_cn
				   , NULL, SYSDATE, v_updated_by, NULL, NULL
				   , '4', NULL, NULL, NULL, p_target_set, NULL
				   , 'LOANER-3', NULL, 4127, NULL, NULL
				   , NULL, NULL, '1', SYSDATE, v_updated_by
				   , '1', NULL, NULL, p_source_cn
				   , NULL, NULL, NULL, NULL, NULL
					);

		--	DBMS_OUTPUT.put_line (' ****Step 1: Create a mirror CN');
		--Step 2: Create mirror CN Items
		INSERT INTO t505_item_consignment
					(c505_item_consignment_id, c504_consignment_id, c205_part_number_id, c505_control_number
				   , c505_item_qty, c505_item_price)
			SELECT s504_consign_item.NEXTVAL, v_target_cn, pnum, 'NOC#', qty, price
			  FROM (SELECT	 t205.c205_part_number_id pnum, t205.c205_part_num_desc pdesc
						   , cons.qty - NVL (miss.qty, 0) qty, t205.c205_loaner_price price, NVL (miss.qty, 0) missqty
						   , NVL (miss.qtyrecon, 0) qtyrecon, cons.qty - NVL (miss.qtyrecon, 0) cogsqty
						   , NVL (setdetails.qty, 0) setlistqty
						FROM t205_part_number t205
						   , (SELECT   t505.c205_part_number_id, SUM (t505.c505_item_qty) qty
								  FROM t505_item_consignment t505
								 WHERE t505.c504_consignment_id = p_source_cn AND t505.c505_void_fl IS NULL
							  GROUP BY t505.c205_part_number_id) cons
						   , (SELECT   t413.c205_part_number_id
									 , SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0)) qty
									 , SUM (DECODE (NVL (t413.c413_status_fl, 'Q'), 'Q', 0, t413.c413_item_qty)
										   ) qtyrecon
								  FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
								 WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
								   AND t412.c412_ref_id = p_source_cn
								   AND t412.c412_void_fl IS NULL
								   AND t413.c413_void_fl IS NULL
							  GROUP BY t413.c205_part_number_id) miss
						   , (SELECT t208.c205_part_number_id, t208.c208_set_qty qty
								FROM t208_set_details t208, t504_consignment t504
							   WHERE t208.c208_void_fl IS NULL
								 AND t208.c208_inset_fl = 'Y'
								 AND t504.c207_set_id = t208.c207_set_id
								 AND t504.c504_void_fl IS NULL
								 AND t504.c504_consignment_id = p_source_cn) setdetails
					   WHERE t205.c205_part_number_id = cons.c205_part_number_id
						 AND t205.c205_part_number_id = miss.c205_part_number_id(+)
						 AND t205.c205_part_number_id = setdetails.c205_part_number_id(+)
					UNION ALL
					SELECT	 t208.c205_part_number_id pnum, get_partnum_desc (t208.c205_part_number_id) pdesc, 0 qty
						   , 0 price, 0 missqty, 0 qtyrecon, 0 cogsqty, t208.c208_set_qty qty
						FROM t208_set_details t208, t504_consignment t504
					   WHERE t208.c208_void_fl IS NULL
						 AND t208.c208_inset_fl = 'Y'
						 AND t504.c207_set_id = t208.c207_set_id
						 AND t504.c504_void_fl IS NULL
						 AND t504.c504_consignment_id = p_source_cn
						 AND t208.c205_part_number_id NOT IN (
											  SELECT t505.c205_part_number_id
												FROM t505_item_consignment t505
											   WHERE t505.c504_consignment_id = p_source_cn
													 AND t505.c505_void_fl IS NULL)
					ORDER BY 1)
			 WHERE qty != 0 OR setlistqty != 0;

		--	DBMS_OUTPUT.put_line (' *******Step 2: Create mirror CN Items');
		--Step 3: Create mirror of T504A Loaner record
		INSERT INTO t504a_consignment_loaner
					(c504_consignment_id, c504a_loaner_dt, c504a_etch_id, c504a_expected_return_dt, c504a_void_fl
				   , c504a_status_fl, c504a_loaner_create_date, c504a_loaner_create_by, c504a_last_updated_date
				   , c504a_last_updated_by, c504a_available_date)
			SELECT v_target_cn, c504a_loaner_dt, c504a_etch_id, c504a_expected_return_dt, c504a_void_fl
				 , c504a_status_fl, c504a_loaner_create_date, c504a_loaner_create_by, c504a_last_updated_date
				 , v_updated_by, c504a_available_date
			  FROM t504a_consignment_loaner
			 WHERE c504_consignment_id = p_source_cn;

		--	DBMS_OUTPUT.put_line (' *******Step 3: Create mirror of T504A Loaner record');
		--Step 4: Inactive in 504 and 504A
		UPDATE t504a_consignment_loaner
		   SET c504a_status_fl = 60
			 , c504a_last_updated_by = v_updated_by
			 , c504a_last_updated_date = SYSDATE
			 , c504a_etch_id =
				   DECODE (SUBSTR (c504a_etch_id, 1, 6)
						 , 'LOANER', REPLACE (TO_CHAR (c504a_etch_id), 'LOANER', 'TRANSFER')
						 , c504a_etch_id
						  )
		 WHERE c504_consignment_id = p_source_cn;

		--	DBMS_OUTPUT.put_line (' *******Step 4: Inactive in 504 and 504A');
		--Step 5: Comments
		INSERT INTO t902_log
					(c902_log_id, c902_ref_id, c902_comments, c902_type
				   , c902_created_by, c902_created_date, c902_last_updated_by, c902_last_updated_date, c902_void_fl
					)
			 VALUES (s507_log.NEXTVAL, v_target_cn, 'Converted to LN Set List from ' || p_source_cn, 1222
				   , v_updated_by, SYSDATE, NULL, NULL, NULL
					);

		--	DBMS_OUTPUT.put_line (' *******Step 5: Comments');
		--Step 6: Add record in T504B_LOANER_SWAP_LOG
		INSERT INTO t504b_loaner_swap_log
					(c504d_loaner_swap_log_id, c504_consignment_id, c901_type, c504d_ref_id, c504d_void_fl
				   , c504d_created_by, c504d_created_date
					)
			 VALUES (s504b_loaner_swap_log.NEXTVAL, p_source_cn, 100100, v_target_cn, NULL
				   , v_updated_by, SYSDATE
					);

		--	DBMS_OUTPUT.put_line (' *******Step 6: Add record in T504B_LOANER_SWAP_LOG');
		--Step 7: Add record in T504A_LOANER_TXN
		INSERT INTO t504a_loaner_transaction
					(c504a_loaner_transaction_id, c504_consignment_id, c504a_loaner_dt, c504a_expected_return_dt
				   , c504a_return_dt, c504a_void_fl, c901_consigned_to, c504a_consigned_to_id, c504a_created_by
				   , c504a_created_date, c504a_last_updated_date, c504a_last_updated_by, c703_sales_rep_id
				   , c704_account_id, c504a_is_loaner_extended, c504a_is_replenished, c504a_parent_loaner_txn_id
				   , c526_product_request_detail_id, c504a_processed_date, c504a_transf_ref_id, c504a_transf_date
				   , c504a_transf_by, c504a_transf_email_flg)
			SELECT s504a_loaner_trans_id.NEXTVAL, v_target_cn, c504a_loaner_dt, c504a_expected_return_dt
				 , c504a_return_dt, c504a_void_fl, c901_consigned_to, c504a_consigned_to_id, c504a_created_by
				 , c504a_created_date, c504a_last_updated_date, c504a_last_updated_by, c703_sales_rep_id
				 , c704_account_id, c504a_is_loaner_extended, c504a_is_replenished, c504a_parent_loaner_txn_id
				 , c526_product_request_detail_id, c504a_processed_date, c504a_transf_ref_id, c504a_transf_date
				 , c504a_transf_by, c504a_transf_email_flg
			  FROM t504a_loaner_transaction
			 WHERE c504a_loaner_transaction_id = (SELECT MAX (TO_NUMBER (c504a_loaner_transaction_id))
													FROM t504a_loaner_transaction
												   WHERE c504_consignment_id = p_source_cn AND c504a_void_fl IS NULL);

		--	DBMS_OUTPUT.put_line (' *******Step 7: Add record in T504A_LOANER_TXN');
		DBMS_OUTPUT.put_line ('Update Sucessfully New CN # **** ' || v_target_cn);
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Update Sucessfully New CN # **** ' || v_target_cn);
		--
		--Step 8: Update new CN# and Set # in T5010
           update t5010_tag set c5010_last_updated_trans_id = v_target_cn,c207_set_id =p_target_set,c5010_last_updated_date=sysdate,
                                c5010_last_updated_by=v_updated_by where C5010_LAST_UPDATED_TRANS_ID = p_source_cn;
         --DBMS_OUTPUT.put_line ('Update new CN # in tag ****' || v_target_cn || 'Source CN# -' ||p_source_cn);
         --

		COMMIT;
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_loaner_swap;

	/*********************************************************
	 * Description : This procedure is used call to move the loaner from pending verification status to  pending process
	 this is used only when all transactions are verified and the loaners is showing as pending verification
	 *********************************************************/
	PROCEDURE gm_sav_verf_process (
		p_help_desk_id	 t102_user_login.c102_login_username%TYPE
	  , p_conid 		 t504_consignment.c504_consignment_id%TYPE
	)
	AS
		v_user_id	   NUMBER;
		v_status_fl    NUMBER;
	BEGIN
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id
											 , 'moving from pending verification to pending process' || p_conid
											  );

		BEGIN
			SELECT c412_status_fl
			  INTO v_status_fl
			  FROM t412_inhouse_transactions
			 WHERE c412_status_fl =
					   (SELECT MIN (c412_status_fl)
						  FROM t412_inhouse_transactions
						 WHERE c412_ref_id = p_conid
						   AND c412_type NOT IN (50159, 50160, 50161, 50162, 100062)
						   AND c412_void_fl IS NULL)
			   AND c412_ref_id = p_conid
			   AND ROWNUM = 1;
		EXCEPTION	-- even if inhouse trans was not created , status has to be updated
			WHEN NO_DATA_FOUND
			THEN
				v_status_fl := 4;
		END;

		IF v_status_fl >= 4
		THEN
			UPDATE t504a_consignment_loaner
			   SET c504a_status_fl = 50
				 , c504a_last_updated_date = SYSDATE
				 , c504a_last_updated_by = v_user_id
			 WHERE c504_consignment_id = p_conid;
		ELSE
			DBMS_OUTPUT.put_line ('Cannot move ' || p_conid || ' to pending process as it has with open transactions.');
			raise_application_error ('-20999'
								   , 'Cannot move' || p_conid
									 || '  to pending process as it has with open transactions.'
									);
		END IF;

		DBMS_OUTPUT.put_line (p_conid || ' moved to pending process sucessfully !.... ');
		COMMIT;
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_verf_process;

	/*********************************************************
	* Description : This procedure is used release the hold flag on the loaner if a loaner is in
	pending verification process and all child ln's are verified
	*********************************************************/
	PROCEDURE gm_sav_rel_hold (
		p_help_desk_id	 t102_user_login.c102_login_username%TYPE
	  , p_conid 		 t504_consignment.c504_consignment_id%TYPE
	)
	AS
		v_user_id	   NUMBER;
		v_status_fl    NUMBER;
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'releasing the hold flag' || p_conid);

		SELECT	   c504a_status_fl
			  INTO v_status_fl
			  FROM t504a_consignment_loaner
			 WHERE c504_consignment_id = p_conid AND c504a_void_fl IS NULL
		FOR UPDATE;

		IF v_status_fl != '10'	 -- pending shipping
		THEN
			-- Loaner is not in valid status to perform this operation.
			raise_application_error (-20335, '');
		END IF;

		UPDATE t504_consignment
		   SET c504_hold_fl = NULL
			 , c504_last_updated_date = SYSDATE
			 , c504_last_updated_by = v_user_id
		 WHERE c504_consignment_id = p_conid;

		DBMS_OUTPUT.put_line (p_conid || 'released sucessfully !.... ');
		COMMIT;
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_rel_hold;
	/*********************************************************
	* Description : This procedure is used to update associate rep id as null	
	*********************************************************/
	PROCEDURE gm_sav_associate_rep_id
	(
       p_consign_id             IN t504a_loaner_transaction.c504_consignment_id%TYPE,
	   p_request_id				IN t525_product_request.c525_product_request_id%TYPE,
	   p_userId				    IN t525_product_request.c525_created_by%TYPE	  
	)
	AS
	    v_request_id  t526_product_request_detail.c525_product_request_id%TYPE;
	BEGIN
		
		IF p_request_id IS NULL AND p_consign_id IS NOT NULL THEN
		BEGIN
		 SELECT t526.c525_product_request_id INTO v_request_id
	   		FROM t504a_loaner_transaction t504a, t526_product_request_detail t526
	  		WHERE t504a.c504_consignment_id            = p_consign_id
	    	 AND t504a.c504a_return_dt               IS NULL
	    	 AND t504a.c504a_void_fl                 IS NULL
	   		 AND t504a.c703_ass_rep_id               IS NOT NULL
	    	 AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
	    	 AND t526.c526_void_fl                   IS NULL;
	    EXCEPTION WHEN NO_DATA_FOUND
	      	 	  THEN
	                         DBMS_OUTPUT.PUT_LINE('Please provide request id');
	              RETURN;
	              END;
		END IF;
	
		-- updating associate rep id is null for passing req id
               UPDATE t525_product_request
					SET c703_ass_rep_id             = NULL, c525_last_updated_by = p_userId, c525_last_updated_date = SYSDATE
					  WHERE c525_product_request_id = p_request_id
					    AND c525_void_fl           IS NULL;
				    
				 UPDATE t504a_loaner_transaction
					SET c703_ass_rep_id                     = NULL, c504a_last_updated_by = p_userId, c504a_last_updated_date = SYSDATE
					  WHERE c526_product_request_detail_id IN
					    (
					         SELECT c526_product_request_detail_id
					           FROM t526_product_request_detail
					          WHERE c525_product_request_id = p_request_id
					            AND c526_void_fl           IS NULL
					    )
					    AND c504a_void_fl IS NULL; 
				    
				    UPDATE t9201_charge_details
						SET c703_ass_rep_id        = NULL, c9201_updated_by = p_userId, c9201_updated_date = sysdate
						  WHERE c9201_void_fl     IS NULL
						    AND c9200_incident_id IN
						    (
						         SELECT t9200.c9200_incident_id
						           FROM t504a_loaner_transaction t504a, t526_product_request_detail t526, t9200_incident t9200
						          WHERE t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
						            AND t526.c525_product_request_id         = p_request_id
						            AND t526.c526_void_fl                   IS NULL
						            AND t504a.c504a_void_fl                 IS NULL
						            AND t504a.c504a_loaner_transaction_id    = t9200.c9200_ref_id
						            AND t9200.c9200_void_fl                 IS NULL
						    ) ;
			  DBMS_OUTPUT.PUT_LINE('Associate rep id updated as NULL');
			  EXCEPTION WHEN OTHERS
      	 	  THEN
                         DBMS_OUTPUT.PUT_LINE('Error' || SQLERRM);
              RETURN;
END gm_sav_associate_rep_id;


	/*********************************************************
	* Description : This procedure is to void duplicate loaner 
	* transaction with return date is null 
	* Author: Xun
	*********************************************************/
	PROCEDURE gm_upd_loaner_trans (
	  p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE 
	)
	AS
	  v_user_id	   NUMBER;
	  v_trans_id	   t504a_loaner_transaction.C504A_LOANER_TRANSACTION_ID%TYPE;
	  
	  CURSOR loaner_cur 
	    IS 
	     SELECT c504_consignment_id cnid
		      , c526_product_request_detail_id dtlid
		   FROM t504a_loaner_transaction
		  WHERE c504a_return_dt IS NULL
		    AND c504a_void_fl     IS NULL
	   GROUP BY c504_consignment_id, c526_product_request_detail_id
   HAVING COUNT (c504_consignment_id) >1 ; 
   
	BEGIN
	  -- Mandatory helpdesk to log executed user information
	  it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);

	  v_user_id	:= it_pkg_cm_user.get_valid_user;
 
	  FOR loaner_val IN loaner_cur 
	    LOOP 
                BEGIN  
				 SELECT c504a_loaner_transaction_id
				   INTO v_trans_id
				   FROM t504a_loaner_transaction
				  WHERE c504_consignment_id = loaner_val.cnid
					AND c526_product_request_detail_id = loaner_val.dtlid
					AND c504a_return_dt IS NULL
					AND c504a_void_fl IS NULL
					AND ROWNUM = 1
			    ORDER BY c504a_loaner_transaction_id;

		        UPDATE t504a_loaner_transaction
		           SET c504a_void_fl = 'Y',
		               c504a_last_updated_by = v_user_id,
		               c504a_last_updated_date = SYSDATE
		         WHERE c504a_loaner_transaction_id = v_trans_id;
  
		  -- logs
				it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'CN ID: ' || loaner_val.cnid);
		  
				DBMS_OUTPUT.put_line (SQL%ROWCOUNT || ' Row(s) are updated. ');
		  
		  COMMIT;
		  --
	      EXCEPTION 
                WHEN OTHERS 
                  THEN 
                    DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM); 
                    ROLLBACK; 
	      END; 
	  END LOOP; 
	EXCEPTION
	  WHEN NO_DATA_FOUND THEN
	    DBMS_OUTPUT.put_line ('Error Occurred : ' || SQLERRM);
	END gm_upd_loaner_trans;
  
  /*********************************************************
	* Description : This procedure is to sync company code of t412 & t504a
  *               with the company in t526
  * Date: 08/Nov/2017
	* Author: Matt
	*********************************************************/
PROCEDURE gm_upd_loaner_company (
  P_LOANER_ID     IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
  p_help_desk_id	IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
  p_userId				IN t412_inhouse_transactions.c412_last_updated_by%TYPE
) 
AS 
v_request_id NUMBER;
v_req_company_id NUMBER;
v_loanr_company_id NUMBER;

BEGIN
    BEGIN
    SELECT t504a.c526_product_request_detail_id,  t526.c1900_company_id, t504a.c1900_company_id consign_country
    INTO v_request_id,v_req_company_id,v_loanr_company_id 
    FROM t412_inhouse_transactions T412
    JOIN t504a_loaner_transaction t504a
    ON t412.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
    JOIN t526_product_request_detail t526
    ON t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
    WHERE c412_inhouse_trans_id             = P_LOANER_ID;
    EXCEPTION
    WHEN NO_DATA_FOUND 
    THEN
          v_request_id := NULL;
    END;
    
    IF v_req_company_id != v_loanr_company_id OR v_request_id IS NULL
    THEN
      -- Mandatory helpdesk to log executed user information
	  it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
    UPDATE t412_inhouse_transactions 
    SET c1900_company_id = v_req_company_id, 
      c412_last_updated_by = p_userId, 
      c412_last_updated_date = CURRENT_DATE 
    WHERE c412_inhouse_trans_id = P_LOANER_ID; 
    
    UPDATE t504a_loaner_transaction 
    SET c1900_company_id = v_req_company_id, 
      c504a_last_updated_by = p_userId, 
      c504a_last_updated_date = CURRENT_DATE 
    WHERE c526_product_request_detail_id = v_request_id; 
    
     -- closing helpdesk log
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Sync Company for Loaner ID: ' || P_LOANER_ID);
  
  END IF;

END gm_upd_loaner_company;

END it_pkg_op_loaner;
/
