/* Formatted on 2011/02/25 14:29 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_rules.bdy"

CREATE OR REPLACE PACKAGE BODY it_pkg_rules
IS
--
	/******************************************************************
	* Description : This procedure is used to  add parts to rule table
	* to skip control number validation
	****************************************************************/
	PROCEDURE gm_sav_skip_contrl_valid (
		p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_part_id		 t906_rules.c906_rule_id%TYPE
	)
	AS
		v_count 	   NUMBER;
		v_user_id	   NUMBER;
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		---Check if the part already existed in the rule table
		SELECT COUNT (1)
		  INTO v_count
		  FROM t906_rules t906
		 WHERE t906.c906_rule_id = p_part_id AND t906.C906_RULE_GRP_ID = 'SKIPCONTROLNUMVAL';

		IF (v_count > 0)
		THEN
			raise_application_error ('-20085', 'Part Number ' || p_part_id || ' already existed in the rule table.');
		ELSE
			INSERT INTO t906_rules
						(c906_rule_seq_id, c906_rule_id, c906_rule_desc, c906_rule_value, C906_RULE_GRP_ID
					   , c906_created_date, c906_created_by)
				SELECT s906_rule.NEXTVAL, c205_part_number_id, 'Skip Control number validation', 'Y'
					 , 'SKIPCONTROLNUMVAL', SYSDATE, v_user_id
				  FROM t205_part_number
				 WHERE c205_part_number_id = p_part_id;
		END IF;

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id
											 ,	  'Part Number ID: '
											   || p_part_id
											   || ' added to skip Control number validation'
											  );
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_skip_contrl_valid;

	/******************************************************************
	* Description : This procedure is used to Insert/ Update  value to the 
	* T906_RULES table.
	****************************************************************/

	PROCEDURE gm_sav_rule_values (
		p_help_desk_id	 IN t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_rule_id		 IN t906_rules.c906_rule_id%TYPE	  
	  , p_rule_value	 IN t906_rules.c906_rule_value%TYPE
	  , p_rule_grp		 IN t906_rules.C906_RULE_GRP_ID%TYPE
	)
	AS
		v_count 	   NUMBER;
		v_user_id	   NUMBER;
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		---Check if the part already existed in the rule table
		SELECT COUNT (1)
		  INTO v_count
		  FROM t906_rules t906
		 WHERE t906.c906_rule_id = p_rule_id AND t906.C906_RULE_GRP_ID = p_rule_grp AND C906_VOID_FL IS NULL;

		IF v_count >1
		THEN
			raise_application_error ('-20085', ' Too Many Record Exist for this combination : c906_rule_id ' || p_rule_id || ' C906_RULE_GRP_ID:'||p_rule_grp);
		
		ELSIF (v_count =1)
		THEN
			UPDATE 
				t906_rules 
			SET 
				c906_rule_value=p_rule_value, 
				C906_LAST_UPDATED_DATE = sysdate, 
				C906_LAST_UPDATED_BY = v_user_id  
			where
				c906_rule_id = p_rule_id AND 
				C906_RULE_GRP_ID = p_rule_grp AND 
				C906_VOID_FL IS NULL;
			
		ELSE
			INSERT INTO t906_rules
				(c906_rule_seq_id, c906_rule_id, 
				 c906_rule_value, C906_RULE_GRP_ID,
				 c906_created_date, c906_created_by)
			VALUES (
				s906_rule.NEXTVAL, 
				p_rule_id, 
				p_rule_value,
				p_rule_grp, 
				SYSDATE, 
				v_user_id);
		END IF;

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id
							,' Rule Table is Modified for '
							||' RULE ID :' ||
							p_rule_id
							|| ' RULE VALUE :'||
							p_rule_value || 'RULE GRP: '
							|| p_rule_grp
							);
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_rule_values;
END it_pkg_rules;
/
