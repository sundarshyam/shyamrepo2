/* Formatted on 2011/01/07 10:34 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_op_lot_exp.bdy"

CREATE OR REPLACE PACKAGE BODY it_pkg_op_lot_exp
IS
--
	PROCEDURE gm_sav_exp_date (
	    p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_exp_date		 VARCHAR2
	  , p_lot_no		 t2550_part_control_number.c2550_control_number%TYPE 
	)
	AS
		v_partnum	   t408_dhr.c205_part_number_id%TYPE;
		v_user_id	   NUMBER; 
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;
		
		 UPDATE t408_dhr
		  SET c2550_expiry_date = to_date(p_exp_date,'mm/dd/yyyy'), 
		   c408_last_updated_by = v_user_id, 
		   c408_last_updated_date = SYSDATE 
		  WHERE c408_control_number IN ( p_lot_no); 	

		UPDATE t2550_part_control_number 
		SET c2550_expiry_date = to_date(p_exp_date,'mm/dd/yyyy'), 
		c2550_last_updated_by = v_user_id, 
		c2550_last_updated_date = SYSDATE 
		WHERE c2550_control_number IN ( p_lot_no); 
 
		-- logs 
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'New expiry date: ' || p_exp_date); 
		
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_exp_date;

	 
END it_pkg_op_lot_exp;
/
