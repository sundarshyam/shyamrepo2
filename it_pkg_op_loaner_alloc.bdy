--@"C:\DataCorrection\Script\it_pkg_op_loaner_alloc.bdy";
CREATE OR REPLACE PACKAGE BODY it_pkg_op_loaner_alloc
IS
	PROCEDURE it_sav_dealloc_alloc(
	   p_alloc_cn	 IN VARCHAR2
	 , p_dealloc_cn  IN VARCHAR2
	 )
	AS
	  v_planned_ship_date 		DATE;
	  v_status 			NUMBER;
	  p_userid 			NUMBER := 303149;
	  v_cn_dealloc_status 		NUMBER;
	  v_cn_alloc_status   		NUMBER;
	  v_consigned_to 	  	NUMBER;
	  v_dist_id 		  	NUMBER;
	  v_request_detail_id 		NUMBER;
	  v_request_ids 	  	VARCHAR2(10000);          
	  v_parent_req_id 	 	t525_product_request.c525_product_request_id%TYPE;
	  v_loaner_dt	  	  	t526_product_request_detail.c526_required_date%TYPE;
	  v_acc_id 		  	t525_product_request.c704_account_id%TYPE;
	  v_rep_id 		  	t525_product_request.c703_sales_rep_id%TYPE;
	  v_exp_return_dt 	  	t526_product_request_detail.c526_exp_return_date%TYPE;
	  v_req_by_type   	  	t525_product_request.c901_request_by_type%TYPE;
	  v_req_for_id 	  	 	t525_product_request.c525_request_for_id%TYPE;
	  v_type 		  	t525_product_request.c901_request_for%TYPE;
	  v_comments			VARCHAR2(400);
	  v_out				VARCHAR2(100);
	  p_req_det_id			NUMBER;
	  v_consign_id			VARCHAR2(20);
	BEGIN

	   BEGIN	
		   SELECT C504_CONSIGNMENT_ID
		     INTO v_consign_id
		     FROM T504A_CONSIGNMENT_LOANER T504a
		    WHERE T504a.c504_consignment_id = p_dealloc_cn;

		   SELECT C504_CONSIGNMENT_ID
		     INTO v_consign_id
		     FROM T504A_CONSIGNMENT_LOANER T504a
		    WHERE T504a.c504_consignment_id = p_alloc_cn;

           EXCEPTION WHEN NO_DATA_FOUND THEN
		  raise_application_error('-20999', 'Entered CNs does not exist' );
	   END;


	   BEGIN
		 SELECT DECODE(c901_consigned_to,50170,4120,50172,4123), c526_product_request_detail_id
		   INTO v_consigned_to, p_req_det_id
	       FROM t504a_loaner_transaction t504a  
		  WHERE T504a.c504_consignment_id = p_dealloc_cn 
		    AND t504a.c504a_return_dt IS NULL
			AND t504a.c504a_void_fl IS NULL;	   				  
	   EXCEPTION WHEN NO_DATA_FOUND THEN
	   		raise_application_error('-20999', 'consigned to not exist');
	   END;
	  
		BEGIN           
		   SELECT c504a_status_fl
		     INTO v_cn_dealloc_status
		     FROM t504a_consignment_loaner t504a
		    where t504a.c504_consignment_id = p_dealloc_cn;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		   raise_application_error('-20999', 'v_cn_dealloc_status not exist');
		END;

	  IF v_cn_dealloc_status NOT IN (5, 10) THEN
		raise_application_error('-20999', 'Entered CN ' || p_dealloc_cn || ' was not it allocated status to de allocate ');
	  END IF;

	  
	    gm_pkg_op_loaner.gm_sav_deallocate(p_dealloc_cn, p_userid);
	    
	   BEGIN              
	     SELECT c504a_status_fl
	     INTO v_cn_alloc_status
	     FROM t504a_consignment_loaner t504a
	     where t504a.c504_consignment_id = p_alloc_cn;
	   EXCEPTION WHEN NO_DATA_FOUND THEN
	          raise_application_error('-20999', 'v_cn_alloc_status not exist');
	    END; 
	
		IF v_cn_alloc_status IN (5,10) -- Allocated / Pending Shipping
	    THEN
	    
	    BEGIN
		    SELECT c526_product_request_detail_id
		      INTO v_request_detail_id       
		      FROM t504a_loaner_transaction t504a
		     where t504a.c504_consignment_id = p_alloc_cn
		       AND t504a.c504a_return_dt is NULL
		       AND t504a.c504a_void_fl is NULL;
	    EXCEPTION WHEN NO_DATA_FOUND THEN
	    	raise_application_error('-20999', p_alloc_cn || ', ' || v_cn_alloc_status || ': No request detail id found.');
	    END;
	    
	    v_request_ids := v_request_ids || v_request_detail_id ;
	    
	    gm_pkg_op_loaner.gm_sav_deallocate(p_alloc_cn, p_userid);
	         
	    ELSIF v_cn_alloc_status = 20 THEN
	        raise_application_error('-20999', p_alloc_cn || ' -alloc cn - is already shipped out');  
	    ELSIF v_cn_alloc_status != 0 THEN    
	        raise_application_error('-20999', p_alloc_cn || ' - alloc cn - is not in open status to allocate to new request');  
	    END IF;
	    
	    BEGIN
	     SELECT t525.c525_product_request_id, t526.c526_required_date
	          , t525.c525_request_for_id, t525.c704_account_id
	          , t525.c703_sales_rep_id
	          , t526.c526_exp_return_date , t525.c901_request_by_type 
	          , t525.c525_request_for_id, t525.c901_request_for
	       INTO v_parent_req_id, v_loaner_dt
	       	  , v_dist_id, v_acc_id
	       	  , v_rep_id 
	       	  , v_exp_return_dt, v_req_by_type 
	       	  , v_req_for_id , v_type
	       FROM t525_product_request t525,t526_product_request_detail t526
	      WHERE t526.c526_product_request_detail_id = p_req_det_id
	        AND t525.c525_product_request_id = t526.c525_product_request_id
	        AND t526.c526_void_fl IS NULL
	        AND t525.c525_void_fl IS NULL;
	     EXCEPTION WHEN NO_DATA_FOUND THEN
	        raise_application_error('-20999','Request Details not found');
	     END;
	  
	          INSERT INTO t504a_loaner_transaction
			                  (c504a_loaner_transaction_id, c504_consignment_id,
			                   c504a_loaner_dt, c504a_expected_return_dt,
			                   c901_consigned_to, c504a_consigned_to_id,
			                   c504a_created_by, c504a_created_date, c703_sales_rep_id,
			                   c704_account_id, c526_product_request_detail_id
			                  )
			           VALUES (s504a_loaner_trans_id.NEXTVAL, p_alloc_cn,
			                   v_loaner_dt, v_exp_return_dt,
	                      	   v_consigned_to, v_dist_id,
			                   p_userid, SYSDATE, v_rep_id,
			                   v_acc_id, p_req_det_id
			                  );
	  
	      gm_pkg_op_loaner_allocation.gm_loaner_allocation_update(p_req_det_id, 'ADD', p_userid); -- reset allocated fl and planned ship date in t504a
	                
	       UPDATE t504a_consignment_loaner
	          SET c504a_status_fl = v_cn_dealloc_status,                                          
	              c504a_last_updated_date = SYSDATE,
	              c504a_last_updated_by = p_userid
	        WHERE c504_consignment_id = p_alloc_cn 
	          AND c504a_void_fl IS NULL;
	
	    UPDATE t526_product_request_detail
	         SET c526_status_fl = 20, -- allocated                                
	             c526_last_updated_by = p_userid,
	             c526_last_updated_date = SYSDATE
	       WHERE c526_product_request_detail_id = p_req_det_id
	         AND c526_void_fl IS NULL;
	 
	      --dist id needs to be updated when the product loaner (4127) request moves to allocated or pending shipping
	      IF v_type = 4127 THEN
		      UPDATE t504_consignment
		         SET c701_distributor_id = v_req_for_id,
		             c704_account_id = v_acc_id
		       WHERE c504_consignment_id = p_alloc_cn 
		         AND c504_void_fl IS NULL;
		       
				   -- Make an entry in t5050 for product loaners for the hand held device to pick (7 )/ put (58) 
			      IF v_status = 58 OR v_status = 7 
			       THEN
			       		gm_pkg_allocation.gm_ins_invpick_assign_detail(93602, p_alloc_cn, p_userid); -- 93602 - Loaner Set
			       END IF;
	      ELSIF v_type = 4119 THEN     
	       UPDATE t504_consignment
		      SET c701_distributor_id = DECODE(v_consigned_to, 50170, v_req_for_id, c701_distributor_id)
		        , c704_account_id = DECODE(v_consigned_to, 50170, v_acc_id, c704_account_id)
	            , c504_ship_to = DECODE(v_consigned_to, 50170, 4120, 50172, 4123)
	            , c504_ship_to_id = v_req_for_id
		    WHERE c504_consignment_id = p_alloc_cn 
		      AND c504_void_fl IS NULL;
		  END IF;
		  
		  -- If new status is pending shipping update shipping table
		  IF v_cn_dealloc_status = 10 
		  THEN
		  	gm_pkg_op_loaner.gm_sav_status_update(p_req_det_id, p_userid);
		  END IF;

		  v_comments := ' CN for request#  ' || p_req_det_id || ' is swapped manually from ' || p_dealloc_cn  || ' to ' || p_alloc_cn || '.' ;

		  IF v_request_ids IS NOT NULL THEN
			   v_comments :=  v_comments || p_alloc_cn || ' was already allocated to '  ||  v_request_ids || ', So updated the  status of ' ||  v_request_ids || ' to open. ';
		   END IF;
		  
		  gm_update_log(v_parent_req_id, v_comments , 1247, p_userid, v_out);
		  
	END it_sav_dealloc_alloc;
END it_pkg_op_loaner_alloc;
/
