/* Formatted on 2011/02/01 15:54 (Formatter Plus v4.8.0) */

-- @"C:\database\Data Correction\Script\it_pkg_tag.bdy"


CREATE OR REPLACE PACKAGE BODY it_pkg_tag
IS
--

	/*********************************************************
	* Description : This procedure is used to updated
	* Tag status from available to released
	*********************************************************/
	PROCEDURE gm_sav_tag_status (
		p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_tag_from		 t5010_tag.c5010_tag_id%TYPE
	  , p_tag_to		 t5010_tag.c5010_tag_id%TYPE
	)
	AS
		v_user_id	   NUMBER;

		--
		CURSOR tag_cur
		IS
			SELECT t5010.c901_status status, t5010.c5010_tag_id tagid
			  FROM t5010_tag t5010
			 WHERE t5010.c5010_tag_id >= p_tag_from AND t5010.c5010_tag_id <= p_tag_to AND t5010.c5010_void_fl IS NULL;
	BEGIN
		FOR tag_val IN tag_cur
		LOOP
			IF (tag_val.status != 51010)
			THEN
				DBMS_OUTPUT.put_line (	 ' The Tag '
									  || tag_val.tagid
									  || ' status is not available, can not change to released. '
									 );
			END IF;
		END LOOP;

		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		UPDATE t5010_tag t5010
		   SET t5010.c901_status = 51011   --51011 released, 51010 available
			 , t5010.c5010_last_updated_by = v_user_id
			 , t5010.c5010_last_updated_date = SYSDATE
		 WHERE t5010.c5010_tag_id >= p_tag_from
		   AND t5010.c5010_tag_id <= p_tag_to
		   AND c901_status = 51010
		   AND t5010.c5010_void_fl IS NULL;

		--
		IF (SQL%ROWCOUNT = 0)
		THEN
			ROLLBACK;
			raise_application_error ('-20085', 'No record is found to be updated.');
		END IF;

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Tag ID from : ' || p_tag_from);
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Tag ID to : ' || p_tag_to);
		DBMS_OUTPUT.put_line (SQL%ROWCOUNT || ' Row(s) are updated. ');
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_tag_status;

	/*********************************************************
	* Description : This procedure is used to Unvoid
	* Tag
	*********************************************************/
	PROCEDURE gm_sav_unvoid_tag (
		p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_tag_id		 t5010_tag.c5010_tag_id%TYPE
	)
	AS
		v_user_id	   NUMBER;
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		UPDATE t5010_tag t5010
		   SET t5010.c5010_void_fl = NULL
			 , t5010.c5010_last_updated_by = v_user_id
			 , t5010.c5010_last_updated_date = SYSDATE
		 WHERE t5010.c5010_tag_id = p_tag_id AND t5010.c5010_void_fl = 'Y';

		--
		IF (SQL%ROWCOUNT = 0)
		THEN
			ROLLBACK;
			raise_application_error ('-20085'
								   ,	'Tag '
									 || p_tag_id
									 || ' does not exist or tag not voided, No record is found to be updated.'
									);
		END IF;

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Unvoided Tag ID : ' || p_tag_id);
		DBMS_OUTPUT.put_line (SQL%ROWCOUNT || ' Row(s) are updated. ');
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_unvoid_tag;

	/*********************************************************
	* Description : This procedure is used to Unlink
	* Tag which has voided CRA.
	*********************************************************/
	PROCEDURE gm_sav_unlink_tag (
		p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_tag_id		 t5010_tag.c5010_tag_id%TYPE
	)
	AS
		v_user_id	   NUMBER;
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		UPDATE t5010_tag
		   SET c901_status = 51011	 --Released
			 , c5010_last_updated_by = v_user_id
			 , c5010_last_updated_date = SYSDATE
			 , c5010_last_updated_trans_id = NULL
			 , c205_part_number_id = NULL
			 , c5010_control_number = NULL
			 , c207_set_id = NULL
			 , c5010_location_id = NULL
			 , c901_trans_type = NULL
		 WHERE c5010_tag_id = p_tag_id;

		IF (SQL%ROWCOUNT = 0)
		THEN
			ROLLBACK;
			raise_application_error ('-20085', ' No record is found to be updated.');
		END IF;

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Unlinked Tag ID : ' || p_tag_id);
		DBMS_OUTPUT.put_line (SQL%ROWCOUNT || ' Row(s) are updated. ');
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_unlink_tag;
	
	PROCEDURE gm_sav_tag_control_num (
		p_partnum	 IN   t205_part_number.c205_part_number_id%TYPE
	  , p_ref_id	 IN	 t5010_tag.c5010_last_updated_trans_id%TYPE
	  , p_control_num IN t5010_tag.c5010_control_number%TYPE
	)
	AS
	v_taggable_part	   VARCHAR2 (10);
	v_oldcontrolnum    t5010_tag.c5010_control_number%TYPE;
	
	BEGIN
		SELECT get_part_attribute_value (p_partnum, 92340)
			INTO v_taggable_part
			  FROM DUAL;
			  
		IF v_taggable_part = 'Y' 
		THEN
			BEGIN	
				SELECT c5010_control_number INTO v_oldcontrolnum FROM t5010_tag WHERE c205_part_number_id=p_partnum
				AND c5010_last_updated_trans_id=p_ref_id;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				null;
			END;
			
			IF ((v_oldcontrolnum <> p_control_num))
			THEN
				UPDATE t5010_tag t5010 
				set
				t5010.c5010_control_number = p_control_num
					 , t5010.c5010_last_updated_date = SYSDATE
				 WHERE t5010.c5010_last_updated_trans_id = p_ref_id
				   AND t5010.c205_part_number_id = p_partnum 
				   AND t5010.c5010_control_number = v_oldcontrolnum;
			END IF;
		END IF;
		
		
	END gm_sav_tag_control_num;

	

	/*********************************************************
	* Description : This procedure is used to load the Tag
	* Author : Bala	
	*********************************************************/
	PROCEDURE gm_sav_load_tag (
		p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_tag_id		 t5010_tag.c5010_tag_id%TYPE
	)
	AS
		v_user_id	   NUMBER;
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

  INSERT
       INTO t5010_tag
        (
            c5010_tag_id, c5010_history_fl, c5010_created_by
          , c5010_created_date, c901_status
        )
        VALUES
        (
            p_tag_id, 'Y', v_user_id
          , SYSDATE, 51011
        ) ; 

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Loaded Tag ID : ' || p_tag_id);
		DBMS_OUTPUT.put_line (SQL%ROWCOUNT || ' Row(s) are inserted. ');
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_load_tag;

END it_pkg_tag;
/
