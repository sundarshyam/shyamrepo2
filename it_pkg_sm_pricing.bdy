/* Formatted on 02/21/2013 (Preferences _3.0.04.34) */
CREATE OR REPLACE
PACKAGE BODY it_pkg_sm_pricing
IS
    --
    /*********************************************************
    * Description : This procedure is used to update old pricing for an account
    *********************************************************/
PROCEDURE gm_sav_revert_2011_pricing (
        p_help_desk_id t914_helpdesk_log.c914_helpdesk_id%TYPE,
        p_acc_id t705_account_pricing.c704_account_id%TYPE
	, p_gpo_id         t705_account_pricing.c101_party_id%TYPE
	)
AS
    v_rowcnt  NUMBER;
    v_acc     NUMBER;
    v_user_id NUMBER;
    --
BEGIN
    -- Mandatory helpdesk to log executed user information
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL) ;
    v_user_id := it_pkg_cm_user.get_valid_user;

     
     SELECT COUNT (1)
       INTO v_rowcnt
       FROM t705_account_pricing
      WHERE 
	(c704_account_id                 = p_acc_id
	or 
	c101_party_id  = p_gpo_id
	)
        AND c705_last_updated_by            = '649291'
        AND (TRUNC (c705_last_updated_date) = TO_DATE ('01/01/2014', 'MM/DD/YYYY'))
        AND c705_old_price                 IS NOT NULL;

     UPDATE t705_account_pricing
    SET c705_unit_price                     = c705_old_price, c705_last_updated_date = SYSDATE, c705_last_updated_by = '649291'
      WHERE 
	(c704_account_id                 = p_acc_id
	or 
	c101_party_id  = p_gpo_id
	)
        AND c705_last_updated_by            = '649291'
        AND (TRUNC (c705_last_updated_date) = TO_DATE ('01/01/2014', 'MM/DD/YYYY'))
        AND c705_old_price                 IS NOT NULL;
    -- logs
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Account ID: ' || p_acc_id) ;
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Rows Updated: ' || v_rowcnt) ;
    COMMIT;
    --
EXCEPTION
WHEN OTHERS THEN
    DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM) ;
    ROLLBACK;
END gm_sav_revert_2011_pricing;
/*********************************************************
* Description : This procedure is used to update old pricing for an account for a specific date (to revert the pricing
if pricing tean entered wrong data)
*********************************************************/
PROCEDURE gm_sav_revert_pricing_for_day (
        p_help_desk_id t914_helpdesk_log.c914_helpdesk_id%TYPE,
        p_acc_id t705_account_pricing.c704_account_id%TYPE,
        p_date_entered VARCHAR2)
AS
    v_rowcnt     NUMBER;
    v_rowcnt_del NUMBER;
    v_acc        NUMBER;
    v_user_id    NUMBER;
    --
BEGIN
    -- Mandatory helpdesk to log executed user information
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL) ;
    v_user_id := it_pkg_cm_user.get_valid_user;
     SELECT 1 INTO v_acc FROM t704_account WHERE c704_account_id = p_acc_id;
     SELECT COUNT (1)
       INTO v_rowcnt
       FROM t705_account_pricing
      WHERE c704_account_id                = p_acc_id
        AND TRUNC (c705_last_updated_date) = TO_DATE (p_date_entered, 'MM/DD/YY')
        AND c705_old_price                IS NOT NULL;
     SELECT COUNT (1)
       INTO v_rowcnt_del
       FROM t705_account_pricing
      WHERE c704_account_id           = p_acc_id
        AND TRUNC (c705_created_date) = TO_DATE (p_date_entered, 'MM/DD/YY')
        AND c705_last_updated_date   IS NULL;
    --Update the record with Old price if record exist else delete
     UPDATE t705_account_pricing
    SET c705_unit_price                    = c705_old_price, c705_last_updated_date = SYSDATE, c705_last_updated_by = '30301'
      , c705_history_fl                    = 'Y'
      WHERE c704_account_id                = p_acc_id
        AND TRUNC (c705_last_updated_date) = TO_DATE (p_date_entered, 'MM/DD/YY')
        AND c705_old_price                IS NOT NULL;
    --delete if the record is newly added
     DELETE
       FROM t705_account_pricing
      WHERE c704_account_id           = p_acc_id
        AND TRUNC (c705_created_date) = TO_DATE (p_date_entered, 'MM/DD/YY')
        AND c705_last_updated_date   IS NULL;
    -- logs
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Account ID: ' || p_acc_id) ;
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Rows Updated: ' || v_rowcnt || 'Rows Deleted: ' ||
    v_rowcnt_del) ;
    COMMIT;
EXCEPTION
WHEN OTHERS THEN
    DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM) ;
    ROLLBACK;
END gm_sav_revert_pricing_for_day;
/*********************************************************
* Description : This Procedure is used to enter the customer
*               reference #.
*********************************************************/
PROCEDURE gm_sav_customer_reference (
        p_user_id          IN t205d_part_attribute.c205d_last_updated_by%TYPE,
        p_jira_id          IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
        p_cust_part_number IN t205e_custom_part_number.C205E_CUST_PART_NUMBER%TYPE,
        p_cust_part_desc   IN t205e_custom_part_number.C205E_CUST_PART_DESC%TYPE,
        p_acc_id           IN VARCHAR2,
        p_ref_type         IN t205e_custom_part_number.C901_REF_TYPE%TYPE,
        p_part_number_id   IN VARCHAR2)
AS
    v_string VARCHAR2 (30000) := CONCAT (p_acc_id, ',') ;
    v_acc_id VARCHAR2 (100) ;
    CURSOR part_number_cur
    IS
         SELECT token FROM v_in_list WHERE token IS NOT NULL;
BEGIN
    my_context.set_my_inlist_ctx (p_part_number_id) ;
    IF p_user_id IS NULL THEN
        raise_application_error ( - 6502, 'User Id cannot be null') ;
    END IF;
    -- Mandatory helpdesk to log executed user information
    IF p_jira_id IS NULL THEN
        raise_application_error ( - 6502, 'Jira Id cannot be null') ;
    ELSE
        it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_jira_id, NULL) ;
    END IF;
    -- Parse part numbers from input
    WHILE INSTR (v_string, ',') <> 0
    LOOP
        v_acc_id             := SUBSTR (v_string, 1, INSTR (v_string, ',') - 1) ;
        v_string             := SUBSTR (v_string, INSTR (v_string, ',')    + 1) ;
        FOR part_number_list                                              IN part_number_cur
        LOOP
            IF (part_number_list.token IS NOT NULL) THEN
                 UPDATE t205e_custom_part_number
                SET C205E_CUST_PART_NUMBER  = p_cust_part_number, C205E_CUST_PART_DESC = p_cust_part_desc,
                    C205E_LAST_UPDATED_BY   = p_user_id, C205E_LAST_UPDATED_DATE = SYSDATE
                  WHERE C901_REF_TYPE       = p_ref_type
                    AND C205E_REF_VALUE     = v_acc_id
                    AND C205_PART_NUMBER_ID = part_number_list.token
                    AND C205E_VOID_FL      IS NULL;
                IF (SQL%ROWCOUNT            = 0) THEN
                     INSERT
                       INTO t205e_custom_part_number
                        (
                            C205E_CUST_PART_NUM_ID, C205_PART_NUMBER_ID, C901_REF_TYPE
                          , C205E_REF_VALUE, C205E_CUST_PART_NUMBER, C205E_CUST_PART_DESC
                          , C205E_CREATED_DATE, C205E_CREATED_BY
                        )
                        VALUES
                        (
                            S205E_CUST_PART_NUM_ID.nextval, part_number_list.token, p_ref_type
                          , v_acc_id, p_cust_part_number, p_cust_part_desc
                          , SYSDATE, p_user_id
                        ) ;
                END IF;
            END IF;
            -- create log after
            it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_jira_id, 'Cust Part Number/desc added for ' ||
            part_number_list.token || ' and account ' || v_acc_id) ;
        END LOOP;
    END LOOP;
    COMMIT;
EXCEPTION
WHEN OTHERS THEN
    DBMS_OUTPUT.put_line ('Error Occurred while executing: ' || SQLERRM) ;
    ROLLBACK;
    --
END gm_sav_customer_reference;
   /**********************************************************************************************
	* Description : This procedure is used to revert pricing for an account/GPO 
	*               for the given list of parts
	* pre-requiste: Set the input part number list in my_temp_list, before calling this procedure. 
	**********************************************************************************************/
	PROCEDURE gm_sav_revert_pricing_bypart (
        p_jira_id      t914_helpdesk_log.c914_helpdesk_id%TYPE,
        p_acc_id       t705_account_pricing.c704_account_id%TYPE,
	    p_gpo_id       t705_account_pricing.c101_party_id%TYPE
	)
	AS
	    v_rowcnt  NUMBER;
	    v_user_id NUMBER;
	    v_acc_id  t705_account_pricing.c704_account_id%TYPE;
	    v_gpo_id  t705_account_pricing.c101_party_id%TYPE;
		v_part_num     CLOB;
		v_part_count   NUMBER :=0;
	BEGIN
	    --When we copy parts from excel or from JIRA tkt we can have space between 
	    --the part numbers or space in the account id/gpo id/Partnumbers. 
	    v_acc_id := TRIM(p_acc_id); 
	    v_gpo_id := TRIM(p_gpo_id);	    
	    --v_part_num := TRIM(p_part_num);
	    --my_context.set_my_cloblist(TRIM(p_part_num));

        --Set the input part number list in my_temp_list, before calling to this procedure.
        --Checking the count of part numbers.  
		SELECT COUNT(my_temp_txn_id) 
		  INTO v_part_count 
		  FROM my_temp_list 
		 WHERE my_temp_txn_id IS NOT NULL;

		--_Validation
	    IF v_acc_id IS NOT NULL AND v_gpo_id IS NOT NULL
	    THEN
	        raise_application_error ( '-20999', 'Account ID/GPO ID cannot be passed at same time') ;
	    END IF; 

	    IF v_acc_id IS NULL AND v_gpo_id IS NULL
	    THEN
	        raise_application_error ( '-20999', 'Account ID/GPO ID cannot be null at same time');
	    END IF;

	    IF v_part_count = 0
	    THEN
	        raise_application_error ( '-20999', 'Part Numbers cannot be null') ;
	    END IF;

	    -- Mandatory helpdesk to log executed user information
	    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_jira_id, NULL) ;
	    v_user_id := it_pkg_cm_user.get_valid_user;	
	    IF v_user_id IS NULL THEN
	        raise_application_error ( '-20999', 'User ID cannot be null') ;
	    END IF;

	    --Get the c705_old_price update count
	    SELECT COUNT (1)
           INTO v_rowcnt
           FROM t705_account_pricing
          WHERE (c704_account_id = v_acc_id  OR c101_party_id = v_gpo_id) 
            AND c205_part_number_id IN ( SELECT TRIM(my_temp_txn_id) FROM my_temp_list WHERE my_temp_txn_id IS NOT NULL )
            AND c705_void_fl IS NULL
            AND c705_old_price IS NOT NULL;
	
	     --Update the c705_old_price for the Accounts/Gpos for given part numbers
         UPDATE t705_account_pricing
            SET c705_unit_price        = c705_old_price, 
                c705_last_updated_date = SYSDATE,
                c705_last_updated_by = v_user_id 
          WHERE (c704_account_id     = v_acc_id  OR c101_party_id   = v_gpo_id)
            AND c205_part_number_id IN ( SELECT TRIM(my_temp_txn_id) FROM my_temp_list WHERE my_temp_txn_id IS NOT NULL )
            AND c705_void_fl IS NULL
            AND c705_old_price IS NOT NULL;  
                     
	    -- logs
	    IF v_acc_id IS NOT NULL
	    THEN
	        it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_jira_id, 'Account ID: ' || v_acc_id) ;
	    END IF;

	    IF v_gpo_id IS NOT NULL
	    THEN
	        it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_jira_id, 'GPO ID: ' || v_gpo_id) ;
	    END IF;
	    
	    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_jira_id, 'Rows Updated: ' || v_rowcnt) ;
	    COMMIT;
	   
	EXCEPTION
	WHEN OTHERS
	THEN
	    DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM) ;
	    ROLLBACK;
	END gm_sav_revert_pricing_bypart;
	
   /***********************************************************************************************************
	* Description : This procedure is used to void pricing for an Account/GPO with given part numbers 
	                Also used to void pricing for an Account/GPO with given part numbers on a specific date
	* pre-requiste: Set the input part number list in my_temp_list, before calling this procedure. 
	************************************************************************************************************/
	PROCEDURE gm_void_accgpo_part_pricing (
	        p_jira_id      t914_helpdesk_log.c914_helpdesk_id%TYPE,
	        p_acc_id       t705_account_pricing.c704_account_id%TYPE,
		    p_gpo_id       t705_account_pricing.c101_party_id%TYPE,
		    p_date_entered VARCHAR2,
	        p_from_date    VARCHAR2,
	        p_to_date      VARCHAR2,
	        p_user_id      t705_account_pricing.c705_last_updated_by%TYPE)
	AS
	    v_rowcnt     NUMBER := 0;
	    v_rowcnt_del NUMBER := 0;
	    v_acc        NUMBER;
	    v_user_id    NUMBER;

	    v_date       DATE;
	    v_from_date  DATE;
	    v_to_date    DATE;

	    v_date_fl    VARCHAR2(1);

	    v_acc_id  t705_account_pricing.c704_account_id%TYPE;
	    v_gpo_id  t705_account_pricing.c101_party_id%TYPE;
	    v_part_count   NUMBER :=0;
	BEGIN		
		--When we copy parts from excel or from JIRA tkt we can have space between 
	    --the part numbers or space in the account id/gpo id/Partnumbers. 
	    v_acc_id := TRIM(p_acc_id); 
	    v_gpo_id := TRIM(p_gpo_id); 

	   -- my_context.set_my_cloblist (TRIM(p_part_num)) ;

       --Set the input part number list in my_temp_list, before calling this procedure.
       --Checking the count of part numbers.  
		SELECT COUNT(my_temp_txn_id) 
		  INTO v_part_count 
		  FROM my_temp_list 
		 WHERE my_temp_txn_id IS NOT NULL;

		IF v_acc_id IS NOT NULL AND v_gpo_id IS NOT NULL THEN
	        raise_application_error ( '-20999', 'Account ID/GPO ID cannot be passed at same time');
	    END IF;
	    
	    IF v_acc_id IS NULL AND v_gpo_id IS NULL THEN
	        raise_application_error ( '-20999', 'Account ID/GPO ID cannot be null at same time');
	    END IF;
	    
	    IF v_acc_id IS NOT NULL THEN
	     SELECT c101_party_id INTO v_gpo_id FROM t704_account WHERE c704_account_id=v_acc_id;
	    END IF;
	    
		--Format the incoming date values	    
	    v_date      := TO_DATE(TRIM(p_date_entered), get_rule_value('DATEFMT', 'DATEFORMAT'));
	    v_from_date := TO_DATE(TRIM(p_from_date), get_rule_value('DATEFMT', 'DATEFORMAT'));
	    v_to_date   := TO_DATE(TRIM(p_to_date), get_rule_value('DATEFMT', 'DATEFORMAT'));
	       
	    --If any one of the date is not null, then the voiding of pricing should be date based.
	    IF v_date IS NOT NULL OR v_from_date IS NOT NULL OR  v_to_date IS NOT NULL 
	    THEN
	   	 	v_date_fl := 'Y';
	    ELSE
	    	v_date_fl := NULL;
	    END IF;
	    
		-- Mandatory helpdesk to log executed user information
	    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_jira_id, NULL) ;
	    v_user_id := p_user_id;	
	    IF v_user_id IS NULL THEN
	        raise_application_error ( '-20999', 'User ID cannot be null') ;
	    END IF;
	    v_user_id := NVL(p_user_id,v_user_id);

	    --Sceanrio 1: When these two values are NULL, Void all parts pricing based on Account/GPO ID.
	    IF v_part_count = 0 AND v_date_fl IS NULL        
	    THEN
	    
	          --Get the c705_old_price update count
		      SELECT COUNT (1)
	           INTO v_rowcnt
	           FROM t705_account_pricing
	          WHERE (c704_account_id = v_acc_id  OR c101_party_id = v_gpo_id)
	            AND c705_void_fl IS NULL;

		     --Update the Void fl for the Accounts/Gpos for given part numbers
	         UPDATE t705_account_pricing
	            SET c705_void_fl         = 'Y', 
	                c705_last_updated_by = v_user_id,
	                c705_last_updated_date = SYSDATE
	          WHERE (c704_account_id     = v_acc_id  OR c101_party_id   = v_gpo_id)
	            AND c705_void_fl IS NULL; 
	            
	          
	            
	    --Sceanrio 2: Void Account/GPO part pricing for the given part numbers.
	    ELSIF v_part_count > 0  AND v_date_fl IS NULL  	    
	    THEN
	    
		     --Get the c705_old_price update count
		     SELECT COUNT (1)
	           INTO v_rowcnt
	           FROM t705_account_pricing
	          WHERE (c704_account_id = v_acc_id  OR c101_party_id = v_gpo_id) 
	            AND c205_part_number_id IN (  SELECT TRIM(my_temp_txn_id) FROM my_temp_list WHERE my_temp_txn_id IS NOT NULL )
	            AND c705_void_fl IS NULL;		
	            
		     --Update the Void fl for the Accounts/Gpos for given part numbers
	         UPDATE t705_account_pricing
	            SET c705_void_fl         = 'Y', 
	                c705_last_updated_date = SYSDATE,
	                c705_last_updated_by = v_user_id 
	          WHERE (c704_account_id     = v_acc_id  OR c101_party_id   = v_gpo_id)
	            AND c205_part_number_id IN (  SELECT TRIM(my_temp_txn_id) FROM my_temp_list WHERE my_temp_txn_id IS NOT NULL)
	            AND c705_void_fl IS NULL;  
	            
	    --Sceanrio 3: When these p_part_num is  NULL, Void all parts pricing based on Account/GPO ID on given Date.
	    ELSIF v_part_count = 0  AND v_date_fl IS NOT NULL 
	    THEN

	       ---count of Updated records on the given date
		     	 SELECT COUNT (1)
		           INTO v_rowcnt
		           FROM t705_account_pricing
		          WHERE (c704_account_id = v_acc_id    OR c101_party_id     = v_gpo_id) 
		            AND TRUNC (c705_last_updated_date) = v_date
		            AND c705_void_fl   IS NULL;
		            
		        ---count of Created records on the given date 
		         SELECT COUNT (1)
		           INTO v_rowcnt_del
		           FROM t705_account_pricing
		          WHERE (c704_account_id = v_acc_id    OR c101_party_id     = v_gpo_id)
		            AND TRUNC (c705_created_date) = v_date
		            AND c705_last_updated_date   IS NULL
		            AND c705_void_fl   IS NULL;
		            
		        --Update the record with void_fl = 'Y' if record exist previously by the given date
		         UPDATE t705_account_pricing
		            SET c705_void_fl = 'Y',
		                c705_last_updated_by = v_user_id,
		                c705_last_updated_date = SYSDATE
		          WHERE (c704_account_id = v_acc_id    OR c101_party_id     = v_gpo_id)
		            AND TRUNC (c705_last_updated_date) = v_date
		            AND c705_void_fl   IS NULL;
		            
		        --Update the record with void_fl = 'Y' if the record is newly added by the given date
		         UPDATE t705_account_pricing
		            SET c705_void_fl  = 'Y',
		                c705_last_updated_by = v_user_id,
		                c705_last_updated_date = SYSDATE
		          WHERE (c704_account_id = v_acc_id    OR c101_party_id     = v_gpo_id)
		            AND TRUNC (c705_created_date) = v_date
		            AND c705_last_updated_date   IS NULL
		            AND c705_void_fl   IS NULL;
		           v_rowcnt := v_rowcnt + v_rowcnt_del ;
		           
	    --Sceanrio 4: When these two values are NOT NULL, Void Account/GPO part pricing for the given part numbers on given Date.
	    ELSIF v_part_count > 0 AND v_date_fl IS NOT NULL 
	    THEN
	      
	        --To void the records that was created or updated on particular date
		    IF v_date IS NOT NULL
		    THEN
		        ---count of Updated records on the given date
		     	 SELECT COUNT (1)
		           INTO v_rowcnt
		           FROM t705_account_pricing
		          WHERE (c704_account_id = v_acc_id    OR c101_party_id     = v_gpo_id) 
		            AND TRUNC (c705_last_updated_date) = v_date
		            AND c205_part_number_id IN  ( SELECT TRIM(my_temp_txn_id) FROM my_temp_list WHERE my_temp_txn_id IS NOT NULL  )
		            AND c705_void_fl   IS NULL;
		            
		        ---count of Created records on the given date 
		         SELECT COUNT (1)
		           INTO v_rowcnt_del
		           FROM t705_account_pricing
		          WHERE (c704_account_id = v_acc_id    OR c101_party_id     = v_gpo_id)
		            AND TRUNC (c705_created_date) = v_date
		            AND c205_part_number_id IN  ( SELECT TRIM(my_temp_txn_id) FROM my_temp_list WHERE my_temp_txn_id IS NOT NULL  )
		            AND c705_last_updated_date   IS NULL
		            AND c705_void_fl   IS NULL;
		            
		        --Update the record with void_fl = 'Y' if record exist previously by the given date
		         UPDATE t705_account_pricing
		            SET c705_void_fl = 'Y',
		                c705_last_updated_by = v_user_id,
		                c705_last_updated_date = SYSDATE
		          WHERE (c704_account_id = v_acc_id    OR c101_party_id     = v_gpo_id)
		            AND TRUNC (c705_last_updated_date) = v_date
		            AND c205_part_number_id IN  ( SELECT TRIM(my_temp_txn_id) FROM my_temp_list WHERE my_temp_txn_id IS NOT NULL  )
		            AND c705_void_fl   IS NULL;
		            
		        --Update the record with void_fl = 'Y' if the record is newly added by the given date
		         UPDATE t705_account_pricing
		            SET c705_void_fl  = 'Y',
		                c705_last_updated_by = v_user_id,
		                c705_last_updated_date = SYSDATE
		          WHERE (c704_account_id = v_acc_id    OR c101_party_id     = v_gpo_id)
		            AND TRUNC (c705_created_date) = v_date
		            AND c205_part_number_id IN  ( SELECT TRIM(my_temp_txn_id) FROM my_temp_list WHERE my_temp_txn_id IS NOT NULL  )
		            AND c705_last_updated_date   IS NULL
		            AND c705_void_fl   IS NULL;
		            
		           v_rowcnt := v_rowcnt + v_rowcnt_del ;
		    END IF;
		   
		    --void the records that was created or updated on or before/after a certain date, 
		    --Also voiding between the two date ranges.
		    IF v_from_date IS NOT NULL OR v_to_date IS NOT NULL 
		    THEN
		    
		        ---count of Updated records on the given date
		     	 SELECT COUNT (1)
		           INTO v_rowcnt
		           FROM t705_account_pricing
		          WHERE (c704_account_id = v_acc_id    OR c101_party_id     = v_gpo_id) 
		            AND ( (v_from_date  IS NOT NULL    OR v_to_date    IS NOT NULL)
		                  AND TRUNC (c705_last_updated_date) >= TRUNC (NVL (v_from_date, c705_last_updated_date))
		                  AND TRUNC (c705_last_updated_date) <= TRUNC (NVL (v_to_date, c705_last_updated_date))
		                )
		            AND c205_part_number_id IN  ( SELECT TRIM(my_temp_txn_id) FROM my_temp_list WHERE my_temp_txn_id IS NOT NULL  )
		            AND c705_void_fl   IS NULL;
		            
		        ---count of Created records on the given date    
		         SELECT COUNT (1)
		           INTO v_rowcnt_del
		           FROM t705_account_pricing
		          WHERE (c704_account_id = v_acc_id    OR c101_party_id     = v_gpo_id)
		           AND ( (v_from_date  IS NOT NULL    OR v_to_date    IS NOT NULL)
		                  AND TRUNC (c705_created_date) >= TRUNC (NVL (v_from_date, c705_created_date))
		                  AND TRUNC (c705_created_date) <= TRUNC (NVL (v_to_date, c705_created_date))
		                )
		            AND c205_part_number_id IN  (  SELECT TRIM(my_temp_txn_id) FROM my_temp_list WHERE my_temp_txn_id IS NOT NULL )
		            AND c705_last_updated_date   IS NULL
		            AND c705_void_fl   IS NULL;

		        --Update the record with void_fl = 'Y' if record exist previously by the given date
		         UPDATE t705_account_pricing
		            SET c705_void_fl = 'Y',  
		                c705_last_updated_by = v_user_id,
		                c705_last_updated_date = SYSDATE
		          WHERE (c704_account_id = v_acc_id    OR c101_party_id     = v_gpo_id)
		           AND ( (v_from_date  IS NOT NULL    OR v_to_date    IS NOT NULL)
		                  AND TRUNC (c705_last_updated_date) >= TRUNC (NVL (v_from_date, c705_last_updated_date))
		                  AND TRUNC (c705_last_updated_date) <= TRUNC (NVL (v_to_date, c705_last_updated_date))
		                )
		            AND c205_part_number_id IN  ( SELECT TRIM(my_temp_txn_id) FROM my_temp_list WHERE my_temp_txn_id IS NOT NULL )
		            AND c705_void_fl   IS NULL;

		        --Update the record with void_fl = 'Y' if the record is newly added by the given date
		         UPDATE t705_account_pricing
		            SET c705_void_fl  = 'Y', 
		                c705_last_updated_by = v_user_id,
		                c705_last_updated_date = SYSDATE
		          WHERE (c704_account_id = v_acc_id    OR c101_party_id     = v_gpo_id)
		           AND ( (v_from_date  IS NOT NULL    OR v_to_date    IS NOT NULL)
		                  AND TRUNC (c705_created_date) >= TRUNC (NVL (v_from_date, c705_created_date))
		                  AND TRUNC (c705_created_date) <= TRUNC (NVL (v_to_date, c705_created_date))
		                )
		            AND c205_part_number_id IN  (  SELECT TRIM(my_temp_txn_id) FROM my_temp_list WHERE my_temp_txn_id IS NOT NULL )
		            AND c705_last_updated_date   IS NULL
		            AND c705_void_fl   IS NULL;
		            
		           v_rowcnt := v_rowcnt + v_rowcnt_del ; 
		    END IF;
		   
	    END IF;
	    
	    --Log entries
	    IF v_acc_id IS NOT NULL THEN
	        it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_jira_id, 'Account ID: ' || v_acc_id) ;
	    END IF;
	    IF p_gpo_id IS NOT NULL THEN	    	
	        it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_jira_id, 'GPO ID: ' || v_gpo_id) ;
	    END IF;
	    --TO UPDATE PRICE IN GROUP LEVEL FOR PRT APP
	    IF v_rowcnt > 0 AND v_gpo_id IS NOT NULL THEN
	    	--to void the existing group level price for removed account
				UPDATE T7540_ACCOUNT_GROUP_PRICING
				SET c7540_void_fl      ='Y',
				  c7540_last_updated_by=v_user_id,
				  c7540_last_updated_on=sysdate
				WHERE c101_party_id   IN (v_gpo_id)
				AND c7540_void_fl     IS NULL;
				
	    	it_pkg_batch_update_t7540.update_pricing_table(v_gpo_id);
	    END IF;
	     
	    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_jira_id, 'Rows Updated: ' || v_rowcnt ) ;
	    
	    
	    COMMIT;
	   
	    EXCEPTION
	    WHEN OTHERS THEN
	        DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM) ;
	    ROLLBACK;
	END gm_void_accgpo_part_pricing;
	
	 /*********************************************************
    * Description : This procedure is used to update pricing request status - IPAD (PRT)
    *********************************************************/
	PROCEDURE gm_sav_pricing_req_status (
	        p_help_desk_id t914_helpdesk_log.c914_helpdesk_id%TYPE,
	        p_pr_req_id t7500_account_price_request.c7500_account_price_request_id%TYPE
		)
	AS	   
	    v_user_id NUMBER;
	    --
	BEGIN
	    -- Mandatory helpdesk to log executed user information
	    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL) ;
	    v_user_id := it_pkg_cm_user.get_valid_user;
	
	    --Updating Status from Initiated to Pending Approval
			UPDATE t7500_account_price_request
			SET c901_request_status             =52121,
			  c7500_last_updated_by             =v_user_id,
			  c7500_last_updated_date           =sysdate
			WHERE c7500_account_price_request_id=p_pr_req_id
			AND c7500_void_fl                  IS NULL;
			
			--Updating Status from Initiated to Pending PC Approval			
			UPDATE t7501_account_price_req_dtl
			SET c901_status                     =52123,
			  c7501_last_updated_by             =v_user_id,
			  c7501_last_updated_date           =sysdate
			WHERE c7500_account_price_request_id=p_pr_req_id
			AND c7501_void_fl                  IS NULL
			AND c7501_proposed_price           IS NOT NULL; 
	
	    -- logs
	    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Pricing Request Status Updated as Pending Approval: ' || p_pr_req_id) ;
	    COMMIT;
	    --
	EXCEPTION
	WHEN OTHERS THEN
	    DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM) ;
	    ROLLBACK;
	END gm_sav_pricing_req_status;
 /***********************************************************************
  * Author : Arockia Prasath
  * Description : To update the price request comparison gpb id
  ************************************************************************/ 
PROCEDURE gm_upd_price_request_gpb_map (
    p_help_desk_id IN t914_helpdesk_log.c914_helpdesk_id%TYPE
  ,	p_requestid         	IN 		T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE	
  , p_gpo_id       IN     t704d_account_affln.c101_gpo%TYPE
  , p_user_id				IN      t7501_account_price_req_dtl.C7501_CREATED_BY%TYPE  
)
AS
   	v_ref_id 	  			t7550_gpo_reference.c7550_gpo_reference_id%TYPE;   
BEGIN	
	
		-- Mandatory helpdesk to log executed user information
	    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL) ;
	
		SELECT MAX(c7550_gpo_reference_id) INTO v_ref_id
		FROM t7550_gpo_reference
		WHERE c7550_void_fl    IS NULL;
		
		UPDATE t7500_account_price_request
		SET c7550_gpo_reference_id          = v_ref_id,
		  c7500_last_updated_by             =p_user_id,
		  c7500_last_updated_date           =sysdate
		WHERE c7500_account_price_request_id=p_requestid
		AND c7500_void_fl                  IS NULL;		
		 			
		DELETE FROM t7553_gpo_ref_price_req_map WHERE c7500_account_price_request_id = p_requestid;				
		
		INSERT
		INTO t7553_gpo_ref_price_req_map
		  (
		    c7553_gpo_ref_price_req_map_id ,
		    c7551_gpo_reference_detail_id ,
		    c7500_account_price_request_id
		  )
		  (
		  SELECT s7553_gpo_ref_price_req_map_id.nextval,  t7551.* FROM (
		  SELECT t7551.c7551_gpo_reference_detail_id,
		      p_requestid c7500_account_price_request_id
		    FROM 
		      t7560_gpo_gpb_mapping t7560,
		      t7551_gpo_reference_detail t7551
		    WHERE  t7560.c7560_void_fl                IS NULL
		    AND t7551.c7551_void_fl                IS NULL
		    AND t7551.c7550_gpo_reference_id = v_ref_id
		    AND t7560.c101_gpo                      = p_gpo_id
		    AND t7551.c101_gpb_id                   = t7560.c101_gpb		    
		    GROUP BY t7551.c7551_gpo_reference_detail_id
        ) t7551		    
		  ) ;
		  
		-- logs
	    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Pricing Request comparison gpb id: ' || p_requestid) ;
	EXCEPTION
	WHEN OTHERS THEN
	    DBMS_OUTPUT.put_line ('Error Occurred while executing gm_upd_price_request_gpb_map: ' || SQLERRM) ;
	    ROLLBACK;	  
END gm_upd_price_request_gpb_map;

/***********************************************************************
  * Author : Arockia Prasath
  * Description : To swap the price request from account to gpb
  ************************************************************************/ 
PROCEDURE gm_upd_price_req_acc_gpb_swap (
 	p_help_desk_id IN t914_helpdesk_log.c914_helpdesk_id%TYPE
  ,	p_requestid         	IN 		T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE	
  , p_gpo_id       IN     t704d_account_affln.c101_gpo%TYPE
  , p_user_id				IN      t7501_account_price_req_dtl.C7501_CREATED_BY%TYPE  
)
AS  
   v_req_id VARCHAR2 (50):= p_help_desk_id; 
   v_request_status NUMBER; 
   v_gpb_id NUMBER; 
   v_acc_id NUMBER; 
   v_impact_id NUMBER ; 
   v_userid NUMBER:=p_user_id; 
 BEGIN     
	 
	 -- Mandatory helpdesk to log executed user information
	    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL) ;
	    
     SELECT c901_request_status, 
       c704_account_id 
     INTO v_request_status, 
       v_acc_id 
     FROM t7500_account_price_request 
     WHERE c7500_account_price_request_id = v_req_id; 
     
     --status sould be 52121 -- pending pc approval 
     IF v_request_status = 52121 THEN 
     --getting gpb id 
       SELECT c101_party_id 
       INTO v_gpb_id 
       FROM t740_gpo_account_mapping 
       WHERE c704_account_id = v_acc_id 
       AND c740_void_fl IS NULL; 
        
       UPDATE t7500_account_price_request 
       SET c901_request_type ='903108', 
         c7530_gpo_reference_id =v_gpb_id, 
         c101_gpb_id =v_gpb_id, 
         c704_account_id =NULL, 
         c7500_last_updated_by =v_userid, 
         c7500_last_updated_date =sysdate 
       WHERE c7500_account_price_request_id = v_req_id 
       AND c7500_void_fl IS NULL; 
        
       SELECT c7520_price_impact_analysis_id 
       INTO v_impact_id 
       FROM t7520_price_impact_analysis 
       WHERE c7500_account_price_request_id =v_req_id; 
        
       UPDATE t7521_price_impact_analy_dtl 
       SET c7521_void_fl ='Y', 
         c7521_last_updated_by =v_userid, 
         c7521_last_updated_date =sysdate 
       WHERE c7520_price_impact_analysis_id = v_impact_id 
       AND c7521_void_fl IS NULL; 
        
       gm_pkg_PRT_impact_analysis.gm_prt_sav_12months_data(v_req_id,v_userid,v_impact_id); 
        
     ELSE 
       DBMS_OUTPUT.put_line('Pricing request not in pending pc approval ' || v_req_id ); 
     END IF; 
-- logs
	    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Pricing Request swapped from account to gpb: ' || p_requestid) ;
	EXCEPTION
	WHEN OTHERS THEN
	    DBMS_OUTPUT.put_line ('Error Occurred while executing gm_upd_price_request_acc_gpb_swap: ' || SQLERRM) ;
	    ROLLBACK;	  
END gm_upd_price_req_acc_gpb_swap;   

/***********************************************************************
  * Author : Arockia Prasath
  * Description : To update parent group id
  ************************************************************************/ 
PROCEDURE gm_upd_parent_group_id (
 	p_help_desk_id IN t914_helpdesk_log.c914_helpdesk_id%TYPE
  ,	p_parent_group_id         	IN 		t4010_group.c4010_parent_group_id%TYPE	
  , p_group_id        IN     t4010_group.c4010_group_id %TYPE
  , p_user_id				IN      t4010_group.c4010_last_updated_by%TYPE  
)
AS  
    v_userid NUMBER:=p_user_id; 
 BEGIN           
	 
	 -- Mandatory helpdesk to log executed user information
	    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL) ;
	    
     IF p_group_id IS NOT NULL THEN        
       UPDATE t4010_group
		SET c4010_parent_group_id=p_parent_group_id,
		  c4010_last_updated_by  =v_userid,
		  c4010_last_updated_date=sysdate
		WHERE c4010_group_id   = p_group_id;                
     END IF; 
-- logs
	    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Parent group id updated : ' || p_group_id) ;
	EXCEPTION
	WHEN OTHERS THEN
	    DBMS_OUTPUT.put_line ('Error Occurred while executing gm_upd_price_req_acc_gpb_swap: ' || SQLERRM) ;
	    ROLLBACK;	  
END gm_upd_parent_group_id;  
  
/***********************************************************************
  * Author : Manikandan Muthusamy
  * Description : To save the part number and group details to temp table
  ************************************************************************/ 
PROCEDURE gm_sav_temp_key_value (
        p_help_desk_id IN my_temp_key_value.my_temp_txn_id%TYPE,
        p_part_number IN my_temp_key_value.my_temp_txn_key%TYPE,
        p_group_name  IN my_temp_key_value.my_temp_txn_value%TYPE)
AS
BEGIN
     INSERT
       INTO my_temp_key_value
        (
            my_temp_txn_id, my_temp_txn_key, my_temp_txn_value 
        )
        VALUES
        (
            p_help_desk_id, p_part_number, p_group_name
        ) ;
END gm_sav_temp_key_value;

/***********************************************************************
  * Author : Manikandan Muthusamy
  * Description : To add the parts to group part mapping
  ************************************************************************/ 
PROCEDURE gm_add_parts_to_group_mapping (
        p_help_desk_id IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
        p_group_name   IN t4010_group.c4010_group_nm%TYPE)
AS
    v_part_cnt_check NUMBER;
    v_primary_fl     VARCHAR2 (2) ;
    v_grp_cn         NUMBER;
    v_user_id t4010_group.c4010_last_updated_by%TYPE;
    v_cnt NUMBER :=0 ;
    v_group_id t4010_group.c4010_group_id%TYPE;
    --
    CURSOR grp_part_dtls_cur
    IS
         SELECT my_temp_txn_key pnum
           FROM my_temp_key_value
          WHERE upper (trim(my_temp_txn_value)) = upper (trim(p_group_name));
BEGIN
    -- helpdesk log executed
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Add the part number to below group : '|| p_group_name) ;
    v_user_id := it_pkg_cm_user.get_valid_user;
    -- to get the group id
    SELECT c4010_group_id INTO v_group_id
   FROM t4010_group
  WHERE upper (C4010_GROUP_NM) = upper (trim(p_group_name));
    -- to validate the group id
     SELECT COUNT (1)
       INTO v_grp_cn
       FROM T4010_GROUP
      WHERE C4010_GROUP_ID = v_group_id
      	AND C901_TYPE   = 40045
        AND C4010_VOID_FL IS NULL;
    --
    IF v_grp_cn = 0 THEN
        raise_application_error ( - 20999, 'Please provide valid Group id') ;
    END IF;
    --
    FOR grp_part_dtls IN grp_part_dtls_cur
    LOOP
        -- to validate the parts
         SELECT COUNT (1)
           INTO v_part_cnt_check
           FROM t4010_group t4010, t4011_group_detail t4011
          WHERE t4011.c205_part_number_id = grp_part_dtls.pnum
            AND t4010.c4010_group_id      = t4011.c4010_group_id
            AND t4010.c901_type           = 40045
            AND T4010.c4010_void_fl      IS NULL;
        --
        IF v_part_cnt_check = 0 THEN
            v_primary_fl   := 'Y';
        ELSE
            v_primary_fl := 'N';
        END IF;
        -- to avoid duplicate insert
         SELECT COUNT (1)
           INTO v_part_cnt_check
           FROM t4011_group_detail t4011
          WHERE t4011.c4010_group_id      = v_group_id
            AND t4011.c205_part_number_id = grp_part_dtls.pnum;
        --
        IF v_part_cnt_check = 0 THEN
        	--
        	v_cnt := v_cnt + 1;
        	--
             INSERT
               INTO t4011_group_detail
                (
                    c4011_group_detail_id, c205_part_number_id, c901_part_pricing_type
                  , c4010_group_id, c4011_part_number_historyfl, c4011_created_by
                  , c4011_created_date, c4011_primary_part_lock_fl
                )
                VALUES
                (
                    s4011_group_detail.NEXTVAL, grp_part_dtls.pnum, 52080
                  , v_group_id, 'Y', v_user_id
                  , CURRENT_DATE, v_primary_fl
                ) ;
        END IF;
    END LOOP;
    -- to update the master data
     UPDATE t4010_group
    SET c4010_last_updated_by = v_user_id, c4010_last_updated_date = CURRENT_DATE
      WHERE c4010_group_id    = v_group_id;
    --
    --This procedure will check if the Group details is modified and will mark the group as updated for Product Catalog.
	gm_pkg_pdpc_prodcattxn.gm_chk_group_detail_update(v_group_id,v_user_id);
	--
    DBMS_OUTPUT.put_line (v_group_id || ' : Total part # to be added - ' || v_cnt) ;
    --
END gm_add_parts_to_group_mapping;

PROCEDURE gm_unvoid_pricing(
    p_help_desk_id IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
    p_acc_id IN t705_account_pricing.c704_account_id%TYPE ,
    p_gpo_id IN t705_account_pricing.c101_party_id%TYPE ,
    p_part_no IN t705_account_pricing.c205_part_number_id%TYPE,    
    p_user_id IN t4010_group.c4010_last_updated_by%TYPE )
AS
  v_rowcnt   NUMBER;
  v_user_id  NUMBER;
  v_party_id NUMBER;
  --
BEGIN
  -- Mandatory helpdesk to log executed user information
  it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL) ;
  v_user_id   := p_user_id;
  IF p_acc_id IS NOT NULL THEN
  BEGIN
    SELECT c101_party_id
    INTO v_party_id
    FROM t704_account
    WHERE c704_account_id=p_acc_id;
  EXCEPTION WHEN NO_DATA_FOUND THEN
   v_party_id := NULL;
  END;
  ELSE
    v_party_id := p_gpo_id;
  END IF;
  
  SELECT COUNT (1)
  INTO v_rowcnt
  FROM t705_account_pricing
  WHERE c101_party_id    = v_party_id
  AND c205_part_number_id = p_part_no
  AND c705_void_fl      IS NOT NULL;
  
  IF v_rowcnt            = 1 THEN
    UPDATE t705_account_pricing
    SET c705_void_fl         = NULL,
      c705_last_updated_date = SYSDATE,
      c705_last_updated_by   = v_user_id
    WHERE c101_party_id      = v_party_id
    AND c205_part_number_id  = p_part_no
    AND c705_void_fl        IS NOT NULL;
  ELSE
    DBMS_OUTPUT.put_line ('Data not found: GPB ID: ' || p_gpo_id||' acc id-' || p_acc_id||'- part '||p_gpo_id) ;
  END IF;
  -- logs
  it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Account ID: ' || p_acc_id||'- part '||p_gpo_id) ;
  it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'GPB ID: ' || p_gpo_id||'- part '||p_gpo_id) ;
  it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Rows Updated: ' || v_rowcnt) ;
  COMMIT;
  --
EXCEPTION
WHEN OTHERS THEN
  DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM) ;
  ROLLBACK;
END gm_unvoid_pricing;

END it_pkg_sm_pricing;
/
