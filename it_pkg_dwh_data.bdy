create or replace PACKAGE BODY it_pkg_dwh_data
IS
    /*********************************************************
    * Description : This procedure will update month_dim data based on max Order date
    *********************************************************/
PROCEDURE gm_cm_upd_month_dim
AS
BEGIN
	
  UPDATE GLOBUS_DW.MONTH_DIM UPD_MONTH_DIM
  SET BUS_DAYS_FOR_MONTH =
    (SELECT COUNT(DATE_DIM.DAY_NUMBER)
      FROM GLOBUS_DW.DATE_DIM
    WHERE YEAR_MONTH = UPD_MONTH_DIM.YEAR_MONTH
      AND BUS_DAY_IND  = 'Y'
    ) ,
  SALES_BUSINESS_DAYS =
    (SELECT COUNT(DATE_DIM.DAY_NUMBER)
      FROM GLOBUS_DW.DATE_DIM
    WHERE CAL_DATE <=
      (SELECT TRUNC(MAX(ORDER_DT))
        FROM GLOBUS_DW.ORDER_DIM
      WHERE TO_CHAR(ORDER_DT, 'YYYY/MM') = UPD_MONTH_DIM.YEAR_MONTH)
        AND YEAR_MONTH  = UPD_MONTH_DIM.YEAR_MONTH
        AND BUS_DAY_IND = 'Y'
     ) ,
  REMAIN_BUS_DAYS_FOR_MONTH =
    (SELECT COUNT(DATE_DIM.DAY_NUMBER)
      FROM GLOBUS_DW.DATE_DIM
    WHERE CAL_DATE >
      (SELECT TRUNC(MAX(ORDER_DT))
        FROM GLOBUS_DW.ORDER_DIM
      WHERE TO_CHAR(ORDER_DT, 'YYYY/MM') = UPD_MONTH_DIM.YEAR_MONTH)
        AND YEAR_MONTH  = UPD_MONTH_DIM.YEAR_MONTH
        AND BUS_DAY_IND = 'Y'
    )
  WHERE YEAR_MONTH = TO_CHAR(
    (SELECT TRUNC(MAX(ORDER_DT)) FROM GLOBUS_DW.ORDER_DIM), 'YYYY/MM');
    
END gm_cm_upd_month_dim ;
END it_pkg_dwh_data;
/