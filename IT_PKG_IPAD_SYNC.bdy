/* Formatted on 2011/02/25 14:29 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\IT_PKG_IPAD_SYNC.bdy"

CREATE OR REPLACE PACKAGE BODY IT_PKG_IPAD_SYNC
IS
--
	/******************************************************************
	* Description : Procedure to update the last updated sync date set as past date like today-5
	* Author: jgurunathan
	****************************************************************/
PROCEDURE GM_SAV_SYNC_REP_ACCOUNTS (
	p_rep_id	IN	 t704_account.c703_sales_rep_id%TYPE,
	p_device_id IN   t9150_device_info.c9150_device_info_id%TYPE
)

AS
  v_update_fl NUMBER := 0;
  
CURSOR v_cur
   IS
      SELECT c704_account_id accid
        FROM t704_account 
        WHERE c703_sales_rep_id = p_rep_id
        AND c704_void_fl is null;
  
BEGIN

 FOR v_accounts IN v_cur
		LOOP		                
	        UPDATE t2001_master_data_updates
	           SET c2001_last_updated_by = 941997
	             , c2001_ref_updated_date = sysdate - 5
	             , c2001_last_updated_date = sysdate - 5
	         WHERE c2001_ref_id = v_accounts.accid
	         AND c2001_void_fl is null;
	    v_update_fl := 1;      
	   	END LOOP;     

IF (v_update_fl = 1) THEN
     UPDATE t9151_device_sync_dtl
	           SET c9151_last_updated_by = 941997
	             , c9151_last_updated_date = sysdate
	             , c9151_last_sync_date = sysdate - 6
	             , c901_sync_status = 103121
	         WHERE c9150_device_info_id = p_device_id
	         AND c901_sync_type = 4000519
	         AND c9151_void_fl is null;
END IF;
            
END GM_SAV_SYNC_REP_ACCOUNTS;

END IT_PKG_IPAD_SYNC;
/
