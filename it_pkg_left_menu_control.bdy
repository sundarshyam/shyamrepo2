/* Formatted on 2011/02/28 11:57 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_left_menu_control.bdy"

CREATE OR REPLACE PACKAGE BODY it_pkg_left_menu_control
IS
--

	/*********************************************************
	* Description : This procedure is used to updated
	* access right for the specific user to left menu link
	*********************************************************/
	PROCEDURE gm_sav_link_access (
		p_help_desk_id	   t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_source_user	   t102_user_login.c102_login_username%TYPE
	  , p_source_deptid    t901_code_lookup.c901_code_id%TYPE
	  , p_target_deptid    VARCHAR2
	  , p_source_func_nm   t1520_function_list.c1520_function_nm%TYPE
	  , p_target_func_nm   t1520_function_list.c1520_function_nm%TYPE
	)
	AS
		v_user_id	   NUMBER;
		v_upper_func_id VARCHAR2 (20);
		v_seq_id	   NUMBER;
		v_curr_func_id VARCHAR2 (20);
		v_parent_func_id VARCHAR2 (20);
		v_source_userid NUMBER;
		v_count 	   NUMBER;
		v_user_partyid NUMBER;
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		BEGIN
			IF (p_source_user IS NOT NULL)
			THEN
				SELECT t102.c101_user_id
				  INTO v_source_userid
				  FROM t102_user_login t102
				 WHERE t102.c102_login_username = p_source_user;

				SELECT t101.c101_party_id
				  INTO v_user_partyid
				  FROM t101_user t101
				 WHERE t101.c101_user_id = v_source_userid;
			END IF;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error ('-20085', 'User name is wrong.');
		END;

		BEGIN
			---ex target 'Reports', target dept 2023(A/C report)
			SELECT t1530.c1520_parent_function_id, t1530.c1530_seq_no, t1520.c1520_function_id
			  INTO v_parent_func_id, v_seq_id, v_upper_func_id
			  FROM t1530_access t1530, t1520_function_list t1520
			 WHERE t1530.c1520_function_id = t1520.c1520_function_id
			   AND t1530.c1500_group_id = p_target_deptid
			   AND t1520.c1520_function_nm = p_target_func_nm
			   AND t1520.c901_type = 92262;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error ('-20085'
									   , 'Either Source left Menu link does not exist or Target Dept ID is wrong.'
										);
		END;

		BEGIN
			SELECT c1520_function_id
			  INTO v_curr_func_id
			  FROM t1520_function_list
			 WHERE c1520_function_nm = p_source_func_nm AND c901_type = 92262 AND c901_department_id = p_source_deptid;   ---ex. target func name 'Held Orders'
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error ('-20085', 'target left Menu link func id does not exist.');
		END;

		SELECT COUNT (1)
		  INTO v_count
		  FROM t1530_access t1530
		 WHERE (   t1530.c1500_group_id = NVL (p_target_deptid, t1530.c1500_group_id)
				OR t1530.c101_party_id = NVL (v_source_userid, t1530.c101_party_id)
			   )
		   AND t1530.c1520_function_id = v_curr_func_id;

		IF (v_count > 0)
		THEN
			raise_application_error ('-20085', 'target left Menu link already existed.');
		END IF;

		INSERT INTO t1530_access
					(c1530_access_id, c1500_group_id, c1520_function_id
				   , c101_party_id, c1530_update_access_fl, c1530_read_access_fl, c1530_created_by, c1530_created_date
				   , c1520_parent_function_id, c1530_function_nm, c1530_seq_no
					)
			 VALUES (s1530_access.NEXTVAL, DECODE (v_source_userid, NULL, p_target_deptid, NULL), v_curr_func_id
				   , v_user_partyid, 'Y', 'Y', v_user_id, SYSDATE
				   , v_parent_func_id, NULL, v_seq_id + 1
					);

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'User Name: ' || p_source_user);
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Left Menu link Name: ' || p_target_func_nm);
		DBMS_OUTPUT.put_line ('Access right is added to the user ' || p_source_user);
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_link_access;
END it_pkg_left_menu_control;
/
