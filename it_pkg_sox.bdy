/* Formatted on 2011/02/01 15:54 (Formatter Plus v4.8.0) */

-- @"C:\DataCorrection\Script\it_pkg_sox.bdy"


CREATE OR REPLACE PACKAGE BODY it_pkg_sox
IS
--

	/*********************************************************
	* Description : This procedure is used to updated
	* Tag status from available to released
	*********************************************************/
	PROCEDURE   gm_unmap_inactive_users (
    p_user_id	   IN	T101_USER.c101_user_id%TYPE
   )
  AS 
  -- Load the Inactive users, who are not already voided from the mapping table
	CURSOR v_cur IS
      SELECT DISTINCT t101.c101_party_id PARTYID, t101.c101_user_id USRID
       FROM t101_user t101, t1501_group_mapping t1501
      WHERE t1501.c101_mapped_party_id = t101.c101_party_id
        AND t101.c901_user_status     <> 311
        AND t101.c101_party_id        IS NOT NULL
        AND t101.c101_party_id        != 0
        AND t1501.c1501_void_fl       IS NULL;
  BEGIN
	
	FOR v_item IN v_cur
	LOOP
	
	-- Void the User from the group mapping table
	     UPDATE t1501_GROUP_MAPPING
	        SET c1501_void_fl            = 'Y',
	            c1501_last_updated_by = p_user_id,
	            c1501_created_date = SYSDATE
	      WHERE c101_mapped_party_id = v_item.PARTYID
	        AND c1501_void_fl       IS NULL;
	        
	-- Void the user mapped directly in T1530_Access
	     UPDATE t1530_access
			SET c1530_void_fl      = 'Y',
			 C1530_LAST_UPDATED_BY = p_user_id,
	 		 C1530_LAST_UPDATED_DATE = SYSDATE
			  WHERE c101_party_id  = v_item.PARTYID
			    AND c101_party_id IS NOT NULL;
	END LOOP;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
  END gm_unmap_inactive_users;
	
END it_pkg_sox;
/
