
CREATE OR REPLACE
PACKAGE body it_pkg_gpb_acct_ship_param
IS
    /**********************************************************************************************
    * Description : This procedure used to fetch GPB accounts and the associated Globus Rep accounts
    Author: gpalani, Date: 09/29/2017
    Ref:TSK-8434, Data correction procedure.
    **********************************************************************************************/
PROCEDURE tmp_gpb_acct_ship_param_main (
        p_help_desk_id IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
        p_gpb_party_id IN t740_gpo_account_mapping.C101_PARTY_ID%TYPE DEFAULT NULL)
AS
    CURSOR cur_gpb_party
    IS
        SELECT DISTINCT (t740.c101_party_id) gpb_party_id, t740.c704_account_id acct_id
           FROM t740_gpo_account_mapping t740
          WHERE t740.c740_void_fl IS NULL
            AND t740.c101_party_id = p_gpb_party_id
		    AND t740.c101_party_id IS NOT NULL
       ORDER BY t740.c101_party_id, t740.c704_account_id;
BEGIN
    FOR cur_gpb IN cur_gpb_party
    LOOP
        BEGIN
            tmp_gpb_acct_ship_param_sync (cur_gpb.acct_id, p_help_desk_id, cur_gpb.gpb_party_id) ;
        EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line ('Group Price Book update failed for the group party id'||cur_gpb.gpb_party_id ||
            SQLERRM) ;
            ROLLBACK;
        END;
        dbms_output.put_line ('Group Price Book update Success for the group id:'||cur_gpb.gpb_party_id||' and for the Acct id:'||cur_gpb.acct_id) ;
    END LOOP;
END tmp_gpb_acct_ship_param_main;

/************************************************************************************************************
* Description : This procedure used to sync the Shipping Parameter values from GPB account to GLobus Accounts
Author: gpalani, Date: 09/29/2017
Ref:TSK-8434: Data correction procedure.
*************************************************************************************************************/

PROCEDURE tmp_gpb_acct_ship_param_sync (
        P_ACCT_ID      IN t704_account.c704_account_id%TYPE,
        P_HELP_DESK_ID IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
        P_PARTY_ID     IN t740_gpo_account_mapping.C101_PARTY_ID%TYPE DEFAULT NULL)
AS
    v_acct_id t740_gpo_account_mapping.c704_account_id%TYPE;
    v_party_id t740_gpo_account_mapping.C101_PARTY_ID%TYPE;
    v_ship_carrier t9080_ship_party_param.c901_ship_carrier%TYPE;
    v_ship_mode t9080_ship_party_param.c901_ship_mode%TYPE;
    v_standard_ship_charge_type t9080_ship_party_param.c901_standard_ship_charge_type%TYPE;
    v_standard_ship_payee t9080_ship_party_param.c901_standard_ship_payee%TYPE;
    v_standard_ship_value_pct t9080_ship_party_param.c9080_standard_ship_value_pct%TYPE;
    v_rush_ship_value_pct t9080_ship_party_param.c9080_rush_ship_value_pct%TYPE;
    v_rush_ship_charge_type t9080_ship_party_param.c901_rush_ship_charge_type%TYPE;
    v_rush_ship_payee t9080_ship_party_param.C901_RUSH_SHIP_PAYEE%TYPE;
    v_ship_param_id t9080_ship_party_param.c9080_ship_param_id%TYPE;
    v_user_id t101_user.c101_user_id%TYPE;
	
    CURSOR cur_gpb_acc
    IS
         SELECT c101_party_id party_id
           FROM t704_account t704
          WHERE t704.c704_void_fl   IS NULL
            AND t704.c704_account_id = P_ACCT_ID;
BEGIN
    -- Mandatory helpdesk to log executed user information
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL) ;
    v_user_id := it_pkg_cm_user.get_valid_user;
	
    SELECT DISTINCT t9080.c901_ship_carrier, t9080.c901_ship_mode, t9080.c901_standard_ship_charge_type
      , t9080.c901_standard_ship_payee, t9080.c901_rush_ship_charge_type, t9080.c901_rush_ship_payee
      , t9080.c9080_standard_ship_value_pct, t9080.c9080_rush_ship_value_pct
       INTO v_ship_carrier, v_ship_mode, v_standard_ship_charge_type
      , v_standard_ship_payee, v_rush_ship_charge_type, v_rush_ship_payee
      , v_standard_ship_value_pct, v_rush_ship_value_pct
       FROM t740_gpo_account_mapping t740, t9080_ship_party_param t9080
      WHERE t740.c740_void_fl       IS NULL
        AND t740.c101_party_id       = P_PARTY_ID
        AND t9080.c9080_void_fl     IS NULL
        AND t740.c101_party_id       = t9080.c101_party_id;        
		
    FOR gpb_acc  IN cur_gpb_acc
    LOOP
        v_party_id := gpb_acc.party_id;
        gm_pkg_cm_party.gm_sav_party_params ( NULL -- Third party id passing it as null
        , v_ship_carrier, v_ship_mode, v_standard_ship_payee, v_standard_ship_charge_type, v_standard_ship_value_pct,
        v_rush_ship_payee, v_rush_ship_charge_type, v_rush_ship_value_pct, P_ACCT_ID, v_party_id, v_user_id, NULL) ;
		
    END LOOP;
END tmp_gpb_acct_ship_param_sync;
END it_pkg_gpb_acct_ship_param;
/