

CREATE OR REPLACE PROCEDURE IT_PRC_CN_TXN_UPDATE_PRICE (
	p_cnid		  IN		 T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE
  , p_userid	  IN		 T504_CONSIGNMENT.C504_LAST_UPDATED_BY%TYPE
  )
 AS  
 
    v_ship_date   T504_CONSIGNMENT.C504_SHIP_DATE%TYPE;
    v_part_price  T2052_PART_PRICE_MAPPING.C2052_EQUITY_PRICE%TYPE;
    v_company_id  T504_CONSIGNMENT.C1900_company_id%TYPE;
    v_error_part  CLOB;
   
    CURSOR cur_consignment
    IS
    	SELECT C205_part_number_id pnum 
    	FROM T505_ITEM_CONSIGNMENT
    	WHERE C504_CONSIGNMENT_ID = p_cnid
    	AND C505_VOID_FL IS NULL;
    
    BEGIN  
	    
	    IF p_cnid is NULL THEN
	   	 	GM_RAISE_APPLICATION_ERROR('-20999','401','Consignment Id');
	    END IF;
	    
	   IF p_userid is NULL THEN
	   	 	GM_RAISE_APPLICATION_ERROR('-20999','401','User Id');
	    END IF;
	    
    	BEGIN
	    	Select C504_SHIP_DATE
	    	into v_ship_date
	    	from T504_CONSIGNMENT
	    	where C504_CONSIGNMENT_ID = p_cnid;
	    	EXCEPTION
		WHEN NO_DATA_FOUND 
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','405',p_cnid);
    	END; 
		
    	IF v_ship_date IS NOT NULL
    	THEN
    		GM_RAISE_APPLICATION_ERROR('-20999','406',p_cnid||'#$#'||v_ship_date);
    	END IF;
       
    	SELECT C1900_COMPANY_ID
    	INTO v_company_id
    	FROM T504_CONSIGNMENT
    	WHERE C504_CONSIGNMENT_ID = p_cnid;
    	
    	
    	
    	FOR consignment IN cur_consignment
    	LOOP
    		BEGIN
	    		SELECT C2052_EQUITY_PRICE
		    	INTO v_part_price
		    	FROM T2052_PART_PRICE_MAPPING
		    	WHERE C2052_VOID_FL IS NULL
		    	AND C1900_COMPANY_ID = v_company_id
		    	AND C205_PART_NUMBER_ID = consignment.pnum;
	    	EXCEPTION WHEN	OTHERS
	    	THEN
	    		v_error_part := v_error_part||consignment.pnum||',';
	    	END;
		--    DBMS_OUTPUT.PUT_LINE(':v_company_id:'||v_company_id||':v_part_price:'||v_part_price); 	
		    	
	    	IF v_error_part IS NULL AND v_part_price IS NOT NULL 
	    	THEN
	    		UPDATE T505_ITEM_CONSIGNMENT 
	    		SET C505_ITEM_PRICE = v_part_price
	    		WHERE C205_PART_NUMBER_ID = consignment.pnum
	    		AND   C504_CONSIGNMENT_ID = p_cnid;
	    		
	    	END IF;
	    	
    	END LOOP;
    	
    	IF v_error_part IS NOT NULL
	    	THEN
	    		GM_RAISE_APPLICATION_ERROR('-20999','407',v_error_part);
	    	END IF;

    END IT_PRC_CN_TXN_UPDATE_PRICE;
/