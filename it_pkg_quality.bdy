/* Formatted on 2011/02/04 11:01 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_quality.bdy"


CREATE OR REPLACE PACKAGE BODY it_pkg_quality
IS
--

	/*********************************************************
	* Description : This procedure is used to updated
	* Project Name and status
	*********************************************************/
	PROCEDURE gm_sav_project (
		p_help_desk_id		t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_project_id_from	t202_project.c202_project_id%TYPE
	  , p_project_id_to 	t202_project.c202_project_id%TYPE
	  , p_status			t202_project.c901_status_id%TYPE
	)
	AS
		v_user_id	   NUMBER;
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		UPDATE t202_project
		   SET c202_project_id = p_project_id_to
			 , c901_status_id = NVL (p_status, c901_status_id)
			 , c202_last_updated_by = v_user_id
			 , c202_last_updated_date = SYSDATE
		 WHERE c202_project_id = p_project_id_from;

		--
		IF (SQL%ROWCOUNT = 0)
		THEN
			ROLLBACK;
			raise_application_error ('-20085', 'No record is found to be updated.');
		END IF;

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Project ID from : ' || p_project_id_from);
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Project ID to : ' || p_project_id_to);
		DBMS_OUTPUT.put_line (SQL%ROWCOUNT || ' Row(s) are updated. ');
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_project;
	
/*********************************************************
* Description : This procedure is used to update partnumber insert id
*********************************************************/
PROCEDURE gm_upd_part_insert
  (
    p_part_num IN t205_part_number.c205_part_number_id%TYPE,
    p_ins_id   IN t205_part_number.c205_insert_id%TYPE )
AS
  v_user_id NUMBER;
BEGIN
  v_user_id := it_pkg_cm_user.get_valid_user;
  UPDATE t205_part_number
  SET c205_insert_id        = p_ins_id ,
    c205_last_updated_by    = v_user_id ,
    c205_last_updated_date  =sysdate
  WHERE c205_part_number_id = p_part_num;
  --
  IF (SQL%ROWCOUNT = 0) THEN
    ROLLBACK;
    raise_application_error ('-20085', 'No record is found to be updated.');
  END IF;
  COMMIT;
  --
EXCEPTION
WHEN OTHERS THEN
  DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
  ROLLBACK;
END gm_upd_part_insert;


/*********************************************************
* Description : This procedure is used to insert new  Material Spec
Author : Ganesh Kumar M
*********************************************************/


PROCEDURE gm_sav_material_Spec(
    p_help_desk_id		t914_helpdesk_log.c914_helpdesk_id%TYPE,
    p_code_nm  IN t901_code_lookup.c901_code_nm%TYPE,
    p_code_grp IN t901_code_lookup.c901_code_grp%TYPE
  )
AS
  v_user_id NUMBER;
  p_code_seq t901_code_lookup.c901_code_seq_no%TYPE;
BEGIN
  v_user_id := it_pkg_cm_user.get_valid_user;
  it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);  
  
  SELECT MAX(C901_Code_Seq_No )+1
  INTO p_code_seq
  FROM T901_Code_Lookup
  WHERE C901_Code_Grp = p_code_grp;
  
  INSERT
  INTO t901_code_lookup
    (
      c901_code_id,
      c901_code_nm,
      c901_code_grp ,
      c901_active_fl,
      c901_code_seq_no,
      c902_code_nm_alt ,
      c901_created_by,
      c901_created_date
    )
    VALUES
    (
      s901_code_lookup.nextval,
      p_code_nm,
      p_code_grp ,
      '1',
      p_code_seq,
      NULL ,
      v_user_id,
      Sysdate
    ) ;

EXCEPTION
WHEN OTHERS THEN
  DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
  ROLLBACK;
END gm_sav_material_Spec; 


/*********************************************************
* Description : This procedure is used to insert new  partnumber temp
Author : Thamarai selvan
*********************************************************/
PROCEDURE gm_sav_part_number_tmp
  (
    p_part_num      IN t205_part_number.c205_part_number_id%TYPE,
    p_part_num_desc IN t205_part_number.c205_part_num_desc%TYPE,
    p_proj_id       IN t205_part_number.c202_project_id%TYPE,
    p_prod_class    IN t205_part_number.c205_product_class%TYPE,
    p_prod_family   IN t205_part_number.c205_product_family%TYPE ,
    p_prod_mat      IN t205_part_number.c205_product_material%TYPE,
    p_part_draw     IN t205_part_number.c205_part_drawing%TYPE,
    p_mat_spec      IN t205_part_number.c205_material_spec%TYPE,
    p_meas_dev      IN t205_part_number.c205_measuring_dev%TYPE,
    p_rev_num       IN t205_part_number.c205_rev_num%TYPE,
    p_insp_rev      IN t205_part_number.c205_inspection_rev%TYPE,
    p_insert_id     IN t205_part_number.c205_insert_id%TYPE,
    p_mat_cerfl     IN t205_part_number.c205_material_cert_fl%TYPE,
    p_pack_prim     IN t205_part_number.c205_packaging_prim%TYPE,
    p_comp_cerfl    IN t205_part_number.c205_compliance_cert_fl%TYPE,
    p_pack_sec      IN t205_part_number.c205_packaging_sec%TYPE,
    p_act_fl        IN t205_part_number.c205_active_fl%TYPE,
    p_stat_id       IN t205_part_number.c901_status_id%TYPE,
    p_rpf           IN t205_part_number.c901_rpf%TYPE,
    p_qty_stock     IN NUMBER,
    p_uom           IN t205_part_number.c901_uom%TYPE,
    p_set_id        IN t207_set_master.c207_set_id% TYPE,
    p_us_req        IN t205d_part_attribute.c205d_attribute_value%TYPE,
    p_eu_req        IN t205d_part_attribute.c205d_attribute_value%TYPE    
    )
AS
 v_user_id NUMBER;
 v_set_id t207_set_master.c207_set_id% TYPE;
BEGIN
/*
select c207_set_id into v_set_id
from t207_set_master
where c207_set_nm = p_set_nm
AND c901_set_grp_type = 1600;
*/
v_user_id := it_pkg_cm_user.get_valid_user;

  INSERT
  INTO t205_part_number_temp
    (
      c205_part_number_id,
      c205_part_num_desc,
      c202_project_id,
      c205_product_class,
      c205_product_family ,
      c205_product_material,
      c205_part_drawing,
      c205_material_spec,
      c205_measuring_dev,
      c205_rev_num,
      c205_inspection_rev,
      c205_insert_id,
      c205_material_cert_fl,
      c205_packaging_prim,
      c205_compliance_cert_fl,
      c205_packaging_sec,
      c205_active_fl,
      c901_status_id,
      c901_rpf,
      c205_created_by,
      c205_created_date,
      --c205_qty_in_stock ,
      c901_uom,
      c207_set_id,
      c205_temp_us_req ,
      c205_temp_eu_req
    )
    VALUES
    (
      p_part_num,
      p_part_num_desc,
      p_proj_id  ,
      p_prod_class,p_prod_family,p_prod_mat,
      p_part_draw,
      p_mat_spec, p_meas_dev,
      p_rev_num,
      p_insp_rev,
      p_insert_id,
      p_mat_cerfl,
      p_pack_prim,
      p_comp_cerfl,
      p_pack_sec,
      p_act_fl,
      p_stat_id,p_rpf,v_user_id,
      SYSDATE,
      --p_qty_stock,
      p_uom,
      p_set_id, 
      p_us_req, 
      p_eu_req
    );


END gm_sav_part_number_tmp;
 /*********************************************************
* Description : This procedure is used to insert new  partnumber record
*********************************************************/
PROCEDURE gm_sav_part_number

 AS
 v_user_id NUMBER;
 v_set_id t207_set_master.c207_set_id% TYPE;
 v_count NUMBER := 0;
 v_row_cnt NUMBER := 0;

	CURSOR cur_temp
	IS
	  SELECT c205_part_number_id pnum FROM t205_part_number_temp;
BEGIN
/*
select c207_set_id into v_set_id
from t207_set_master
where c207_set_nm = p_set_nm
AND c901_set_grp_type = 1600;
*/
v_user_id := it_pkg_cm_user.get_valid_user;


      FOR var_temp IN cur_temp
      LOOP
      BEGIN
		insert into 
    t205_part_number (
              c205_part_number_id,
              c205_part_num_desc,
              c202_project_id,
              c205_product_class,
              c205_product_family ,
              c205_product_material,
              c205_part_drawing,
              c205_material_spec,
              c205_measuring_dev,
              c205_rev_num,
              c205_inspection_rev,
              c205_insert_id,
              c205_material_cert_fl,
              c205_packaging_prim,
              c205_compliance_cert_fl,
              c205_packaging_sec,
              c205_active_fl,
              c901_status_id,
              c901_rpf,
              c205_created_by,
              c205_created_date,
              --c205_qty_in_stock,
              c901_uom )
      select  c205_part_number_id,
              c205_part_num_desc,
              c202_project_id, 
              c205_product_class,
              c205_product_family ,
              c205_product_material,
              c205_part_drawing,
              c205_material_spec,
              c205_measuring_dev,
              c205_rev_num,
              c205_inspection_rev,
              c205_insert_id,
              c205_material_cert_fl,
              c205_packaging_prim,
              c205_compliance_cert_fl,
              c205_packaging_sec,
              c205_active_fl,
              c901_status_id,
              c901_rpf,
              c205_created_by,
              c205_created_date,
              --c205_qty_in_stock,
              c901_uom
        from t205_part_number_temp  
        where c205_part_number_id = var_temp.pnum ;

		 -- 300.026  Unassigned

		insert into    t208_set_details (c208_set_details_id, c207_set_id, c205_part_number_id, c208_set_qty)
		      select s208_setmap_id.NEXTVAL , c207_set_id, c205_part_number_id, 1
		       from t205_part_number_temp  where c205_part_number_id = var_temp.pnum;

		--80070	US
		--80088	N/A

		 INSERT INTO t205d_part_attribute
			  (c2051_part_attribute_id, c205_part_number_id, c901_attribute_type, c205d_attribute_value
			  , c205d_created_date, c205d_created_by
						)
			    SELECT s205d_part_attribute.NEXTVAL, c205_part_number_id, 80070, c205_temp_us_req , SYSDATE, v_user_id
			     from t205_part_number_temp  where c205_part_number_id = var_temp.pnum;

		--80071	EU
		--80104	N/A

		     INSERT INTO t205d_part_attribute
			  (c2051_part_attribute_id, c205_part_number_id, c901_attribute_type, c205d_attribute_value
			  , c205d_created_date, c205d_created_by
						)
			    SELECT s205d_part_attribute.NEXTVAL, c205_part_number_id, 80071, c205_temp_eu_req , SYSDATE, v_user_id
			     From T205_Part_Number_Temp  where c205_part_number_id = var_temp.pnum;
		EXCEPTION
		WHEN DUP_VAL_ON_INDEX THEN
		  DBMS_OUTPUT.put_line ('The Part : ' || var_temp.pnum || ' is  already exist.'  );
    WHEN OTHERS THEN 
      DBMS_OUTPUT.put_line ('The Part : ' || var_temp.pnum || ' has error.'|| SQLERRM );
      v_count := v_count + 1;    
    END;
    v_row_cnt := v_row_cnt + SQL%ROWCOUNT ;
	END LOOP;
     DBMS_OUTPUT.put_line ('Totally  ' || v_row_cnt || ' records are successfully inserted.' );     
  IF v_count > 0 THEN
      DBMS_OUTPUT.put_line('Totally ' || v_count || ' records has Error.');    
  END IF;
  --Delete the part number temp table
  BEGIN
  DELETE FROM t205_part_number_temp;
  END;
  COMMIT;
  --
EXCEPTION
WHEN OTHERS THEN
  DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
ROLLBACK;
END gm_sav_part_number;
/*********************************************************
* Description : This procedure is used to update the new project id (all place)
* Author :
*********************************************************/
PROCEDURE gm_upd_project_id (
        p_help_desk_id   IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
        p_old_project_id IN t202_project.c202_project_id%TYPE,
        p_new_project_id IN t202_project.c202_project_id%TYPE)
AS
    v_user_id NUMBER;
    --
    v_project_mile_ids VARCHAR2 (4000) ;
    v_project_team_ids VARCHAR2 (4000) ;
    --
    v_old_proj_cnt NUMBER;
    v_new_proj_cnt NUMBER;
BEGIN
    v_user_id := it_pkg_cm_user.get_valid_user;
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL) ;
    -- validate the old project id
        SELECT COUNT (1)
		   INTO v_old_proj_cnt
		   FROM t202_project
		  WHERE c202_project_id = p_old_project_id;
	-- validate the new project id
        SELECT COUNT (1)
		   INTO v_new_proj_cnt
		   FROM t202_project
		  WHERE c202_project_id = p_new_project_id;
	--		  
	IF v_old_proj_cnt = 0
	THEN
		raise_application_error ('-20999', 'Entered Project not found, please enter the valid old project ID.');
	END IF;
	IF v_new_proj_cnt <> 0
	THEN
		raise_application_error ('-20999', 'Entered Project already exists, Please enter the valid new project ID.');
	END IF;
    -- to get the old project milestone ids
    BEGIN
         SELECT RTRIM (XMLAGG (XMLELEMENT (e, c2005_project_milestone_id || ',')) .EXTRACT ('//text()'), ',')
           INTO v_project_mile_ids
           FROM t2005_project_milestone
          WHERE c202_project_id = p_old_project_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_project_mile_ids := NULL;
    END ;
    -- to get the old project team count ids
    BEGIN
         SELECT RTRIM (XMLAGG (XMLELEMENT (e, c2004_project_team_count_id || ',')) .EXTRACT ('//text()'), ',')
           INTO v_project_team_ids
           FROM t2004_project_team_count
          WHERE c202_project_id = p_old_project_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_project_team_ids := NULL;
    END ;
    --
    my_context.set_my_double_inlist_ctx (v_project_mile_ids, v_project_team_ids) ;
    -- to update the temp project id
     UPDATE t2005_project_milestone
    SET c202_project_id                 = 'QB00-00', C2005_LAST_UPDATED_BY = v_user_id, C2005_LAST_UPDATED_DATE = CURRENT_DATE
      WHERE C2005_PROJECT_MILESTONE_ID IN (SELECT TO_NUMBER(token) FROM v_double_in_list WHERE token IS NOT NULL) ;
    --
    DBMS_OUTPUT.put_line ('Project Milestone Ids: ' || v_project_mile_ids) ;
    DBMS_OUTPUT.put_line ('Project Milestone Ids update count : ' || SQL%ROWCOUNT) ;
    -- to update the temp project id
     UPDATE t2004_project_team_count
    SET C202_PROJECT_ID                  = 'QB00-00', c2004_last_updated_by = v_user_id, c2004_last_updated_date = CURRENT_DATE
      WHERE C2004_PROJECT_TEAM_COUNT_ID IN (SELECT TO_NUMBER(tokenii) FROM v_double_in_list WHERE tokenii IS NOT NULL) ;
    --
    DBMS_OUTPUT.put_line ('Project team count Ids: ' || v_project_team_ids) ;
    DBMS_OUTPUT.put_line ('Project team update count : ' || SQL%ROWCOUNT) ;
    --
  UPDATE T2021_PROJECT_COMPANY_MAPPING
	SET C202_PROJECT_ID     = 'QB00-00', c2021_last_updated_by = v_user_id, c2021_last_updated_date = CURRENT_DATE
  WHERE c202_project_id = p_old_project_id;
  --
    DBMS_OUTPUT.put_line ('Project company mapping update count : ' || SQL%ROWCOUNT) ;
  -- 
     UPDATE t202_project
    SET c202_project_id     = p_new_project_id, c202_last_updated_by = v_user_id, c202_last_updated_date = CURRENT_DATE
      WHERE c202_project_id = p_old_project_id;
    -- to update the new project id
     UPDATE t2005_project_milestone
    SET c202_project_id                 = p_new_project_id, C2005_LAST_UPDATED_BY = v_user_id, C2005_LAST_UPDATED_DATE = CURRENT_DATE
      WHERE C2005_PROJECT_MILESTONE_ID IN (SELECT TO_NUMBER(token) FROM v_double_in_list WHERE token IS NOT NULL) ;
    --
    DBMS_OUTPUT.put_line ('Project Milestone new project update count : ' || SQL%ROWCOUNT) ;
    -- to update the new project id
     UPDATE t2004_project_team_count
    SET C202_PROJECT_ID                  = p_new_project_id, c2004_last_updated_by = v_user_id, c2004_last_updated_date = CURRENT_DATE
      WHERE C2004_PROJECT_TEAM_COUNT_ID IN (SELECT TO_NUMBER(tokenii) FROM v_double_in_list WHERE tokenii IS NOT NULL) ;
    --
     UPDATE T2021_PROJECT_COMPANY_MAPPING
	SET C202_PROJECT_ID     = p_new_project_id, c2021_last_updated_by = v_user_id, c2021_last_updated_date = CURRENT_DATE
  WHERE c202_project_id = 'QB00-00';
  --
    DBMS_OUTPUT.put_line ('Project company mapping new project update count : ' || SQL%ROWCOUNT) ;
    --
END gm_upd_project_id; 
/*********************************************************
* Description : This procedure used to unlink the DI number details
* Author : Manikandan
*********************************************************/
PROCEDURE gm_unlink_di_number (
        p_help_desk_id IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
        p_part_number  IN t205_part_number.c205_part_number_id%TYPE)
AS
    v_part_cnt NUMBER;
    v_user_id NUMBER;
BEGIN
    v_user_id := it_pkg_cm_user.get_valid_user;
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL) ;
     SELECT COUNT (1)
       INTO v_part_cnt
       FROM t2060_di_part_mapping
      WHERE c205_part_number_id = p_part_number;
    -- to check part number already mapped to DI numer.
    IF v_part_cnt = 0 THEN
        raise_application_error ('-20999',
        'Part number '|| p_part_number ||' not mapped with any DI number; please provide the correct part number.') ;
    END IF;
    --to set the empty value for part number filed and update the status as Available.   
     UPDATE t2060_di_part_mapping
    SET c205_part_number_id     = NULL, c901_status = 103391 -- Available
      , c2060_last_updated_by   = v_user_id, c2060_last_updated_date = SYSDATE
      WHERE c205_part_number_id = p_part_number;
      
      -- to update all DI related details as null value
      
      -- 4000539 Issuing Agency Configuration
      -- 103379 DI Number Flag
      -- 103342 Subject to DM Exempt
      -- 4000540 Bulk Packaging
      -- 103381 UDI Etch Required
	  -- 103382 DI Only Etch Required
	  -- 103383 PI Only Etch Required
       UPDATE t205d_part_attribute
    SET c205d_attribute_value   = NULL, c205d_last_updated_by = v_user_id, c205d_last_updated_date = SYSDATE
      WHERE c205_part_number_id = p_part_number
        AND c901_attribute_type IN (4000539, 103379, 103342, 4000540, 103381, 103382, 103383); 
      -- 103343 DM DI is not Primary DI
      -- 103344 DM DI Number
      gm_pkg_pd_sav_udi.gm_sav_qa_dmdi_param (p_part_number, v_user_id);
      --103785 Device Count
      --4000542 Unit of Use DI Number
      --4000543 Package DI Number
      --4000544 Quantity per Package
      --4000545 Contains DI Package
      gm_pkg_pd_sav_udi.gm_sav_sub_com_bulk_parameter (p_part_number, v_user_id);
      --103782 Lot or Batch Number
      --103783 Manufacturing Date
      --103784 Serial Number
      --103347 Expiration Date
      --103786 Donation Identification Number
      gm_pkg_pd_sav_udi.gm_upd_product_identifiers(p_part_number, v_user_id);
      --
      DBMS_OUTPUT.put_line ('DI number and related parameters are removed successfully for this part: ' || p_part_number) ;
END gm_unlink_di_number;
/*****************************************************************************************
* Description : This procedure used to save the PD data attribute to part attribute table
* Author : Manikandan
******************************************************************************************/
PROCEDURE gm_pd_data_attribute_sync (
        p_help_desk_id IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
        p_project_id   IN t205_part_number.c202_project_id%TYPE)
AS
    v_user_id NUMBER;
    v_partnumber T205G_PD_PART_NUMBER.c205_part_number_id%TYPE;
    v_cnt       NUMBER;
    v_total_cnt NUMBER := 0;
    --
    v_inputstr VARCHAR2 (4000) ;
    v_has_latex t205g_pd_part_number.c901_has_latex%TYPE;
    v_req_sterile t205g_pd_part_number.c901_requires_sterilization%TYPE;
    v_sterile_method t205g_pd_part_number.c901_sterilization_method%TYPE;
    v_dev_more_size t205g_pd_part_number.c901_has_more_than_one_Size%TYPE;
    v_size1_type t205g_pd_part_number.c901_size1_type%TYPE;
    v_size1_text t205g_pd_part_number.c205g_size1_type_text%TYPE;
    v_size1_value t205g_pd_part_number.c205g_size1_value%TYPE;
    v_size1_uom t205g_pd_part_number.c901_size1_uom%TYPE;
    v_size2_type t205g_pd_part_number.c901_size2_type%TYPE;
    v_size2_text t205g_pd_part_number.c205g_size2_type_text%TYPE;
    v_size2_value t205g_pd_part_number.c205g_size2_value%TYPE;
    v_size2_uom t205g_pd_part_number.c901_size2_uom%TYPE;
    v_size3_type t205g_pd_part_number.c901_size3_type%TYPE;
    v_size3_text t205g_pd_part_number.c205g_size3_type_text%TYPE;
    v_size3_value t205g_pd_part_number.c205g_size3_value%TYPE;
    v_size3_uom t205g_pd_part_number.c901_size3_uom%TYPE;
    v_size4_type t205g_pd_part_number.c901_size4_type%TYPE;
    v_size4_text t205g_pd_part_number.c205g_size4_type_text%TYPE;
    v_size4_value t205g_pd_part_number.c205g_size4_value%TYPE;
    v_size4_uom t205g_pd_part_number.c901_size4_uom%TYPE;
    --
    CURSOR pd_existing_data_cur
    IS
         SELECT t205g.c205_part_number_id pnum, NVL (t205g.c205g_last_updated_by, t205g.c205g_created_by) user_id
           FROM t205g_pd_part_number t205g, t205_part_number t205
          WHERE t205.c205_part_number_id        = t205g.c205_part_number_id
            AND T205G.C205G_VOID_FL            IS NULL
            AND t205g.c205g_part_setup_sync_fl IS NULL
            AND t205g.C202_PROJECT_ID           = p_project_id;
BEGIN
    --
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL) ;
    --
     SELECT COUNT (1)
       INTO v_cnt
       FROM T202_PROJECT
      WHERE C202_PROJECT_ID = p_project_id;
    --
    IF v_cnt = 0 THEN
        raise_application_error ('-20999', 'Please enter the valid project id.') ;
    END IF;
    --
    FOR pd_data IN pd_existing_data_cur
    LOOP
        --
        v_partnumber := pd_data.pnum;
        v_user_id    := pd_data.user_id;
        v_total_cnt  := v_total_cnt + 1;
        --
        BEGIN
         --
         SELECT t205g.c901_has_latex latex, CASE
                WHEN t205g.c205_part_number_id LIKE '%S'
                THEN 80131
                ELSE t205g.c901_requires_sterilization
            END AS req_sterile, t205g.c901_sterilization_method AS sterile_method, t205g.c901_has_more_than_one_Size
            more_size, t205g.c901_size1_type size1, t205g.c205g_size1_type_text size1_text
          , t205g.c205g_size1_value size1_value, t205g.c901_size1_uom size1_uom, t205g.c901_size2_type size2
          , t205g.c205g_size2_type_text size2_text, t205g.c205g_size2_value size2_value, t205g.c901_size2_uom size2_uom
          , t205g.c901_size3_type size3, t205g.c205g_size3_type_text size3_text, t205g.c205g_size3_value size3_value
          , t205g.c901_size3_uom size3_uom, t205g.c901_size4_type size4, t205g.c205g_size4_type_text size4_text
          , t205g.c205g_size4_value size4_value, t205g.c901_size4_uom size4_uom
           INTO v_has_latex, v_req_sterile, v_sterile_method
          , v_dev_more_size, v_size1_type, v_size1_text
          , v_size1_value, v_size1_uom, v_size2_type
          , v_size2_text, v_size2_value, v_size2_uom
          , v_size3_type, v_size3_text, v_size3_value
          , v_size3_uom, v_size4_type, v_size4_text
          , v_size4_value, v_size4_uom
           FROM t205g_pd_part_number t205g
          WHERE t205g.c205_part_number_id = v_partnumber;
        --
        v_inputstr := '104460^'||v_has_latex||'|104461^'||v_req_sterile || '|104462^'|| v_sterile_method ||'|104463^'||
        v_dev_more_size || '|104464^'||v_size1_type ||'|104465^'||v_size1_text|| '|104466^'||v_size1_value ||'|104467^'
        ||v_size1_uom || '|104468^'||v_size2_type ||'|104469^'||v_size2_text || '|104470^'||v_size2_value ||'|104471^'
        ||v_size2_uom || '|104472^'||v_size3_type ||'|104473^'||v_size3_text || '|104474^'||v_size3_value ||'|104475^'
        ||v_size3_uom || '|104476^'||v_size4_type ||'|104477^'||v_size4_text || '|104478^'||v_size4_value ||'|104479^'
        ||v_size4_uom ||'|';
        --
        gm_pkg_pd_sav_udi.gm_sav_part_pd_udi_dtls (v_inputstr, v_partnumber, v_user_id) ;
        --
        gm_pkg_pd_sav_udi.gm_upd_proj_partdesc (v_partnumber, v_user_id) ;
        --
        COMMIT;
		--
        EXCEPTION
        WHEN OTHERS THEN
            DBMS_OUTPUT.put_line ('Error Occurred while Sync part #: ' || v_partnumber ||' Error: ' || SQLERRM) ;
            ROLLBACK;
        END;
    END LOOP ;
    --
    IF v_total_cnt = 0 THEN
        raise_application_error ('-20999', 'No PD data attribute to Sync.') ;
    END IF;
END gm_pd_data_attribute_sync;

/*****************************************************************************************
* Description : This procedure used to skip the UDI other validation.
* Author : Manikandan
******************************************************************************************/
PROCEDURE gm_skip_udi_other_validation(
	p_help_desk_id IN t914_helpdesk_log.c914_helpdesk_id%TYPE)
AS
  v_type_1_cnt NUMBER;
    v_type_2_cnt NUMBER;
    v_type_3_cnt NUMBER;
    v_type_4_cnt NUMBER;
    --
    v_di_number T2060_DI_PART_MAPPING.C2060_DI_NUMBER%TYPE;
    v_user_id NUMBER;
    v_cnt NUMBER := 0;
    v_part_cnt NUMBER;
    --
    CURSOR load_temp_parts_cur
    IS
         SELECT MY_TEMP_TXN_ID pnum FROM MY_TEMP_LIST;
BEGIN
	SELECT count(1) INTO v_part_cnt FROM MY_TEMP_LIST;
	
	IF v_part_cnt = 0 
	THEN
		raise_application_error ('-20999', 'No Part # available to skip the other validation.') ;
	END IF;
	
	v_user_id := it_pkg_cm_user.get_valid_user;
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL) ;
    
    -- Delete the Previous load
     DELETE
       FROM T2070A_UDI_PREV_DATA_LOAD
      WHERE C2060_DI_NUMBER IN
        (
             SELECT C2060_DI_NUMBER
               FROM T2060_DI_PART_MAPPING
              WHERE c205_part_number_id IN
                (
                     SELECT MY_TEMP_TXN_ID pnum FROM MY_TEMP_LIST
                )
        ) ;
        
    FOR tmp_parts IN load_temp_parts_cur
    LOOP
    	v_cnt := v_cnt + 1;
        -- get the Others count
         SELECT DECODE (C901_SIZE1_TYPE, 104047, 1, 0), DECODE (C901_SIZE2_TYPE, 104047, 1, 0), DECODE (C901_SIZE3_TYPE
            , 104047, 1, 0), DECODE (C901_SIZE4_TYPE, 104047, 1, 0)
           INTO v_type_1_cnt, v_type_2_cnt, v_type_3_cnt
          , v_type_4_cnt
           FROM T205G_PD_PART_NUMBER
          WHERE C205_PART_NUMBER_ID = tmp_parts.pnum;
          
          dbms_output.put_line (v_cnt || ' Before parts  ' || tmp_parts.pnum );
        -- check all the types and insert the temp data to attribute
        IF v_type_1_cnt = 1 THEN
             INSERT
               INTO t205d_part_attribute
                (
                    C2051_PART_ATTRIBUTE_ID, C205D_ATTRIBUTE_VALUE, C205D_CREATED_BY
                  , C205D_CREATED_DATE, C205_PART_NUMBER_ID, C901_ATTRIBUTE_TYPE
                )
                VALUES
                (
                    S205D_PART_ATTRIBUTE.NEXTVAL, 20, v_user_id
                  , SYSDATE, tmp_parts.pnum, '104466'
                ) ;
             INSERT
               INTO t205d_part_attribute
                (
                    C2051_PART_ATTRIBUTE_ID, C205D_ATTRIBUTE_VALUE, C205D_CREATED_BY
                  , C205D_CREATED_DATE, C205_PART_NUMBER_ID, C901_ATTRIBUTE_TYPE
                )
                VALUES
                (
                    S205D_PART_ATTRIBUTE.NEXTVAL, 104056, v_user_id
                  , SYSDATE, tmp_parts.pnum, '104467'
                ) ;
        END IF;
        
        --
        
        IF v_type_2_cnt = 1 THEN
             INSERT
               INTO t205d_part_attribute
                (
                    C2051_PART_ATTRIBUTE_ID, C205D_ATTRIBUTE_VALUE, C205D_CREATED_BY
                  , C205D_CREATED_DATE, C205_PART_NUMBER_ID, C901_ATTRIBUTE_TYPE
                )
                VALUES
                (
                    S205D_PART_ATTRIBUTE.NEXTVAL, 20, v_user_id
                  , SYSDATE, tmp_parts.pnum, '104470'
                ) ;
             INSERT
               INTO t205d_part_attribute
                (
                    C2051_PART_ATTRIBUTE_ID, C205D_ATTRIBUTE_VALUE, C205D_CREATED_BY
                  , C205D_CREATED_DATE, C205_PART_NUMBER_ID, C901_ATTRIBUTE_TYPE
                )
                VALUES
                (
                    S205D_PART_ATTRIBUTE.NEXTVAL, 104056, v_user_id
                  , SYSDATE, tmp_parts.pnum, '104471'
                ) ;
        END IF;
        
        --
        
        IF v_type_3_cnt = 1 THEN
             INSERT
               INTO t205d_part_attribute
                (
                    C2051_PART_ATTRIBUTE_ID, C205D_ATTRIBUTE_VALUE, C205D_CREATED_BY
                  , C205D_CREATED_DATE, C205_PART_NUMBER_ID, C901_ATTRIBUTE_TYPE
                )
                VALUES
                (
                    S205D_PART_ATTRIBUTE.NEXTVAL, 20, v_user_id
                  , SYSDATE, tmp_parts.pnum, '104474'
                ) ;
             INSERT
               INTO t205d_part_attribute
                (
                    C2051_PART_ATTRIBUTE_ID, C205D_ATTRIBUTE_VALUE, C205D_CREATED_BY
                  , C205D_CREATED_DATE, C205_PART_NUMBER_ID, C901_ATTRIBUTE_TYPE
                )
                VALUES
                (
                    S205D_PART_ATTRIBUTE.NEXTVAL, 104056, v_user_id
                  , SYSDATE, tmp_parts.pnum, '104475'
                ) ;
        END IF;
        
        --
        
        IF v_type_4_cnt = 1 THEN
             INSERT
               INTO t205d_part_attribute
                (
                    C2051_PART_ATTRIBUTE_ID, C205D_ATTRIBUTE_VALUE, C205D_CREATED_BY
                  , C205D_CREATED_DATE, C205_PART_NUMBER_ID, C901_ATTRIBUTE_TYPE
                )
                VALUES
                (
                    S205D_PART_ATTRIBUTE.NEXTVAL, 20, v_user_id
                  , SYSDATE, tmp_parts.pnum, '104478'
                ) ;
             INSERT
               INTO t205d_part_attribute
                (
                    C2051_PART_ATTRIBUTE_ID, C205D_ATTRIBUTE_VALUE, C205D_CREATED_BY
                  , C205D_CREATED_DATE, C205_PART_NUMBER_ID, C901_ATTRIBUTE_TYPE
                )
                VALUES
                (
                    S205D_PART_ATTRIBUTE.NEXTVAL, 104056, v_user_id
                  , SYSDATE, tmp_parts.pnum, '104479'
                ) ;
        END IF;
        --
    END LOOP;
    
    -- to run the Job
    gm_pkg_udi_data_load.gm_data_load_main;
    
    
    -- After job run
     UPDATE T2072_DI_GUDID_STATUS
    SET C901_INTERNAL_SUBMIT_STATUS = 104822 -- Review-In Progress
      WHERE C901_INTERNAL_SUBMIT_STATUS = 104821 -- WIP
      AND C2060_DI_NUMBER        IN
        (
             SELECT C2060_DI_NUMBER
               FROM T2060_DI_PART_MAPPING
              WHERE c205_part_number_id IN
                (
                     SELECT MY_TEMP_TXN_ID pnum FROM MY_TEMP_LIST
                )
        ) ;
        
    FOR after_tmp_parts IN load_temp_parts_cur
    LOOP
        -- get the Others count
         SELECT C2060_DI_NUMBER
           INTO v_di_number
           FROM T2060_DI_PART_MAPPING
          WHERE c205_part_number_id = after_tmp_parts.pnum;
          
        -- get the Others count
         SELECT DECODE (C901_SIZE1_TYPE, 104047, 1, 0), DECODE (C901_SIZE2_TYPE, 104047, 1, 0), DECODE (C901_SIZE3_TYPE
            , 104047, 1, 0), DECODE (C901_SIZE4_TYPE, 104047, 1, 0)
           INTO v_type_1_cnt, v_type_2_cnt, v_type_3_cnt
          , v_type_4_cnt
           FROM T205G_PD_PART_NUMBER
          WHERE C205_PART_NUMBER_ID = after_tmp_parts.pnum;
          
          
        IF v_type_1_cnt             = 1 THEN
            -- delete the source data
             DELETE
               FROM T9031_JOB_SOURCE_SYSTEM
              WHERE C9031_REF_ID          = v_di_number
                AND C9031_ATTRIBUTE_TYPE IN (104466, 104467) ;
            -- delete the part attribute data
             DELETE
               FROM t205d_part_attribute
              WHERE C205_PART_NUMBER_ID  = after_tmp_parts.pnum
                AND C901_ATTRIBUTE_TYPE IN (104466, 104467) ;
        END IF;
        
        
        IF v_type_2_cnt = 1 THEN
            -- delete the source data
             DELETE
               FROM T9031_JOB_SOURCE_SYSTEM
              WHERE C9031_REF_ID          = v_di_number
                AND C9031_ATTRIBUTE_TYPE IN (104470, 104471) ;
            -- delete the part attribute data
             DELETE
               FROM t205d_part_attribute
              WHERE C205_PART_NUMBER_ID  = after_tmp_parts.pnum
                AND C901_ATTRIBUTE_TYPE IN (104470, 104471) ;
        END IF;
        
        
        IF v_type_3_cnt = 1 THEN
            -- delete the source data
             DELETE
               FROM T9031_JOB_SOURCE_SYSTEM
              WHERE C9031_REF_ID          = v_di_number
                AND C9031_ATTRIBUTE_TYPE IN (104474, 104475) ;
            -- delete the part attribute data
             DELETE
               FROM t205d_part_attribute
              WHERE C205_PART_NUMBER_ID  = after_tmp_parts.pnum
                AND C901_ATTRIBUTE_TYPE IN (104474, 104475) ;
        END IF;
        
        
        IF v_type_4_cnt = 1 THEN
            -- delete the source data
             DELETE
               FROM T9031_JOB_SOURCE_SYSTEM
              WHERE C9031_REF_ID          = v_di_number
                AND C9031_ATTRIBUTE_TYPE IN (104478, 104479) ;
            -- delete the part attribute data
             DELETE
               FROM t205d_part_attribute
              WHERE C205_PART_NUMBER_ID  = after_tmp_parts.pnum
                AND C901_ATTRIBUTE_TYPE IN (104478, 104479) ;
        END IF;
        
        
    END LOOP; -- after_tmp_parts
	
	END gm_skip_udi_other_validation;

	/*****************************************************************************************
* Description : This procedure used to update the PD data atrribute Degree values.
*     Currently System not allow 0 vlaues. But some parts need to update 0.
*     Based on Part number and size name to update the Values.
*
* Author : Manikandan
******************************************************************************************/
	
PROCEDURE gm_update_pd_degree_value (
        p_part_number_id IN T205G_PD_PART_NUMBER.c205_part_number_id%TYPE,
        p_size_name      IN VARCHAR2,
        p_old_attr_value IN NUMBER,
        p_new_attr_value IN NUMBER,
        p_help_desk_id   IN t914_helpdesk_log.c914_helpdesk_id%TYPE)
        
AS
    v_cnt NUMBER := 0 ;
    v_user_id NUMBER; 
BEGIN
	-- to get the user information and insert log to helpdesk table
	v_user_id := it_pkg_cm_user.get_valid_user;
	
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL) ;
    
    --
     
    IF p_size_name              = 'SIZE1' THEN
    
         SELECT COUNT (1)
           INTO v_cnt
           FROM t205g_pd_part_number
          WHERE c205_part_number_id = p_part_number_id
            AND c901_size1_type     = 104046
            AND c205g_size1_value   = p_old_attr_value
            AND c205g_void_fl      IS NULL;
            
        --
        IF v_cnt <> 0 
		THEN
		
         UPDATE t205g_pd_part_number
        SET c205g_size1_value = p_new_attr_value, c205g_last_updated_by = v_user_id, c205g_last_updated_date =
            CURRENT_DATE
          WHERE c205_part_number_id = p_part_number_id
            AND c901_size1_type     = 104046
            AND c205g_size1_value   = p_old_attr_value
            AND c205g_void_fl      IS NULL;
			
        END IF;    
    ELSIF p_size_name               = 'SIZE2' THEN
    
         SELECT COUNT (1)
           INTO v_cnt
           FROM t205g_pd_part_number
          WHERE c205_part_number_id = p_part_number_id
            AND c901_size2_type     = 104046
            AND c205g_size2_value   = p_old_attr_value
            AND c205g_void_fl      IS NULL;
            
            
        --
        IF v_cnt <> 0 
		THEN
        
         UPDATE t205g_pd_part_number
        SET c205g_size2_value = p_new_attr_value, c205g_last_updated_by = v_user_id, c205g_last_updated_date =
            CURRENT_DATE
          WHERE c205_part_number_id = p_part_number_id
            AND c901_size2_type     = 104046
            AND c205g_size2_value   = p_old_attr_value
            AND c205g_void_fl      IS NULL;
            
        END IF;    
    ELSIF p_size_name               = 'SIZE3' THEN
    
    
         SELECT COUNT (1)
           INTO v_cnt
           FROM t205g_pd_part_number
          WHERE c205_part_number_id = p_part_number_id
            AND c901_size3_type     = 104046
            AND c205g_size3_value   = p_old_attr_value
            AND c205g_void_fl      IS NULL;
            
        --
        IF v_cnt <> 0 
		THEN
		
         UPDATE t205g_pd_part_number
        SET c205g_size3_value = p_new_attr_value, c205g_last_updated_by = v_user_id, c205g_last_updated_date =
            CURRENT_DATE
          WHERE c205_part_number_id = p_part_number_id
            AND c901_size3_type     = 104046
            AND c205g_size3_value   = p_old_attr_value
            AND c205g_void_fl      IS NULL;
        
		END IF;		
            
    ELSIF p_size_name               = 'SIZE4' THEN
    
         SELECT count (1)
           INTO v_cnt
           FROM t205g_pd_part_number
          WHERE c205_part_number_id = p_part_number_id
            AND c901_size4_type     = 104046
            AND c205g_size4_value   = p_old_attr_value
            AND c205g_void_fl      is null;
            
        --
        IF v_cnt <> 0 
		THEN
		
         UPDATE t205g_pd_part_number
        SET c205g_size4_value = p_new_attr_value, c205g_last_updated_by = v_user_id, c205g_last_updated_date =
            CURRENT_DATE
          WHERE c205_part_number_id = p_part_number_id
            AND c901_size4_type     = 104046
            AND c205g_size4_value   = p_old_attr_value
            AND c205g_void_fl      IS NULL;
			
		END IF;
		
    END IF;
    --
    
    dbms_output.put_line (' Part Number - ' || p_part_number_id || ' Size values update '|| p_size_name || ' Count '||
    v_cnt) ;
    
    --
    
END gm_update_pd_degree_value;
	
END it_pkg_quality;
/


