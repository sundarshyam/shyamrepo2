/* Formatted on 2011/01/20 14:53 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_order.bdy"


CREATE OR REPLACE PACKAGE BODY it_pkg_order
IS
--

	/*********************************************************
	* Description : This procedure is used to updated
	* rejected qty when its status is Inspection
	*********************************************************/
	PROCEDURE gm_sav_swap_rep_id (
		p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_order_id		 t501_order.c501_order_id%TYPE
	  , p_target_repid	 t501_order.c703_sales_rep_id%TYPE
	)
	AS
		v_user_id	   NUMBER;
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		UPDATE t501_order t501
		   SET t501.c703_sales_rep_id = p_target_repid
			 , t501.c501_last_updated_by = v_user_id
			 , t501.c501_last_updated_date = SYSDATE
		 WHERE t501.c501_order_id = p_order_id AND c501_void_fl IS NULL;

		--
		IF (SQL%ROWCOUNT = 0)
		THEN
			ROLLBACK;
			raise_application_error ('-20085', 'No record is found to be updated.');
		END IF;

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Order ID: ' || p_order_id);
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'New Rep Name : ' || get_rep_name (p_target_repid));
		DBMS_OUTPUT.put_line (SQL%ROWCOUNT || ' Row(s) are updated. ');
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_swap_rep_id;

	/*********************************************************
	* Description : This procedure is used to update Back Order date
	* Author: Yoga
	*********************************************************/
	PROCEDURE gm_upd_back_order_date (
	  p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_order_id		 t501_order.c501_order_id%TYPE

	)
	AS
		v_user_id	   NUMBER;
		v_inv_id	   t501_order.C503_INVOICE_ID%TYPE;
		v_status_fl	   t501_order.C501_STATUS_FL%TYPE;
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		BEGIN
		  SELECT t501.c501_status_fl, c503_invoice_id
		    INTO v_status_fl,v_inv_id
		    FROM t501_order t501
		   WHERE t501.c501_order_id = p_order_id 
		     AND c501_void_fl IS NULL 
	          FOR UPDATE;
		EXCEPTION
		  WHEN NO_DATA_FOUND THEN
		    raise_application_error ('-20085', 'No record found');
		END;

		IF v_status_fl>0 THEN
		  raise_application_error ('-20085', 'It is not a Back Order');
		END IF;

		IF v_inv_id is not null THEN
		  raise_application_error ('-20085', 'It is already invoiced, cannot be updated');
		END IF;

		UPDATE t501_order t501
		   SET t501.c501_order_date = trunc(sysdate)
			 , t501.c501_last_updated_by = v_user_id
			 , t501.c501_last_updated_date = SYSDATE
		 WHERE t501.c501_order_id = p_order_id AND c501_void_fl IS NULL;

		--
		IF (SQL%ROWCOUNT = 0)
		THEN
			ROLLBACK;
			raise_application_error ('-20085', 'Unable to update');
		END IF;

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Order ID: ' || p_order_id);
		DBMS_OUTPUT.put_line (SQL%ROWCOUNT || ' Row(s) are updated. ');
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_upd_back_order_date;

	/*********************************************************
	* Description : This procedure is used to update Order date
	*               (This procedure is created for orders 
	*				entered for the Holidays ( Thanksgiving / Easter ))
	*				created as part of TKT-11428
	* Author: VPrasath
	*********************************************************/
	PROCEDURE gm_upd_order_date (
		p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_order_id		 t501_order.c501_order_id%TYPE
	  , p_order_date	 t501_order.c501_order_date%TYPE
	)
	AS
		v_user_id	   NUMBER;
		v_inv_id	   t501_order.C503_INVOICE_ID%TYPE;
		v_status_fl	   t501_order.C501_STATUS_FL%TYPE;
		v_format 	   VARCHAR2(20);	
	--
	BEGIN
		-- Mandatory help desk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);

		v_user_id	:= it_pkg_cm_user.get_valid_user;

		BEGIN
		  SELECT t501.c501_status_fl, c503_invoice_id
		    INTO v_status_fl,v_inv_id
		    FROM t501_order t501
		   WHERE t501.c501_order_id = p_order_id 
		     AND c501_void_fl IS NULL 
	          FOR UPDATE;
		EXCEPTION
		  WHEN NO_DATA_FOUND THEN
		    raise_application_error ('-20085', 'No record found');
		END;

		UPDATE t501_order t501
		   SET t501.c501_order_date = p_order_date
			 , t501.c501_last_updated_by = v_user_id
			 , t501.c501_last_updated_date = SYSDATE
		 WHERE t501.c501_order_id = p_order_id 
		   AND c501_void_fl IS NULL;

 	   --
		IF (SQL%ROWCOUNT = 0)
		THEN
			ROLLBACK;
			raise_application_error ('-20085', 'Unable to update');
		END IF;

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Order ID: ' || p_order_id);
		DBMS_OUTPUT.put_line (SQL%ROWCOUNT || ' Row(s) are updated. ');
		COMMIT;
		--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_upd_order_date;
	
	/*********************************************************
	* Description : This procedure is used to update part
	*				number in an order if the month is
	*				crossed and the users are not able to
	*				void the order.
	* Author: VPrasath
	*********************************************************/
	PROCEDURE gm_upd_pnum_in_order (
		p_help_desk_id	 	t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_order_id		 	t501_order.c501_order_id%TYPE
	  , p_from_part_number	t502_item_order.c205_part_number_id%TYPE
	  , p_to_part_number	t502_item_order.c205_part_number_id%TYPE
	)
	AS
		v_user_id	   NUMBER;
		v_inv_id	   t501_order.C503_INVOICE_ID%TYPE;
		v_status_fl	   t501_order.C501_STATUS_FL%TYPE;
		v_from_price   t502_item_order.c502_item_price%TYPE;
		v_to_price     t502_item_order.c502_item_price%TYPE;	
		v_acc_id	   t501_order.c704_account_id%TYPE;
		v_row_count	   NUMBER; 	
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);

		v_user_id	:= it_pkg_cm_user.get_valid_user;

		BEGIN
		  SELECT t501.c501_status_fl, c503_invoice_id, c704_account_id
		    INTO v_status_fl,v_inv_id, v_acc_id
		    FROM t501_order t501
		   WHERE t501.c501_order_id = p_order_id 
		     AND c501_void_fl IS NULL 
	          FOR UPDATE;
		EXCEPTION
		  WHEN NO_DATA_FOUND THEN
		    raise_application_error ('-20085', 'No record found');
		END;
		
		IF v_status_fl>0 THEN
		  raise_application_error ('-20085', 'It is not a Back Order');
		END IF;

		IF v_inv_id is not null THEN
		  raise_application_error ('-20085', 'It is already invoiced, cannot be updated');
		END IF;

		UPDATE t501_order t501
		   SET t501.c501_last_updated_by = v_user_id
			 , t501.c501_last_updated_date = SYSDATE
		 WHERE t501.c501_order_id = p_order_id 
		   AND c501_void_fl IS NULL;

        UPDATE t502_item_order t502 
		   SET t502.c205_part_number_id = p_to_part_number
		 WHERE t502.c501_order_id = p_order_id
		   AND t502.c205_part_number_id = p_from_part_number;
   
         v_row_count := SQL%ROWCOUNT ;
		--
		IF (v_row_count = 0)
		THEN
			ROLLBACK;
			raise_application_error ('-20085', 'Unable to update order #: ' || p_order_id);
		END IF;
		
		IF (v_row_count > 1)
		THEN
			ROLLBACK;
			raise_application_error ('-20085', 'Same part number has more than one row, verify manually for order #: ' || p_order_id || ', part number: ' || p_from_part_number);
		END IF;

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Order ID: ' || p_order_id);
		DBMS_OUTPUT.put_line (SQL%ROWCOUNT || ' Row(s) are updated. ');
		COMMIT;
		--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_upd_pnum_in_order;
	
	/*********************************************************
	* Description : This procedure is used convert Back order to 
	* Author: Yoga
	*********************************************************/
	PROCEDURE gm_upd_bo_to_bill_only (
	  p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	 , p_order_id		 varchar2
	)
	AS
	  v_user_id	   NUMBER;
	  v_status_fl	   t501_order.C501_STATUS_FL%TYPE;

	  CURSOR order_cur 
	    IS 
	      SELECT t501.c501_order_id ID, get_account_name(t501.c704_account_id) aname 
		, get_dist_rep_name (t501.c703_sales_rep_id) repdistnm, TO_CHAR(t501.c501_order_date, 'MM/DD/YYYY') dt 
		, t501.c501_customer_po po, t501.c501_total_cost + NVL(t501.c501_ship_cost, 0) sales 
		, t501.c501_ship_to shipto, t501.c501_ship_to_id shiptoid, t501.c704_account_id accid 
		, t501.c501_parent_order_id master_id, t502.c205_part_number_id num, t502.c502_item_qty qty 
		, t502.c502_item_price price, get_partnum_desc (t502.c205_part_number_id) dsc 
		, c901_order_type ordertype, t501.c501_status_fl status_fl
		FROM t501_order t501 , t502_item_order t502 , 
		  (SELECT DISTINCT ad_id, ad_name, region_id, region_name, d_id, d_name, rep_id, 
		    rep_name, d_active_fl , ter_name 
		    FROM v700_territory_mapping_detail v700) v700 
	       WHERE t501.c501_order_id = t502.c501_order_id 
		 AND t501.c901_order_type = 2525 
		 AND t501.c501_delete_fl IS NULL 
		 AND t501.c501_void_fl IS NULL 
		 AND t501.c501_status_fl = 0 
		 AND v700.rep_id = t501.c703_sales_rep_id 
		 AND t501.C501_ORDER_ID IN 
		 (SELECT token FROM v_in_list)
	      ORDER BY t502.c205_part_number_id, t501.c501_order_date; 
	BEGIN
	  -- Mandatory helpdesk to log executed user information
	  it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
	  v_user_id	:= it_pkg_cm_user.get_valid_user;

	  my_context.set_my_inlist_ctx (p_order_id);

	  FOR order_val IN order_cur 
	    LOOP 
              BEGIN 
		IF order_val.status_fl>0 THEN
		  DBMS_OUTPUT.put_line (order_val.ID || ' is not a Back Order');
		ELSE

                  gm_save_order_consign_to_cogs (order_val.ID, v_user_id); 

		  -- logs
		  it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Order ID: ' || order_val.ID);
		  DBMS_OUTPUT.put_line (SQL%ROWCOUNT || ' Row(s) are updated. ');
		  COMMIT;
		  --
		END IF;
	      EXCEPTION 
                WHEN OTHERS 
                  THEN 
                    DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM); 
                    ROLLBACK; 
	      END; 
	  END LOOP; 
	EXCEPTION
	  WHEN NO_DATA_FOUND THEN
	    DBMS_OUTPUT.put_line ('Error Occurred : ' || SQLERRM);
	END gm_upd_bo_to_bill_only;
	
	/*************************************************************************************************
	* Description : This procedure is used convert order types to Bill Only - From Sales Consignments
	* Author      : karthik
	**************************************************************************************************/
	
	PROCEDURE gm_upd_order_type (
	  p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_order_id		 t501_order.c501_order_id%TYPE

	)
	AS
  		v_user_id	   NUMBER;
 		v_order_cnt    NUMBER;
 		v_void_fl      t501_order.c501_void_fl%TYPE;
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
    	v_user_id	:= it_pkg_cm_user.get_valid_user;

    	BEGIN
    	   SELECT t501.c501_void_fl 
        	  INTO v_void_fl
		    FROM   t501_order t501
		   WHERE t501.c501_order_id = p_order_id;
		   EXCEPTION
		  WHEN NO_DATA_FOUND THEN
		    raise_application_error ('-20085', 'No record found');
		 END;
		
		IF v_void_fl IS NOT NULL THEN
		  raise_application_error ('-20085', 'The order is already voided');
		END IF;
		
		   SELECT count(t502.c205_part_number_id)
         	 INTO v_order_cnt  
		    FROM t502_item_order t502 , t501_order t501
		   WHERE t502.c501_order_id = p_order_id
        	 AND t501.c501_order_id = t502.c501_order_id
        	 AND t501.c501_void_fl IS NULL
		     AND t502.c502_void_fl IS NULL;

		IF v_order_cnt > 0 THEN
		  raise_application_error ('-20085', 'Cannot convert the order type,this order id having  part numbers');
		END IF;
    
		/*
		 * if the part numbers are not available for the orderand the price is $0 value in WIP dash
		 * Then we are removing the order's from WIP Dash by changing the order type to 2532 -- Bill Only - From Sales Consignments
		 */
		IF v_order_cnt = 0 THEN
			UPDATE t501_order t501
			   SET t501.c901_order_type =  2532 -- Bill Only - From Sales Consignments
				 , t501.c501_last_updated_by = v_user_id
				 , t501.c501_last_updated_date = SYSDATE
			 WHERE t501.c501_order_id = p_order_id 
	     		AND c501_void_fl IS NULL;
		END IF;
		--
		
		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Order ID: ' || p_order_id);
		  DBMS_OUTPUT.put_line (' order id '||p_order_id || ' type changed to Bill Only - From Sales Consignments ');
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing: ' || SQLERRM);
			ROLLBACK;
	END gm_upd_order_type;
	
	/*************************************************************************************************
	* Description : This procedure is used to update hold flag
	* Author      : Aprasath
	**************************************************************************************************/
	
	PROCEDURE gm_upd_order_hold_flag (
	  p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_order_id		 t501_order.c501_order_id%TYPE
	  , p_hold_flag 	 t501_order.c501_hold_fl%TYPE
	)
	AS
  		v_user_id	   NUMBER;
 		v_hold_fl      t501_order.c501_hold_fl%TYPE;
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
    	v_user_id	:= it_pkg_cm_user.get_valid_user;

    	BEGIN
    	   SELECT t501.c501_hold_fl 
        	  INTO v_hold_fl
		    FROM   t501_order t501
		   WHERE t501.c501_order_id = p_order_id;
		   EXCEPTION
		  WHEN NO_DATA_FOUND THEN
		    raise_application_error ('-20085', 'No record found');
		 END;			
		
			UPDATE t501_order t501
			   SET t501.c501_hold_fl =  p_hold_flag 
				 , t501.c501_last_updated_by = v_user_id
				 , t501.c501_last_updated_date = SYSDATE
			 WHERE t501.c501_order_id = p_order_id 
	     		AND c501_void_fl IS NULL;				
		
		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Order ID: ' || p_order_id);
		  DBMS_OUTPUT.put_line (' order id '||p_order_id || ' hold flag updated ');
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing: ' || SQLERRM);
			ROLLBACK;
	END gm_upd_order_hold_flag;
	
	/*************************************************************************************************
	* Description : This procedure is used to update order to bill only loaner
	* Author      : Aprasath
	**************************************************************************************************/
	
	PROCEDURE gm_upd_to_bill_only_loaner (
	  p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_order_id		 t501_order.c501_order_id%TYPE
	)
	AS
  		v_user_id	   NUMBER;
  		v_tot_item	   NUMBER;
  		v_ln_item	   NUMBER;
 		v_order_type   t501_order.c901_order_type%TYPE;
 		v_company_id   t1900_company.c1900_company_id%TYPE;
        v_plant_id     t5040_plant_master.c5040_plant_id%TYPE;
 		v_msg VARCHAR2(1000);
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
    	v_user_id	:= it_pkg_cm_user.get_valid_user;

    	BEGIN
    	   SELECT t501.c901_order_type, t501.c1900_company_id, t501.c5040_plant_id 
        	  INTO v_order_type,v_company_id,v_plant_id
		    FROM   t501_order t501
		   WHERE t501.c501_order_id = p_order_id AND t501.c501_void_fl IS NULL
		   	AND NVL(c901_order_type,'-999') <> 2530;
		   EXCEPTION
		  WHEN NO_DATA_FOUND THEN
		    raise_application_error ('-20085', 'Order type already in Bill only loaner' || p_order_id);
		 END;
		 
		 gm_pkg_cor_client_context.gm_sav_client_context(v_company_id,v_plant_id);
		
			SELECT COUNT(*) ,SUM(DECODE(C901_TYPE,50301,1,0)) 
			 INTO v_tot_item,v_ln_item 
			 FROM T502_ITEM_ORDER 
			 WHERE C501_ORDER_ID=p_order_id AND c502_void_fl IS NULL; 
			
			 --If all Items in order are of Loaner type, order should be updated to BillOnlyLoaner. 
			 --This happens when few items are maked as 'L' and few are maked as 'C' and all of 'C' parts. 
			 IF(v_tot_item =v_ln_item ) 
			 THEN 			 
			
			 UPDATE t501_order 
			 SET c501_status_fl = 3 
			 , c501_last_updated_by = v_user_id 
			 , c501_last_updated_date = CURRENT_DATE 
			 , c901_order_type = 2530 
			 WHERE c501_order_id = p_order_id; 		
			 
			 --TO UPDATE POSTING
			 	gm_pkg_cm_shipping_trans.gm_sav_order_ship (p_order_id, 0, v_user_id, v_msg); 
			 	
			 UPDATE t502_item_order
		        SET c502_control_number='NOC#'
		        WHERE c901_type        =50301
		        AND c501_order_id      =p_order_id
		        AND c502_void_fl      IS NULL;
	        
	         UPDATE t907_shipping_info
	          SET c907_void_fl        ='Y',
	            c907_last_updated_by  =706328,
	            c907_last_updated_date=sysdate
	          WHERE c907_ref_id       = p_order_id
	          AND c907_void_fl       IS NULL;	
			 
			 END IF;  
		
		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Order ID: ' || p_order_id);
		  DBMS_OUTPUT.put_line (' order id '||p_order_id || ' changed to Bill only loaner ');
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing: ' || SQLERRM);
			ROLLBACK;
	END gm_upd_to_bill_only_loaner;
	
	
	
	/*****************************************************************************************************
	* Description : This procedure is used to reclass invoices
  	*                to reflect for the month prior for OUS
	* Author      : Mahavishnu
	**************************************************************************************************/
	
	
	PROCEDURE IT_PRC_INVOICE_RECLASS (
	p_invoiceid		  IN		 T503_INVOICE.C503_INVOICE_ID%TYPE
  , p_reclassifyto	  IN		 VARCHAR2
  , p_dateformat      IN		 VARCHAR2
  , p_userid		  IN		 T503_INVOICE.C503_CREATED_BY%TYPE
  )
 AS  
 
    v_void_fl        T503_INVOICE.c503_void_fl%TYPE;
    v_company_id     T503_INVOICE.C1900_COMPANY_ID%TYPE;
      
    BEGIN  
	    dbms_output.put_line('p_invoiceid'||p_invoiceid);
		dbms_output.put_line('p_reclassifyto'||p_reclassifyto);
		dbms_output.put_line('p_dateformat'||p_dateformat);
		dbms_output.put_line('p_userid'||p_userid);
	    IF p_invoiceid is NULL THEN
		
	   	 	GM_RAISE_APPLICATION_ERROR('-20999','401','Invoice Id');
	    END IF;
	    
	     IF p_reclassifyto is NULL THEN
	   	 	GM_RAISE_APPLICATION_ERROR('-20999','401','Reclassify Date');
	    END IF;
	    
	     IF p_dateformat is NULL THEN
	   	 	GM_RAISE_APPLICATION_ERROR('-20999','401','Date format');
	    END IF;
	    
	     IF p_userid is NULL THEN
	   	 	GM_RAISE_APPLICATION_ERROR('-20999','401','User Id');
	    END IF;
	    
	     IF (to_date(p_reclassifyto,p_dateformat) > CURRENT_DATE) THEN
	     	GM_RAISE_APPLICATION_ERROR('-20999','404','User Id');
	     END IF;
	    
    	BEGIN
	    	Select c503_void_fl,C1900_COMPANY_ID into v_void_fl,v_company_id from T503_INVOICE
	    	where C503_INVOICE_ID=p_invoiceid;
	    	EXCEPTION
		WHEN NO_DATA_FOUND 
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','403',p_invoiceid);
    	END; 
	
       
    	IF v_void_fl IS NULL THEN
    	
    		update T503_INVOICE set C503_LAST_UPDATED_BY=p_userid,C503_LAST_UPDATED_DATE=SYSDATE,C503_INVOICE_DATE= to_date(p_reclassifyto,p_dateformat) 
    		where C503_INVOICE_ID=p_invoiceid;
    
    		update t501_order set C501_ORDER_DATE= to_date(p_reclassifyto,p_dateformat), C501_SHIPPING_DATE= to_date(p_reclassifyto,p_dateformat), 
    		C501_ORDER_DATE_TIME= to_date(p_reclassifyto,p_dateformat) 
    		,C501_LAST_UPDATED_BY=p_userid,C501_LAST_UPDATED_DATE=SYSDATE 
    		where C503_INVOICE_ID=p_invoiceid AND C1900_COMPANY_ID=v_company_id;
        
    	ELSE 
    			GM_RAISE_APPLICATION_ERROR('-20999','402',p_invoiceid);
    	END IF;
    

    END IT_PRC_INVOICE_RECLASS;
    
    /*****************************************************************************************************
	* Description : This procedure is used to update the lot status to sold
	* Author      : karthik
	**************************************************************************************************/
	PROCEDURE GM_UPDATE_LOT_STATUS(
	    p_help_desk_id IN t914_helpdesk_log.c914_helpdesk_id%TYPE ,
	    P_ORDER_ID IN T501_ORDER.C501_ORDER_ID%TYPE,
	    p_user_id IN T2550_PART_CONTROL_NUMBER.C2550_LAST_UPDATED_BY%TYPE
	    )
	AS
	v_lot_status t2550_part_control_number.c2550_lot_status%TYPE;
	v_lot_num    t2550_part_control_number.c2550_control_number%TYPE;
	
	  CURSOR cur_lot
	  IS
		SELECT t502b.C502b_usage_lot_num lotnum,
	        t2550.c2550_lot_status lotstatus
	      FROM t502b_item_order_usage T502B ,
	        t2550_part_control_number t2550 ,
	         t501_order t501
	      WHERE T502B.C502B_USAGE_LOT_NUM = T2550.C2550_CONTROL_NUMBER (+)
	      AND get_part_attr_value_by_comp(T502B.c205_part_number_id,'104480',t501.c1900_company_id) = 'Y'
	      AND T502B.C501_ORDER_ID = P_ORDER_ID
	      AND T502B.C501_ORDER_ID = t501.C501_ORDER_ID
	      AND C502B_USAGE_LOT_NUM  IS NOT NULL
	      AND t2550.C2550_LOT_STATUS IS NULL;

	BEGIN
	  FOR lot_to_update IN cur_lot
	  LOOP
	   		v_lot_status := lot_to_update.lotstatus;
            v_lot_num      := lot_to_update.lotnum;
            
	    IF v_lot_status IS NULL THEN
	    
	      UPDATE T2550_PART_CONTROL_NUMBER
	      SET C2550_LOT_STATUS       = '105006', --SOLD
	          C2550_LAST_UPDATED_DATE = SYSDATE,
        	  C2550_LAST_UPDATED_BY= p_user_id
	      WHERE C2550_CONTROL_NUMBER = v_lot_num;
	      
	      UPDATE T5060_CONTROL_NUMBER_INV
	      SET C5060_QTY              = '0',
	      	c5060_last_update_trans_id = p_order_id,
	        c901_last_transaction_type = '4310', 
      		c5060_last_updated_by = p_user_id, 
      		c5060_last_updated_date = SYSDATE 
	      WHERE C5060_CONTROL_NUMBER = v_lot_num
	      AND C5060_QTY              = '1'
	       AND c901_warehouse_type = 4000339 ;
	      
	    END IF;
	     it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Order ID: ' || p_order_id);
		  DBMS_OUTPUT.put_line ('LOT <'|| v_lot_num || '> status changed to sold');
		COMMIT;
	    
	  END LOOP;
	 EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing: ' || SQLERRM);
			ROLLBACK;
	END GM_UPDATE_LOT_STATUS;
	
	/*****************************************************************************************************
	* Description : This procedure is used to update the lot comapny id
	* Author      : karthik
	**************************************************************************************************/
	PROCEDURE gm_upd_lot_dtl(
	    p_help_desk_id IN t914_helpdesk_log.c914_helpdesk_id%TYPE ,
	    p_user_id IN T2550_PART_CONTROL_NUMBER.C2550_LAST_UPDATED_BY%TYPE
	    )
	AS
	v_lot_num    t2550_part_control_number.c2550_control_number%TYPE;
	v_comp_id    t2550_part_control_number.C1900_COMPANY_ID%TYPE;
	v_count NUMBER;
	  CURSOR cur_lot
	  IS
		SELECT DISTINCT T502B.C502_CONTROL_NUMBER lotnum
	      FROM T502_ITEM_ORDER T502B ,
	        t2550_part_control_number t2550 ,
	         T501_ORDER T501
	      WHERE T502B.C502_CONTROL_NUMBER = T2550.C2550_CONTROL_NUMBER
	      AND T501.C704_ACCOUNT_ID = 14040 --Globus Medical Inc.
	      AND T502B.C501_ORDER_ID = T501.C501_ORDER_ID
       	  AND T501.C1900_COMPANY_ID = 1001 --BBA
       	  AND  T501.C1900_COMPANY_ID =  t2550.C1900_COMPANY_ID
          AND T502B.C502_CONTROL_NUMBER <> 'NOC#'
          AND T501.C501_VOID_FL IS NULL;


	BEGIN
	  FOR lot_to_update IN cur_lot
	  LOOP
            v_lot_num      := lot_to_update.lotnum;
            
                    
            SELECT  COUNT(1) INTO v_count           
             FROM t408a_dhr_control_number t408a, t408_dhr t408
            WHERE   t408a.c408_dhr_id=t408.c408_dhr_id
            AND t408.c1900_company_id = 1000
            AND c408_status_fl = 4
            AND c408a_conrol_number = v_lot_num
            AND c408_void_fl IS NULL
            AND c408a_void_fl IS NULL;

        IF v_count > 0 THEN

        UPDATE T2550_PART_CONTROL_NUMBER
	      SET C1900_COMPANY_ID       = 1000, 
	          C2550_LAST_UPDATED_DATE = SYSDATE,
        	  C2550_LAST_UPDATED_BY = p_user_id
	      WHERE C2550_CONTROL_NUMBER = v_lot_num
	       AND C1900_COMPANY_ID = 1001;
	       
	        it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Lot Number: ' || v_lot_num);
	        DBMS_OUTPUT.put_line ('LOT <'|| v_lot_num || '> companyid updated');
	      
	    END IF;
	    
	    
		  
		COMMIT;
	    
	  END LOOP;
	 EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing: ' || SQLERRM);
			ROLLBACK;
	END gm_upd_lot_dtl;
	
	
	/*************************************************************************************************
	* Description : This procedure is used to void the draft order
	* Author      : Aprasath
	**************************************************************************************************/
	
	PROCEDURE gm_void_draft_order (
	  p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_order_id		 t501_order.c501_order_id%TYPE
	  , p_user_id 	 t501_order.c501_last_updated_by%TYPE
	)
	AS  		
		v_order_id t501_order.c501_order_id%TYPE;
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
    	

    	BEGIN
    	    SELECT c501_order_id INTO 
				 v_order_id
				 FROM t501_order 
				WHERE c501_order_id=p_order_id
				AND c901_order_type='2518' -- draft
				AND c501_void_fl  IS NULL
				AND c501_status_fl =8; -- Release to CS
		   EXCEPTION
		  WHEN NO_DATA_FOUND THEN
		    raise_application_error ('-20085', 'Draft order not found');
		 END;			
		
			UPDATE T501_ORDER 
			  SET C501_VOID_FL = 'Y',C501_DELETE_FL='Y',
			  	c501_comments=c501_comments||' voided for'||p_help_desk_id,
			    C501_LAST_UPDATED_BY = p_user_id, 
			    C501_LAST_UPDATED_DATE = SYSDATE 
			  WHERE C501_ORDER_ID = p_order_id; 
			
			 UPDATE T502_ITEM_ORDER 
			     SET C502_VOID_FL = 'Y' 
			   WHERE C501_ORDER_ID = p_order_id; 
			
			 UPDATE T907_SHIPPING_INFO 
			  SET C907_VOID_FL = 'Y', 
			    C907_LAST_UPDATED_BY = p_user_id, 
			    C907_LAST_UPDATED_DATE = SYSDATE 
			  WHERE C907_REF_ID = p_order_id; 				
		
		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Order ID: ' || p_order_id);
		  DBMS_OUTPUT.put_line (' order id '||p_order_id || ' voided ');
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing: ' || SQLERRM);
			ROLLBACK;
	END gm_void_draft_order;
	
	/*************************************************************************************************
	* Description : This procedure is used to update hold order process flag
	* Author      : ppandiyan
	**************************************************************************************************/
	
	PROCEDURE gm_upd_order_hold_process_fl (
	     p_ord_type 	     VARCHAR2
	  ,  p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  ,  p_order_id		 t501_order.c501_order_id%TYPE DEFAULT NULL
	)
	AS
  		v_user_id  	   NUMBER;
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
    	v_user_id	:= it_pkg_cm_user.get_valid_user;
	
		IF p_ord_type = 'CHILD_ORDER'
		THEN
			UPDATE T501_ORDER 
			   SET C501_HOLD_ORDER_PROCESS_FL =  'Y' 
			   	 , C501_HOLD_ORDER_PROCESS_DATE = current_date
				 , C501_LAST_UPDATED_BY = v_user_id
				 , C501_LAST_UPDATED_DATE = current_date
			 WHERE C501_HOLD_ORDER_PROCESS_FL = 'N'
			 	AND C501_PARENT_ORDER_ID IS NOT NULL
			 	AND C501_HIST_HOLD_FL = 'Y'
	     		AND C501_VOID_FL IS NULL
	     		AND C901_ORDER_TYPE not in ('2535');				
		ELSIF p_ord_type = 'DISCOUNT_ORDER'
		THEN
			UPDATE T501_ORDER 
				   SET C501_HOLD_ORDER_PROCESS_FL =  'Y' 
				   	 , C501_HOLD_ORDER_PROCESS_DATE = current_date
					 , C501_LAST_UPDATED_BY = v_user_id
					 , C501_LAST_UPDATED_DATE = current_date
				 WHERE C501_HOLD_ORDER_PROCESS_FL = 'N'
				 	AND C901_ORDER_TYPE = '2535'
		     		AND C501_VOID_FL IS NULL;
		 END IF;
		 
		 IF P_ORDER_ID IS NOT NULL
		 THEN
		 	UPDATE t501_order 
			   SET C501_HOLD_ORDER_PROCESS_FL =  'Y'
			     , C501_HOLD_ORDER_PROCESS_DATE = current_date
				 , c501_last_updated_by = v_user_id
				 , c501_last_updated_date = current_date
			 WHERE c501_order_id = p_order_id 
	     		AND c501_void_fl IS NULL;
		 END IF;
		 
		--save logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Order ID: ' || p_order_id);
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing: ' || SQLERRM);
			ROLLBACK;
	END gm_upd_order_hold_process_fl;
	
	/*************************************************************************************************
	* Description : This procedure is used to update the usage lot details
	* Author      : Mahavishnu
	**************************************************************************************************/
	PROCEDURE  gm_upd_lot_detail(
	 	  p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	    , p_usage_id t502b_item_order_usage.C502B_ITEM_ORDER_USAGE_ID%TYPE
	    , p_order_id t501_order.c501_order_id%TYPE
	    , p_part_num t502b_item_order_usage.C205_PART_NUMBER_ID%TYPE
		, p_qty t502b_item_order_usage.C502B_ITEM_QTY%TYPE
		, p_usg_lot t502b_item_order_usage.C502B_USAGE_LOT_NUM%TYPE
		, p_user  t502b_item_order_usage.C502B_LAST_UPDATED_BY%TYPE
	)
	AS
	v_void_fl t501_order.c501_void_fl%TYPE;
	v_order_id t502b_item_order_usage.C501_ORDER_ID%TYPE;
	v_part_num t502b_item_order_usage.C205_PART_NUMBER_ID%TYPE;
	v_lot_void_fl  t502b_item_order_usage.C502B_VOID_FL%TYPE;
	
	BEGIN
	
	-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
	
		BEGIN
		  SELECT t501.c501_void_fl
		    INTO v_void_fl
		    FROM t501_order t501
		   WHERE t501.c501_order_id = p_order_id ;
		EXCEPTION
		  WHEN NO_DATA_FOUND THEN
		    raise_application_error ('-20085', 'No record found');
		END;
		
		IF v_void_fl ='Y' THEN
			raise_application_error ('-20085', 'The order is already voided');
		END IF;
		
		BEGIN 
		
		 SELECT C501_ORDER_ID,C205_PART_NUMBER_ID,C502B_VOID_FL
		 INTO v_order_id,v_part_num,v_lot_void_fl
		 FROM t502b_item_order_usage 
		 WHERE C502B_ITEM_ORDER_USAGE_ID = p_usage_id;
		EXCEPTION
		  WHEN NO_DATA_FOUND THEN
		    raise_application_error ('-20085', 'No record found in t502b_item_order_usage table');
		END;
		
		IF v_lot_void_fl ='Y' THEN
			raise_application_error ('-20085', 'Lot item is already voided');
		END IF;
		
		IF v_order_id <> p_order_id THEN
			raise_application_error ('-20085', 'Order id is mismatch');
		END IF;
	
		IF v_part_num <> p_part_num THEN
			raise_application_error ('-20085', 'Part number is mismatch');
		END IF;
	
		UPDATE t502b_item_order_usage SET C502B_ITEM_QTY=p_qty,C502B_USAGE_LOT_NUM=p_usg_lot,C502B_LAST_UPDATED_BY=p_user,C502B_LAST_UPDATED_DATE=CURRENT_DATE
		WHERE C502B_ITEM_ORDER_USAGE_ID=p_usage_id;
	
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Order ID: ' || p_order_id);
		  DBMS_OUTPUT.put_line (' order id '||p_order_id || ' lot detail is updated');
		COMMIT;
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing: ' || SQLERRM);
			ROLLBACK;
	END gm_upd_lot_detail;
	
	/*************************************************************************************************
	* Description : This procedure is used to void the usage lot details
	* Author      : Mahavishnu
	**************************************************************************************************/
	PROCEDURE  gm_void_lot_detail(
	 	  p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	    , p_usage_id t502b_item_order_usage.C502B_ITEM_ORDER_USAGE_ID%TYPE
	    , p_order_id t501_order.c501_order_id%TYPE
	    , p_part_num t502b_item_order_usage.C205_PART_NUMBER_ID%TYPE
		, p_user  t502b_item_order_usage.C502B_LAST_UPDATED_BY%TYPE
	)
	AS
	v_void_fl t501_order.c501_void_fl%TYPE;
	v_order_id t502b_item_order_usage.C501_ORDER_ID%TYPE;
	v_part_num t502b_item_order_usage.C205_PART_NUMBER_ID%TYPE;
	v_lot_void_fl  t502b_item_order_usage.C502B_VOID_FL%TYPE;
	
	BEGIN
	
	-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
	
		BEGIN
		  SELECT t501.c501_void_fl
		    INTO v_void_fl
		    FROM t501_order t501
		   WHERE t501.c501_order_id = p_order_id ;
		EXCEPTION
		  WHEN NO_DATA_FOUND THEN
		    raise_application_error ('-20085', 'No record found');
		END;
		
		IF v_void_fl ='Y' THEN
			raise_application_error ('-20085', 'The order is already voided');
		END IF;
		
		BEGIN
		 SELECT C501_ORDER_ID,C205_PART_NUMBER_ID,C502B_VOID_FL
		 INTO v_order_id,v_part_num,v_lot_void_fl
		 FROM t502b_item_order_usage 
		 WHERE C502B_ITEM_ORDER_USAGE_ID = p_usage_id;
		EXCEPTION
		  WHEN NO_DATA_FOUND THEN
		    raise_application_error ('-20085', 'No record found in t502b_item_order_usage table');
		END;
		
		IF v_lot_void_fl ='Y' THEN
			raise_application_error ('-20085', 'Lot item is already voided');
		END IF;
		
		IF v_order_id <> p_order_id THEN
			raise_application_error ('-20085', 'Order id is mismatch');
		END IF;
	
		IF v_part_num <> p_part_num THEN
			raise_application_error ('-20085', 'Part number is mismatch');
		END IF;
	
		UPDATE t502b_item_order_usage SET C502B_VOID_FL='Y',C502B_LAST_UPDATED_BY=p_user,C502B_LAST_UPDATED_DATE=CURRENT_DATE
		WHERE C502B_ITEM_ORDER_USAGE_ID=p_usage_id;
	
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Order ID: ' || p_order_id);
		  DBMS_OUTPUT.put_line (' order id '||p_order_id || ' lot item is voided ');
		COMMIT;
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing: ' || SQLERRM);
			ROLLBACK;
	END gm_void_lot_detail;
	
	/*************************************************************************************************
	* Description : This procedure is used to insert the usage lot details
	* Author      : Mahavishnu
	**************************************************************************************************/
	PROCEDURE  gm_insert_lot_detail(
	 	  p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	    , p_order_id t502b_item_order_usage.C501_ORDER_ID%TYPE
	    , p_part_num t502b_item_order_usage.C205_PART_NUMBER_ID%TYPE
		, p_qty t502b_item_order_usage.C502B_ITEM_QTY%TYPE
		, p_usg_lot t502b_item_order_usage.C502B_USAGE_LOT_NUM%TYPE
		, p_order_info t502b_item_order_usage.C500_ORDER_INFO_ID%TYPE
		, p_user  t502b_item_order_usage.C502B_LAST_UPDATED_BY%TYPE
	)
	AS
	v_void_fl t501_order.c501_void_fl%TYPE;
	v_order_id t502b_item_order_usage.C501_ORDER_ID%TYPE;
	v_part_num t502b_item_order_usage.C205_PART_NUMBER_ID%TYPE;
	v_info_void_fl  t502b_item_order_usage.C502B_VOID_FL%TYPE;
	v_order_info  t502b_item_order_usage.C500_ORDER_INFO_ID%TYPE;
	
	BEGIN
	
	-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
	
		BEGIN
		  SELECT t501.c501_void_fl
		    INTO v_void_fl
		    FROM t501_order t501
		   WHERE t501.c501_order_id = p_order_id ;
		EXCEPTION
		  WHEN NO_DATA_FOUND THEN
		    raise_application_error ('-20085', 'No record found');
		END;
		
		IF v_void_fl ='Y' THEN
			raise_application_error ('-20085', 'The order is already voided');
		END IF;
		
		BEGIN
		 SELECT C205_PART_NUMBER_ID,C500_VOID_FL,C501_ORDER_ID
		 INTO v_part_num,v_info_void_fl,v_order_id
		 FROM t500_order_info 
		 WHERE C500_ORDER_INFO_ID = p_order_info;
		EXCEPTION
		  WHEN NO_DATA_FOUND THEN
		    raise_application_error ('-20085', 'No record found in t500_order_info table');
		END;
		
		IF v_info_void_fl ='Y' THEN
			raise_application_error ('-20085', 'order info is already voided');
		END IF;
		
		IF v_order_id <> p_order_id THEN
			raise_application_error ('-20085', 'Order id is mismatch');
		END IF;
	
		IF v_part_num <> p_part_num THEN
			raise_application_error ('-20085', 'Part number is mismatch');
		END IF;
		
		INSERT INTO t502b_item_order_usage(C502B_ITEM_ORDER_USAGE_ID,C501_ORDER_ID,C205_PART_NUMBER_ID,C502B_ITEM_QTY,C502B_USAGE_LOT_NUM,C500_ORDER_INFO_ID,C502B_CREATED_BY,C502B_CREATED_DATE)
		VALUES (S502B_ITEM_ORDER_USAGE.NEXTVAL,p_order_id,p_part_num,p_qty,p_usg_lot,p_order_info,p_user,CURRENT_DATE);
		
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Order ID: ' || p_order_id);
		  DBMS_OUTPUT.put_line (' order id '||p_order_id || ' lot item inserted ');
		COMMIT;
	        
	   EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing: ' || SQLERRM);
			ROLLBACK;     
	END gm_insert_lot_detail;
	
END it_pkg_order;
/
