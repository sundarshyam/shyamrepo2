--@"C:\DataCorrection\Script\it_pkg_missing_part_charge.bdy";

CREATE OR REPLACE PACKAGE BODY it_pkg_missing_part_charge
IS
/******************************************************************
 * Description : Procedure to writeoff the missing parts
 * Author		: VPrasath
 ****************************************************************/
PROCEDURE gm_missing_part_charge_main( 
  p_date  IN  VARCHAR2
 ,p_str	  OUT  VARCHAR2
)
AS

v_d_cur TYPES.cursor_type;
v_date DATE := TO_DATE(p_date,'MM/DD/YYYY');

CURSOR v_cur IS
SELECT DISTINCT (req_id) req_id, DECODE (LEAST (SECOND, third), third, 'Y', 'N') skipflg, req_detail_id
   FROM
    (SELECT t525.c525_product_request_id req_id, gm_pkg_cm_loaner_jobs.gm_fch_req_det_ids (t525.c525_product_request_id
        , 'Y', v_date, v_date) req_detail_id, t525.c525_email_date + TO_NUMBER ('0') SECOND, TO_DATE (TO_CHAR
        (ADD_MONTHS (t525.c525_email_date, 1), 'MM')
        || '/'
        || TO_NUMBER ('21')
        || '/'
        || TO_CHAR (ADD_MONTHS (t525.c525_email_date, 1), 'YYYY'), 'mm/dd/yyyy') third
       FROM t412_inhouse_transactions t412, t504a_loaner_transaction t504a, t526_product_request_detail t526,
        t525_product_request t525
      WHERE t412.c412_status_fl                 < 4
        AND t525.c525_email_date               <= DECODE ('Y', 'Y', t525.c525_email_date, v_date)
        AND t525.c525_email_date               >= DECODE ('Y', 'Y', t525.c525_email_date, v_date)
        AND t412.c412_expected_return_date      = DECODE ('Y', 'Y', v_date, t412.c412_expected_return_date)
        AND t525.c525_email_date               IS NOT NULL
        AND t504a.c504a_loaner_transaction_id   = t412.c504a_loaner_transaction_id
        AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
        AND t525.c525_product_request_id        = t526.c525_product_request_id
        AND t412.c412_type                      = 50159
        AND t412.c412_void_fl                  IS NULL
        AND t504a.c504a_void_fl                IS NULL
        AND t526.c526_void_fl                  IS NULL
        AND t525.c525_void_fl                  IS NULL
    ) ;
BEGIN
p_str:='';

  FOR v_index IN v_cur 
  LOOP
	 gm_sav_missingparts_writeoff(v_index.req_id, p_date, v_d_cur);	
	 p_str :=  v_index.req_id  || ' , ' || p_str  ;
  END LOOP;
END gm_missing_part_charge_main;

/******************************************************************
 * Description : Procedure to writeoff the missing parts
 * Author		: VPrasath
 ****************************************************************/
PROCEDURE gm_sav_missingparts_writeoff (
	  p_request_id   IN   t525_product_request.c525_product_request_id%TYPE
	, p_date	 IN   VARCHAR2	
	, p_cursor       OUT  TYPES.cursor_type	
	)
	AS
		CURSOR item_cursor
		IS
			SELECT	 t413.c412_inhouse_trans_id trans_id, t413.c205_part_number_id pnum ,t526.c207_set_id set_id
				   ,   SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0))
					 - SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty)) qty
				   , get_part_price_list (t413.c205_part_number_id, '') price
				   , gm_pkg_cm_loaner_jobs.get_missing_part_price (t413.c205_part_number_id)
			          * (  SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0))
			            - SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty))
			           ) echarge_price,
					 get_part_price_list (t413.c205_part_number_id, '')
					 * (  SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0))
						- SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty))
					   ) eprice
				   ,	'Qty:'
					 || (  SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0))
						 - SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty))
						)
					 || ' * Charge: $'
					 || gm_pkg_cm_loaner_jobs.get_missing_part_price (t413.c205_part_number_id) incidentvalue
				FROM t413_inhouse_trans_items t413
				   , t412_inhouse_transactions t412
				   , t504a_loaner_transaction t504a
				   , t526_product_request_detail t526
			   WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
			   	 AND t413.c205_part_number_id not in (
			         select c205_part_number_id from t208_set_details where  
			         c207_set_id in (  select c906_rule_value
			               from t906_rules
			              where c906_rule_grp_id = 'CHARGES'
			                AND c906_rule_id     = 'SETS'
			                AND c906_void_fl IS NULL)    
			                       AND c208_void_fl is null)
 				 AND c412_status_fl < 4
				 AND c412_type = 50159
				 AND t412.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
				 AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
				 AND t526.c525_product_request_id = p_request_id
				 AND c412_expected_return_date = TO_DATE(p_date,'MM/DD/YYYY')
				 AND t413.c413_void_fl IS NULL
				 AND t412.c412_void_fl IS NULL
				 AND t504a.c504a_void_fl IS NULL
				 AND t526.c526_void_fl IS NULL
			GROUP BY t413.c205_part_number_id, t413.c412_inhouse_trans_id,t526.c207_set_id
			  HAVING   SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0))
					 - SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty)) > 0
			ORDER BY t413.c412_inhouse_trans_id;

		v_repid 	   t703_sales_rep.c703_sales_rep_id%TYPE;
		v_distid	   t701_distributor.c701_distributor_id%TYPE;
		v_incident_id  t9200_incident.c9200_incident_id%TYPE;
		v_assoc_rep_id t525_product_request.c703_ass_rep_id%TYPE;
		v_chrg_dtl_id  NUMBER;
		
	BEGIN
		
		OPEN p_cursor FOR 
		SELECT	t526.c526_product_request_detail_id REQ_DET_ID, t413.c412_inhouse_trans_id trans_id, (t413.c205_part_number_id
	         || ' '
	         || get_partnum_desc (t413.c205_part_number_id)) pnum
				   ,   SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0))
					 - SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty)) qty
				   , get_part_price_list (t413.c205_part_number_id, '') price
				   ,   get_part_price_list (t413.c205_part_number_id, '')
					 * (  SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0))
						- SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty))
					   ) eprice
				   ,	'Qty:'
					 || (  SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0))
						 - SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty))
						)
					 || ' * List Price: $'
					 || get_part_price_list (t413.c205_part_number_id, '') incidentvalue
				FROM t413_inhouse_trans_items t413
				   , t412_inhouse_transactions t412
				   , t504a_loaner_transaction t504a
				   , t526_product_request_detail t526
			   WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
				 AND c412_status_fl < 4
				 AND c412_type = 50159
				 AND t412.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
				 AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
				 AND t526.c525_product_request_id = p_request_id
				 AND c412_expected_return_date = TO_DATE(p_date,'MM/DD/YYYY')
				 AND t413.c413_void_fl IS NULL
				 AND t412.c412_void_fl IS NULL
				 AND t504a.c504a_void_fl IS NULL
				 AND t526.c526_void_fl IS NULL
			GROUP BY t413.c205_part_number_id, t413.c412_inhouse_trans_id, t526.c526_product_request_detail_id
			  HAVING   SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0))
					 - SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty)) > 0
			ORDER BY REQ_DET_ID, t413.c412_inhouse_trans_id;
			
			-- to log the missing charges
			SELECT c703_sales_rep_id, get_distributor_id (c703_sales_rep_id), c703_ass_rep_id
			  INTO v_repid, v_distid, v_assoc_rep_id
			  FROM t525_product_request
			 WHERE c525_product_request_id = p_request_id AND c525_void_fl IS NULL;
			 
		FOR currindex IN item_cursor
		LOOP
			gm_cs_sav_loaner_recon_insert (currindex.trans_id
										 , currindex.pnum
										 , currindex.qty
										 , 'NOC#'
										 , currindex.price
										 , 'W'
										  );
			gm_save_ledger_posting (4845, SYSDATE, currindex.pnum, '01', currindex.trans_id, currindex.qty, NULL, 30301);

			UPDATE t412_inhouse_transactions
			   SET c412_status_fl = 4	--completed
				 , c412_verified_date = SYSDATE
				 , c412_verified_by = 30301
				 , c412_last_updated_date = SYSDATE
				 , c412_last_updated_by = 30301
			 WHERE c412_inhouse_trans_id = currindex.trans_id AND c412_void_fl IS NULL;


			gm_pkg_op_loaner.gm_sav_incidents (50891
											 , p_request_id
											 , 92069
											 , currindex.set_id
											 , 30301
											 , TO_DATE(p_date,'MM/DD/YYYY')
											 , v_incident_id
											  );
			gm_pkg_op_loaner.gm_sav_charge_details (v_incident_id, v_repid, v_distid, currindex.echarge_price, 20, 30301,v_assoc_rep_id);   -- approved
			BEGIN
				 SELECT c9201_charge_details_id
				   INTO v_chrg_dtl_id
				   FROM t9201_charge_details
				  WHERE c9200_incident_id = v_incident_id
				    AND c9201_void_fl    IS NULL;
			 EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_chrg_dtl_id := NULL;
			 END;
			IF v_chrg_dtl_id IS NOT NULL
			THEN
			  UPDATE t9201_charge_details
			     SET c205_part_number = currindex.pnum
			   , c9201_unit_cost = currindex.price, c9201_qty_missing = currindex.qty
			   WHERE c9201_charge_details_id = v_chrg_dtl_id
			     AND c9201_void_fl IS NULL; 
			END IF;
		END LOOP;
		gm_sav_set_level_missing_chrg(v_repid,v_distid,p_date);
	END gm_sav_missingparts_writeoff;

/*******************************************************************
 *  Description : this procedure is calculate missing part charge at set level
 *  Author      : dreddy
 *****************************************************************/
PROCEDURE gm_sav_set_level_missing_chrg (
         p_repid  IN VARCHAR2
	,p_distid IN VARCHAR2
	,p_date	  IN VARCHAR2
	)
AS
    v_set_charge NUMBER;
    v_incident_id t9200_incident.c9200_incident_id%TYPE;
    CURSOR v_setcursor
    IS
      select distinct t526.c526_product_request_detail_id reqdet, t526.c207_set_id inst_value, t526.c525_product_request_id ref_id,
	    t412.c412_inhouse_trans_id trans_id,t504a.c703_ass_rep_id assocrep_id
	   from t413_inhouse_trans_items t413, t412_inhouse_transactions t412, t504a_loaner_transaction t504a
	  , t526_product_request_detail t526
	  where t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
	    and t413.c205_part_number_id  in
	    (
	         select c205_part_number_id
	           from t208_set_details
	          where c207_set_id in
	            (
	                 select c906_rule_value
	                   from t906_rules
	                  where c906_rule_grp_id = 'CHARGES'
	                    and c906_rule_id     = 'SETS'
	                    and c906_void_fl    is null
	            )
	            and c208_void_fl is null
	    )
	    and c412_status_fl                       < 4
	    and c412_type                            = 50159
	    and t412.c504a_loaner_transaction_id     = t504a.c504a_loaner_transaction_id
	    and t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
	    and c412_expected_return_date = TO_DATE(p_date,'MM/DD/YYYY')
	    and t413.c413_void_fl        is null
	    and t412.c412_void_fl        is null
	    and t504a.c504a_void_fl      is null
	    and t526.c526_void_fl        is null
	order by t526.c525_product_request_id;   
BEGIN
    v_set_charge := get_rule_value ('SET_CHARGE', 'CHARGES') ;
    FOR v_index  IN v_setcursor
    LOOP
       -- IF v_index.amount > v_set_charge THEN commenting if to make set level charge always $500
       
            gm_pkg_op_loaner.gm_sav_incidents (50891, v_index.ref_id, 92069, v_index.inst_value, 30301, TRUNC (SYSDATE), v_incident_id);
            gm_pkg_op_loaner.gm_sav_charge_details (v_incident_id, p_repid, p_distid, v_set_charge, 20, 30301,v_index.assocrep_id) ;  -- approved

            UPDATE t412_inhouse_transactions
	       SET c412_status_fl = 4	--completed
				 , c412_verified_date = SYSDATE
				 , c412_verified_by = 30301
				 , c412_last_updated_date = SYSDATE
				 , c412_last_updated_by = 30301
			 WHERE c412_inhouse_trans_id = v_index.trans_id AND c412_void_fl IS NULL;

       -- END IF;
    END LOOP;
END gm_sav_set_level_missing_chrg;

END it_pkg_missing_part_charge;
/