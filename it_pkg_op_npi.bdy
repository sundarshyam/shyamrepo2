/* Formatted on 2011/01/20 14:53 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_op_npi.bdy"

CREATE OR REPLACE PACKAGE BODY it_pkg_op_npi
IS
--

	/****************************************************************
	* Description : This procedure is used to mark the NPI as valid
	****************************************************************/
	PROCEDURE gm_upd_npi_details (
		p_help_desk_id	 	t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_party_id		 	t6640_npi_transaction.c101_party_surgeon_id%TYPE
	  , p_npi_id			t6640_npi_transaction.c6600_surgeon_npi_id%TYPE
	  , p_user_id	 		t6640_npi_transaction.c6640_last_updated_by%TYPE
	)
	AS
	BEGIN

		UPDATE t6640_npi_transaction
		SET C101_PARTY_SURGEON_ID  = p_party_id,
		  C6640_VALID_FL           = 'Y',
		  C6640_REASON             = NULL,
		  C6640_LAST_UPDATED_BY    = p_user_id,
		  C6640_LAST_UPDATED_DATE  = sysdate
		WHERE C6600_SURGEON_NPI_ID = p_npi_id
		AND C6640_VOID_FL         IS NULL;

		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'p_npi_id : ' || p_npi_id);
		DBMS_OUTPUT.put_line (SQL%ROWCOUNT || ' Row(s) are updated. ');
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_upd_npi_details;
	
	
	
	/**********************************************************
    * Description : Procedure to save the npi suregeon 
    ***********************************************************/
	PROCEDURE gm_sav_npi(
	 	p_help_desk_id	 	t914_helpdesk_log.c914_helpdesk_id%TYPE,
		p_surgeon_npi_id 	t6640_npi_transaction.c6600_surgeon_npi_id%TYPE,
		p_surgeon_nm		t6640_npi_transaction.c6640_surgeon_name%TYPE,
		p_user_id 			t6640_npi_transaction.c6640_last_updated_by%TYPE
	)
	AS
		v_party_id		t6640_npi_transaction.c101_party_surgeon_id%TYPE;		
		v_last_nm		VARCHAR2(200);
		v_first_nm		VARCHAR2(200);
		v_company_id	t6640_npi_transaction.c1900_company_id%TYPE:=1000;		
		v_plant_id	t6640_npi_transaction.C5040_plant_id%TYPE:=3000;
		v_npi_cnt		NUMBER;
	BEGIN
	
		-- While marking as user validate, if the npi record already available in T6600_PARTY_SURGEON table, do not insert again
		SELECT COUNT(1)
			INTO v_npi_cnt
		FROM T6600_PARTY_SURGEON
		WHERE C6600_SURGEON_NPI_ID = p_surgeon_npi_id
		AND C6600_VOID_FL         IS NULL
		AND C1900_COMPANY_ID       = v_company_id;
		
		IF v_npi_cnt > 0
		THEN
			DBMS_OUTPUT.put_line ( ' NPI already found ' || p_surgeon_npi_id);
		
		ELSE
			BEGIN
				SELECT c101_party_surgeon_id
				 INTO v_party_id
				FROM T6600_PARTY_SURGEON
				WHERE C6600_SURGEON_NPI_ID = p_surgeon_npi_id
				AND C6600_VOID_FL         IS NULL
				AND C1900_COMPANY_ID       = v_company_id;			
			EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_party_id:= NULL;
			END;
	
			IF v_party_id IS NULL OR v_party_id = ''
			THEN
				SELECT s101_party.NEXTVAL
		           INTO v_party_id
		        FROM DUAL;
		        
				SELECT SUBSTR(p_surgeon_nm, INSTR(p_surgeon_nm,' ',-1) + 1)
					INTO v_last_nm
				FROM dual;
				
				SELECT SUBSTR(p_surgeon_nm, 0, LENGTH(p_surgeon_nm) - LENGTH(v_last_nm)-1)
					INTO v_first_nm
				FROM dual;
			
				INSERT INTO t101_party
	                     (c101_party_id, c101_last_nm, c101_first_nm,
	                      c101_middle_initial, c901_party_type, c101_party_nm,
	                      c101_active_fl, c101_created_by, c101_created_date , c1900_company_id,c101_party_nm_en
	                     )
	              VALUES (v_party_id, v_last_nm, v_first_nm,
	                      NULL, 7000, NULL,
	                      NULL, p_user_id, CURRENT_DATE , v_company_id, NULL
	                     );
				
			  END IF;				
			
				-- Insert user validated record into t6600 table, 106580: Checked
				INSERT
				INTO T6600_PARTY_SURGEON
				  (
				    C6600_SURGEON_NPI_ID,
				    C101_PARTY_SURGEON_ID,
				    C6600_CREATED_BY,
				    C6600_CREATED_DATE,
				    C1900_COMPANY_ID,
				    C5040_PLANT_ID,
				    C6600_STATUS_FL
				  ) VALUES (p_surgeon_npi_id,
				  v_party_id,
				  p_user_id,
				  CURRENT_DATE,
				  v_company_id,
				  v_plant_id,
				  106580);
				  
				  INSERT INTO T106_ADDRESS (C106_ADDRESS_ID, C101_PARTY_ID, C106_ADD1, C106_ADD2,C106_CITY, C106_ZIP_CODE, C901_STATE, C901_COUNTRY, C901_ADDRESS_TYPE,C106_PRIMARY_FL, C106_CREATED_BY, C106_CREATED_DATE) VALUES
				  (S106_ADDRESS.NEXTVAL,v_party_id,NULL,NULL,NULL,NULL,NULL,NULL,90401,'Y',p_user_id,SYSDATE); --90401 OFFICE
				  
				  -- Mandatory helpdesk to log executed user information
					it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'p_surgeon_npi_id : ' || p_surgeon_npi_id);
					DBMS_OUTPUT.put_line ( ' NPI updated successfully ' || p_surgeon_npi_id);
		END IF;
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;	
	END gm_sav_npi;
	
END it_pkg_op_npi;
/
