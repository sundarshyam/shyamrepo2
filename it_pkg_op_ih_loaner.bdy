CREATE OR REPLACE PACKAGE BODY GLOBUS_APP.it_pkg_op_ih_loaner
IS
--
    /***************************************************
   * Description : This procedure is used swap Inhouse loaner
   *********************************************************/
	PROCEDURE gm_sav_loaner_swap (
	   p_help_desk_id	 t102_user_login.c102_login_username%TYPE
	  , p_source_cn 	 t504_consignment.c504_consignment_id%TYPE
	  , p_target_set	 t504_consignment.c207_set_id%TYPE
	)
	AS
		v_target_cn    t504_consignment.c504_consignment_id%TYPE;
		v_updated_by   t504_consignment.c504_last_updated_by%TYPE;
		v_status	   t504a_consignment_loaner.c504a_status_fl%TYPE;
		v_status_fl    t412_inhouse_transactions.c412_status_fl%TYPE;
		v_inhouse_purpose t504_consignment.c504_inhouse_purpose%TYPE;
		v_cmpny_id		t504a_consignment_loaner.c1900_company_id%TYPE;
		v_plant_id		t504a_consignment_loaner.c5040_plant_id%TYPE;
--
	BEGIN
		-- check to see if the source is inactive, if it is , then error
		SELECT t504a.c504a_status_fl, t504.c504_inhouse_purpose, t504.c1900_company_id, t504a.c5040_plant_id
		  INTO v_status, v_inhouse_purpose, v_cmpny_id, v_plant_id
		  FROM t504a_consignment_loaner t504a, t504_consignment t504
		 WHERE t504a.c504_consignment_id = p_source_cn
		 AND   t504a.c504_consignment_id = t504.c504_consignment_id
		 for UPDATE;

		-- Temporary code added to convert inhouse set
		-- IF (v_inhouse_purpose != 40002 AND  v_inhouse_purpose != 40004)
		-- THEN
		-- 	raise_application_error ('-20085', 'Only can convenrt Inhouse and Internal Caderve Lab');
		-- END IF;

		IF v_status = 60
		THEN
			raise_application_error ('-20085', 'This CN is inactive, can not swap');
		END IF;

		IF v_status != 0
		THEN
			raise_application_error ('-20085', 'This CN is not in available status , can not swap');
		END IF;

		IF v_cmpny_id IS NULL OR v_plant_id IS NULL
		THEN
			raise_application_error ('-20999', 'The Company Id/Plant Id is not updated for the source CN, cannot swap');
		END IF;

		-- check if CN status changed when do inhouse transaction for inactive CN
		BEGIN
			SELECT c412_status_fl
			  INTO v_status_fl
			  FROM t412_inhouse_transactions
			 WHERE c412_status_fl =
					   (SELECT MIN (c412_status_fl)
						  FROM t412_inhouse_transactions
						 WHERE c412_ref_id = p_source_cn
						   AND c412_type NOT IN (50159, 50160, 50161, 50162, 100062)
						   AND c412_void_fl IS NULL)
			   AND c412_ref_id = p_source_cn
			   AND ROWNUM = 1;
		EXCEPTION	-- even if inhouse trans was not created , status has to be updated
			WHEN NO_DATA_FOUND
			THEN
				v_status_fl := 4;
		END;

		--Not required from inhouse transaction
		/* IF v_status_fl < 4
		THEN
			DBMS_OUTPUT.put_line ('Cannot move ' || p_source_cn
								  || ' to pending process as it has with open transactions.'
								 );
			raise_application_error ('-20999'
								   ,	'Cannot move '
									 || p_source_cn
									 || '  to pending process as it has with open transactions.'
									);
		END IF;*/

		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_updated_by := it_pkg_cm_user.get_valid_user;

		--Step 1: Create a mirror CN
		SELECT 'GM-CN-' || s504_consign.NEXTVAL
		  INTO v_target_cn
		  FROM DUAL;

		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Target Set ' || p_target_set);
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Source CN ' || p_source_cn);
		--DBMS_OUTPUT.put_line (' ****Step 1: Create CN  ' || v_target_cn);

		INSERT INTO t504_consignment
					(c504_consignment_id, c701_distributor_id, c504_ship_date, c504_return_date, c504_comments
				   , c504_total_cost, c504_created_date, c504_created_by, c504_last_updated_date, c504_last_updated_by
				   , c504_status_fl, c504_void_fl, c704_account_id, c504_ship_to, c207_set_id, c504_ship_to_id
				   , c504_final_comments, c504_inhouse_purpose, c504_type, c504_delivery_carrier, c504_delivery_mode
				   , c504_tracking_number, c504_ship_req_fl, c504_verify_fl, c504_verified_date, c504_verified_by
				   , c504_update_inv_fl, qb_consignment_txnid, c504_reprocess_id, c504_master_consignment_id
				   , c520_request_id, c504_ship_cost, c704_location_account_id, c703_location_sales_rep_id, c504_hold_fl
				   , c1900_company_id, c5040_plant_id
					)
			 VALUES (v_target_cn, NULL, NULL, NULL, 'Converted to LN Set List from ' || p_source_cn
				   , NULL, SYSDATE, v_updated_by, NULL, NULL
				   , '4', NULL, NULL, NULL, p_target_set, NULL
				   , 'LOANER-3', v_inhouse_purpose, 4119, NULL, NULL
				   , NULL, NULL, '1', SYSDATE, v_updated_by
				   , '1', NULL, NULL, p_source_cn
				   , NULL, NULL, NULL, NULL, NULL
				   , v_cmpny_id, v_plant_id
					);

		--DBMS_OUTPUT.put_line (' ****Step 1: Create a mirror CN');
		--Step 2: Create mirror CN Items
		INSERT INTO t505_item_consignment
					(c505_item_consignment_id, c504_consignment_id, c205_part_number_id, c505_control_number
				   , c505_item_qty, c505_item_price)
			SELECT s504_consign_item.NEXTVAL, v_target_cn, pnum, 'NOC#', qty, price
			  FROM (SELECT   t205.c205_part_number_id pnum, t205.c205_part_num_desc pdesc,
                 NVL (cons.qty, 0) - NVL (miss.qty, 0) qty,
                 t205.c205_loaner_price price,
                 (NVL (miss.qty, 0) + NVL (missing_qty, 0)) missqty,
                 NVL (miss.qtyrecon, 0) qtyrecon,
                   NVL (cons.qty, 0)
                 + NVL (missing_qty, 0)
                 - NVL (miss.qtyrecon, 0) cogsqty,
                 NVL (setdetails.qty, 0) setlistqty,
                 NVL (loanerboqty.qty, 0) boqty
            FROM t205_part_number t205,
                 (SELECT   t505.c205_part_number_id,
                           SUM (t505.c505_item_qty) qty
                      FROM t505_item_consignment t505
                     WHERE t505.c504_consignment_id = p_source_cn
                       AND t505.c505_void_fl IS NULL
                  GROUP BY t505.c205_part_number_id) cons,
                 (SELECT   c205_part_number_id, SUM (qty) qty,
                           SUM (qtyrecon) qtyrecon,
                           SUM (missing_qty) missing_qty
                      FROM (SELECT t413.c205_part_number_id,
                                   DECODE(c412_type,50151, 0,
                                        DECODE (t413.c413_status_fl,'Q', NVL (t413.c413_item_qty,0),0)) qty,
                                   DECODE(c412_type,50151,
                                        DECODE(t413.c413_status_fl,'Q', NVL(t413.c413_item_qty,0),0),0) missing_qty,
                                   DECODE(c412_type,50151, NVL (t413.c413_item_qty, 0),
                                        DECODE (NVL (t413.c413_status_fl, 'Q'),'Q', 0,NVL (t413.c413_item_qty, 0))
                                      ) qtyrecon
                              FROM t412_inhouse_transactions t412,
                                   t413_inhouse_trans_items t413
                             WHERE t412.c412_inhouse_trans_id =t413.c412_inhouse_trans_id
                               AND t412.c412_ref_id = p_source_cn
                               AND t412.c412_void_fl IS NULL
                               AND t413.c413_void_fl IS NULL)
                  GROUP BY c205_part_number_id) miss,
                 (SELECT t413.c205_part_number_id, t413.c413_item_qty qty
                    FROM t412_inhouse_transactions t412,
                         t413_inhouse_trans_items t413
                   WHERE t412.c412_inhouse_trans_id =
                                                    t413.c412_inhouse_trans_id
                     AND c412_status_fl < 4
                     AND t412.c412_type = 100062
                     AND t413.c413_void_fl IS NULL
                     AND c412_void_fl IS NULL
                     AND t412.c412_ref_id = p_source_cn) loanerboqty,
                 (SELECT t208.c205_part_number_id, t208.c208_set_qty qty
                    FROM t208_set_details t208, t504_consignment t504
                   WHERE t208.c208_void_fl IS NULL
                     AND t208.c208_inset_fl = 'Y'
                     AND t504.c207_set_id = t208.c207_set_id
                     AND t504.c504_void_fl IS NULL
                     AND t504.c504_consignment_id = p_source_cn) setdetails
           WHERE t205.c205_part_number_id = cons.c205_part_number_id
             AND t205.c205_part_number_id = miss.c205_part_number_id(+)
             AND t205.c205_part_number_id = setdetails.c205_part_number_id(+)
             AND t205.c205_part_number_id = loanerboqty.c205_part_number_id(+)
        UNION ALL
        SELECT   t208.c205_part_number_id pnum,
                 get_partnum_desc (t208.c205_part_number_id) pdesc, 0 qty,
                 0 price, 0 missqty, 0 qtyrecon, 0 cogsqty,
                 t208.c208_set_qty qty, 0 boqty
            FROM t208_set_details t208, t504_consignment t504
           WHERE t208.c208_void_fl IS NULL
             AND t208.c208_inset_fl = 'Y'
             AND t504.c207_set_id = t208.c207_set_id
             AND t504.c504_void_fl IS NULL
             AND t504.c504_consignment_id = p_source_cn
             AND t208.c205_part_number_id NOT IN (
                    SELECT t505.c205_part_number_id
                      FROM t505_item_consignment t505
                     WHERE t505.c504_consignment_id = p_source_cn
                       AND t505.c505_void_fl IS NULL)
        ORDER BY 1)
 WHERE qty != 0 OR setlistqty != 0;

		--DBMS_OUTPUT.put_line (' *******Step 2: Create mirror CN Items');
		--Step 3: Create mirror of T504A Loaner record
		INSERT INTO t504a_consignment_loaner
					(c504_consignment_id, c504a_loaner_dt, c504a_etch_id, c504a_expected_return_dt, c504a_void_fl
				   , c504a_status_fl, c504a_loaner_create_date, c504a_loaner_create_by, c504a_last_updated_date
				   , c504a_last_updated_by, c504a_available_date, c1900_company_id, c5040_plant_id)
			SELECT v_target_cn, c504a_loaner_dt, c504a_etch_id, c504a_expected_return_dt, c504a_void_fl
				 , c504a_status_fl, c504a_loaner_create_date, c504a_loaner_create_by, c504a_last_updated_date
				 , v_updated_by, c504a_available_date, v_cmpny_id, v_plant_id
			  FROM t504a_consignment_loaner
			 WHERE c504_consignment_id = p_source_cn;

		--DBMS_OUTPUT.put_line (' *******Step 3: Create mirror of T504A Loaner record');
		--Step 4: Inactive in 504 and 504A
		UPDATE t504a_consignment_loaner
		   SET c504a_status_fl = 60
			 , c504a_last_updated_by = v_updated_by
			 , c504a_last_updated_date = SYSDATE
			 , c504a_etch_id =
				   DECODE (SUBSTR (c504a_etch_id, 1, 6)
						 , 'LOANER', REPLACE (TO_CHAR (c504a_etch_id), 'LOANER', 'TRANSFER')
						 , c504a_etch_id
						  )
		 WHERE c504_consignment_id = p_source_cn;

		--DBMS_OUTPUT.put_line (' *******Step 4: Inactive in 504 and 504A');
		--Step 5: Comments
		INSERT INTO t902_log
					(c902_log_id, c902_ref_id, c902_comments, c902_type
				   , c902_created_by, c902_created_date, c902_last_updated_by, c902_last_updated_date, c902_void_fl
				   , c1900_company_id
					)
			 VALUES (s507_log.NEXTVAL, v_target_cn, 'Converted to LN Set List from ' || p_source_cn, 1222
				   , v_updated_by, SYSDATE, NULL, NULL, NULL
				   , v_cmpny_id
					);

		--DBMS_OUTPUT.put_line (' *******Step 5: Comments');
		--Step 6: Add record in T504B_LOANER_SWAP_LOG
		INSERT INTO t504b_loaner_swap_log
					(c504d_loaner_swap_log_id, c504_consignment_id, c901_type, c504d_ref_id, c504d_void_fl
				   , c504d_created_by, c504d_created_date
					)
			 VALUES (s504b_loaner_swap_log.NEXTVAL, p_source_cn, 100100, v_target_cn, NULL
				   , v_updated_by, SYSDATE
					);

		--DBMS_OUTPUT.put_line (' *******Step 6: Add record in T504B_LOANER_SWAP_LOG');
		--Step 7: Add record in T504A_LOANER_TXN
		-- Not required for Inhouse loaner conversation
		/* INSERT INTO t504a_loaner_transaction
					(c504a_loaner_transaction_id, c504_consignment_id, c504a_loaner_dt, c504a_expected_return_dt
				   , c504a_return_dt, c504a_void_fl, c901_consigned_to, c504a_consigned_to_id, c504a_created_by
				   , c504a_created_date, c504a_last_updated_date, c504a_last_updated_by, c703_sales_rep_id
				   , c704_account_id, c504a_is_loaner_extended, c504a_is_replenished, c504a_parent_loaner_txn_id
				   , c526_product_request_detail_id, c504a_processed_date, c504a_transf_ref_id, c504a_transf_date
				   , c504a_transf_by, c504a_transf_email_flg)
			SELECT s504a_loaner_trans_id.NEXTVAL, v_target_cn, c504a_loaner_dt, c504a_expected_return_dt
				 , c504a_return_dt, c504a_void_fl, c901_consigned_to, c504a_consigned_to_id, c504a_created_by
				 , c504a_created_date, c504a_last_updated_date, c504a_last_updated_by, c703_sales_rep_id
				 , c704_account_id, c504a_is_loaner_extended, c504a_is_replenished, c504a_parent_loaner_txn_id
				 , c526_product_request_detail_id, c504a_processed_date, c504a_transf_ref_id, c504a_transf_date
				 , c504a_transf_by, c504a_transf_email_flg
			  FROM t504a_loaner_transaction
			 WHERE c504a_loaner_transaction_id = (SELECT MAX (TO_NUMBER (c504a_loaner_transaction_id))
													FROM t504a_loaner_transaction
		WHERE c504_consignment_id = p_source_cn AND c504a_void_fl IS NULL);
        */

		--DBMS_OUTPUT.put_line (' *******Step 7: Add record in T504A_LOANER_TXN');
		--DBMS_OUTPUT.put_line ('New CN # **** ' || v_target_cn);
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Update Sucessfully New CN # **** ' || v_target_cn);
		--
		--Step 8: Update new CN# and Set # in T5010
           update t5010_tag set c5010_last_updated_trans_id = v_target_cn,c207_set_id =p_target_set,c5010_last_updated_date=sysdate,
                                c5010_last_updated_by=v_updated_by where C5010_LAST_UPDATED_TRANS_ID = p_source_cn;
         DBMS_OUTPUT.put_line ('New CN # -> ' || v_target_cn || '  Old CN# -> ' ||p_source_cn);


	COMMIT;
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_loaner_swap;
	
/***********************************************************************
  * Author : Arockia Prasath
  * Description : To inactivate CN
 ************************************************************************/ 
PROCEDURE gm_upd_inactivate_set (
 	p_help_desk_id IN t914_helpdesk_log.c914_helpdesk_id%TYPE
  ,	p_consignment_id         	IN 		t504a_consignment_loaner.c504_consignment_id%TYPE	  
  , p_user_id				IN      t4010_group.c4010_last_updated_by%TYPE  
)
AS  
    v_userid NUMBER:=p_user_id; 
    v_status	   t504a_consignment_loaner.c504a_status_fl%TYPE;
 BEGIN        
	 
	 -- Mandatory helpdesk to log executed user information
	    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL) ;	   	

		-- check to see if the source is inactive, if it is , then error
		SELECT t504a.c504a_status_fl
		  INTO v_status
		  FROM t504a_consignment_loaner t504a, t504_consignment t504
		 WHERE t504a.c504_consignment_id = p_consignment_id
		 AND   t504a.c504_consignment_id = t504.c504_consignment_id
		 AND t504.c504_void_fl IS NULL AND t504a.c504a_void_fl IS NULL
		 FOR UPDATE;
	    
	    IF v_status = 60
		THEN
			raise_application_error ('-20085', 'This CN is inactive, can not swap');
		END IF;

		IF v_status != 0
		THEN
			raise_application_error ('-20085', 'This CN is not in available status , can not swap');
		END IF;
	    
    	--Step 4: Inactive in 504A
		UPDATE t504a_consignment_loaner
		   SET c504a_status_fl = 60
			 , c504a_last_updated_by = v_userid
			 , c504a_last_updated_date = SYSDATE			
		 WHERE c504_consignment_id = p_consignment_id; 
-- logs
	    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'CN is inactived : ' || p_consignment_id) ;
	EXCEPTION
	WHEN OTHERS THEN
	    DBMS_OUTPUT.put_line ('Error Occurred while executing gm_upd_inactivate_set: ' || SQLERRM) ;
	    ROLLBACK;	  
END gm_upd_inactivate_set;  

END it_pkg_op_ih_loaner;
/
