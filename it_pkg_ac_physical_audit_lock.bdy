/* Formatted on 2011/12/02 17:03 (Formatter Plus v4.8.0) */
-- @"C:\Data Correction\Script\it_pkg_ac_physical_audit_lock.bdy"
CREATE OR REPLACE PACKAGE BODY it_pkg_ac_physical_audit_lock
IS
    --
PROCEDURE gm_ac_inventory_lock (
        p_help_desk_id IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
        p_audit_id     IN t2600_physical_audit.c2600_physical_audit_id%TYPE,
        p_company_id   IN t2600_physical_audit.c1900_company_id%TYPE,
        p_plant_id     IN t2600_physical_audit.c5040_plant_id%TYPE
   )
AS
    v_inv_lock_id t250_inventory_lock.c250_inventory_lock_id%TYPE;
    v_cogs_lock_id t256_cogs_lock.c256_cogs_lock_id%TYPE;
    v_user_id NUMBER;
    --20431 Physical Audit
    v_lock_type t250_inventory_lock.c901_lock_type%TYPE := 20431;
    v_pi_valid_cnt NUMBER;
BEGIN
    -- Mandatory helpdesk to log executed user information
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (TRIM (p_help_desk_id), NULL) ;
    
    v_user_id := it_pkg_cm_user.get_valid_user;
    
    -- 1 to set the company and plant to context
    
    gm_pkg_cor_client_context.gm_sav_client_context (p_company_id, p_plant_id) ;
    
    -- validate the audit id
      SELECT COUNT (1) INTO v_pi_valid_cnt
   FROM t2600_physical_audit
  WHERE c5040_plant_id          = p_plant_id
    AND c2600_physical_audit_id = p_audit_id
    AND c2600_void_fl          IS NULL;
    
    IF v_pi_valid_cnt = 0
    THEN
    	raise_application_error ('-20999', 'Physical Audit not yet setup for this plant : ' ||get_plant_name (p_plant_id)) ;
    END IF;
    
    -- 1.1 to lock the inventory
    gm_pkg_op_ld_inventory.gm_op_ld_demand_main (v_lock_type, p_company_id, p_plant_id) ;
    
    -- 1.2 to get the lock id and update the physical audit table
     SELECT MAX (c250_inventory_lock_id)
       INTO v_inv_lock_id
       FROM t250_inventory_lock
      WHERE C5040_PLANT_ID = p_plant_id;
      
    --1.3 to update the negative qty as 0
     UPDATE t251_inventory_lock_detail
    SET c251_qty                   = 0
      WHERE c250_inventory_lock_id = v_inv_lock_id
        AND c251_qty               < 0;
        
    -- 1.4 to update teh lock id to PA master table
     UPDATE t2600_physical_audit
    SET c250_inventory_lock_id      = v_inv_lock_id
      WHERE c2600_physical_audit_id = p_audit_id
        AND c1900_company_id        = p_company_id
        AND C5040_PLANT_ID          = p_plant_id;
        
    dbms_output.put_line (' v_inv_lock_id ID :: '|| v_inv_lock_id);
    
    -- 2.1 to lock the cost table
    gm_pkg_ac_ld_cogs.gm_ld_main (v_lock_type, p_company_id, p_plant_id) ;
    
    -- 2.2 to get the cost id and update to physical audit table
     SELECT MAX (c256_cogs_lock_id)
       INTO v_cogs_lock_id
       FROM t256_cogs_lock
      WHERE C5040_PLANT_ID = p_plant_id;
      
    --2.3 to update the cogs lock id to PA master table
     UPDATE t2600_physical_audit
    SET c256_cogs_lock_id           = v_cogs_lock_id
      WHERE c2600_physical_audit_id = p_audit_id
        AND c1900_company_id        = p_company_id
        AND C5040_PLANT_ID          = p_plant_id;
    --
    dbms_output.put_line (' v_cogs_lock_id ID :: '|| v_inv_lock_id);
    --
    -- to exclude the do not count parts 
    -- So, that Tag should not be generated
    
	 	UPDATE t251_inventory_lock_detail
	   SET c251_qty = 0
	 WHERE c250_inventory_lock_id = v_inv_lock_id
	 AND c205_part_number_id IN (
	 			SELECT c205_part_number_id
                   FROM t205d_part_attribute
                  WHERE c901_attribute_type = '92342'
                    AND c205d_void_fl      IS NULL
                    AND c1900_company_id= p_company_id
	 );
	 
	 dbms_output.put_line (' After update Inv lock details :: '|| SQL%ROWCOUNT);
	 
	 
	  -- to exclude the do not count parts - So, that Tag should not be generated
	 
	 UPDATE t253b_item_consignment_lock
	   SET C505_ITEM_QTY = 0,
	   c250_inventory_lock_id = NULL
	 WHERE c250_inventory_lock_id = v_inv_lock_id
	 AND c205_part_number_id IN (
	  			SELECT c205_part_number_id
                   FROM t205d_part_attribute
                  WHERE c901_attribute_type = '92342'
                    AND c205d_void_fl      IS NULL
                    AND c1900_company_id= p_company_id
	 );
	 
	 --
	 
	 dbms_output.put_line (' After update item consignment lock details :: '|| SQL%ROWCOUNT);
	 
END gm_ac_inventory_lock;
--
/***************************************************
Description : This procedure used lock the PA location
*********************************************************/
PROCEDURE gm_ac_pa_tag_lock (
        p_audit_id    IN t2600_physical_audit.c2600_physical_audit_id%TYPE,
        p_location_id IN t2603_audit_location.c901_location_id%TYPE,
        p_user_id	  IN t2603_audit_location.c2603_created_by%TYPE)
AS
    v_user_id NUMBER;
    v_cnt NUMBER;
    v_pi_valid_cnt NUMBER;
BEGIN
	
	 -- validate the audit id
    SELECT COUNT (1) INTO v_pi_valid_cnt
   FROM t2600_physical_audit
  WHERE c2600_physical_audit_id = p_audit_id
    AND c2600_void_fl          IS NULL;
    
    IF v_pi_valid_cnt = 0
    THEN
    	raise_application_error ('-20999', 'Physical Audit not yet setup. Please verify the Audit Id ') ;
    END IF;
    
	-- to validate the location (already tag update)
	SELECT count(1) INTO v_cnt
   FROM t2605_audit_tag_detail
  WHERE c2600_physical_audit_id = p_audit_id
  	AND c901_location_id = p_location_id
    AND c2605_void_fl          IS NULL ;
    
   --
    IF v_cnt <> 0
    THEN
    	raise_application_error ('-20999', 'Tag already created for this location : ' ||get_code_name (p_location_id)) ;
    END IF;
    
    -- 1 to lock the PA audit location
    gm_pkg_ac_ld_tags.gm_ld_main (p_audit_id, p_location_id, p_user_id) ;
    --
END gm_ac_pa_tag_lock;

/********************************************************
Description : This procedure used delte the exclude part list (PI)
92342 PI :Exclude list of parts from Physical Inventory
*********************************************************/
PROCEDURE gm_ac_remove_pi_excl_parts (
        p_attr_type  IN t205d_part_attribute.c901_attribute_type%TYPE,
        p_company_id IN t2600_physical_audit.c1900_company_id%TYPE)
AS
BEGIN
     DELETE
       FROM t205d_part_attribute
      WHERE c901_attribute_type = p_attr_type
        AND c1900_company_id    = p_company_id;
END gm_ac_remove_pi_excl_parts;

END it_pkg_ac_physical_audit_lock;
/
