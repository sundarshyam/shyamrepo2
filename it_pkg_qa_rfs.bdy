/* Formatted on 2011/05/11 11:55 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_qa_rfs.bdy"

CREATE OR REPLACE PACKAGE BODY it_pkg_qa_rfs
IS
--

	/******************************************************************************
		* Description : This procedure is used to	save rfs for parts entered
		* part number string format : '101.109, 101.111, 101.113'
	*******************************************************************************/
	PROCEDURE gm_sav_rfs (
		p_help_desk_id	 IN   t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_attr_value	 IN   t205d_part_attribute.c205d_attribute_value%TYPE
	  , p_num_ids		 IN   VARCHAR2
	  , p_rfs_flag		 IN   VARCHAR2
	)
	AS
		v_user_id	   t205_part_number.c205_last_updated_by%TYPE;
		v_out_flag	   VARCHAR2 (1) := 'N';

		CURSOR cur_details
		IS
			SELECT token
			  FROM v_in_list
			 WHERE token IS NOT NULL;
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;
		my_context.set_my_inlist_ctx (p_num_ids);

		FOR var_detail IN cur_details
		LOOP
			IF (p_rfs_flag = 'Y')
			THEN
				gm_sav_part_attr (var_detail.token, 80180, p_attr_value, v_user_id, v_out_flag);

				IF (v_out_flag = 'Y')
				THEN
					INSERT INTO t902_log
								(c902_log_id, c902_ref_id, c902_comments, c902_type, c902_created_by
							   , c902_created_date)
						SELECT s507_log.NEXTVAL, c205_part_number_id
							 , 'Added RFS (' || get_code_name (TO_NUMBER (p_attr_value)) || ')', 1218, 303011, SYSDATE
						  FROM t205_part_number
						 WHERE c205_part_number_id = var_detail.token;
				END IF;
			ELSIF (p_rfs_flag = 'N')
			THEN
				DELETE FROM t205d_part_attribute
					  WHERE c205_part_number_id = var_detail.token
						AND c901_attribute_type = 80180
						AND c205d_attribute_value = p_attr_value;	--rfs

				INSERT INTO t902_log
								(c902_log_id, c902_ref_id, c902_comments, c902_type, c902_created_by
							   , c902_created_date)
						SELECT s507_log.NEXTVAL, c205_part_number_id
							 , ''Remove RFS (' || get_code_name (TO_NUMBER (p_attr_value)) || ')', 1218, 303011, SYSDATE
						  FROM t205_part_number
						 WHERE c205_part_number_id = var_detail.token;
			END IF;
		END LOOP;

		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id
											 ,	  'RFS parts: '
											   || get_code_name (TO_NUMBER (p_attr_value))
											   || ' '
											   || p_num_ids
											  );
		DBMS_OUTPUT.put_line ('Save RFS Completed.....');
		COMMIT;
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_rfs;

	/************************************************************************
	* Description : Procedure to save attribute value to T205D_PART_ATTRIBUTE
	*******************************************************************/
	PROCEDURE gm_sav_part_attr (
		p_num_id	   IN		t205_part_number.c205_part_number_id%TYPE
	  , p_attr_type    IN		t205d_part_attribute.c901_attribute_type%TYPE
	  , p_attr_value   IN		t205d_part_attribute.c205d_attribute_value%TYPE
	  , p_user_id	   IN		t205_part_number.c205_last_updated_by%TYPE
	  , p_out_flag	   OUT		VARCHAR2
	)
	AS
		v_attr_id	   t205d_part_attribute.c2051_part_attribute_id%TYPE;
	BEGIN
		UPDATE t205d_part_attribute
		   SET c205d_attribute_value = p_attr_value
			 , c205d_last_updated_date = SYSDATE
			 , c205d_last_updated_by = p_user_id
		 WHERE c205_part_number_id = p_num_id
		   AND c901_attribute_type = p_attr_type
		   AND c205d_attribute_value = p_attr_value;

		IF (SQL%ROWCOUNT = 0)
		THEN
			p_out_flag	:= 'Y';

			SELECT s205d_part_attribute.NEXTVAL
			  INTO v_attr_id
			  FROM DUAL;

			INSERT INTO t205d_part_attribute
						(c2051_part_attribute_id, c205_part_number_id, c901_attribute_type, c205d_attribute_value
					   , c205d_created_date, c205d_created_by
						)
				 VALUES (v_attr_id, p_num_id, p_attr_type, p_attr_value
					   , SYSDATE, p_user_id
						);
		END IF;
	END gm_sav_part_attr;
END it_pkg_qa_rfs;
/
