/* Formatted on 2011/02/16 17:24 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_inhouse_txn.bdy"


CREATE OR REPLACE PACKAGE BODY it_pkg_inhouse_txn
IS
--

	/*********************************************************
	* Description : This procedure is used to updated
	* control Number for the part of Loaner TXN
	*********************************************************/
	PROCEDURE gm_sav_control_num (
		p_help_desk_id		IN	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_txn_id			IN	 t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
	  , p_part_num			IN	 t413_inhouse_trans_items.c205_part_number_id%TYPE
	  , p_control_no_from	IN	 t413_inhouse_trans_items.c413_control_number%TYPE
	  , p_control_no_to 	IN	 t413_inhouse_trans_items.c413_control_number%TYPE
	)
	AS
		v_user_id	   NUMBER;
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		UPDATE t413_inhouse_trans_items t413
		   SET t413.c413_control_number = p_control_no_to
		 WHERE t413.c412_inhouse_trans_id = p_txn_id
		   AND t413.c205_part_number_id = p_part_num
		   AND NVL(t413.c413_control_number, 99999) = NVL(p_control_no_from, 99999)
		   AND t413.c413_void_fl IS NULL;

		--
		IF (SQL%ROWCOUNT = 0)
		THEN
			ROLLBACK;
			raise_application_error ('-20085', 'No record is found to be updated.');
		END IF;

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id
											 ,	  'TXN ID: '
											   || p_txn_id
											   || ' Part NUmber: '
											   || p_part_num
											   || ' Control NUmber: '
											   || p_control_no_from
											  );
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id
											 ,	  'TXN ID: '
											   || p_txn_id
											   || ' Part NUmber: '
											   || p_part_num
											   || ' New Control NUmber: '
											   || p_control_no_to
											  );
		DBMS_OUTPUT.put_line (SQL%ROWCOUNT || ' Row(s) are updated. ');
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
END gm_sav_control_num;
	
	
PROCEDURE GM_UPD_INHOUSE_TRANSACTIONS
  (
  p_help_desk_id		IN	 t914_helpdesk_log.c914_helpdesk_id%TYPE,
  p_comp_id   IN t412_inhouse_transactions.c1900_company_id%TYPE,
  p_plant_id  IN t412_inhouse_transactions.c5040_plant_id%TYPE,
  p_upd_by    IN t412_inhouse_transactions.c412_last_updated_by%TYPE,
  p_txn_id    IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
  )
  AS
  BEGIN
	  
	  -- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		
        UPDATE t412_inhouse_transactions 
         SET c1900_company_id= p_comp_id 
             ,c5040_plant_id= p_plant_id 
            ,c412_last_updated_by= p_upd_by 
            ,c412_last_updated_date= SYSDATE 
        	WHERE c412_inhouse_trans_id= p_txn_id;

		IF (SQL%ROWCOUNT = 0)
		THEN
			ROLLBACK;
			RAISE_APPLICATION_ERROR ('-20085', 'No record is found to be updated.');
		END IF;
		COMMIT;
  --
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;	
  END GM_UPD_INHOUSE_TRANSACTIONS;

  /*********************************************************
	* Description : This procedure is used to update
	* In-House consignments  stuck in Pending Verification to Pend-processing
	* (TSK-11689)
	*********************************************************/

PROCEDURE GM_UPD_IH_STATUS
  (
  p_help_desk_id		IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
  P_TXN_ID   			IN T504a_CONSIGNMENT_LOANER.C504_CONSIGNMENT_ID%TYPE,
  P_UPD_BY     			IN T504a_CONSIGNMENT_LOANER.C504A_LAST_UPDATED_BY%TYPE
  )
  AS
  BEGIN
  
     -- Mandatory helpdesk to log executed user information
	 it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
     
    UPDATE T504a_CONSIGNMENT_LOANER
         SET c504a_status_fl = 50, 
         c504a_last_updated_date = CURRENT_DATE, 
         c504a_last_updated_by = P_UPD_BY 
     WHERE c504_consignment_id = P_TXN_ID 
           AND c504a_void_fl IS NULL; 

        IF (SQL%ROWCOUNT = 0)
		THEN
			ROLLBACK;
			RAISE_APPLICATION_ERROR ('-20085', 'No record is found to be updated.');
		END IF;
    
       -- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id
											 , 'CONSIGNMENT ID: '
											   || p_txn_id
											  );
        DBMS_OUTPUT.put_line (SQL%ROWCOUNT || ' Row(s) are updated. ');											  
		COMMIT;
    
  END GM_UPD_IH_STATUS;
  
END it_pkg_inhouse_txn;
/
