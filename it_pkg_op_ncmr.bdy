/* Formatted on 2011/01/14 15:17 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_op_ncmr.bdy"


CREATE OR REPLACE PACKAGE BODY it_pkg_op_ncmr
IS
--

	/*********************************************************
	* Description : This procedure is used to updated
	* rejected qty when its status is Inspection
	*********************************************************/
	PROCEDURE gm_sav_rej_qty (
		p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_dhr_id		 t408_dhr.c408_dhr_id%TYPE
	  , p_rej_qty		 t408_dhr.c408_qty_rejected%TYPE
	)
	AS
		v_user_id	   NUMBER;
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		UPDATE t408_dhr
		   SET c408_qty_rejected = p_rej_qty
			 , c408_last_updated_by = v_user_id
			 , c408_last_updated_date = SYSDATE
		 WHERE c408_dhr_id = p_dhr_id AND c408_void_fl IS NULL;

		--
		IF (SQL%ROWCOUNT = 0)
		THEN
			ROLLBACK;
			raise_application_error ('-20085', 'No record is found to be updated.');
		END IF;

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'DHR ID: ' || p_dhr_id);
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'New Reject Qty: ' || p_rej_qty);
		DBMS_OUTPUT.put_line (SQL%ROWCOUNT || ' Row(s) are updated. ');
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_rej_qty;
END it_pkg_op_ncmr;
/
