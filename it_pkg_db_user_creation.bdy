



/******************************************************************
	 * Description : Procedure for create new DB user type
	 * Author 	 : Gomathi Palani
	 ****************************************************************/
create or replace
PACKAGE BODY it_pkg_db_user_creation
IS

PROCEDURE gm_sav_db_user
(
    p_helpdesk_id         IN       T914_HELPDESK_LOG.C914_HELPDESK_ID%TYPE,
    p_user_login_name     IN       t102_user_login.c102_login_username%TYPE,
    p_password            IN       t102_user_login.c102_user_password%TYPE,
    p_first_name          IN       t101_user.c101_user_f_name%TYPE,
    p_last_name           IN       t101_user.c101_user_l_name%TYPE,
    p_short_name          IN       t101_user.c101_user_sh_name%TYPE,
    p_access              IN       t101_user.c101_access_level_id%TYPE,
    p_email               IN       t101_user.c101_email_id%TYPE,
    p_deptid              IN       t101_user.c901_dept_id%TYPE,
    p_work_phone          IN       t101_user.c101_work_phone%TYPE,
    p_title               IN       t101_user.c101_title%TYPE,
    p_user_type           IN       t101_user.c901_user_type%TYPE,
    p_date_fmt            IN       t101_user.c101_date_format%TYPE,
    p_ext_access          IN       t102_user_login.c102_ext_access%TYPE,
    p_ldap_id		  IN 	   t102_user_login.c906c_ldap_master_id%TYPE,
    p_group_id            IN       t1501_group_mapping.c1500_group_id%TYPE,
    p_default_fl          IN       varchar2,
    p_compid		  IN	   t1900_company.C1900_COMPANY_ID%TYPE,
    p_plantid		  IN	   t5040_plant_master.c5040_plant_id%TYPE,
    p_type	          IN 	   t901_code_lookup.C901_CODE_ID%TYPE,
    p_c902_comments       IN       t902_log.c902_comments%TYPE,
    p_c902_type		  IN       t902_log.c902_type%TYPE,
    p_auth_type           IN       t102_user_login.c901_auth_type%TYPE,
    p_password_expired_fl IN       varchar2
)

	AS
	 
	v_userid t101_user.c101_user_id%TYPE; 
	v_created_by t101_user.c101_user_id%TYPE; 
	v_sales_rep_id T703_SALES_REP.C703_SALES_REP_ID%TYPE; 
	v_grp_mapping_id VARCHAR2(1000); 
	v_count number;


	BEGIN 

	 SELECT COUNT (1)
	   INTO v_count
	   FROM T102_USER_LOGIN
	  WHERE C102_LOGIN_USERNAME = p_user_login_name;
  
  IF (v_count=0) THEN 
	
    gm_pkg_cor_client_context.gm_sav_client_context(p_compid,p_plantid);

	it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_helpdesk_id, NULL);

	v_created_by := it_pkg_cm_user.get_valid_user;

	gm_pkg_it_user.gm_save_user(v_userid,p_user_login_name,p_password,p_first_name,p_last_name,p_short_name,p_access,p_email,p_deptid,p_work_phone,
	p_title,v_created_by,p_user_type,p_date_fmt,p_ext_access,p_ldap_id,v_sales_rep_id); 

	gm_pkg_cm_accesscontrol.gm_sav_user_group(p_group_id,v_userid,p_default_fl,v_created_by,p_compid,v_grp_mapping_id); 

	gm_pkg_cm_party.gm_cm_sav_partycompany(p_compid,p_type,v_userid,v_created_by); 

	gm_update_log(v_userid,p_c902_comments,p_c902_type,v_created_by,v_grp_mapping_id); 

	update T102_USER_LOGIN
	set	C901_AUTH_TYPE=P_AUTH_TYPE
	,C102_LAST_UPDATED_BY=v_created_by
	,C102_LAST_UPDATED_DATE=CURRENT_DATE
	,C102_PASSWORD_EXPIRED_FL=p_password_expired_fl
	where c101_user_id=v_userid; 

     dbms_output.put_line('User Created successfully for the User Login' || p_user_login_name);


   END IF;

 IF (v_count>=1)  THEN
   
    dbms_output.put_line('User Already exists for the user login ' || p_user_login_name);

 
  END IF;
	
	END gm_sav_db_user;
END it_pkg_db_user_creation;
/