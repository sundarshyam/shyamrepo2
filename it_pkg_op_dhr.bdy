/* Formatted on 2011/01/07 10:34 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_op_dhr.pkg"

create or replace
PACKAGE BODY it_pkg_op_dhr
IS
--
	PROCEDURE gm_sav_control_num (
	    p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_dhr_id		 t408_dhr.c408_dhr_id%TYPE
	  , p_lot_no		 t408_dhr.c408_control_number%TYPE
	  , p_pack_slip		 t408_dhr.c408_packing_slip%TYPE
	)
	AS
		v_partnum	   t408_dhr.c205_part_number_id%TYPE;
		v_user_id	   NUMBER;
		v_ctlno		   t408_dhr.c408_control_number%TYPE;
		v_count 	   NUMBER;
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;
		
		BEGIN
			SELECT c205_part_number_id,c408_control_number 
			  INTO v_partnum, v_ctlno
			  FROM t408_dhr
			 WHERE c408_dhr_id = p_dhr_id AND c408_void_fl IS NULL;

		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error ('-20999', 'DHR ID ' || p_dhr_id || ' does not exist or voided');
		END;

		UPDATE t408_dhr
		   SET c408_control_number = p_lot_no
		     , c408_packing_slip = NVL(TRIM(p_pack_slip), c408_packing_slip)
			 , c408_last_updated_by = v_user_id
			 , c408_last_updated_date = SYSDATE
		 WHERE c408_dhr_id = p_dhr_id;
		
		SELECT COUNT (1)
		  INTO v_count
		  FROM t2550_part_control_number 
		  where c205_part_number_id = v_partnum
 		       and c2550_control_number = v_ctlno;
		
		IF (v_count = 0)
		THEN
		
		 update t2550_part_control_number 
 		   set c2550_control_number = p_lot_no,
 		       c2550_last_updated_by = v_user_id,
 		       c2550_last_updated_date = sysdate
 		   where c205_part_number_id = v_partnum
 		       and c2550_control_number = v_ctlno;		
		END IF;
		
		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'DHR ID: ' || p_dhr_id);
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'New Lot Number: ' || p_lot_no);
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'New Pack Slip: ' || p_pack_slip);
		
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_control_num;

	/******************************************************************
	* Description : This procedure is used to enter attribute value to the 
	* part number attribute table.
	****************************************************************/
	PROCEDURE gm_sav_control_num_attribute (
	   p_help_desk_id	IN t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_part_num		IN t205_part_number.C205_PART_NUMBER_ID%TYPE
	  , p_type		    IN t205d_part_attribute.C901_ATTRIBUTE_TYPE%TYPE
	  , p_action	    IN t205d_part_attribute.C205D_ATTRIBUTE_VALUE%TYPE
	)
	AS
		v_count 	   NUMBER;
		v_user_id	   NUMBER;
		v_jira_id      t914_helpdesk_log.c914_helpdesk_id%TYPE;
		v_part_num     t205_part_number.c205_part_number_id%TYPE;
		v_type		   t205d_part_attribute.c901_attribute_type%TYPE;
		v_attr_value   t205d_part_attribute.c205d_attribute_value%TYPE;
	BEGIN		
		--As all the data's are copied from excel TRIM the incoming values	
		v_jira_id    := TRIM(p_help_desk_id);
		v_part_num   := TRIM(p_part_num);
		v_type       := TRIM(p_type);
		v_attr_value := TRIM(p_action);
		
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (v_jira_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		---Check if the part number and the attribute already exists in the table, if its there, the attribute value will be updated.
		SELECT COUNT (1)
		  INTO v_count
		  FROM t205d_part_attribute t205d
		 WHERE C205_PART_NUMBER_ID = v_part_num AND C901_ATTRIBUTE_TYPE = v_type AND C205D_VOID_FL IS NULL;
		
		IF (v_count > 0)
		THEN
			UPDATE t205d_part_attribute SET C205D_ATTRIBUTE_VALUE=v_attr_value , C205D_LAST_UPDATED_DATE = sysdate, 
			C205D_LAST_UPDATED_BY= v_user_id
			WHERE C205_PART_NUMBER_ID = v_part_num AND C901_ATTRIBUTE_TYPE = v_type AND C205D_VOID_FL IS NULL;
		ELSE
			INSERT
		       INTO t205d_part_attribute
			(
			    C2051_PART_ATTRIBUTE_ID, C205_PART_NUMBER_ID, C205D_ATTRIBUTE_VALUE
			  , C901_ATTRIBUTE_TYPE    , C205D_CREATED_BY   , C205D_CREATED_DATE
			)
			VALUES
			(
			    S205D_PART_ATTRIBUTE.NEXTVAL,v_part_num,v_attr_value,
			    v_type,v_user_id,SYSDATE
			 );		

		END IF;
		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (v_jira_id,
							' Part Number ID: '|| p_part_num || 
							' Type :' ||GET_CODE_NAME(p_type) ||
							' Value :'||p_action
							);
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_control_num_attribute;
	/******************************************************************
	* Description : This procedure is used to modify lot number.
	****************************************************************/
	PROCEDURE GM_UPDATE_CONTROL_NUMBER(
		p_help_desk_id	IN t914_helpdesk_log.c914_helpdesk_id%TYPE,
		p_dhrid         IN T408a_dhr_control_number.c408_dhr_id%TYPE,
		p_partnum       IN T408a_dhr_control_number.c205_part_number_id%TYPE,
		p_fromcontrolno IN T408a_dhr_control_number.c408a_conrol_number%TYPE,
		p_tocontrolno   IN T408a_dhr_control_number.c408a_conrol_number%TYPE)
	AS
	  v_inhouse_trans_id T413_inhouse_trans_items.C412_INHOUSE_TRANS_ID%TYPE;
	  v_jira_id      t914_helpdesk_log.c914_helpdesk_id%TYPE;
	  v_user_id	   NUMBER;
	  V_INHOURS_TRANS_CNT NUMBER;
    V_T408A_CONTROLNO_COUNT   NUMBER;
    V_T2550_CONTROLNO_COUNT  NUMBER;
    V_T502_CONTROLNO_COUNT  NUMBER;
    V_T505_CONTROLNO_COUNT  NUMBER;
    
	BEGIN
	  
	  --As all the data's are copied from excel TRIM the incoming values	
		v_jira_id    := TRIM(p_help_desk_id);
		
	  -- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (v_jira_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

    SELECT COUNT(T408A.C408A_CONROL_NUMBER )  
    INTO V_T408A_CONTROLNO_COUNT
    FROM T408A_DHR_CONTROL_NUMBER T408A
    WHERE T408A.C408_DHR_ID = P_DHRID
    AND T408A.C408A_CONROL_NUMBER = p_fromcontrolno
    AND T408A.C408A_VOID_FL IS NULL;
    
    SELECT COUNT(T2550.C2550_CONTROL_NUMBER)
    INTO V_T2550_CONTROLNO_COUNT
    FROM T2550_PART_CONTROL_NUMBER T2550
    WHERE T2550.C2550_CONTROL_NUMBER = P_FROMCONTROLNO 
    AND T2550.C205_PART_NUMBER_ID = P_PARTNUM;
        
IF (V_T408A_CONTROLNO_COUNT > 0 AND  V_T2550_CONTROLNO_COUNT > 0) THEN
	  --updating control number DHR and control number master table       
	  UPDATE t408a_dhr_control_number
	  SET c408a_conrol_number  =p_tocontrolno,
		c408a_last_updated_by  =v_user_id,
		c408a_last_updated_date=sysdate
	  WHERE c205_part_number_id=p_partnum
	  AND c408a_conrol_number  =p_fromcontrolno
	  AND c408_dhr_id          = p_dhrid
	  AND c408a_void_fl       IS NULL;
	  
	  UPDATE t2550_part_control_number
	  SET c2550_control_number  =p_tocontrolno,
		c2550_last_updated_by   =v_user_id,
		c2550_last_updated_date =sysdate
	  WHERE c2550_control_number=p_fromcontrolno
	  AND c2550_last_updated_trans_id  =p_dhrid
	  AND c205_part_number_id =p_partnum;
	  
	  --validating control number is available in multiple transaction
	  SELECT COUNT(t413.c412_inhouse_trans_id)
	  INTO v_inhours_trans_cnt
	  FROM T413_inhouse_trans_items t413, t412_inhouse_transactions t412 
	  WHERE t413.C205_PART_NUMBER_ID = p_partnum
	  AND t413.C413_control_number   = p_fromcontrolno 
	  AND t413.c412_inhouse_trans_id = t412.c412_inhouse_trans_id
	  AND t412.c412_void_fl IS NULL
	  GROUP BY t413.c412_inhouse_trans_id;
	  
	  IF(v_inhours_trans_cnt = 1) THEN
	  
		SELECT C412_INHOUSE_TRANS_ID
		INTO v_inhouse_trans_id
		FROM T413_inhouse_trans_items
		WHERE C205_PART_NUMBER_ID = p_partnum
		AND C413_control_number   = p_fromcontrolno 
		AND C413_void_fl IS NULL 
		GROUP BY c412_inhouse_trans_id;
		
		UPDATE t413_inhouse_trans_items
		SET c413_control_number  =p_tocontrolno,
		  c413_last_updated_by   =v_user_id,
		  c413_last_updated_date =sysdate
		WHERE c413_control_number=p_fromcontrolno
		AND c205_part_number_id  =p_partnum
		AND c412_inhouse_trans_id=v_inhouse_trans_id;    
	  
		 DBMS_OUTPUT.put_line('Success - Updated inhouse transaction from ' || p_fromcontrolno || 'to ' || p_tocontrolno) ;
	  ELSIF v_inhours_trans_cnt = 0 THEN
		DBMS_OUTPUT.put_line('Failure - No  inhouse transactions with ' || p_fromcontrolno) ;
	  ELSIF v_inhours_trans_cnt > 1 THEN
		DBMS_OUTPUT.PUT_LINE('Failure - More than one inhouse transactions found for ' || P_FROMCONTROLNO || 'to ' || P_TOCONTROLNO );
	  END IF;
    
    --- Checking  Item order table
     SELECT COUNT(T502.C502_CONTROL_NUMBER ) 
     INTO V_T502_CONTROLNO_COUNT
     FROM T502_ITEM_ORDER T502
     WHERE T502.C502_CONTROL_NUMBER = P_FROMCONTROLNO
     AND T502.C205_PART_NUMBER_ID = P_PARTNUM
     AND T502.C502_DELETE_FL IS NULL
     AND T502.C502_VOID_FL IS NULL;
    
     IF(V_T502_CONTROLNO_COUNT = 1) THEN
      -- Updating Item Order table
       UPDATE T502_ITEM_ORDER 
       SET C502_CONTROL_NUMBER = P_TOCONTROLNO,
         C502_LAST_UPDATED_DATE_UTC = SYSDATE
       WHERE C502_CONTROL_NUMBER = P_FROMCONTROLNO
         AND C205_PART_NUMBER_ID = P_PARTNUM
         AND C502_DELETE_FL IS NULL
         AND C502_VOID_FL IS NULL;
      
        DBMS_OUTPUT.PUT_LINE('Success - Updated Item Order  from ' || P_FROMCONTROLNO || ' to ' || P_TOCONTROLNO) ;
     ELSIF(V_T502_CONTROLNO_COUNT = 0) THEN
        DBMS_OUTPUT.PUT_LINE('Failure - No Item Order with ' || P_FROMCONTROLNO || ' found ' ) ;
      ELSIF(V_T502_CONTROLNO_COUNT > 1) THEN
        DBMS_OUTPUT.PUT_LINE('Failure - More than one Item Order with ' || P_FROMCONTROLNO || ' found ' ) ;
      END IF;
      
      -- Checking Item Consignment Table
      SELECT COUNT(T505.C505_CONTROL_NUMBER )
      INTO  V_T505_CONTROLNO_COUNT
      FROM T505_ITEM_CONSIGNMENT T505
      WHERE T505.C505_CONTROL_NUMBER = P_FROMCONTROLNO
      AND T505.C205_PART_NUMBER_ID = P_PARTNUM
      AND T505.C505_VOID_FL IS NULL;
      
      IF (V_T505_CONTROLNO_COUNT = 1) THEN
       -- Updating Item Consignment table
        UPDATE T505_ITEM_CONSIGNMENT 
        SET C505_CONTROL_NUMBER = P_TOCONTROLNO,
          C505_LAST_UPDATED_DATE_UTC = SYSDATE
       WHERE C505_CONTROL_NUMBER =  P_FROMCONTROLNO
       AND C205_PART_NUMBER_ID = P_PARTNUM
       AND C505_VOID_FL IS NULL;
      
       DBMS_OUTPUT.PUT_LINE('Success - Updated Item consignment  from ' || P_FROMCONTROLNO || ' to ' || P_TOCONTROLNO) ;
      ELSIF(V_T505_CONTROLNO_COUNT = 0) THEN
         DBMS_OUTPUT.PUT_LINE('Failure - No Item consignment  with ' || P_FROMCONTROLNO || ' found ' ) ;
      ELSIF(V_T505_CONTROLNO_COUNT > 1) THEN 
        DBMS_OUTPUT.PUT_LINE('Failure - More than one Item Consignment  with ' || P_FROMCONTROLNO || ' found ' ) ;  
      END IF;
    
  -- Error messages if no transactions found in T408a and T2550 tables  
  ELSIF V_T408A_CONTROLNO_COUNT = 0  THEN
    DBMS_OUTPUT.PUT_LINE('Failure - No transactions found with DHR Id ' || P_DHRID || 'and control no ' || P_FROMCONTROLNO ) ;
  ELSIF V_T2550_CONTROLNO_COUNT = 0  THEN
    DBMS_OUTPUT.PUT_LINE('Failure - No transactions found with Control no ' || P_FROMCONTROLNO || 'and part number ' || P_PARTNUM ) ;
  END IF;  --
	  -- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'DHR ID: ' || p_dhrid);
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'New Lot Number: ' || p_tocontrolno);
	  
	EXCEPTION
	WHEN OTHERS THEN
	  DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
	  ROLLBACK;
	END GM_UPDATE_CONTROL_NUMBER;

END IT_PKG_OP_DHR;
/
