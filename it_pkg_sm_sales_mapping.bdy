/* Formatted on 2010/10/25 19:22 (Formatter Plus v4.8.0) */
--@"C:\Data Correction\Script\it_pkg_sm_sales_mapping.bdy";

CREATE OR REPLACE PACKAGE BODY it_pkg_sm_sales_mapping
IS
	
	/*
	Below procedure is used to call the child procedure based on the different scenarios.
	There are 5 scenarios: 1. Remove/Merge Zone 2. New VP-New Zone AD 3. Existing VP-New Zone 4. New VP-Old Zone 5. Existing VP-old Zone.
	*/
	PROCEDURE gm_sav_sales_vp_map (
		p_help_desk_id	IN		t914_helpdesk_log.c914_helpdesk_id%TYPE,
		p_vp_id    		IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		p_old_vp_id   	IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		p_zone_id		IN		t710_sales_hierarchy.c901_zone_id%TYPE,
		p_old_zone_id	IN		t710_sales_hierarchy.c901_zone_id%TYPE,
		p_regn_id      	IN   	t708_region_asd_mapping.c901_region_id%TYPE,
		p_action_id	    IN		VARCHAR2	
	)
	AS
		v_extn_vp  VARCHAR2(10);
		v_extn_zone VARCHAR2(10);
		v_user_id	   NUMBER;
		v_access	NUMBER;
		v_vp_type 	NUMBER;
	BEGIN
		v_extn_vp  := chk_actv_regn_advp(p_vp_id);
		v_extn_zone := chk_existing_zone(p_zone_id);
		
		v_access := get_user_accesslvl(p_vp_id);
		
		SELECT DECODE(v_access,4,8001,NULL) 
		  INTO v_vp_type
		  FROM dual; 
		
		IF v_vp_type IS NULL THEN
			raise_application_error ('-20999', 'The access level for provided ad/vp is not correct. Pls contact system admin.');
		END IF;
		
		DBMS_OUTPUT.put_line ('Zone - '||get_code_name(p_zone_id)||': Existing -'||v_extn_zone) ;
		DBMS_OUTPUT.put_line ('VP - '||get_user_name(p_vp_id)||': Existing -'||v_extn_vp) ;
		
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id , NULL) ;
		v_user_id := it_pkg_cm_user.get_valid_user;
		
		IF p_action_id = 'REM_MRG_ZONE'
		THEN
			gm_sav_rem_mrg_zone(p_vp_id,p_old_vp_id,p_zone_id,p_old_zone_id,v_user_id);
		END IF;
		IF v_extn_vp = 'NO' 
		THEN
			gm_sav_new_vp_map(p_vp_id,p_old_vp_id,p_zone_id,v_extn_zone,p_action_id, v_user_id);
		END IF;
		IF v_extn_vp = 'YES' 
		THEN
			gm_sav_existing_vp_map(p_vp_id,p_old_vp_id,p_zone_id,v_extn_zone,p_regn_id, p_action_id, v_user_id);
		END IF;	
	
	EXCEPTION
		WHEN OTHERS THEN
		DBMS_OUTPUT.put_line ('Error Occurred while executing : ' || SQLERRM) ;
		ROLLBACK;	
			
	END gm_sav_sales_vp_map;
	/*
	*/
	PROCEDURE gm_sav_existing_vp_map(
		p_vp_id    		IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		p_old_vp_id   	IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		p_zone_id		IN		t710_sales_hierarchy.c901_zone_id%TYPE,
		p_extn_zone	    IN		VARCHAR2,
		p_regn_id      	IN   	t708_region_asd_mapping.c901_region_id%TYPE,
		p_action_id	    IN		VARCHAR2,
		p_user_id     	IN   	t708_region_asd_mapping.c708_created_by%TYPE
	)
	AS
		CURSOR C_VP_REGN IS
		SELECT c901_region_id regnid
	      FROM t708_region_asd_mapping
         WHERE c101_user_id = p_old_vp_id
           AND c708_delete_fl IS null
           AND c708_inactive_fl IS null;
		v_extn_old_vp varchar2(10);
	BEGIN
		DBMS_OUTPUT.put_line ('Existing VP mapping started...') ;
		IF p_extn_zone = 'NO'
		THEN
			raise_application_error ('-20999', 'Please complete the AD mapping first.');
		END IF;
		
		IF p_action_id = 'REPLACE_NO_VP' THEN
			raise_application_error ('-20999', 'Please provide valid data for this mapping.');	
		END IF;
		
		v_extn_old_vp := chk_actv_regn_advp(p_old_vp_id);
		IF v_extn_old_vp = 'YES' AND p_action_id = 'REPLACE_VP' 
		THEN
			FOR vrec IN C_VP_REGN
			LOOP
				gm_sav_asd_regn_map(NULL, 8001, p_vp_id, vrec.regnid, p_user_id);
				gm_sav_del_asd_map(NULL, p_old_vp_id, vrec.regnid, p_user_id);
			END LOOP;
			DBMS_OUTPUT.put_line ('Existing VP ' ||get_user_name(p_old_vp_id)||' got replaced by...'||get_user_name(p_vp_id)) ;
		ELSIF p_regn_id IS NOT NULL AND p_action_id = 'NO_ACTION' THEN
			gm_sav_asd_regn_map(NULL, 8001, p_vp_id, p_regn_id, p_user_id);	
			DBMS_OUTPUT.put_line ('Existing VP ' ||get_user_name(p_vp_id)||' got mapped to...'||get_code_name(p_regn_id)) ;		
		END IF;
		DBMS_OUTPUT.put_line ('Existing VP mapping completed...') ;
	END gm_sav_existing_vp_map;
	
	/*
	Below procedure is used to map the new vp to existing zone and throw validation for new zone to complete the 
	ad mapping.
	*/
	PROCEDURE gm_sav_new_vp_map(
		p_vp_id    		IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		p_old_vp_id   	IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		p_zone_id		IN		t710_sales_hierarchy.c901_zone_id%TYPE,
		p_extn_zone	    IN		VARCHAR2,
		p_action_id	    IN		VARCHAR2,
		p_user_id     	IN   	t708_region_asd_mapping.c708_created_by%TYPE	
	)
	AS
		CURSOR C_VP_REGN IS
		SELECT c901_region_id regnid
	      FROM t708_region_asd_mapping
         WHERE c101_user_id = p_old_vp_id
           AND c708_delete_fl IS null
           AND c708_inactive_fl IS null;
         
        CURSOR C_ZONE_REGN IS
		SELECT c901_area_id regnid			   
		  FROM t710_sales_hierarchy 
		 WHERE c710_active_fl = 'Y'
		   AND c710_void_fl IS NULL
		   AND c901_zone_id = p_zone_id;
		      
		v_extn_old_vp varchar2(10);
	BEGIN
		IF p_extn_zone = 'YES'
		THEN
			v_extn_old_vp := chk_actv_regn_advp(p_old_vp_id);
			IF v_extn_old_vp = 'YES' AND p_action_id = 'REPLACE_VP' 
			THEN
				FOR vrec IN C_VP_REGN
				LOOP
					gm_sav_asd_regn_map(NULL, 8001, p_vp_id, vrec.regnid, p_user_id);
					gm_sav_del_asd_map(NULL, p_old_vp_id, vrec.regnid, p_user_id);
				END LOOP;
			ELSIF p_action_id = 'REPLACE_NO_VP' 
			THEN
				FOR v_rec IN C_ZONE_REGN
				LOOP
					gm_sav_asd_regn_map(NULL, 8001, p_vp_id, v_rec.regnid, p_user_id);
				END LOOP;
			ELSE
				raise_application_error ('-20999', 'Please provide valid data for this mapping.');	
			END IF;			
		ELSE
			raise_application_error ('-20999', 'Please complete the AD mapping first.');
		END IF;
	END gm_sav_new_vp_map;
	
	/*
	Below procedure is used to remove or merge zone.
	It will take all old regions based on zone and map them to new zone.
	Also it will remove the old vp to region mapping and will map new vp to region.
	*/
	PROCEDURE gm_sav_rem_mrg_zone(
		p_vp_id    		IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		p_old_vp_id   	IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		p_zone_id		IN		t710_sales_hierarchy.c901_zone_id%TYPE,
		p_old_zone_id	IN		t710_sales_hierarchy.c901_zone_id%TYPE,
		p_user_id     	IN   	t708_region_asd_mapping.c708_created_by%TYPE	
	)
	AS
		CURSOR C_ZONE_REGN IS
		SELECT c901_area_id regnid,
			   c901_division_id divid,
			   c901_company_id compid,
			   c901_country_id cntryid
		  FROM t710_sales_hierarchy 
		 WHERE c710_active_fl = 'Y'
		   AND c710_void_fl IS NULL
		   AND c901_zone_id = p_old_zone_id;
		
	BEGIN
		DBMS_OUTPUT.put_line ('old_vp_id - '||get_user_name(p_old_vp_id)||': old_zone_id -'||get_code_name(p_old_zone_id)) ;
		FOR vrec IN C_ZONE_REGN
		LOOP
			gm_sav_sales_hierarchy(vrec.divid, p_zone_id, vrec.regnid, vrec.compid, vrec.cntryid, p_user_id);
			gm_sav_asd_regn_map(NULL, 8001, p_vp_id, vrec.regnid, p_user_id);
			gm_sav_del_sales_map(vrec.regnid,p_old_zone_id,p_user_id);
			gm_sav_del_asd_map(NULL, p_old_vp_id, vrec.regnid, p_user_id);
		END LOOP;
		DBMS_OUTPUT.put_line ('Remove or merge zone completed.') ;
	END gm_sav_rem_mrg_zone;
	
	/*
	Below procedure is used to call the child procedure based on the different scenarios.
	There are 4 scenarios: 1. Remove/Merge Regn 2. Replace AD 3. Map new region to new/old AD 4. Map old region to new/old AD.
	*/
	PROCEDURE gm_sav_sales_ad_map (
		p_help_desk_id	IN		t914_helpdesk_log.c914_helpdesk_id%TYPE,
		p_ad_id    		IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		p_old_ad_id   	IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		--p_vp_id    		IN   	t708_region_asd_mapping.c101_user_id%TYPE, -- TO BE REMOVED
		p_regn_id      	IN   	t708_region_asd_mapping.c901_region_id%TYPE,
		p_div_id		IN		t710_sales_hierarchy.c901_division_id%TYPE,
		p_comp_id		IN		t710_sales_hierarchy.c901_company_id%TYPE,
		p_contry_id		IN		t710_sales_hierarchy.c901_country_id%TYPE,
		p_action_id	    IN		VARCHAR2		
	)
	AS
		v_extn_reg  VARCHAR2(10);
		v_extn_zone VARCHAR2(10);
		v_extn_ad   VARCHAR2(10);
		v_user_id	   NUMBER;
		v_access	NUMBER;
		v_zone_id NUMBER;
		v_ad_type t708_region_asd_mapping.c901_user_role_type%TYPE;
	BEGIN
		
		v_extn_reg := chk_existing_regn(p_regn_id);
		v_zone_id  := get_regn_zone(p_regn_id);
		v_extn_ad  := chk_actv_regn_advp(p_ad_id);
		v_access := get_user_accesslvl(p_ad_id);
		
		SELECT DECODE(v_access,6,8002,4,8001,3,8000,NULL) 
		  INTO v_ad_type
		  FROM dual; 
		
		
		
		IF v_ad_type IS NULL AND p_action_id != 'REGN_ZONE_ONLY' THEN
			raise_application_error ('-20999', 'The access level for '||get_user_name(p_ad_id) ||' is not correct. Pls contact system admin.');
		END IF;
		
		IF v_zone_id = -999 THEN
			raise_application_error ('-20999', 'The code look up alt-name entry for region '||get_code_name(p_regn_id)||' to zone '||get_code_name(v_zone_id)||' is not correct. Pls contact system admin.');
		END IF;
		
		v_extn_zone := chk_existing_zone(v_zone_id);
		
		DBMS_OUTPUT.put_line ('Zone - '||get_code_name(v_zone_id)||': Existing -'||v_extn_zone) ;
		DBMS_OUTPUT.put_line ('Regn - '||get_code_name(p_regn_id)||': Existing -'||v_extn_reg) ;
		DBMS_OUTPUT.put_line ('AD - '||get_user_name(p_ad_id)||': Existing -'||v_extn_ad) ;
		DBMS_OUTPUT.put_line ('AD access - '||v_access||': AD type -'||get_code_name(v_ad_type)) ;
		
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id , NULL) ;
		v_user_id := it_pkg_cm_user.get_valid_user;
		
		IF p_action_id = 'REM_MRG_REGN'
		THEN
			gm_sav_rem_mrg_regn(p_old_ad_id, p_regn_id,v_zone_id, v_user_id);
		END IF;
		
		IF p_action_id = 'REGN_ZONE_ONLY'
		THEN
			gm_sav_regn_zone_map(p_regn_id,v_zone_id, p_div_id,p_comp_id, p_contry_id, v_user_id);
		END IF;
		
		
		IF p_action_id = 'REPLACE_AD'
		THEN
			gm_sav_replace_ad(p_ad_id,p_old_ad_id,v_user_id);
		END IF;
		
		IF v_extn_ad = 'NO' AND  p_action_id = 'NO_ACTION'
		THEN
			gm_sav_new_ad_map(p_ad_id
								, p_old_ad_id
								, v_ad_type
								--, p_vp_id -- TO BE REMOVED
								, p_div_id
								, v_zone_id
								, v_extn_zone
								, p_regn_id
								, v_extn_reg
								, p_comp_id
								, p_contry_id
								, v_user_id);
			
		END IF;
		
		IF v_extn_ad = 'YES' AND  p_action_id = 'NO_ACTION'
		THEN
			gm_sav_existing_ad_map( p_ad_id
								, p_old_ad_id
								, v_ad_type
								--, p_vp_id -- TO BE REMOVED
								, p_div_id
								, v_zone_id
								, p_regn_id
								, v_extn_reg
								, p_comp_id
								, p_contry_id
								, v_user_id);
		END IF;		
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id , 'AD : '|| get_user_name(p_ad_id) || 'mapped/un-mapped to/from :'||get_code_name(p_regn_id)) ;
		DBMS_OUTPUT.put_line ('AD mapping/un-mapping Completed.....') ;
		
		--COMMIT;
	
	EXCEPTION
		WHEN OTHERS THEN
		DBMS_OUTPUT.put_line ('Error Occurred while executing : ' || SQLERRM) ;
		ROLLBACK;	
	END gm_sav_sales_ad_map;
	
	/*
	Below procedure is used to remove or merge the region to a zone.
	*/
	PROCEDURE gm_sav_rem_mrg_regn (
	    p_old_ad_id		IN		t708_region_asd_mapping.c101_user_id%TYPE,
		p_regn_id      	IN   	t708_region_asd_mapping.c901_region_id%TYPE,
		p_zone_id		IN		t710_sales_hierarchy.c901_zone_id%TYPE,
		p_user_id     	IN   	t708_region_asd_mapping.c708_created_by%TYPE	
	)
	AS
	BEGIN
		gm_sav_del_sales_map(p_regn_id,p_zone_id,p_user_id);
		gm_sav_del_asd_map(p_old_ad_id, NULL, p_regn_id, p_user_id);
		DBMS_OUTPUT.put_line ('Remove or merge region completed.') ;
	END gm_sav_rem_mrg_regn;
	/*
	Below procedure is used when some region is going to move to different zone and no change in AD/VP mapping.
	*/
	PROCEDURE gm_sav_regn_zone_map (
		p_regn_id      	IN   	t708_region_asd_mapping.c901_region_id%TYPE,
		p_zone_id		IN		t710_sales_hierarchy.c901_zone_id%TYPE,
		p_div_id		IN		t710_sales_hierarchy.c901_division_id%TYPE,
		p_comp_id		IN		t710_sales_hierarchy.c901_company_id%TYPE,
		p_contry_id		IN		t710_sales_hierarchy.c901_country_id%TYPE,
		p_user_id     	IN   	t708_region_asd_mapping.c708_created_by%TYPE
	)
	AS
		v_zone	t710_sales_hierarchy.c901_zone_id%TYPE;
	BEGIN 
		SELECT t710.c901_zone_id 
		  INTO v_zone  
  		  FROM t710_sales_hierarchy t710
 	     WHERE t710.c710_void_fl IS NULL
   		   AND t710.c710_active_fl = 'Y'
   		   AND t710.c901_area_id = p_regn_id; 
   		
   		IF p_zone_id = v_zone
   		THEN
   			raise_application_error ('-20999', 'The code look up alt-name entry for region '||get_code_name(p_regn_id)||' to zone '||get_code_name(p_zone_id)||' is not correct. Pls contact system admin.');
   		END IF;
   		gm_sav_del_sales_map(p_regn_id,v_zone,p_user_id);
   		gm_sav_sales_hierarchy(p_div_id, p_zone_id, p_regn_id, p_comp_id, p_contry_id, p_user_id);
   	EXCEPTION WHEN OTHERS THEN
   		raise_application_error ('-20999','Error Occurred while executing : ' || SQLERRM) ;   		   
	END gm_sav_regn_zone_map;
	
	/*
	Below procedure is used to replace the existing AD completely.
	*/
	
	PROCEDURE gm_sav_replace_ad (
		p_ad_id    		IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		p_old_ad_id		IN		t708_region_asd_mapping.c101_user_id%TYPE,
		p_user_id     	IN   	t708_region_asd_mapping.c708_created_by%TYPE
	)
	AS
		CURSOR C_AD_REGN IS
		SELECT c901_region_id regnid, c901_user_role_type adtype
	      FROM t708_region_asd_mapping
         WHERE c101_user_id = p_old_ad_id
           AND c708_delete_fl IS null
           AND c708_inactive_fl IS null;
	BEGIN
		DBMS_OUTPUT.put_line ('Replacing existing AD...') ;
		FOR v_rec in C_AD_REGN
		LOOP
			gm_sav_del_asd_map(p_old_ad_id, NULL, v_rec.regnid, p_user_id);
			DBMS_OUTPUT.put_line ('Old AD :'||get_user_name(p_old_ad_id) ||'..got removed from region : '||get_code_name(v_rec.regnid)) ;
			gm_sav_asd_regn_map(p_ad_id, v_rec.adtype, NULL, v_rec.regnid, p_user_id);
			DBMS_OUTPUT.put_line ('New AD :'||get_user_name(p_ad_id) ||'..got mapped to region : '||get_code_name(v_rec.regnid)) ;			
		END LOOP;
		DBMS_OUTPUT.put_line ('Existing AD got replaced...') ;
	END gm_sav_replace_ad;
	/*
	Below procedure is used for new AD mapping to new/old region based on new/old zone.
	*/
	PROCEDURE gm_sav_new_ad_map (
		p_ad_id    		IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		p_old_ad_id		IN		t708_region_asd_mapping.c101_user_id%TYPE,
		p_ad_type    	IN   	t708_region_asd_mapping.c901_user_role_type%TYPE,
		--p_vp_id    		IN   	t708_region_asd_mapping.c101_user_id%TYPE, -- TO BE REMOVED
		p_div_id		IN		t710_sales_hierarchy.c901_division_id%TYPE,
		p_zone_id		IN		t710_sales_hierarchy.c901_zone_id%TYPE,
		p_extn_zone     IN      VARCHAR2,
		p_regn_id      	IN   	t708_region_asd_mapping.c901_region_id%TYPE,
		p_extn_reg      IN      VARCHAR2,
		p_comp_id		IN		t710_sales_hierarchy.c901_company_id%TYPE,
		p_contry_id		IN		t710_sales_hierarchy.c901_country_id%TYPE,
		p_user_id     	IN   	t708_region_asd_mapping.c708_created_by%TYPE	
	)
	AS
	BEGIN
		DBMS_OUTPUT.put_line ('New AD mapping started...') ;
		IF p_extn_reg = 'NO' THEN
			gm_sav_sales_hierarchy(p_div_id, p_zone_id, p_regn_id, p_comp_id, p_contry_id, p_user_id);
			gm_sav_asd_regn_map(p_ad_id, p_ad_type, NULL, p_regn_id, p_user_id);
		END IF;
		IF p_extn_reg = 'YES' THEN
			gm_sav_del_asd_map(p_old_ad_id, NULL, p_regn_id, p_user_id);
			gm_sav_asd_regn_map(p_ad_id, p_ad_type, NULL, p_regn_id, p_user_id);
			IF p_extn_zone = 'NO' THEN
				gm_sav_sales_hierarchy(p_div_id, p_zone_id, p_regn_id, p_comp_id, p_contry_id, p_user_id);
			END IF;
		END IF;	
		DBMS_OUTPUT.put_line ('New AD mapping completed.') ;
	END gm_sav_new_ad_map;
	
	
	/*
	Below procedure is used to do and existing ad mapping with new/old region for new/old zone.
	*/
	PROCEDURE gm_sav_existing_ad_map (
		p_ad_id    		IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		p_old_ad_id		IN		t708_region_asd_mapping.c101_user_id%TYPE,
		p_ad_type    	IN   	t708_region_asd_mapping.c901_user_role_type%TYPE,
		--p_vp_id    		IN   	t708_region_asd_mapping.c101_user_id%TYPE, -- TO BE REMOVED
		p_div_id		IN		t710_sales_hierarchy.c901_division_id%TYPE,
		p_zone_id		IN		t710_sales_hierarchy.c901_zone_id%TYPE,
		p_regn_id      	IN   	t708_region_asd_mapping.c901_region_id%TYPE,
		p_extn_reg      IN      VARCHAR2,
		p_comp_id		IN		t710_sales_hierarchy.c901_company_id%TYPE,
		p_contry_id		IN		t710_sales_hierarchy.c901_country_id%TYPE,
		p_user_id     	IN   	t708_region_asd_mapping.c708_created_by%TYPE
	)
	AS
	CURSOR C_AD_REGN IS
		SELECT c901_region_id regnid
	      FROM t708_region_asd_mapping
         WHERE c101_user_id = p_ad_id
           AND c708_delete_fl IS null
           AND c708_inactive_fl IS null
           AND c901_user_role_type = 8000;
	v_old_regnid t708_region_asd_mapping.c901_region_id%TYPE;
	BEGIN
		DBMS_OUTPUT.put_line ('Existing AD mapping started...') ;
		IF p_extn_reg = 'NO' 
		THEN			
			IF p_ad_type = 8002 
			THEN -- super ad
				FOR v_rec IN C_AD_REGN
				LOOP
					gm_sav_asd_regn_map(p_ad_id, p_ad_type, NULL, v_rec.regnid, p_user_id);
				END LOOP;
				gm_sav_sales_hierarchy(p_div_id, p_zone_id, p_regn_id, p_comp_id, p_contry_id, p_user_id);
				gm_sav_asd_regn_map(p_ad_id, p_ad_type, NULL, p_regn_id, p_user_id);
				gm_sav_asd_regn_map(p_ad_id, 8000, NULL, p_regn_id, p_user_id);	
				--gm_sav_asd_regn_map(NULL, 8001, p_vp_id, p_regn_id, p_user_id);	
			
			ELSIF p_ad_type = 8000 
			THEN -- regular ad
				IF chk_actv_regn_advp(p_ad_id) = 'YES' THEN
				
					SELECT c901_region_id 
					  INTO v_old_regnid
					  FROM t708_region_asd_mapping
					 WHERE c101_user_id = p_ad_id
					   AND c708_delete_fl IS NULL
					   AND c708_inactive_fl IS NULL;
					   
					gm_sav_del_asd_map(p_ad_id, NULL, v_old_regnid, p_user_id);
				END IF;
				gm_sav_sales_hierarchy(p_div_id, p_zone_id, p_regn_id, p_comp_id, p_contry_id, p_user_id);
				gm_sav_asd_regn_map(p_ad_id, p_ad_type, NULL, p_regn_id, p_user_id);	
			
			END IF;
		END IF;
		IF p_extn_reg = 'YES' 
		THEN
			IF p_ad_type = 8000 THEN 
				gm_sav_del_asd_map(p_old_ad_id, NULL, p_regn_id, p_user_id);
				gm_sav_asd_regn_map(p_ad_id, p_ad_type, NULL, p_regn_id, p_user_id);
			END IF;
			IF p_ad_type = 8002 THEN
				gm_sav_asd_regn_map(p_ad_id, p_ad_type, NULL, p_regn_id, p_user_id);
			END IF;
		END IF;
		
		DBMS_OUTPUT.put_line ('Existing AD mapping completed.') ;
	END gm_sav_existing_ad_map;
	/*
	
	*/
	PROCEDURE gm_sav_del_sales_map(
	p_regn_id      	IN   	t708_region_asd_mapping.c901_region_id%TYPE,
	p_zone_id		IN		t710_sales_hierarchy.c901_zone_id%TYPE,
	p_user_id     	IN   	t708_region_asd_mapping.c708_created_by%TYPE
	)
	AS
	BEGIN
		UPDATE t710_sales_hierarchy
           SET c710_active_fl = 'N',
               c710_void_fl = 'Y',
               c710_last_updated_by = p_user_id,
               c710_last_updated_date = sysdate
         WHERE c901_area_id = p_regn_id
           AND c901_zone_id = p_zone_id;
	END gm_sav_del_sales_map;
	/*
	Below procedure is master save for sales hierarchy table.
	*/
	PROCEDURE gm_sav_sales_hierarchy (
		p_div_id		IN		t710_sales_hierarchy.c901_division_id%TYPE,
		p_zone_id		IN		t710_sales_hierarchy.c901_zone_id%TYPE,
		p_regn_id      	IN   	t708_region_asd_mapping.c901_region_id%TYPE,
		p_comp_id		IN		t710_sales_hierarchy.c901_company_id%TYPE,
		p_contry_id		IN		t710_sales_hierarchy.c901_country_id%TYPE,
		p_user_id     	IN   	t708_region_asd_mapping.c708_created_by%TYPE
	)
	AS
		v_id varchar2(40);
	BEGIN
		
		UPDATE t710_sales_hierarchy
		SET   c901_country_id = p_contry_id
			, c710_last_updated_by = 303503
			, c710_last_updated_date = sysdate
		WHERE c901_area_id = p_regn_id
		AND c901_zone_id = p_zone_id
		AND c901_division_id = p_div_id
		AND c710_active_fl = 'Y'
		AND c710_void_fl IS NULL;
			
		IF (SQL%ROWCOUNT = 0) THEN
			SELECT max(c710_sales_hierarchy_id) + 2
			  INTO v_id
			  FROM t710_sales_hierarchy;
			INSERT INTO t710_sales_hierarchy
					(c710_sales_hierarchy_id, c901_division_id, c901_zone_id, c901_area_id
					, c901_company_id, c901_country_id, c710_active_fl, c710_created_by, c710_created_date)
			values (v_id, p_div_id, p_zone_id, p_regn_id
					, p_comp_id, p_contry_id, 'Y', p_user_id, sysdate);
					
		END IF;
		
		DBMS_OUTPUT.put_line ('Sales Hierarchy (T710) record get saved for '||'ZONE - '||get_code_name(p_zone_id)|| ': Regn - '||get_code_name(p_regn_id) ||': DIV - '||get_code_name(p_div_id) ||': COMP -' ||get_code_name(p_comp_id)) ;
	END gm_sav_sales_hierarchy;
	/*
	Below procedure is master save for asd regn table.
	*/

	PROCEDURE gm_sav_asd_regn_map (
		p_ad_id    		IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		p_type    	    IN   	t708_region_asd_mapping.c901_user_role_type%TYPE,
		p_vp_id    		IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		p_regn_id      	IN   	t708_region_asd_mapping.c901_region_id%TYPE,
		p_user_id     	IN   	t708_region_asd_mapping.c708_created_by%TYPE
	)
	AS
		v_id	t708_region_asd_mapping.c101_user_id%TYPE;
	BEGIN
		SELECT DECODE(p_type,8000,p_ad_id, 8002, p_ad_id, 8001,p_vp_id,NULL ) 
		  INTO v_id 
		  FROM DUAL;
		
		UPDATE t708_region_asd_mapping 
		   SET c708_last_updated_by = p_user_id,
		       c708_last_updated_date = SYSDATE
		 WHERE c101_user_id = 303503 --v_id
		   AND c901_region_id = p_regn_id
		   AND c708_delete_fl IS null
		   AND c708_inactive_fl IS null;
		
		IF (SQL%ROWCOUNT = 0) THEN
			INSERT INTO t708_region_asd_mapping
				( C708_REGION_ASD_ID, C901_REGION_ID, C101_USER_ID, C708_START_DT,
					C708_CREATED_BY, C708_CREATED_DATE, C901_USER_ROLE_TYPE
				)
			VALUES ( S708_region_asd_mapping.NEXTVAL, p_regn_id, v_id, sysdate,
					p_user_id, sysdate, p_type);
		END IF;
		DBMS_OUTPUT.put_line ('AD/VP - Regn mapping (T708) record get saved for '||'AD - '||get_user_name(p_ad_id)|| ': Regn - '||get_code_name(p_regn_id) ||': VP - '||get_user_name(p_vp_id) ||': Type -' ||p_type) ;
	END gm_sav_asd_regn_map;
	
	/*
	Below procedure is used to delete or deactivate asd mapping.
	*/
	
	PROCEDURE gm_sav_del_asd_map (
		p_old_ad_id 	IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		p_old_vp_id 	IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		p_regn_id      	IN   	t708_region_asd_mapping.c901_region_id%TYPE,
		p_user_id     	IN   	t708_region_asd_mapping.c708_created_by%TYPE
	)
	AS
	BEGIN
		
		UPDATE t708_region_asd_mapping 
	   	   SET c708_delete_fl = 'Y',
	   		   c708_inactive_fl = 'Y',
	   		   c708_last_updated_date = sysdate,
	   		   c708_last_updated_by = p_user_id
	   	 WHERE c901_region_id = p_regn_id
		  AND c101_user_id  = NVL(p_old_ad_id,p_old_vp_id);	
		IF (SQL%ROWCOUNT > 0) THEN   
			DBMS_OUTPUT.put_line ('AD/VP mapping(T708) get deactivated for '||'AD - '||get_user_name(p_old_ad_id)|| ': Regn - '||get_code_name(p_regn_id) ||': VP - '||get_user_name(p_old_vp_id)) ;
		END IF;    		
	END gm_sav_del_asd_map;
	
	/*
	Below function is used to check the regn is existing or new based on T708 entry.
	*/
	FUNCTION chk_existing_regn (
		p_regn_id  		IN   	t708_region_asd_mapping.c901_region_id%TYPE		
	) RETURN VARCHAR2
	AS
		v_count   NUMBER;
	BEGIN
		SELECT count(1)
		  INTO v_count
		  FROM t708_region_asd_mapping
		 WHERE c901_region_id = p_regn_id
		   AND c708_delete_fl IS NULL
		   AND c708_inactive_fl IS NULL;
		
		IF v_count > 0
		THEN
			return 'YES';
		END IF;
		return 'NO';
      		
	END chk_existing_regn;
	/*
	Below function is used to check the zone is existing or new based on T710 entry.
	*/
	
	FUNCTION chk_existing_zone (
		p_zone_id  		IN   	t710_sales_hierarchy.c901_zone_id%TYPE		
	)
	RETURN VARCHAR2
	AS
		v_count   NUMBER;
	BEGIN
		SELECT count(1)
		  INTO v_count
		  FROM t710_sales_hierarchy
		 WHERE c901_zone_id = p_zone_id
		   AND c710_void_fl IS NULL
		   AND c710_active_fl = 'Y';
		
		IF v_count > 0
		THEN
			return 'YES';
		END IF;
		return 'NO';
      		
	END chk_existing_zone;
	
	/*
	Below function is used to check the ad is existing or not.
	*/
	FUNCTION chk_actv_regn_advp (
		p_advp_id    		IN   	t708_region_asd_mapping.c101_user_id%TYPE		
	)
	RETURN VARCHAR2
	AS
		v_count   NUMBER;
	BEGIN
		SELECT count(1)
		  INTO v_count
		  FROM t708_region_asd_mapping
		 WHERE c101_user_id = p_advp_id
		   AND c708_delete_fl IS NULL
		   AND c708_inactive_fl IS NULL;
		
		IF v_count > 0
		THEN
			return 'YES';
		ELSE
			return 'NO';
		END IF;
	   		
	END chk_actv_regn_advp;
	/*
	Below function is used to get the zone for given region.
	*/
	FUNCTION get_regn_zone (
		p_regnid    		IN   	t708_region_asd_mapping.c901_region_id%TYPE		
	)
	RETURN NUMBER
	AS
		v_zone   VARCHAR2(20);
	BEGIN
		SELECT c902_code_nm_alt
		  INTO v_zone
		  FROM t901_code_lookup
		 WHERE c901_code_id = p_regnid
		   AND c901_active_fl = '1'
		   AND c901_void_fl IS NULL
		   AND c901_code_grp = 'REGN';
		
		IF v_zone IS NULL THEN			
			RETURN -999;
		END IF;		
		
		RETURN to_number(v_zone);
		
	EXCEPTION WHEN OTHERS THEN		
		RETURN -999;	
	END get_regn_zone;
/*
	Below function is used to get the access level id from user table which will used to decide ad, vp or super ad type.
	*/
FUNCTION get_user_accesslvl (
		p_userid    		IN   	t708_region_asd_mapping.c101_user_id%TYPE		
	)
	RETURN NUMBER
	AS
		v_accesslvl   NUMBER;
		v_user_status t101_user.c901_user_status%TYPE;
	BEGIN
		SELECT c101_access_level_id, c901_user_status
		  INTO v_accesslvl, v_user_status
		  FROM t101_user
		 WHERE c101_user_id = p_userid;
			
		IF v_user_status = 317 THEN
			raise_application_error ('-20999', 'This is not an active user');
		END IF;
		
		IF v_accesslvl IS NULL THEN			
			RETURN -999;
		END IF;
		   
		return v_accesslvl;
	EXCEPTION WHEN OTHERS THEN
		IF v_user_status = 317 THEN
			raise_application_error ('-20999', 'This is not an active user');
		END IF;		
		RETURN -999;
	END get_user_accesslvl;	
	
END it_pkg_sm_sales_mapping;
/