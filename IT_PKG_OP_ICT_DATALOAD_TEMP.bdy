create or replace PACKAGE BODY IT_PKG_OP_ICT_DATALOAD_TEMP
IS
/*
 *   This is the main procedure that will be called by the ETL tool to load the Set, Loose Item into the DB.
 *   Logic :
 *    Take distinct of all the US Consignment ID.
 *    Check if the Consignment ID is already loded [ DHR table will have the CN ID in the comments field].
 *    If the CN data is not loaded, it will create the PO , WO for the parts in the CN.
 *    Create DHBL / DHFG based on if the CN is having a set ID or not.
 *    If the CN is having a set ID, the part quantity will be added to the main inventory [ Bulk].
 *    The Newly created transaction DHBL will be verified. If its a DHFG it will be in the Pending verification [3] status.
 *    Once the Inventory quantity is increased, a new CN will be created and the status will be updated.
 *    When the CN is not loaded , it will send an email to the support team.
 *    Upon loading the data , Email will be sent to the Users [ One email for Set Load, another for Loose Items ].
 *    The Load Complete fl, Email Notification fl will be update in the TE504a_Consignment_LD_Status	.
 */

	PROCEDURE gm_sav_process_dataload
	AS
		v_po_id 	   t401_purchase_order.c401_purchase_ord_id%TYPE;
		v_vendor_id    t301_vendor.c301_vendor_id%TYPE;
		v_comments	   VARCHAR2 (500);
		v_po_type	   t401_purchase_order.c401_type%TYPE := 102365;--Stock Transfer
		v_packslip	   t408_dhr.c408_packing_slip%TYPE;
		v_inhouse_trans_type VARCHAR2 (100) := '100065'; -- DHBL
		v_user_id	   NUMBER := 649291;
		v_set_id	   t208_set_details.c207_set_id%TYPE;
		v_distributor_id VARCHAR2 (100);
		v_new_req_id   t520_request.c520_request_id%TYPE;
		v_new_consign_id t504_consignment.c504_consignment_id%TYPE;
		v_request_status t520_request.c520_status_fl%TYPE;
		v_source_consign_id TE504_consignment_data.c504_consignment_id%TYPE;
		v_tag_id TE504_consignment_data.c5010_tag_id%TYPE;
		v_loc_type number := 40033 ; -- Inhouse
		v_dateformat   VARCHAR2 (10);
		v_etl_ld_status_id NUMBER;
		v_skipped_cn_notfi VARCHAR2(100);
		v_skipped_cn_notfi_msg CLOB;
		v_skipped_cn_notfi_sub VARCHAR2(100);
		v_skipped_cn CLOB;
		v_loaded_cn_cnt NUMBER;
		crlf varchar2(2) := chr(13)||chr(10);
		v_temp_err_msg CLOB;

		v_wo_qty NUMBER;
		v_po_void_cnt NUMBER;
		v_cn_pend_qty NUMBER;
        v_source_oustffrom_id TE504_consignment_data.C8902_OUS_REF_ID%TYPE;
        v_company_id t1900_company.c1900_company_id%TYPE;
        v_us_time_zone t901_code_lookup.c901_code_nm%TYPE;
        v_us_time_format t901_code_lookup.c901_code_nm%TYPE;
        v_plant_id NUMBER :=3004;
		v_src_cn 	t902_log.c902_comments%TYPE;
		p_message	t902_log.c902_comments%TYPE;
		v_comp_cd t1900_company.c1900_company_cd%TYPE;
		

		CURSOR tag_item
		IS
			SELECT TE504.c5010_tag_id tag_id FROM TE504_consignment_data TE504
				   WHERE c207_consignment_set_id IS NULL
				   AND c5010_tag_id IS NOT NULL;


		CURSOR cur_source_consign
		IS
			SELECT DISTINCT c504_consignment_id source_cons_id, etl.C8902_OUS_REF_ID source_oustffrom_id
					   FROM TE504_consignment_data etl,
					   TE504a_Consignment_LD_Status stat
					  WHERE etl.c504_consignment_id IS NOT NULL
					--	AND etl.c207_consignment_set_id IS NOT NULL [ NEED TO GET SET, LOOSE ITEM CN ]
						AND etl.c504_consignment_id = stat.c504_consign_id_src(+)
						AND stat.ce504a_ld_complete_fl IS NULL;
		BEGIN

		BEGIN
		-- Fetching company id, plant for context
				BEGIN
					SELECT c1900_company_id,c5040_plant_id
					INTO v_company_id,v_plant_id
					FROM TE504_consignment_data WHERE ROWNUM=1;
				EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						RETURN;
				END;
          /* SELECT c5040_plant_id
            INTO v_plant_id
            FROM t5041_plant_company_mapping
            WHERE c1900_company_id=v_company_id;*/

        SELECT GET_CODE_NAME(C901_TIMEZONE), GET_CODE_NAME(C901_DATE_FORMAT)
        INTO v_us_time_zone,v_us_time_format
        FROM t1900_company
        WHERE c1900_company_id=v_company_id;

 gm_pkg_cor_client_context.gm_sav_client_context(v_company_id,v_us_time_zone,v_us_time_format,v_plant_id);

		-- For Getting the vendor id
           SELECT c301_vendor_id
			  INTO v_vendor_id
			  FROM t301_vendor
			 WHERE c301_vendor_name = 'Globus Audubon'
             AND c1900_company_id=v_company_id;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error ( - 20999, SQLERRM) ;
		END;


		FOR source_consign IN cur_source_consign
		LOOP
			BEGIN
				v_source_consign_id := source_consign.source_cons_id;
                v_source_oustffrom_id := source_consign.source_oustffrom_id;

      				-- Reset the Global Variable in each loop
				v_err_src_cn_id := '';
				v_err_id := '';
				v_inhouse_trans_type := '100065'; -- DHBL

				BEGIN

					   select distinct C401_PURCHASE_ORD_ID into v_po_id from TE504_consignment_data where c504_consignment_id= v_source_consign_id;

				EXCEPTION
				  WHEN TOO_MANY_ROWS
				  THEN

				    -- To log the exception into the TE504b_Consignment_LD_Error table.
				    gm_sav_exception_msg(v_err_src_cn_id,v_err_id,v_err_msg, v_user_id);

				    -- To store the skipped CN
				    v_skipped_cn := v_skipped_cn||v_source_consign_id ||crlf;

				    -- To continue the loop with next CN
				    CONTINUE;

				END;

				-- WHEN THE SET ID IS NULL, THEN IT DENOTES THE CN IS FOR LOOSE ITEM.
				BEGIN
					SELECT set_id
					  INTO v_set_id
					  FROM (SELECT c207_consignment_set_id set_id
							  FROM TE504_consignment_data
							 WHERE c504_consignment_id = v_source_consign_id AND ROWNUM = 1);
				EXCEPTION WHEN NO_DATA_FOUND
				THEN
					v_set_id := '';
				END;

				IF v_set_id IS NULL
				THEN
					v_inhouse_trans_type := '100067'; -- DHFG
				END IF;

				-- Check if the CN [Loose/Set] is already loaded in the DHR table, if its loaded, those CN should not be loaded again.
				select COUNT(C408_DHR_ID) INTO v_loaded_cn_cnt from t408_dhr where c408_comments=v_source_consign_id AND c408_status_fl = '4' and C1900_COMPANY_ID=v_company_id;

        			-- If DHR is available, fetch WO Pending quantity and fetch WO pending for PO.

				IF v_loaded_cn_cnt > 0 THEN

				    SELECT SUM( get_wo_pend_qty( t408.c402_work_order_id,
				      (SELECT SUM(c402_qty_ordered)
					  FROM t402_work_order
					  WHERE c402_work_order_id=t408.c402_work_order_id)))  INTO v_cn_pend_qty
				    FROM t408_dhr t408
				    WHERE c408_comments=v_source_consign_id
				    AND c408_status_fl = '4' AND c1900_company_id=v_company_id;

				    -- To fetch WO pending for PO.

				    SELECT COUNT(c402_work_order_id ) into v_wo_qty
				      FROM t402_work_order
				      WHERE c401_purchase_ord_id =  v_po_id
				      AND c402_status_fl !=3 AND c1900_company_id=v_company_id;

           			ELSE

           			     -- If PO ID exists in etl consignment data table but does not exist in OUS

				     IF v_po_id is not NULL
				     THEN

				     	 -- Check if PO ID exist in OUS

				     	 SELECT COUNT(c401_purchase_ord_id) INTO v_po_void_cnt
				     	     FROM t401_purchase_order
				     		  WHERE c401_purchase_ord_id = v_po_id 
							  AND c1900_company_id=v_company_id;

					  IF v_po_void_cnt = 0 THEN

					    -- Assign PO ID as NULL so that it will create a new PO ID and new WO ID in OUS.

					    v_po_id:=NULL;

					  END IF;



				     END IF;

				END IF;
       				-- Processing the consignment for these scenario. - If no DHR present or pending qty exists or all WO ID are not closed.

				IF v_loaded_cn_cnt = 0 OR v_cn_pend_qty !=0 OR v_wo_qty >0 THEN

					SELECT setl504a_consign_ld_status_id.NEXTVAL
					  INTO v_etl_ld_status_id
					  FROM DUAL;

					INSERT INTO TE504a_Consignment_LD_Status
								(ce504a_consign_ld_status_id, ce504a_ld_start_time, c504_consign_id_src, c207_consign_set_id, ce504a_ld_complete_fl ,C8902_OUS_REF_ID,C1900_COMPANY_ID
								)
						 VALUES (v_etl_ld_status_id, CURRENT_DATE, v_source_consign_id, v_set_id, 'N',v_source_oustffrom_id,v_company_id
								);

					-- Check if PO ID is null before creating PO and WO

					IF v_po_id is NULL
					THEN
						create_po_wo (v_source_consign_id, v_po_type, v_user_id, v_po_id, v_vendor_id);
					ELSE

						-- Permitting voided PO - Update Void Flag as Null and Log status - If PO ID exists check if the PO ID is voided

						  SELECT COUNT(c401_purchase_ord_id) INTO v_po_void_cnt
						     FROM t401_purchase_order
						     WHERE c401_void_fl = 'Y'
						     AND c401_purchase_ord_id = v_po_id
							 AND c1900_company_id=v_company_id;

						  IF v_po_void_cnt = 1 THEN

						    -- if PO ID is voided, un void the PO ID and Log the comments
						    gm_permit_void_po_wo(v_po_id,NULL,v_user_id);

						  END IF;

					END IF;

					gm_load_dhr (v_source_consign_id
									   , v_po_id
									   , v_vendor_id
									   , v_comments
									   , v_packslip
									   , v_po_type
									   , v_inhouse_trans_type
									   , v_set_id
									   , v_user_id
                                       , v_us_time_format
										);
					-- Paddy v_dateformat := get_rule_value ('DATEFMT', 'DATEFORMAT');

					IF v_set_id IS NOT NULL
					THEN
                   	gm_pkg_op_process_request.gm_sav_initiate_set (
                     ''
									 , TO_CHAR (TRUNC (CURRENT_DATE), v_us_time_format)
									 , '50618'
									 , ''
									 , v_set_id
									 , 40021
									 , v_distributor_id
									 , 50626
									 , v_user_id
									 , NULL
									 , NULL
									 , ''
									 , 15
									 , v_user_id
									 , '50061'
                   , ''
									 , v_new_req_id
									 , v_new_consign_id
									  );
						-- AS the above procedure will create the lot number with NOC#, we are calling the gm_upd_control_number procedure to update the control number in the T505 table.
						gm_upd_control_number (v_source_consign_id, v_new_consign_id, v_set_id);
--DBMS_OUTPUT.PUT_LINE('step 2:'||v_new_consign_id);
						UPDATE t520_request
						   SET c520_status_fl = 15
						   , C520_LAST_UPDATED_BY = v_user_id
						   , C520_LAST_UPDATED_DATE = CURRENT_DATE
						 WHERE c520_request_id = v_new_req_id
						 AND c1900_company_id=v_company_id;


						UPDATE t504_consignment
						   SET c504_status_fl = 2
							 , c504_verify_fl = 0
							 , c504_verified_date = NULL
							 , c504_verified_by = NULL
							 , c504_last_updated_by = v_user_id
							 , c504_last_updated_date = CURRENT_DATE
							 , c504_comments = v_source_consign_id	 -- comments
							 , C504_REPROCESS_ID = v_source_consign_id
							 , c504_update_inv_fl = NULL
						 WHERE c504_consignment_id = v_new_consign_id
						 AND c1900_company_id=v_company_id;

						gm_update_bulk_qty(v_new_consign_id,4302,4311); -- To Reduce quantity from Bulk.
					END IF;  -- v_set_id IS NOT NULL


					UPDATE TE504a_Consignment_LD_Status
					   SET ce504a_ld_complete_fl = 'Y'
						 , c504_consign_id_tgt = v_new_consign_id
						 , ce504a_ld_complete_date = CURRENT_DATE
					 WHERE ce504a_consign_ld_status_id = v_etl_ld_status_id;
					 v_new_consign_id := '';
				ELSE
					v_skipped_cn := v_skipped_cn||v_source_consign_id ||crlf;
				END IF; -- v_loaded_cn_cnt

				BEGIN
					SELECT NVL(c8902_ous_ref_id,c504_consign_id_src) INTO v_src_cn
					FROM te504a_consignment_ld_status
					WHERE c504_consign_id_tgt=v_new_consign_id;
					
					SELECT get_company_code(c1900_company_id) INTO v_comp_cd
					FROM t504_consignment
					WHERE c504_consignment_id=v_src_cn;
					
					v_src_cn:='From '||v_comp_cd||': '||v_src_cn;
					--
					GM_UPDATE_LOG(v_new_consign_id,v_src_cn,1220,v_user_id,p_message); -- set build comments
					GM_UPDATE_LOG(v_new_consign_id,v_new_req_id,1235,v_user_id,p_message); -- Request comments
				EXCEPTION WHEN NO_DATA_FOUND THEN
					v_src_cn := NULL;
				END;
				
				 COMMIT;  -- THIS is to commit the consignment that is loaded now.

				EXCEPTION WHEN OTHERS THEN
				 	ROLLBACK; -- WHEN the CN is failed due to any reason, rollback the transaction.
                    gm_sav_exception_msg(v_err_src_cn_id,v_err_id,v_err_msg, v_user_id);
				 	commit;  -- To log the exception into the TE504b_Consignment_LD_Error table.
				 END;
		END LOOP;
	
	FOR tag_update IN tag_item
		LOOP
		 BEGIN
			 v_tag_id := tag_update.tag_id;
		
/* Loaction type as distributor then only update the c5010_last_updated_trans_id so that here two update  satements need.
 * First update the c5010_last_updated_trans_id then change the Locatation type as In House
 * Update the Tag id information based on the consignmened tag
 */
			
				UPDATE t5010_tag
					SET 
  						c5010_last_updated_trans_id  =NULL ,
 						c1900_company_id             =v_company_id,
						c5040_plant_id               =v_plant_id,
  						c5010_last_updated_by        =v_user_id,
  						c5010_last_updated_date      =CURRENT_DATE,
  						c5010_sub_location_comments  ='For Item Consignment: Ref_id removed'
						WHERE c5010_tag_id           =v_tag_id;
											
				UPDATE t5010_tag
					SET 
  						c901_location_type           =v_loc_type ,--4033
 						c5010_last_updated_by        =v_user_id,
  						c5010_last_updated_date      =CURRENT_DATE,
  						c5010_sub_location_comments  = NULL
						WHERE c5010_tag_id           =v_tag_id
						AND c5010_last_updated_trans_id is NULL;	
						
			commit;			
			END;
	    END LOOP;

		-- When the US CN is not loaded , email should be sent to check why the data is not loaded. This is to avoid multiple dataload.
		IF LENGTH(v_skipped_cn)>0 THEN

			SELECT get_rule_value_by_company('SKIPPED_CN_NOTIF_TO','DATA_LOAD_NOTIF',v_company_id) INTO v_skipped_cn_notfi FROM DUAL;
			SELECT GET_RULE_VALUE('SKIPPED_CN_NOTIF_SUB','DATA_LOAD_NOTIF') INTO v_skipped_cn_notfi_sub FROM DUAL;
			SELECT GET_RULE_VALUE('SKIPPED_CN_NOTIF_MSG','DATA_LOAD_NOTIF') INTO v_skipped_cn_notfi_msg FROM DUAL;

			IF v_skipped_cn_notfi_msg IS NULL
			THEN
				v_skipped_cn_notfi_msg := 'The Following Consignment ID"s are not loaded during the current data load, as the CN data was already available in the DHR Table. ';
			END IF;

			v_skipped_cn_notfi_msg := v_skipped_cn_notfi_msg||crlf||v_skipped_cn;

			GM_COM_SEND_EMAIL_PRC(v_skipped_cn_notfi,v_skipped_cn_notfi_sub,v_skipped_cn_notfi_msg);

		END IF;

		-- Email confirmation for the Sets that is loaded.
		GM_PKG_CM_JOB.gm_cm_notify_set_load_info();
		COMMIT;

		-- Email Confirmation for the Loose Items.
		GM_PKG_CM_JOB.gm_cm_notify_item_load_info();
		COMMIT;

		--WHen there is an error, throwing the error which will have all the part/CN that had issues along with error message.
		IF v_err_msg IS NOT NULL
		THEN
			v_temp_err_msg :=v_err_msg ;
			v_err_msg  :='';
			raise_application_error(-20999, v_temp_err_msg);
		END IF;
	END gm_sav_process_dataload;

	PROCEDURE create_po_wo (
		p_source_consign_id   IN	   VARCHAR2
	  , p_po_type			  IN	   t401_purchase_order.c401_type%TYPE
	  , p_user_id			  IN	   NUMBER
	  , p_po_id 			  OUT	   VARCHAR2
	  , p_vendor_id 	 IN OUT	 t301_vendor.c301_vendor_id%TYPE  -- We are passing value for vendor id, so the parameter changed to IN OUT
	)
	AS
		v_wo_id 	   NUMBER;
		v_string	   VARCHAR2 (20);
		v_id_string    VARCHAR2 (20);
		v_id		   NUMBER;
		v_cost_price   NUMBER;
--ADDED BY RAJESH
		v_exclude_pnum TE504_consignment_data.c205_part_number_id%TYPE;
		v_exclude_qty  TE504_consignment_data.c505_item_qty%TYPE;
		v_po_id 	   t401_purchase_order.c401_purchase_ord_id%TYPE;
		v_msg		   VARCHAR2 (100);
		v_exclude_pkey TE504_consignment_data.ce504_item_consignment_id%TYPE;
		v_temp_pnum TE504_consignment_data.c205_part_number_id%TYPE;
		v_company_id	t1900_company.c1900_company_id%TYPE;
		v_source_company_id t1900_company.c1900_company_id%TYPE;

		CURSOR cur_etl_cons_data
		IS
			SELECT	 c205_part_number_id pnum, SUM (c505_item_qty) qty
				FROM TE504_consignment_data
			   WHERE ce504_item_consignment_id NOT IN (v_exclude_pkey) AND c504_consignment_id = p_source_consign_id
			GROUP BY c205_part_number_id;
	BEGIN
	SELECT 	get_compid_frm_cntx() INTO v_company_id  FROM 	DUAL;
		BEGIN
			IF p_source_consign_id IS NULL
			THEN
				raise_application_error ('-20505'
									   , 'Source Consignment ID is not passed. Cannot proceed with Empty Consign ID.'
										);
			END IF;

			BEGIN
				SELECT pnum, qty, etl_pkey
				  INTO v_exclude_pnum, v_exclude_qty, v_exclude_pkey
				  FROM (SELECT c205_part_number_id pnum, c505_item_qty qty, ce504_item_consignment_id etl_pkey
						  FROM TE504_consignment_data
						 WHERE c504_consignment_id = p_source_consign_id AND ROWNUM = 1) temp;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_exclude_pnum := '';
					v_exclude_qty := 0;
					raise_application_error ('-20505'
										   , 'NO RECORD AVAILABLE IN TE504_consignment_data TABLE. CANNOT CREATE PO/DHR.'
											);
			END;
			v_temp_pnum := v_exclude_pnum;

			IF (v_exclude_qty > 0 AND v_exclude_pnum IS NOT NULL)
			THEN

				BEGIN
					SELECT NVL(c405_cost_price * v_exclude_qty,0)
					  INTO v_cost_price
					  FROM t405_vendor_pricing_details
					 WHERE c205_part_number_id = v_exclude_pnum AND c405_active_fl = 'Y' 
					 AND c301_vendor_id = p_vendor_id
					 AND c1900_company_id=v_company_id;
				EXCEPTION WHEN NO_DATA_FOUND
				THEN
					v_cost_price := 0;
				END ;

				SELECT c1900_company_id
				   INTO v_source_company_id
				   FROM t504_consignment
				  WHERE c504_consignment_id = p_source_consign_id;
			--For MNTTASK-5199   For creating the PO (like 'COUNTRYCODE-PO-XXXX'),Since this body file is executed manually only in EDC, hot coding the country code as 'en'.
				gm_place_po (p_vendor_id, v_cost_price, p_user_id, p_po_type, v_source_company_id, p_po_id, v_msg);

				gm_create_work_order (p_po_id, p_vendor_id, v_exclude_pnum, v_exclude_qty, p_user_id);
			END IF;


			FOR etl_cons_data IN cur_etl_cons_data
			LOOP
				v_temp_pnum := etl_cons_data.pnum;
				gm_create_work_order (p_po_id, p_vendor_id, etl_cons_data.pnum, etl_cons_data.qty, p_user_id);
			END LOOP;
		EXCEPTION WHEN OTHERS THEN
			v_err_src_cn_id:= p_source_consign_id;
			v_err_id := v_temp_pnum;
			v_err_msg := v_err_msg||' : CN ID: '||p_source_consign_id ||' Part Num:'||v_temp_pnum||' :: Exception: '||SQLERRM;
			raise_application_error(-20999, SQLERRM);
		END;

	END create_po_wo;

	PROCEDURE gm_load_dhr (
		p_source_consign_id    IN	VARCHAR2
	  , p_po_id 			   IN	t401_purchase_order.c401_purchase_ord_id%TYPE
	  , p_vendid			   IN	t301_vendor.c301_vendor_id%TYPE
	  , p_comments			   IN	t408_dhr.c408_comments%TYPE
	  , p_packslip			   IN	t408_dhr.c408_packing_slip%TYPE
	  , p_po_type			   IN	NUMBER
	  , p_inhouse_trans_type   IN	VARCHAR2
	  , p_setid                IN   TE504_consignment_data.c207_consignment_set_id%TYPE
	  , p_userid			   IN	t408_dhr.c408_created_by%TYPE
      , p_us_time_format       IN t901_code_lookup.c901_code_nm%type
	)
	AS
		p_str		   VARCHAR2 (4000);
		p_recdate	   VARCHAR2 (20);
		p_manfdate	   VARCHAR2 (20);
		p_pdt_id	   VARCHAR2 (20);
		p_message	   VARCHAR2 (4000);
		v_wo_id 	   t402_work_order.c402_work_order_id%TYPE;
		v_qty		   NUMBER;
		p_inputstr	   VARCHAR2 (4000);
		v_pnum		   VARCHAR2 (20);
		v_trans_id	   VARCHAR2 (20);
		v_dhr_wo_cnt   NUMBER;
		v_wo_uom_str   VARCHAR2 (100);
		v_wo_cnt	   NUMBER;
		v_wo_uom_qty   NUMBER;
		v_cnt		   NUMBER;
		v_loaded_cn_cnt NUMBER;
		v_temp_pnum TE504_consignment_data.c205_part_number_id%TYPE;
		v_company_id	t1900_company.c1900_company_id%TYPE;

		v_wo_qty_ordered   t402_work_order.c402_qty_ordered%TYPE;
		v_dhr_cnt	   NUMBER;
		v_dhr_qty          t408_dhr.c408_qty_received%TYPE;
		v_diff_qty     NUMBER;
		v_wo_void_flg      CHAR(1);
		v_wo_void_cmt       VARCHAR2(4000);
		v_wo_rec_qty   NUMBER;
    	v_wo_create  NUMBER;
    	v_cntrl VARCHAR2 (40);
		pop_val TYPES.cursor_type;
		v_grouping_sts   CHAR (1):= get_rule_value ('ETLDHRPARTGRP', 'GROUPINGSTS') ;

	BEGIN
		BEGIN
			SELECT TO_CHAR(CURRENT_DATE,p_us_time_format)
			  INTO p_recdate
			  FROM DUAL;

			-- Do not remove the hardcoded format.
			SELECT TO_CHAR (CURRENT_DATE,p_us_time_format)
			  INTO p_manfdate
			  FROM DUAL;
			  
			  SELECT get_compid_frm_cntx() INTO v_company_id  FROM 	DUAL;
			  
			--calling to fetch the part control no details
			gm_fch_consign_data(p_source_consign_id,v_grouping_sts, pop_val);

			LOOP
			FETCH pop_val INTO v_pnum,v_cntrl,v_wo_rec_qty;
			EXIT
			WHEN pop_val%NOTFOUND;

 v_cnt		:= v_cnt + 1;

        			v_wo_create := 0;

				-- Fetch WO ID Void Flag along with WO ID and quantity and existing Void Flag condition is removed.

				BEGIN

					SELECT c402_work_order_id,c402_qty_ordered,c402_void_fl
					  INTO v_wo_id,v_wo_qty_ordered,v_wo_void_flg
						 FROM t402_work_order
						 WHERE c205_part_number_id = v_pnum  
						 AND c401_purchase_ord_id = p_po_id
						 AND c1900_company_id=v_company_id ;


			        EXCEPTION WHEN NO_DATA_FOUND
			        THEN

					-- If WO Id does not exist, create Work Order and process the parts.
          				v_wo_create := 1;

				END;

			        IF v_wo_create = 1
			        THEN
			             gm_create_work_order (p_po_id, p_vendid, v_pnum, v_wo_rec_qty, p_userid);

			             --Fetch WO ID Void Flag along with WO ID and quantity and existing Void Flag condition is removed.
			             SELECT c402_work_order_id,c402_qty_ordered,c402_void_fl
			              INTO v_wo_id,v_wo_qty_ordered,v_wo_void_flg
			                FROM t402_work_order
			                WHERE c205_part_number_id = v_pnum  AND c401_purchase_ord_id = p_po_id
							AND c1900_company_id=v_company_id;

			        END IF;

				-- Check if the WO ID is Voided and un-void the WO ID
				IF v_wo_void_flg = 'Y'
        			THEN

				  -- if WO ID is voided, un void the WO ID and Log the comments
				  gm_permit_void_po_wo(p_po_id,v_wo_id,p_userid);

  			          -- CALL PROCEDURE gm_cm_sav_partqty TO INSERT PART QUANTITY (rad_val.qty)
				  -- 4000100 - Sales replenishment
				  -- 4302 - minus
				  -- 4000116 - Overage WO Qty
				  -- Passing quantity received of un voided WO to gm_cm_sav_partqty


				  gm_cm_sav_partqty(v_pnum , v_wo_rec_qty * -1 ,  v_wo_id, p_userid, 4000100, 4302 , 4000116 );

				END IF;

				--Added below code to get the control no. if part has one control number, it will return the same, otherwise control no will be NOC# (If grouping rule value is Y)
				IF v_grouping_sts = 'Y' THEN

					BEGIN

					SELECT  c505_control_number cntrl INTO v_cntrl
					  FROM TE504_consignment_data
					    WHERE c504_consignment_id = p_source_consign_id
					    AND c205_part_number_id = v_pnum;

					EXCEPTION
					  WHEN TOO_MANY_ROWS
					  THEN
					  gm_sav_consign_data(p_source_consign_id, v_pnum, p_userid);
					   v_cntrl:= 'NOC#';
					END;

				END IF;
                 -- Create the DHR
				gm_create_dhr (v_wo_id
							 , p_vendid
							 , v_pnum
							 , v_cntrl
							 , v_wo_rec_qty
							 , p_manfdate  -- MANFDATE
							 , p_packslip
							 , p_recdate
							 , p_source_consign_id	 -- Comments field.
							 , p_po_type
							 , p_userid
							 , p_pdt_id
							 , p_message
							  );
		p_inputstr	:= p_inhouse_trans_type || ',' || TO_CHAR (v_wo_rec_qty) || '|';
				-- Create the In House Transaction
                gm_pkg_op_dhr.gm_op_sav_ihtxns (p_pdt_id, p_inputstr, v_pnum, v_cntrl, p_userid, v_qty);
                -- Verify the DHR
				UPDATE t408_dhr
				   SET c408_verified_by = p_userid
					 , c408_verified_ts = CURRENT_DATE
					 , c408_qty_on_shelf = v_wo_rec_qty
					 , c408_status_fl = '4'
					 , c408_last_updated_by = p_userid
					 , c408_last_updated_date = CURRENT_DATE
				 WHERE c408_dhr_id = p_pdt_id
				 AND c1900_company_id=v_company_id;

				-- Verify the Inhouse Transaction
				SELECT c412_inhouse_trans_id
				  INTO v_trans_id
				  FROM t412_inhouse_transactions t412
				 WHERE t412.c412_ref_id = p_pdt_id AND t412.c412_void_fl IS NULL
				 AND c1900_company_id=v_company_id;

				UPDATE t412_inhouse_transactions
				   SET c412_update_inv_fl = DECODE(p_setid,NULL,c412_update_inv_fl,'1')
				    --  If set id is null, verified by  date should be null. - In Main Inventory Report DHR Allocation fetching quantity based on the Verify Date.
					 , c412_verified_by = DECODE(p_setid,NULL,NULL,p_userid)
					 , c412_verified_date = DECODE(p_setid,NULL,NULL,CURRENT_DATE)
					 , c412_verify_fl = DECODE(p_setid,NULL,c412_verify_fl,'1')
					 , c412_status_fl = DECODE(p_setid,NULL,'3','4') -- WHEN PROCESSING LOOSE ITEMS, THE DHFG STATUS IS 3 , otherwise 4.
					 , c412_last_updated_date = CURRENT_DATE
					 , c412_last_updated_by = p_userid
				 WHERE c412_inhouse_trans_id = v_trans_id
				 AND c1900_company_id=v_company_id;

				-- To Update the part Number Table and do the posting when we are processing a SET.
				IF p_setid IS NOT NULL
				THEN
				 gm_pkg_op_inventory_qty.gm_sav_inventory_main (v_trans_id, p_inhouse_trans_type, p_userid);
				END IF;

				-- Count the number of DHR is not processed under the passed WO.

				SELECT COUNT(c408_dhr_id) into v_dhr_cnt
					FROM t408_dhr
					WHERE c402_work_order_id = v_wo_id
					AND c408_void_fl        IS NULL
					AND c408_status_fl      !=4
					AND c1900_company_id=v_company_id;

				-- if there is no DHR exists for processing. call the procedure gm_update_part_neg_qty

				IF v_dhr_cnt = 0
				THEN
				     -- This procedure will close the WO status and Fetch diff qty and update the negative quantity

				    gm_update_part_neg_qty(v_wo_id,v_pnum,v_wo_qty_ordered,p_userid);

				END IF;

			END LOOP;

		EXCEPTION WHEN OTHERS THEN
			v_err_src_cn_id:= p_source_consign_id;
			v_err_id := v_pnum;
			v_err_msg :=  v_err_msg||' : CN ID: '||p_source_consign_id ||':'||v_pnum||' :: Exception: '||SQLERRM;
			raise_application_error(-20999, SQLERRM);
		END;

	END gm_load_dhr;

	/************************************************************************
    * Description : Procedure to fetch consign data
    * Author   : aprasath
    ************************************************************************/
	PROCEDURE gm_fch_consign_data (
        p_source_consign_id IN VARCHAR2,
        p_grouping_sts IN CHAR,
        p_out OUT TYPES.cursor_type)
	AS
	BEGIN
		--If grouping rule value is Y then create query string to grouping the parts without having actual control number
	    IF p_grouping_sts = 'Y' THEN
	        OPEN p_out FOR SELECT c205_part_number_id pnum, 'NOC#' cntrl, SUM (c505_item_qty) qty FROM TE504_consignment_data
	        WHERE C504_CONSIGNMENT_ID = p_source_consign_id GROUP BY C205_PART_NUMBER_ID;
	    ELSE
	    --	If grouping rule value is null then query string will existing query
	        OPEN p_out FOR SELECT c205_part_number_id pnum, c505_control_number cntrl, c505_item_qty qty FROM
	        TE504_consignment_data WHERE c504_consignment_id = p_source_consign_id;
	    END IF;

	END gm_fch_consign_data;

	/************************************************************************
    * Description : Procedure to save consign data
    * Author   : aprasath
    ************************************************************************/
	PROCEDURE gm_sav_consign_data (
    p_source_consign_id IN VARCHAR2,
    p_part_num       IN t205_part_number.c205_part_number_id%TYPE,
    p_userid		 IN	t408_dhr.c408_created_by%TYPE
    )
    AS
    CURSOR cur_consignment
    IS
	SELECT c205_part_number_id pnum, c505_control_number cntrl, c505_item_qty qty FROM
	        TE504_consignment_data WHERE c504_consignment_id = p_source_consign_id AND c205_part_number_id=p_part_num;
	v_cnt		   NUMBER;
	BEGIN
		FOR rec IN cur_consignment
    	LOOP
    	--Validate if already existing on t4080_exception_controlnumber
		     SELECT COUNT (1)
		       INTO v_cnt
		       FROM t4080_exception_controlnumber
		       WHERE c205_part_number_id = rec.pnum
		        AND c4080_control_id = rec.cntrl
		        AND c4080_void_fl IS NULL;
         --	If not insert into t4080_exception_controlnumber
		      IF v_cnt = 0 THEN
		       INSERT INTO t4080_exception_controlnumber
			      (c4080_control_asd_id, c205_part_number_id,c4080_control_id, c4080_created_by, c4080_created_date)
			     VALUES
			      (s4080_control_asd_id.NEXTVAL, rec.pnum, rec.cntrl, p_userid, CURRENT_DATE);
			   END IF;
	    	END LOOP;
	END gm_sav_consign_data;

	PROCEDURE gm_create_work_order (
		p_po_id 	  IN   t401_purchase_order.c401_purchase_ord_id%TYPE
	  , p_vendor_id   IN   t301_vendor.c301_vendor_id%TYPE
	  , p_part_num	  IN   t205_part_number.c205_part_number_id%TYPE
	  , p_quantity	  IN   NUMBER
	  , p_userid	  IN   t408_dhr.c408_created_by%TYPE
	)
	AS
		v_wo_id 	   t402_work_order.c402_work_order_id%TYPE;
		v_id_string    VARCHAR2 (100);
		v_pricing_id   t405_vendor_pricing_details.c405_pricing_id%TYPE;
		v_cost_price   t405_vendor_pricing_details.c405_cost_price%TYPE;
		v_company_id	t1900_company.c1900_company_id%TYPE;


	BEGIN
		SELECT s402_work.NEXTVAL
		  INTO v_wo_id
		  FROM DUAL;

		  SELECT 	get_compid_frm_cntx() INTO v_company_id  FROM 	DUAL;
		--
		IF LENGTH (v_wo_id) = 1
		THEN
			v_id_string := '0' || v_wo_id;
		ELSE
			v_id_string := v_wo_id;
		END IF;

		--
		SELECT 'GM-WO-' || v_id_string
		  INTO v_wo_id
		  FROM DUAL;

		  -- When the Vendor Price is not available for the part, load it with 0 Price in WO table.
		  BEGIN
			SELECT c405_pricing_id, c405_cost_price
			  INTO v_pricing_id, v_cost_price
			  FROM t405_vendor_pricing_details
			 WHERE c205_part_number_id = p_part_num AND c301_vendor_id = p_vendor_id AND c405_active_fl = 'Y'
			 AND c1900_company_id=v_company_id;
		  EXCEPTION WHEN NO_DATA_FOUND THEN
		  	v_cost_price :=0;
		  	v_pricing_id := '';
		  END;

		-- Existing Void Flag condition is removed.

		UPDATE t402_work_order
		   SET c402_qty_ordered = c402_qty_ordered + p_quantity
		 WHERE c401_purchase_ord_id = p_po_id AND c205_part_number_id = p_part_num AND c301_vendor_id = p_vendor_id;

		IF (SQL%ROWCOUNT = 0)
		THEN
			INSERT INTO t402_work_order
						(c402_work_order_id, c401_purchase_ord_id, c205_part_number_id, c301_vendor_id
					   , c402_created_date, c402_status_fl, c402_cost_price, c402_created_by, c402_last_updated_date
					   , c402_last_updated_by, c402_qty_ordered, c402_rev_num, c402_critical_fl
					   , c402_sub_component_fl, c402_sterilization_fl, c408_dhr_id, c402_far_fl, c402_void_fl
					   , c405_pricing_id, c402_completed_date, c402_qty_history_fl, c402_price_history_fl
					   , c402_split_percentage, c402_posting_fl, c402_rc_fl, c402_split_cost, c402_rm_inv_fl,c1900_company_id
						)
				 VALUES (v_wo_id, p_po_id, p_part_num, p_vendor_id
					   , CURRENT_DATE, '1', v_cost_price, p_userid, NULL
					   , NULL, p_quantity, 'A', NULL
					   , NULL, NULL, NULL, NULL, NULL
					   , v_pricing_id, NULL, NULL, NULL
					   , NULL, NULL, NULL, NULL, NULL,v_company_id
						);
		END IF;
	END gm_create_work_order;

	PROCEDURE gm_upd_control_number (
		p_source_consign_id   IN   t505_item_consignment.c504_consignment_id%TYPE
	  , p_new_consign_id	  IN   t505_item_consignment.c504_consignment_id%TYPE
	  , p_set_id			  IN   TE504_consignment_data.c207_consignment_set_id%TYPE
	)
	AS
		v_temp_pnum TE504_consignment_data.c205_part_number_id%TYPE;
		v_temp_tag TE504_consignment_data.c5010_tag_id%TYPE;
		v_company_id t1900_company.c1900_company_id%TYPE;
        v_plant_id t5040_plant_master.c5040_plant_id%TYPE;

		CURSOR cur_src_cns_item
		IS
			SELECT c205_part_number_id pnum, c505_control_number cntrl, c505_item_qty qty, c5010_tag_id tag_id
			  FROM TE504_consignment_data
			 WHERE c504_consignment_id = p_source_consign_id;
	BEGIN

		BEGIN
			 SELECT c1900_company_id,c5040_plant_id
					INTO v_company_id,v_plant_id
					FROM TE504_consignment_data WHERE ROWNUM=1;

			DELETE FROM t505_item_consignment
				  WHERE c504_consignment_id = p_new_consign_id;

			FOR src_cns_item IN cur_src_cns_item
			LOOP
				v_temp_pnum := src_cns_item.pnum;
				INSERT INTO t505_item_consignment
							(c505_item_consignment_id, c504_consignment_id, c205_part_number_id, c505_control_number
						   , c505_item_qty, c505_item_price
							)
					 VALUES (s504_consign_item.NEXTVAL, p_new_consign_id, src_cns_item.pnum, src_cns_item.cntrl
						   , src_cns_item.qty, get_part_price (src_cns_item.pnum, 'C')
							);

				IF (src_cns_item.tag_id IS NOT NULL)
				THEN
					v_temp_tag := src_cns_item.tag_id;

					UPDATE t5010_tag
					SET c5010_control_number=src_cns_item.cntrl
					,c5010_last_updated_trans_id=p_new_consign_id
					,c5010_location_id=52098
					,C5010_LAST_UPDATED_BY='30301'
					,C5010_LAST_UPDATED_DATE=CURRENT_DATE
					,c205_part_number_id=src_cns_item.pnum
					,c901_trans_type=51000
					,c901_location_type = 40033  --INHOUSE
					,c207_set_id=p_set_id
					,c901_status=51012
					,c1900_company_id=v_company_id
					,c5040_plant_id=v_plant_id
					WHERE c5010_tag_id=src_cns_item.tag_id
					AND C5010_VOID_FL IS NULL;

					IF SQL%ROWCOUNT                = 0 THEN

					INSERT INTO t5010_tag
								(c5010_tag_id, c5010_control_number, c5010_last_updated_trans_id, c5010_location_id
							   , c5010_history_fl, c5010_created_by, c5010_created_date, c205_part_number_id
							   , c901_trans_type, c207_set_id, c901_status,c1900_company_id,c5040_plant_id
								)
						 VALUES (src_cns_item.tag_id, src_cns_item.cntrl, p_new_consign_id, 52098
							   , 'Y', '30301', CURRENT_DATE, src_cns_item.pnum
							   , 51000, p_set_id, 51012,v_company_id,v_plant_id
								);
					END IF;
				END IF;
			END LOOP;
		EXCEPTION WHEN OTHERS THEN
			v_err_src_cn_id:= p_source_consign_id;
			v_err_msg := v_err_msg||' : Tag ID : '||v_temp_tag|| ': Part Num: '||v_temp_pnum ||' :: Exception: '||SQLERRM;
			raise_application_error(-20999, SQLERRM);
		END;
	END gm_upd_control_number;

	/* This Procedure will log all the exception that is caused during the execution of the data load package.
	 * This data will be used to debug/troubleshoot the dataload failure.
	 */
	PROCEDURE gm_sav_exception_msg
	(p_txn_id 	IN TE504b_Consignment_LD_Error.ce504b_txn_id%TYPE
	,p_pnum 	IN TE504b_Consignment_LD_Error.c205_part_number_id%TYPE
	,p_err_msg 	IN TE504b_Consignment_LD_Error.ce504b_txn_error%TYPE
	,p_user_id 	IN TE504b_Consignment_LD_Error.ce504b_created_by%TYPE
        ,p_country_id IN TE504b_Consignment_LD_Error.c1900_company_id%TYPE DEFAULT NULL
        ,p_job_run_id IN TE504b_Consignment_LD_Error.ce504b_job_run_id%TYPE DEFAULT NULL
        ,p_email_flag IN TE504b_Consignment_LD_Error.ce504b_email_flag%TYPE DEFAULT NULL
	)
	AS

	BEGIN
		IF p_txn_id IS NOT NULL OR p_pnum IS NOT NULL
	 	THEN
		 	INSERT INTO TE504b_Consignment_LD_Error(ce504b_consign_ld_err_id,ce504b_txn_id ,c205_part_number_id,ce504b_txn_error,ce504b_created_date,ce504b_created_by,c1900_company_id,ce504b_job_run_id,ce504b_email_flag)
		 	VALUES (se504b_consign_ld_err_id.NEXTVAL,p_txn_id,p_pnum,p_err_msg,CURRENT_DATE,p_user_id,p_country_id,p_job_run_id,p_email_flag);
		 END IF;

	END gm_sav_exception_msg;


	PROCEDURE gm_sync_vendor_price
    AS

    v_vendor_id t906_rules.C906_RULE_VALUE%TYPE;
    v_part_num t405_vendor_pricing_details.C205_PART_NUMBER_ID%TYPE;
    v_to_mail   VARCHAR2 (2000) ;
    v_subject   VARCHAR2 (2000) ;
    v_mail_body VARCHAR2 (2000) ;
    v_company_id	t1900_company.c1900_company_id%TYPE;
	 v_plant_id t5040_plant_master.c5040_plant_id%TYPE;

    CURSOR cur_vendor_pricing
    IS
         SELECT C405_PRICING_ID priceid, C301_VENDOR_ID vendid, C205_PART_NUMBER_ID pnum
          , C405_COST_PRICE price, C405_ACTIVE_FL actfl, C405_PRICE_VALID_DATE vdate
          , C405_CREATED_DATE cdate, C405_CREATED_BY createdby, C405_LAST_UPDATED_DATE updateddt
          , C405_LAST_UPDATED_BY updatedby, C405_QTY_QUOTED qtyquoted, C405_DELIVERY_FRAME dframe
          , C405_VENDOR_QUOTE_ID quoteid, C901_UOM_ID uomid, C405_UOM_QTY uomqty
          , C405_LOCK_FL lockfl
           FROM t405_vendor_pricing_details
           WHERE (TRUNC (c405_created_date)    = TRUNC (CURRENT_DATE)
            OR TRUNC (c405_last_updated_date) = TRUNC (CURRENT_DATE))
            AND c301_vendor_id                = v_vendor_id
            AND c1900_company_id=v_company_id			;
    CURSOR cur_vendor
    IS
     	-- For Getting the vendor id
           SELECT c301_vendor_id vendor_id
			  INTO v_vendor_id
			  FROM t301_vendor
			 WHERE c301_vendor_name = 'Globus Audubon'
             AND c1900_company_id=v_company_id;

    BEGIN
        SELECT 	get_compid_frm_cntx() INTO v_company_id  FROM 	DUAL;
		SELECT c301_vendor_id
			  INTO v_vendor_id
			  FROM t301_vendor
			 WHERE c301_vendor_name = 'Globus Audubon'
             AND c1900_company_id=v_company_id;

    FOR vendor IN cur_vendor
    LOOP
        v_vendor_id        := vendor.vendor_id;
        FOR vendor_pricing IN cur_vendor_pricing
        LOOP
        v_part_num := vendor_pricing.pnum;
            gm_pkg_pc_vendorprice_txn.gm_sav_vendorprice (vendor_pricing.priceid, v_vendor_id, vendor_pricing.pnum,
            vendor_pricing.price, vendor_pricing.actfl, vendor_pricing.vdate, vendor_pricing.cdate,
            vendor_pricing.createdby, vendor_pricing.updateddt, vendor_pricing.updatedby, vendor_pricing.qtyquoted,
            vendor_pricing.dframe, vendor_pricing.quoteid, vendor_pricing.uomid, vendor_pricing.uomqty,
            vendor_pricing.lockfl) ;
        END LOOP;
    END LOOP;
    COMMIT;
        v_to_mail   := get_rule_value_by_company ('VNDPRC_SYNC_NOTIF_TO', 'VNDPRC_SYNC',v_company_id);
        v_subject   := get_rule_value ('VNDPRC_SYNC_NOTIF_SUB', 'VNDPRC_SYNC') ;
        v_mail_body := get_rule_value ('VNDPRC_SYNC_NOTIF_BDY', 'VNDPRC_SYNC') ;
        gm_com_send_email_prc (v_to_mail, v_subject, v_mail_body) ;

    EXCEPTION WHEN OTHERS THEN
    ROLLBACK;
        v_subject   := get_rule_value ('VNDPRC_SYNC_ERR_SUB', 'VNDPRC_SYNC') ;
        v_mail_body := get_rule_value ('VNDPRC_SYNC_ERR_BDY', 'VNDPRC_SYNC') ;
        gm_com_send_email_prc (v_to_mail, v_subject,v_mail_body) ;
        v_err_msg       := v_err_msg||' Vendor ID: '|| v_vendor_id || 'Part Num:' || v_part_num || ' :: Exception: ' || SQLERRM;
        raise_application_error ( - 20999, SQLERRM) ;

    END gm_sync_vendor_price;

  /* This procedure is to update the status of void flag to null for the voided WO and PO. Also it logs the entry in the log table.
   * p_po_id is passed to Log procedure for both WO and PO is to view the details in the PO LOG report.
   */

 PROCEDURE gm_permit_void_po_wo(
    p_po_id IN t401_purchase_order.c401_purchase_ord_id%TYPE ,
    p_wo_id IN t402_work_order.c402_work_order_id%TYPE,
    p_user_id IN NUMBER
  )
   AS

		v_void_cmt     VARCHAR2(4000);
		v_message	     VARCHAR2 (4000);
   		v_log_ref_id   VARCHAR2(20);
		v_company_id	t1900_company.c1900_company_id%TYPE;

    BEGIN
	SELECT 	get_compid_frm_cntx() INTO v_company_id  FROM 	DUAL;
      IF p_wo_id is NULL
      THEN
        -- Updating the PO void flag as null to permit PO
        UPDATE t401_purchase_order
          SET c401_void_fl           = NULL
            WHERE c401_purchase_ord_id = p_po_id
            AND c401_void_fl           = 'Y'
			AND c1900_company_id=v_company_id;

        v_void_cmt := 'Permitting voided Purchase Order ' || p_po_id ;
        -- Assigning PO ID to log ID
        v_log_ref_id := p_po_id;

      ELSE
        -- Updating the WO void flag as null to permit WO
        UPDATE t402_work_order
            SET c402_void_fl = null
              WHERE c402_work_order_id = p_wo_id
              AND c401_purchase_ord_id = p_po_id
              AND C402_void_fl = 'Y'
			  AND c1900_company_id=v_company_id;

        v_void_cmt := 'Permitting voided Work Order ' || p_wo_id || ' for PO ID ' || p_po_id;
        -- Assigning WO ID to log ID
        v_log_ref_id := p_wo_id;


      END IF;

  	-- 1203 PO Call Log	-  v_log_id is passed to Log procedure for both WO and PO.
        GM_UPDATE_LOG (	v_log_ref_id, v_void_cmt , '1203' , p_user_id , v_message );

     EXCEPTION WHEN OTHERS THEN
        raise_application_error ( - 20999, SQLERRM) ;

    END gm_permit_void_po_wo;

  /* To update the WO status to close
   * and fetch the differential quantity between WO and DHR. If DHR qty is more pass the negative quantity to procedure gm_cm_sav_partqty
   */

  PROCEDURE gm_update_part_neg_qty(
       p_wo_id 	    IN	 t402_work_order.c402_work_order_id%TYPE
    ,  p_part_num	  IN   t205_part_number.c205_part_number_id%TYPE
    ,  p_wo_qty_ordered IN NUMBER
    ,  p_user_id	  IN  NUMBER
  )
   AS
        v_dhr_qty      NUMBER;
		v_diff_qty     NUMBER;
		v_company_id	t1900_company.c1900_company_id%TYPE;

    BEGIN

   	-- Find the number of Quantity received through DHR for the WO
	SELECT 	get_compid_frm_cntx() INTO v_company_id  FROM 	DUAL;

	SELECT SUM(c408_qty_received) into v_dhr_qty
		FROM t408_dhr
		WHERE c402_work_order_id = p_wo_id
		AND c408_void_fl        IS NULL
		AND c408_status_fl      =4
		AND c1900_company_id=v_company_id;

		-- Check If DHR quantity is greater than WO quantity

		IF v_dhr_qty >=p_wo_qty_ordered
		THEN

			-- Update WO ID Status to Close Work Order
			UPDATE t402_work_order t402
			   SET c402_status_fl = 3
				 , c402_completed_date = TRUNC (CURRENT_DATE)
				 , c402_last_updated_by = p_user_id
				 , c402_last_updated_date = CURRENT_DATE
			 WHERE c402_work_order_id = p_wo_id
			 AND c1900_company_id=v_company_id;

			 v_diff_qty := v_dhr_qty-p_wo_qty_ordered;

			 IF v_diff_qty > 0
			 THEN
		               -- CALL PROCEDURE gm_cm_sav_partqty TO INSERT PART QUANTITY (v_dhr_qty-v_wo_qty_ordered)
		               -- 4000100 - Sales replenishment
		               -- 4302 - minus
		               -- 4000116 - Overage WO Qty
        		       -- Passing negative Quantity to gm_cm_sav_partqty
		               gm_cm_sav_partqty(p_part_num , v_diff_qty * -1 ,  p_wo_id, p_user_id, 4000100, 4302 , 4000116 );
		     	END IF;
           	END IF;

     EXCEPTION WHEN OTHERS THEN
        raise_application_error ( - 20999, SQLERRM) ;

   END gm_update_part_neg_qty;

END IT_PKG_OP_ICT_DATALOAD_TEMP;
/