
CREATE OR REPLACE PACKAGE BODY it_pkg_consignment
IS

/*
* Procedure to update the Consignment Item Price by taking the Equity price.
* Author : Rajeshwaran
*/

PROCEDURE GM_UPD_CN_ITEM_PRICE (
	p_cnid		  IN		 T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE
	,p_help_desk_id	 IN t914_helpdesk_log.c914_helpdesk_id%TYPE
  )
 AS  
 
    v_ship_date   T504_CONSIGNMENT.C504_SHIP_DATE%TYPE;
    v_part_price  T2052_PART_PRICE_MAPPING.C2052_EQUITY_PRICE%TYPE;
    v_company_id  T504_CONSIGNMENT.C1900_company_id%TYPE;
    v_error_part  CLOB;
	v_user_id	   NUMBER;
   
    CURSOR cur_consignment
    IS
    	SELECT C205_part_number_id pnum 
    	FROM T505_ITEM_CONSIGNMENT
    	WHERE C504_CONSIGNMENT_ID = p_cnid
    	AND C505_VOID_FL IS NULL;
    
    BEGIN  
	    
	    IF p_cnid is NULL THEN
	   	 	GM_RAISE_APPLICATION_ERROR('-20999','401','Consignment Id');
	    END IF;

		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;
	    
    	BEGIN
	    	Select C504_SHIP_DATE
	    	into v_ship_date
	    	from T504_CONSIGNMENT
	    	where C504_CONSIGNMENT_ID = p_cnid;
	    	EXCEPTION
		WHEN NO_DATA_FOUND 
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','405',p_cnid);
    	END; 
		
    	IF v_ship_date IS NOT NULL
    	THEN
    		GM_RAISE_APPLICATION_ERROR('-20999','406',p_cnid||'#$#'||v_ship_date);
    	END IF;
       
    	SELECT C1900_COMPANY_ID
    	INTO v_company_id
    	FROM T504_CONSIGNMENT
    	WHERE C504_CONSIGNMENT_ID = p_cnid;
    	
    	
    	
    	FOR consignment IN cur_consignment
    	LOOP
    		BEGIN
	    		SELECT C2052_EQUITY_PRICE
		    	INTO v_part_price
		    	FROM T2052_PART_PRICE_MAPPING
		    	WHERE C2052_VOID_FL IS NULL
		    	AND C1900_COMPANY_ID = v_company_id
		    	AND C205_PART_NUMBER_ID = consignment.pnum;
	    	EXCEPTION WHEN	OTHERS
	    	THEN
	    		v_error_part := v_error_part||consignment.pnum||',';
	    	END;
		--    DBMS_OUTPUT.PUT_LINE(':v_company_id:'||v_company_id||':v_part_price:'||v_part_price); 	
		    	
	    	IF v_error_part IS NULL AND v_part_price IS NOT NULL 
	    	THEN
	    		UPDATE T505_ITEM_CONSIGNMENT 
	    		SET C505_ITEM_PRICE = v_part_price
	    		WHERE C205_PART_NUMBER_ID = consignment.pnum
	    		AND   C504_CONSIGNMENT_ID = p_cnid;
	    		
				it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Consignment ID: ' || p_cnid);
				COMMIT;
	    	END IF;
	    	
    	END LOOP;
    	
    	IF v_error_part IS NOT NULL
	    	THEN
				ROLLBACK;
	    		GM_RAISE_APPLICATION_ERROR('-20999','407',v_error_part);
				
	    	END IF;

    END GM_UPD_CN_ITEM_PRICE;
    
	 /******************************************************************
	* Description : Procedure is used to update Allograft status to sold.
  * Ref : TSK-11435
	****************************************************************/
	PROCEDURE gm_upd_CN_Status (
	    p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE,
    	p_cn_id         T504A_CONSIGNMENT_LOANER.C504_CONSIGNMENT_ID%TYPE,
      p_cn_status     T504A_CONSIGNMENT_LOANER.C504A_STATUS_FL%TYPE,
    	p_user_id       T504A_CONSIGNMENT_LOANER.C504A_LAST_UPDATED_BY%TYPE   	   
	)
	AS
	  v_count 	   NUMBER;
	BEGIN
	  -- Mandatory help desk to log executed user information
     it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
      
      SELECT Count(1) INTO v_count FROM T504A_CONSIGNMENT_LOANER WHERE C504_CONSIGNMENT_ID = p_cn_id;
        
        IF(v_count = 1) THEN
          UPDATE T504A_CONSIGNMENT_LOANER 
            SET C504A_STATUS_FL = p_cn_status, 
            C504A_LAST_UPDATED_DATE = CURRENT_DATE, 
            C504A_LAST_UPDATED_BY = p_user_id 
          WHERE C504_CONSIGNMENT_ID = p_cn_id
            AND C504A_VOID_FL IS NULL;
    			 -- help desk log	   
			    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Consignment Id ' || p_cn_id||' status updated to'|| p_cn_status); 
	     ELSE
	        -- help desk log
	        it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Consignment Id  ' || p_cn_id||' not found');
	     END IF; 
	EXCEPTION
	  WHEN OTHERS
	  THEN
		DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
	 	ROLLBACK;
	END gm_upd_CN_Status;


 /******************************************************************
* Description : Procedure to update the Lot number for Consignment.
* Author : ppandiyan
****************************************************************/	

PROCEDURE GM_UPD_CONS_LOT_NUMBER (
	 p_cnid		    IN	T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE
	,p_input_str    IN  CLOB
	,p_help_desk_id	IN  t914_helpdesk_log.c914_helpdesk_id%TYPE

  )
 AS  
 
    v_strlen     		 NUMBER          := NVL (LENGTH (p_input_str), 0) ;
    v_string     		 VARCHAR2 (4000) := p_input_str;
    v_substring  		 VARCHAR2 (4000) ;
    v_item_consign_id    T505_ITEM_CONSIGNMENT.C505_ITEM_CONSIGNMENT_ID%TYPE;
	v_partnum    		 T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
    v_old_cnum       	 T505_ITEM_CONSIGNMENT.C505_CONTROL_NUMBER%TYPE;
    v_new_cnum       	 T505_ITEM_CONSIGNMENT.C505_CONTROL_NUMBER%TYPE;
    v_qty		 		 T505_ITEM_CONSIGNMENT.C505_ITEM_QTY%TYPE;
    v_user_id	  		 NUMBER;
	
    BEGIN  
	    
	    IF p_cnid is NULL THEN
	   	 	GM_RAISE_APPLICATION_ERROR('-20999','401','Consignment Id');
	    END IF;

		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;
	    
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring       := SUBSTR (v_string, 1, INSTR (v_string, '|')       - 1) ;
            v_string          := SUBSTR (v_string, INSTR (v_string, '|')          + 1) ;
            v_item_consign_id := NULL;
			v_partnum	   	  := NULL;
			v_qty		      := NULL;
		    v_old_cnum		  := NULL; 
		    v_new_cnum		  := NULL;
		    
    	    v_partnum         := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
    	    v_substring       := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
    	    v_qty             := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
    	    v_substring       := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
			v_old_cnum        := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
    	    v_substring       := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
			v_new_cnum        := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
    	    v_substring       := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
			v_item_consign_id := v_substring ;

			IF v_partnum IS NOT NULL AND v_new_cnum IS NOT NULL 
	    	THEN
	    		UPDATE T505_ITEM_CONSIGNMENT 
					SET C505_CONTROL_NUMBER=v_new_cnum
					WHERE C504_CONSIGNMENT_ID=p_cnid
					AND C505_ITEM_CONSIGNMENT_ID=NVL(v_item_consign_id,C505_ITEM_CONSIGNMENT_ID)
					AND C205_PART_NUMBER_ID=v_partnum
					AND C505_ITEM_QTY=v_qty
					AND C505_CONTROL_NUMBER=NVL(v_old_cnum,C505_CONTROL_NUMBER)
					AND C505_VOID_FL IS NULL;
	    				
	    	END IF;	    	
    	END LOOP;
		
		UPDATE T504_CONSIGNMENT 
		SET C504_LAST_UPDATED_BY=v_user_id,
		C504_LAST_UPDATED_DATE=CURRENT_DATE 
		WHERE C504_CONSIGNMENT_ID=p_cnid
		AND C504_VOID_FL IS NULL;
    	
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Consignment ID: ' || p_cnid);
		COMMIT;
		EXCEPTION
			  WHEN OTHERS
			  THEN
				DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
				ROLLBACK;

    END GM_UPD_CONS_LOT_NUMBER;

END it_pkg_consignment;
/