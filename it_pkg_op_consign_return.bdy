/* Formatted on 2011/02/15 15:05 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_op_consign_return.bdy"


CREATE OR REPLACE PACKAGE BODY it_pkg_op_consign_return
IS
--
 /*********************************************************
	* Description : This procedure is used to update
	* control number for the item consignment and item return.
	*********************************************************/
	PROCEDURE gm_sav_control_num (
		p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_cong_id		 t505_item_consignment.c504_consignment_id%TYPE
	  , p_rma_id		 t507_returns_item.c506_rma_id%TYPE
	  , p_control_no	 t505_item_consignment.c505_control_number%TYPE
	)
	AS
		v_rowcnt	   NUMBER;
		v_user_id	   NUMBER;
		v_consignment_id t505_item_consignment.c504_consignment_id%TYPE;
		v_item_consignment_id t505_item_consignment.c504_consignment_id%TYPE;
		v_returns_item_id t507_returns_item.c507_returns_item_id%TYPE;
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		BEGIN
			IF (p_cong_id IS NOT NULL)
			THEN
				SELECT c505_item_consignment_id
				  INTO v_item_consignment_id
				  FROM t505_item_consignment
				 WHERE c504_consignment_id = p_cong_id;

				UPDATE t505_item_consignment
				   SET c505_control_number = p_control_no
				 WHERE c505_item_consignment_id = v_item_consignment_id;
			END IF;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error ('-20085', 'Consignment ID ' || p_cong_id || ' does not exist.');
		END;

		BEGIN
			IF (p_rma_id IS NOT NULL)
			THEN
				SELECT c507_returns_item_id
				  INTO v_returns_item_id
				  FROM t507_returns_item
				 WHERE c506_rma_id = p_rma_id;

				UPDATE t507_returns_item
				   SET c507_control_number = p_control_no
				 WHERE c507_returns_item_id = v_returns_item_id;
			END IF;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error ('-20085', 'Return	ID ' || p_rma_id || ' does not exist.');
		END;

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Consignment ID: ' || p_cong_id);
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Return ID: ' || p_rma_id);
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'New Control Number: ' || p_control_no);
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_control_num;

	/******************************************************************
	   * Description : This procedure is used to  create a return
	   * for the consignment txn or void the consignment
	   ****************************************************************/
	PROCEDURE gm_sav_return_consignment (
		p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_cong_id		 t504_consignment.c504_consignment_id%TYPE
	  , p_comments		 t506_returns.c506_comments%TYPE
	  , p_type			 VARCHAR2
	)
	AS
		v_user_id	   NUMBER;
		v_consignment_id t505_item_consignment.c504_consignment_id%TYPE;
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		BEGIN
			IF (p_cong_id IS NOT NULL)
			THEN
				SELECT t504.c504_consignment_id
				  INTO v_consignment_id
				  FROM t504_consignment t504
				 WHERE t504.c504_consignment_id = p_cong_id AND t504.c504_void_fl IS NULL;
			END IF;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error ('-20085'
									   , 'Consignment ID ' || p_cong_id || ' does not exist or its already voided'
										);
		END;

		IF p_type = 'Void Consignment'
		THEN
			gm_load_reverse_cons (p_cong_id, p_help_desk_id);
		END IF;

		IF p_type = 'Create Return'
		THEN
			gm_cs_sav_ret_writedown (p_cong_id, p_comments, p_help_desk_id);
		END IF;

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Consignment ID: ' || p_cong_id);
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_return_consignment;

	 /******************************************************************
	* Description : This procedure is used to  create a return
	* for  the consignment txn
	****************************************************************/
	PROCEDURE gm_cs_sav_ret_writedown (
		p_consignment_id   t504_consignment.c504_consignment_id%TYPE
	  , p_comments		   t506_returns.c506_comments%TYPE
	  , p_help_desk_id	   t914_helpdesk_log.c914_helpdesk_id%TYPE
	)
	AS
		v_string	   VARCHAR2 (20);
		v_comments	   t504_consignment.c504_comments%TYPE;
		v_distributor_id t504_consignment.c701_distributor_id%TYPE;
		v_set_id	   t504_consignment.c207_set_id%TYPE;
		v_consignment_id t504_consignment.c504_consignment_id%TYPE;
		v_type		   t506_returns.c506_type%TYPE;

		CURSOR v_get_item_consigment_cur
		IS
			SELECT t505.c205_part_number_id part_number_id, t505.c505_item_qty item_qty
				 , t505.c505_item_price item_price
			  FROM t505_item_consignment t505
			 WHERE t505.c504_consignment_id = p_consignment_id AND TRIM (t505.c504_consignment_id) IS NOT NULL;
	BEGIN
		SELECT 'GM-RA-' || s506_return.NEXTVAL
		  INTO v_string
		  FROM DUAL;

		DBMS_OUTPUT.put_line ('RMA_id....' || v_string);
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Return writedown, Create New Return: ' || v_string);

		SELECT c701_distributor_id, c207_set_id
		  INTO v_distributor_id, v_set_id
		  FROM t504_consignment
		 WHERE c504_consignment_id = p_consignment_id;

		SELECT DECODE ((TRIM (v_set_id)), NULL, 3302, 3301)
		  INTO v_type
		  FROM DUAL;

		INSERT INTO t506_returns
					(c506_rma_id, c506_comments, c506_created_date, c506_type, c701_distributor_id, c506_reason
				   , c506_status_fl, c506_created_by, c506_expected_date, c207_set_id, c506_credit_date
				   , c506_return_date, c501_reprocess_date
					)
			 VALUES (v_string, p_comments || '- ' || v_string, SYSDATE, v_type, v_distributor_id, 3313
				   , '2', 30301, TRUNC (SYSDATE), v_set_id, TRUNC (SYSDATE)
				   , TRUNC (SYSDATE), TRUNC (SYSDATE)
					);

		FOR recs IN v_get_item_consigment_cur
		LOOP
			INSERT INTO t507_returns_item
						(c507_returns_item_id, c507_item_qty, c507_item_price, c205_part_number_id
					   , c507_control_number, c506_rma_id, c507_status_fl
						)
				 VALUES (s507_return_item.NEXTVAL, recs.item_qty, recs.item_price, recs.part_number_id
					   , 'NOC#', v_string, 'C'
						);

			gm_save_ledger_posting (4841   -- Suchi
								  , SYSDATE
								  , recs.part_number_id   -- part#
								  , v_distributor_id   -- DistributorID
								  , v_string   -- RAID
								  , recs.item_qty	--QTY
								  , get_ac_cogs_value (recs.part_number_id, 4904)	--
								  , 30301	-- 30003
								   );
		END LOOP;
	END gm_cs_sav_ret_writedown;

/******************************************************************************
 * Description : This procedure is written as temp solution to reverse consignment
 *				 Created as part of Audit -- Physical Adjustment
 *				-------------------------------------------------
 *				Below procedure will reverse and void the consignment
 *		p_consignment_id		- consignment
 *
 *
  Sample SQL
  ===========
 select * from t413_inhouse_trans_items where c412_inhouse_trans_id =
'GM-LN-125509' and c205_part_number_id = '150.226';

exec tmp_load_gm_reconciliation(140468,'150.226',1,'S','R');

 select * from t413_inhouse_trans_items where c412_inhouse_trans_id =
'GM-LN-125509' and c205_part_number_id = '150.226';


 *******************************************************************************/
	PROCEDURE gm_load_reverse_cons (
		p_consignment_id   t504_consignment.c504_consignment_id%TYPE
	  , p_help_desk_id	   t914_helpdesk_log.c914_helpdesk_id%TYPE
	)
	AS
		v_string	   VARCHAR2 (20);
		v_distributor_id t504_consignment.c701_distributor_id%TYPE;
		v_consignment_id t504_consignment.c504_consignment_id%TYPE;

		CURSOR v_get_item_consigment_cur
		IS
			SELECT t505.c205_part_number_id part_number_id, t505.c505_item_qty item_qty
				 , t505.c505_item_price item_price
			  FROM t505_item_consignment t505
			 WHERE t505.c504_consignment_id = p_consignment_id AND TRIM (t505.c504_consignment_id) IS NOT NULL;
	BEGIN
		SELECT c701_distributor_id
		  INTO v_distributor_id
		  FROM t504_consignment
		 WHERE c504_consignment_id = p_consignment_id;

		DBMS_OUTPUT.put_line ('Distributor ID' || v_distributor_id);
		DBMS_OUTPUT.put_line ('Distributor NAME' || get_distributor_name (v_distributor_id));
		DBMS_OUTPUT.put_line ('Consignment ID' || p_consignment_id);

		FOR recs IN v_get_item_consigment_cur
		LOOP
			gm_save_ledger_posting (4841   -- Suchi
								  , SYSDATE
								  , recs.part_number_id   -- part#
								  , v_distributor_id   -- DistributorID
								  , p_consignment_id   --consignment_id
								  , recs.item_qty	--QTY
								  , get_ac_cogs_value (recs.part_number_id, 4904)	--
								  , 30301	-- 30003
								   );
		END LOOP;

		DBMS_OUTPUT.put_line ('Posting Completed.....');

		UPDATE t504_consignment
		   SET c504_void_fl = 'Y'
			 , c504_last_updated_by = 30301
			 , c504_last_updated_date = SYSDATE
		 WHERE c504_consignment_id = p_consignment_id;

		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Void consignment: ' || p_consignment_id);
		DBMS_OUTPUT.put_line ('Voding CN Completed.....');
	END gm_load_reverse_cons;
END it_pkg_op_consign_return;
/
