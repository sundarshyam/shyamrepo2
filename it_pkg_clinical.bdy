/* Formatted on 2011/01/25 11:28 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_clinical.bdy"

CREATE OR REPLACE PACKAGE BODY it_pkg_clinical
IS
--

	/*********************************************************
	* Description : This procedure is used to updated
	* rejected qty when its status is Inspection
	*********************************************************/
	PROCEDURE gm_upt_patient (
		p_help_desk_id		 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_patient_ide_id	 t621_patient_master.c621_patient_ide_no%TYPE
	  , p_study_period_ds	 t613_study_period.c613_study_period_ds%TYPE
	  , p_patient_event_no	 t622_patient_list.c622_patient_event_no%TYPE
	)
	AS
		v_user_id	   NUMBER;
		v_study_id	   t621_patient_master.c611_study_id%TYPE;
		v_patient_id   t621_patient_master.c621_patient_id%TYPE;
	--
	BEGIN
		BEGIN
			SELECT t621.c611_study_id study_id, t621.c621_patient_id patient_id
			  INTO v_study_id, v_patient_id
			  FROM t621_patient_master t621
			 WHERE t621.c621_patient_ide_no = p_patient_ide_id;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error ('-20085', 'Patient IDE ID does not exist.');
		END;

		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		UPDATE t622_patient_list t622
		   SET t622.c621_patient_id = 412	--hard code here for the patient who is deleted
			 , t622.c622_last_updated_by = v_user_id
			 , t622.c622_last_updated_date = SYSDATE
		 WHERE t622.c621_patient_id = v_patient_id
		   AND NVL (t622.c622_patient_event_no, 9999) = NVL (p_patient_event_no, NVL (t622.c622_patient_event_no, 9999))
		   AND (t622.c601_form_id, t622.c613_study_period_id) IN (
				   SELECT t612.c601_form_id, t622.c613_study_period_id
					 FROM t612_study_form_list t612, t613_study_period t613
					WHERE t612.c611_study_id = v_study_id
					  AND t613.c613_study_period_ds = p_study_period_ds
					  AND t612.c612_study_list_id = t613.c612_study_list_id);

		--
		IF (SQL%ROWCOUNT = 0)
		THEN
			ROLLBACK;
			raise_application_error ('-20085', 'No record is found to be updated.');
		END IF;

		-- logs
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Patient IDE ID: ' || p_patient_ide_id);
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Study Period : ' || p_study_period_ds);
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Updated rows : ' || SQL%ROWCOUNT);
		DBMS_OUTPUT.put_line (SQL%ROWCOUNT || ' Row(s) are updated. ');
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_upt_patient;
END it_pkg_clinical;
/
