/* Formatted on 2011/01/20 14:53 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_op_inserts.bdy"

CREATE OR REPLACE PACKAGE BODY it_pkg_op_inserts
IS
--

	/****************************************************************
	* Description : This procedure is used to void the insert
	****************************************************************/
	PROCEDURE gm_void_insert (
		p_help_desk_id	 	t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_refid    			T5056_TRANSACTION_INSERT.C5056_REF_ID%TYPE
	  , p_insert_id			T5056_TRANSACTION_INSERT.C205_INSERT_ID%TYPE
	  , p_user_id	 		T5056_TRANSACTION_INSERT.C5056_LAST_UPDATED_BY%TYPE
	)
	AS
	BEGIN

		UPDATE T5056_TRANSACTION_INSERT
			SET C5056_VOID_FL         = 'Y' ,
			  C5056_LAST_UPDATED_BY   = p_user_id ,
			  c5056_last_updated_date = CURRENT_DATE
			WHERE C5056_REF_ID        = p_refid
			AND C205_INSERT_ID        = p_insert_id ;

		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'p_refid : ' || p_refid);
		DBMS_OUTPUT.put_line (SQL%ROWCOUNT || ' Row(s) are updated. ');
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_void_insert;
	
	/*******************************************************************************
	* Description : This procedure is used to add the Inserts for ETL consignments
	********************************************************************************/
	PROCEDURE gm_add_insert_ous_restock (
		p_help_desk_id	 	t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_user_id	 		T5056_TRANSACTION_INSERT.C5056_LAST_UPDATED_BY%TYPE
	)
	AS
	 CURSOR inserts_cur 
	    IS 
	      SELECT c504_consignment_id refid
			FROM t504_consignment
			WHERE c504_inhouse_purpose='4000097'  --OUS �Sales Replenishments
			AND c504_created_by = '649291' --Globus ETL
			AND c504_void_fl IS NULL
			AND c504_status_fl <= '2'--Not picked
			AND c504_type = '4110' --Consignments
			AND TRUNC(c504_created_date) =TRUNC(SYSDATE);
	BEGIN

		gm_pkg_cor_client_context.gm_sav_client_context('1000',null,null,'3000'); 
		
		FOR insert_val IN inserts_cur 
	    LOOP 
           BEGIN 
          gm_pkg_op_sav_insert_txn.gm_sav_insert_txn_dtl(insert_val.refid,'106761','93342',p_user_id,NULL);
		  -- logs
		  it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Order ID: ' || insert_val.refid);
		  DBMS_OUTPUT.put_line (SQL%ROWCOUNT || ' Row(s) are updated. ');
		  COMMIT;
	      EXCEPTION 
                WHEN OTHERS 
                  THEN 
                    DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM); 
                    ROLLBACK; 
	      END; 
	  END LOOP; 
	--
	EXCEPTION
	  WHEN NO_DATA_FOUND THEN
	    DBMS_OUTPUT.put_line ('Error Occurred : ' || SQLERRM);
	END gm_add_insert_ous_restock;
	
END it_pkg_op_inserts;
/
