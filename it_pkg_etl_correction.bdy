CREATE OR REPLACE
PACKAGE BODY IT_PKG_ETL_CORRECTION
IS
/***************************************************
		Description : This procedure is used for ICT job issue
*********************************************************/
PROCEDURE GM_ICT_CN_TO_DHR(
    P_HELP_DESK_ID IN T914_HELPDESK_LOG.C914_HELPDESK_ID%TYPE,
    P_CNID         IN T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE )
AS
  V_REFID T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE;
BEGIN
  DELETE TE504_CONSIGNMENT_DATA;
  INSERT
  INTO TE504_CONSIGNMENT_DATA
    (
      CE504_ITEM_CONSIGNMENT_ID,
      CE504_LOAD_DATE,
      C504_CONSIGNMENT_ID,
      C207_CONSIGNMENT_SET_ID,
      C205_PART_NUMBER_ID,
      C505_CONTROL_NUMBER,
      C505_ITEM_QTY,
      C5010_TAG_ID,
      C401_PURCHASE_ORD_ID,
      C8902_OUS_REF_ID,
      C1900_COMPANY_ID,
      C5040_PLANT_ID
    )
  SELECT T505.C505_ITEM_CONSIGNMENT_ID,
    TRUNC(SYSDATE),
    T504.C504_CONSIGNMENT_ID,
    NULL,
    T505.C205_PART_NUMBER_ID,
    T505.C505_CONTROL_NUMBER,
    T505.C505_ITEM_QTY,
    '',
    '',
    '',
    TE214.C1900_COMPANY_ID,
    TE214.C5040_PLANT_ID
  FROM T505_ITEM_CONSIGNMENT T505,
    T504_CONSIGNMENT T504,
    TE214_REPL_MASTER TE214,
    TE214A_CN_LOAD_DETAIL TE214A
  WHERE T504.C504_CONSIGNMENT_ID    =T505.C504_CONSIGNMENT_ID
  AND T504.C504_STATUS_FL           =4
  AND T504.C504_TYPE                =40057
  AND T504.C504_EXT_LOAD_DATE      IS NULL
  AND T504.C504_CONSIGNMENT_ID     IN (P_CNID)
  AND T504.C207_SET_ID             IS NULL
  AND TE214A.CE214_REPL_MASTER_ID	=TE214.CE214_REPL_MASTER_ID
  AND T504.C701_DISTRIBUTOR_ID      =TE214A.CE701_DISTRIBUTOR_ID
  AND T504.C504_CONSIGNMENT_ID NOT IN
    (SELECT C408_COMMENTS
    FROM T408_DHR
    WHERE C408_VOID_FL IS NULL
    AND C408_COMMENTS  IS NOT NULL
    );
  GM_PKG_OP_ICT_DATALOAD.GM_SAV_PROCESS_DATALOAD();
  -- LOGS
  IT_PKG_CM_USER.GM_CM_SAV_HELPDESK_LOG(P_HELP_DESK_ID, 'TXN ID: ' || P_CNID);
  COMMIT;
  BEGIN
    SELECT DISTINCT C504_CONSIGNMENT_ID
    INTO V_REFID
    FROM TE504_CONSIGNMENT_DATA
    WHERE C504_CONSIGNMENT_ID = P_CNID;
    DBMS_OUTPUT.PUT_LINE ('TRANSACTION SUCCESSFUL FOR: ' || V_REFID);
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    DBMS_OUTPUT.PUT_LINE ('RE-CHECK THE ID IS VAILD OR NOT: ' || P_CNID);
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE ('ERROR OCCURRED: ' || SQLERRM);
  END;
EXCEPTION
WHEN NO_DATA_FOUND THEN
    DBMS_OUTPUT.PUT_LINE ('No Data Found for: ' || P_CNID);
WHEN OTHERS THEN
  DBMS_OUTPUT.PUT_LINE ('ERROR OCCURRED WHILE EXECUTING SP: ' || SQLERRM);
  ROLLBACK ;
END GM_ICT_CN_TO_DHR;

/***************************************************
		Description : This procedure is used for updating india 
		              account created without currency type
*********************************************************/
PROCEDURE GM_PKG_IT_OUS_ACC(P_HELP_DESK_ID IN T914_HELPDESK_LOG.C914_HELPDESK_ID%TYPE,P_ACCID T704_ACCOUNT.C704_ACCOUNT_ID%TYPE) AS
 
BEGIN

UPDATE T704_ACCOUNT SET C901_CURRENCY=101180, C704_LAST_UPDATED_DATE=SYSDATE,C704_LAST_UPDATED_BY=303043
WHERE C901_EXT_COUNTRY_ID in (102461,102460)
AND C901_CURRENCY IS NULL AND C704_ACCOUNT_ID=P_ACCID;	

--LOG
IT_PKG_CM_USER.GM_CM_SAV_HELPDESK_LOG(P_HELP_DESK_ID, 'Account ID: ' ||P_ACCID|| ' created without currency or Corp price is updated');
COMMIT;

DBMS_OUTPUT.PUT_LINE ('Currency has been updated for Account id= ' || P_ACCID);

EXCEPTION
  WHEN NO_DATA_FOUND THEN
    DBMS_OUTPUT.PUT_LINE ('Invalid Account ID: ' || P_ACCID);
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE ('ERROR OCCURRED: ' || SQLERRM);
END GM_PKG_IT_OUS_ACC;


/***************************************************
		Description : This procedure is used to correct the 
		DHR and DHFG transactions created with NOC# during ICT
*********************************************************/
PROCEDURE GM_PKG_IT_ICT_NOC(
P_HELP_DESK_ID   IN T914_HELPDESK_LOG.C914_HELPDESK_ID%TYPE,
P_consignment_id IN t504_consignment.c504_consignment_id%TYPE
) 
AS
   v_cn_id      VARCHAR2(100);
  v_part_id    VARCHAR2(100);
  v_crtl_no    VARCHAR2(100);
  v_item_price NUMBER(15,2);
  v_item_qty   NUMBER;
  v_row_num    NUMBER;
  v_dhr_id     VARCHAR2(100);
  v_inhouse    VARCHAR2(100);
  v_trans_id   NUMBER;
  v_wo_id      VARCHAR2(100);
  v_po_id      VARCHAR2(100);
  
  CURSOR ict_temp
  IS
    SELECT DISTINCT c504_consignment_id,
      c205_part_number_id,
      c505_item_price,
      c408_dhr_id,
      c412_inhouse_trans_id,
      c413_trans_id,
      c402_work_order_id,
      c401_purchase_ord_id
    FROM
      (SELECT DISTINCT t504.c504_consignment_id,
        t505.c205_part_number_id,
        c505_item_price,
        c408_dhr_id,
        t412.c412_inhouse_trans_id,
        c413_trans_id,
        c402_work_order_id,
        c401_purchase_ord_id
      FROM t504_consignment t504,
        t505_item_consignment t505,
        t408_dhr t408 ,
        t412_inhouse_transactions t412,
        t413_inhouse_trans_items t413
      WHERE t504.c504_consignment_id=t505.c504_consignment_id
      AND t408.c408_comments        =t504.c504_consignment_id
      AND t505.c205_part_number_id  =t408.c205_part_number_id
      AND c412_ref_id               =c408_dhr_id
      AND t412.c412_inhouse_trans_id=t413.c412_inhouse_trans_id
      AND t505.c205_part_number_id  =t413.c205_part_number_id
      AND t504.c504_consignment_id  =P_consignment_id
      --AND c504_ship_date            >to_date('01/18/2017','mm/dd/yyyy') -- Only transactions happened after this date will be handled
      AND c701_distributor_id      IN (689,1899)                        -- NED and ATEC distributor
      AND c504_type                 =40057                              -- ICT consignment type
      AND c504_ext_load_date       IS NOT NULL
      AND c408_control_number       ='NOC#'
      AND c505_control_number      <>'NOC#'
      AND c412_status_fl           <> 4
      ORDER BY t504.c504_consignment_id,
        t505.c205_part_number_id
      );
  CURSOR ict_main
  IS
    SELECT rownum rn,
      c505_item_consignment_id,
      c505_item_qty,
      c505_control_number
    FROM t505_item_consignment
    WHERE c504_consignment_id=v_cn_id
    AND c205_part_number_id  =v_part_id
    ORDER BY c505_item_consignment_id ASC;
    
BEGIN
FOR ict_loop IN ict_temp
  LOOP
    v_cn_id           :=ict_loop.c504_consignment_id;
    v_part_id         := ict_loop.c205_part_number_id;
    v_dhr_id          :=ict_loop.c408_dhr_id;
    v_inhouse         := ict_loop.c412_inhouse_trans_id;
    v_trans_id        :=ict_loop.c413_trans_id;
    v_wo_id           :=ict_loop.c402_work_order_id;
    v_po_id           :=ict_loop.c401_purchase_ord_id;
    v_item_price      :=ict_loop.c505_item_price;
    FOR ict_loop_main IN ict_main
    LOOP
      v_row_num  :=ict_loop_main.rn;
      v_item_qty :=ict_loop_main.c505_item_qty;
      v_crtl_no  :=ict_loop_main.c505_control_number;
      IF v_row_num=1 THEN -- To handle insert and update: When a single CN have a part with different crtl numbers, First line item will update the existing DHR and DHFG record
        UPDATE t408_dhr
        SET c408_qty_received   =v_item_qty,
          c408_qty_on_shelf     =v_item_qty,
          c408_shipped_qty      =v_item_qty,
          c408_control_number   =v_crtl_no,
          c408_last_updated_by  ='941711',
          c408_last_updated_date=sysdate
        WHERE c408_dhr_id       =v_dhr_id;
        DBMS_OUTPUT.PUT_LINE ('CN ID :' || v_cn_id || ', Updated DHR ID :' || v_dhr_id || ', Updated CTRL NO :' || v_crtl_no);
        IT_PKG_CM_USER.GM_CM_SAV_HELPDESK_LOG(P_HELP_DESK_ID, 'CN ID :' || v_cn_id || ', Updated DHR ID :' || v_dhr_id || ', Updated CTRL NO :' || v_crtl_no);
        UPDATE t413_inhouse_trans_items
        SET c413_control_number = v_crtl_no,
          c413_item_qty         =v_item_qty,
          c413_last_updated_by  =941711,
          c413_last_updated_date=sysdate
        WHERE c413_trans_id     = v_trans_id;
        DBMS_OUTPUT.PUT_LINE ('CN ID :' || v_cn_id || ', Updated DHFG ID :' || v_inhouse|| ', Updated CTRL NO :' || v_crtl_no);
        IT_PKG_CM_USER.GM_CM_SAV_HELPDESK_LOG(P_HELP_DESK_ID,'CN ID :' || v_cn_id || ', Updated DHFG ID :' || v_inhouse|| ', Updated CTRL NO :' || v_crtl_no);
      ELSE --if v_row_num <>1, Since 1st line item is used for updating existing DHR and DHFG remaining line item will be inserted into DHR and DHFG
        SELECT 'GM-DHR-'||s408_dhr.nextval INTO v_dhr_id FROM dual;
        SELECT 'DHFG-'||s504_consign.NEXTVAL INTO v_inhouse FROM dual;
        INSERT
        INTO t408_dhr
          (
            c408_dhr_id,
            c408_qty_received,
            c408_control_number,
            c408_created_date,
            c408_created_by,
            c402_work_order_id,
            c205_part_number_id,
            c408_comments,
            c301_vendor_id,
            c401_purchase_ord_id,
            c1900_company_id,
            c5040_plant_id,
            c408_received_date,
            c408_status_fl,
            c408_manf_date,
            c408_qty_on_shelf,
            c408_verified_by,
            c408_verified_ts,
            c408_shipped_qty
          )
          VALUES
          (
            v_dhr_id,
            v_item_qty,
            v_crtl_no,
            sysdate,
            '941711',
            v_wo_id,
            v_part_id,
            v_cn_id,
            429,
            v_po_id,
            1010,3003,
            sysdate,
            4,
            sysdate,
            v_item_qty,
            '941711',
            sysdate,
            v_item_qty
          );
        DBMS_OUTPUT.PUT_LINE ('CN ID :' || v_cn_id || ', New DHR ID :' || v_dhr_id || ', Inserted CTRL NO :' || v_crtl_no);
        IT_PKG_CM_USER.GM_CM_SAV_HELPDESK_LOG(P_HELP_DESK_ID,'CN ID :' || v_cn_id || ', New DHR ID :' || v_dhr_id || ', Inserted CTRL NO :' || v_crtl_no);
        INSERT
        INTO t412_inhouse_transactions
          (
            c412_inhouse_trans_id,
            c412_status_fl,
            c412_inhouse_purpose,
            c412_type,
            c412_created_by,
            c412_created_date,
            c412_ref_id,
            c1900_company_id,
            c5040_plant_id
          )
          VALUES
          (
            v_inhouse,
            '3',
            41278,100067,
            '941711',
            sysdate,
            v_dhr_id,
            1010,3003
          );
        DBMS_OUTPUT.PUT_LINE ('CN ID :' || v_cn_id || ', New DHFG ID :' || v_inhouse|| ' DHR ID :' || v_dhr_id);
        IT_PKG_CM_USER.GM_CM_SAV_HELPDESK_LOG(P_HELP_DESK_ID,'CN ID :' || v_cn_id || ', New DHFG ID :' || v_inhouse|| ' DHR ID :' || v_dhr_id);
        INSERT
        INTO t413_inhouse_trans_items
          (
            c413_trans_id,
            c413_control_number,
            c413_item_qty,
            c413_item_price,
            c413_void_fl,
            c205_part_number_id,
            c412_inhouse_trans_id,
            c413_status_fl,
            c413_created_date
          )
          VALUES
          (
            s413_inhouse_item.nextval,
            v_crtl_no,
            v_item_qty,
            v_item_price,
            NULL,
            v_part_id,
            v_inhouse,
            NULL,
            sysdate
          );
        DBMS_OUTPUT.PUT_LINE ('CN ID :' || v_cn_id || ', DHFG ID :' || v_inhouse|| ', Inserted CTRL NO :' || v_crtl_no);
        IT_PKG_CM_USER.GM_CM_SAV_HELPDESK_LOG(P_HELP_DESK_ID,'CN ID :' || v_cn_id || ', DHFG ID :' || v_inhouse|| ', Inserted CTRL NO :' || v_crtl_no);
      END IF;
    END LOOP;
  END LOOP;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    DBMS_OUTPUT.PUT_LINE ('Invalid : ' || P_consignment_id);
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE ('ERROR OCCURRED: ' || SQLERRM);  
END GM_PKG_IT_ICT_NOC;


END IT_PKG_ETL_CORRECTION;
/