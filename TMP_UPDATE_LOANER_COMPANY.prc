-- @"C:\Database\Datacorrection\Script\TMP_UPDATE_LOANER_COMPANY.prc";
create or replace
PROCEDURE TMP_UPDATE_LOANER_COMPANY
(
  P_LOANER_ID IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
) 
AS 
v_request_id NUMBER;
v_req_company_id NUMBER;
v_loanr_company_id NUMBER;

BEGIN

BEGIN
SELECT t504a.c526_product_request_detail_id,  t526.c1900_company_id, t504a.c1900_company_id consign_country
INTO v_request_id,v_req_company_id,v_loanr_company_id 
FROM t412_inhouse_transactions T412
JOIN t504a_loaner_transaction t504a
ON t412.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
JOIN t526_product_request_detail t526
ON t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
WHERE c412_inhouse_trans_id             = P_LOANER_ID;
EXCEPTION
WHEN NO_DATA_FOUND 
THEN
      v_request_id := NULL;
END;

IF v_req_company_id != v_loanr_company_id OR v_request_id IS NULL
THEN

UPDATE t412_inhouse_transactions 
SET c1900_company_id = v_req_company_id, 
  c412_last_updated_by = '2163127', 
  c412_last_updated_date = CURRENT_DATE 
WHERE c412_inhouse_trans_id = P_LOANER_ID; 

UPDATE t504a_loaner_transaction 
SET c1900_company_id = v_req_company_id, 
  c504a_last_updated_by = '2163127', 
  c504a_last_updated_date = CURRENT_DATE 
WHERE c526_product_request_detail_id = v_request_id; 

END IF;
  
END TMP_UPDATE_LOANER_COMPANY;
/