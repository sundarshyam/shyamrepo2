-- @"C:\Data Correction\Script\it_pkg_bba_lot.bdy"

CREATE OR REPLACE PACKAGE BODY it_pkg_bba_lot
IS
--
/*********************************************************
	* Description : This procedure is used to correct Lot Discrepancy by Initiate, Control and Verify given Txn and adjust Lot inventory
	*********************************************************/
  PROCEDURE gm_lot_data_correct (
    p_help_desk_id		t914_helpdesk_log.c914_helpdesk_id%TYPE,
    p_part_number     t205_part_number.c205_part_number_id%TYPE,
    p_control_number	t5061_control_number_inv_log.c5060_control_number%TYPE,
    p_txn_type        NUMBER, -- Transaction type of correcting Txn
    p_qty             NUMBER,
    p_source_wh       NUMBER,
    p_target_wh       NUMBER,
    p_issue_txn_id    t5061_control_number_inv_log.c5061_last_update_trans_id%TYPE, -- Transaction id of issue Txn in Lot Inventory
    p_bucket          NUMBER
  )
  AS
  v_user t101_user.c101_user_id%TYPE		:='30301';
  v_rec_count      NUMBER								:=0;
  v_msg VARCHAR2(100);
  v_input_str VARCHAR2(2000);
  v_part_number t205_part_number.c205_part_number_id%TYPE;
  v_part_price t205_part_number.c205_equity_price%TYPE;
  v_txn_id t504_consignment.c504_consignment_id%TYPE;
  fg_qty NUMBER;
  qn_qty NUMBER;
  pn_qty NUMBER;
  fg_lot_qty NUMBER;
  qn_lot_qty NUMBER;
  pn_lot_qty NUMBER;
  fg_qty_diff NUMBER;
  qn_qty_diff NUMBER;
  pn_qty_diff NUMBER;
  v_reason NUMBER;
  v_txn_desc VARCHAR2(50);
  v_txntype NUMBER;
  v_table t906_rules.c906_rule_value%TYPE;
  v_rec_id  VARCHAR2(50);
  v_rec_received_date DATE;
  v_out VARCHAR2(100);
  v_src_lot_qty  NUMBER;
  v_trg_lot_qty  NUMBER;
  
  BEGIN
    -- Steps:
    -- 1. Initiate, Control and verify Txn for given Txn type. Storage tables will be t504 or t412 decides based on the Rule value for given Txn type. For 
    --    Lot Adjustment input (issu_txn=LTAJ), will not do this.
    -- 2. Based on given WH type, increase / decrease qty. If WH=0, that warehouse Lot inventoy will not be updated
    -- 3. Using LOT_ADJ make the qty as 0 in target WH because it is correction Txn. This kind will be deleted from Lot Inventory
    -- 4. Update Lot Flag to 'A' (Adjustment) so Lot engine will not take this.
    -- 5. Update the Issue txn Orig_qty to 1, so this will not be selected as issue txn
    -- 6. Set the initiated txn created by date as received date
    -- 7. For RSFG, RSPN & RSQN, TRANS_CONTY records missing, create as DH_FG and change the name as required later
    
    BEGIN
      --dbms_output.put_line('issue txn' || instr(p_issue_txn_id,'LTAJ'));
      BEGIN
        SELECT --Part_Number,
          nvl(fg_qty,0) fg_qty , 
          nvl(lot_fg_qty,0) lot_fg_qty, 
          CASE WHEN nvl(fg_qty,0) > nvl(lot_fg_qty,0) THEN nvl(fg_qty,0) - nvl(lot_fg_qty,0) ELSE nvl(lot_fg_qty,0) - nvl(fg_qty,0) END fg_qty_diff,
          nvl(qn_qty ,0) qn_qty,
          nvl(lot_qn_qty,0) lot_qn_qty,
          CASE WHEN nvl(qn_qty ,0) >nvl(lot_qn_qty,0) THEN nvl(qn_qty ,0) - nvl(lot_qn_qty,0) ELSE nvl(lot_qn_qty,0) -nvl(qn_qty ,0) END qn_qty_diff,
          nvl(pn_qty ,0) pn_qty,
          nvl(lot_pn_qty,0) lot_pn_qty,
          CASE WHEN nvl(pn_qty ,0) >nvl(lot_pn_qty,0) THEN nvl(pn_qty ,0) - nvl(lot_pn_qty,0) ELSE nvl(lot_pn_qty,0) -nvl(pn_qty ,0) END pn_qty_diff
          INTO fg_qty,fg_lot_qty,fg_qty_diff,qn_qty,qn_lot_qty,qn_qty_diff,pn_qty,pn_lot_qty,pn_qty_diff
        FROM
          (
          SELECT 
          t205.c205_part_number_id part_number,
          nvl(t205.c205_qty_in_stock,0) fg_qty ,
          nvl(t205.c205_qty_in_quarantine,0) qn_qty ,
          nvl(t205.c205_qty_in_packaging,0) pn_qty ,
          (SELECT sum(t5060.c5060_qty)
            FROM t5060_control_number_inv t5060
            WHERE t5060.c901_warehouse_type = 90800 --FG
            AND t5060.c205_part_number_id = t205.c205_part_number_id
            AND t205.c205_part_number_id = p_part_number
            GROUP BY t5060.c205_part_number_id
            ) lot_fg_qty,
            
            (SELECT sum(t5060.c5060_qty) qn_qty
            FROM t5060_control_number_inv t5060
            WHERE t5060.c901_warehouse_type = 90813 --Quarantine
            AND t5060.c205_part_number_id = t205.c205_part_number_id
            AND t205.c205_part_number_id = p_part_number
            GROUP BY t5060.c205_part_number_id
            ) lot_qn_qty,
            
            (SELECT sum(t5060.c5060_qty)
            FROM t5060_control_number_inv t5060
            WHERE t5060.c901_warehouse_type = 90815 --PN
            AND t5060.c205_part_number_id = t205.c205_part_number_id
            AND t205.c205_part_number_id = p_part_number
            GROUP BY t5060.c205_part_number_id
            ) lot_pn_qty
            
          FROM t205_part_number t205 
          WHERE t205.c205_active_fl!='N' 
        )
        WHERE fg_qty!=lot_fg_qty OR qn_qty !=lot_qn_qty OR pn_qty !=lot_pn_qty
        ORDER BY part_number;
      EXCEPTION WHEN NO_DATA_FOUND THEN
         dbms_output.put_line('No Data Found for p_part_number=' || p_part_number );
      END;

      dbms_output.put_line('Txn=' || v_txn_id || ' Graft=' || p_control_number || ' Previous FG_Qty=' || fg_qty ||  ' FG_Lot_Qty=' || fg_lot_qty ||', QN_Qty=' || qn_qty ||  ', QN_lot_Qty=' || qn_lot_qty || ', PN_Qty=' || pn_qty ||  ', PN_lot_Qty=' || pn_lot_qty);
  
      IF (instr(p_issue_txn_id,'LTAJ')<=0 OR p_issue_txn_id IS NULL) THEN
        /*  
        Sample input feeding of this Proc:
        --PNFG
        set serveroutput on;
        BEGIN
          it_pkg_bba_lot.gm_lot_data_correct('3732','721', 'THB777704',400063, 1, 90815, 90800,'FGPN-190374',120488);
        END;
        
        --FG Lot Adj
        set serveroutput on;
        BEGIN
          it_pkg_bba_lot.gm_lot_data_correct('3732','721', 'THB777704',400063, 1,90800, 0,'LTAJ',120488);
        END;
        
        --RSFG
        set serveroutput on;
        BEGIN
          it_pkg_bba_lot.gm_lot_data_correct('3732','721', 'THB777701',104620, 1, 0, 90800,'FGPN-190351',120602);
        END;


        */
        dbms_output.put_line('Start');
        -- Get th Transaction description ex:Shelf-InvAdj
        BEGIN
          SELECT c906_rule_id 
            INTO v_txn_desc
          FROM t906_rules 
          WHERE c906_rule_grp_id = 'TRANS_CONTY' 
            AND c906_rule_value = TO_CHAR(p_txn_type);
        EXCEPTION WHEN NO_DATA_FOUND THEN
          -- TRANS_CONTY not available for RSFG,RSPN and RSQN. So change to DHR-XX
          SELECT DECODE(
            p_txn_type, 104620, 'DHR-FG',
                        104621, 'DHR-REPACK', 
                        104622, 'DHR-QUARAN',
                        NULL)
          INTO v_txn_desc
          FROM dual;
        END;
          
        dbms_output.put_line('p_txn_type=' || p_txn_type);
        
        --Get New txn id
        SELECT get_next_consign_id(v_txn_desc) 
          INTO v_txn_id
        FROM dual;

        -- For RSFG,RSPN and RSQN, use DHFG then replace RSFG, etc., because Trans_conty not defined this
        SELECT DECODE(
          p_txn_type, 104620, REPLACE(v_txn_id, 'DH', 'RS'),
                      104621, REPLACE(v_txn_id, 'DH', 'RS'), 
                      104622, REPLACE(v_txn_id, 'DH', 'RS'),
                      v_txn_id)
          INTO v_txn_id
        FROM dual;

        dbms_output.put_line('v_txn_id=' || v_txn_id);

        --In which table txn needs to be created
        SELECT get_rule_value ('TRANS_TABLE', p_txn_type)
        INTO v_table
        FROM DUAL;
        
        dbms_output.put_line('v_table=' || v_table);
        
        BEGIN 
          SELECT t4082.c4081_dhr_bulk_id, t4081.c4081_received_date -- Update date as 10 mins before Issue Txn
            INTO v_rec_id, v_rec_received_date
          FROM t408a_dhr_control_number t408a, t4082_bulk_dhr_mapping t4082, t4081_dhr_bulk_receive t4081
          WHERE t408a.c408_dhr_id = t4082.c408_dhr_id
            AND t4082.c4081_dhr_bulk_id = t4081.c4081_dhr_bulk_id
            AND t408a.c408a_conrol_number= p_control_number;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_rec_id := NULL;
            v_rec_received_date := NULL;
        END;
        
        dbms_output.put_line('v_rec_id =' || v_rec_id || ' v_rec_received_date=' || v_rec_received_date);
        
        IF (v_rec_received_date IS NULL) THEN
          dbms_output.put_line('Is Null v_rec_received_date =' || v_rec_received_date);
          BEGIN 
            SELECT (C5061_CREATED_DATE -  10/(24*60)) -- keep as our new txn happened 1 hour before
              INTO v_rec_received_date
            FROM t5061_control_number_inv_log 
            WHERE c5061_last_update_trans_id = p_issue_txn_id
              AND c5060_control_number = p_control_number
              AND c205_part_number_id = p_part_number
              AND rownum=1; -- one txn for 2 WH
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
              v_rec_id := NULL;
              v_rec_received_date := NULL;
            END;
        END IF;
        
        --Get Part Price
        --SELECT get_part_price (p_part_number, 'E') INTO v_price FROM dual;
      
        --Get Part data
        BEGIN
          SELECT	c205_equity_price price, c205_qty_in_stock, c205_qty_in_quarantine, c205_qty_in_packaging
            INTO v_part_price, fg_qty, qn_qty, pn_qty
          FROM t205_part_number   
          WHERE c205_part_number_id = p_part_number;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
          v_part_price := NULL;
          fg_qty := NULL;
          qn_qty:=NULL;
          pn_qty:=NULL;
        END;
        
            
        -- Get Reason code
        SELECT 1109 -- '-'  -- decode(lot.c901_warehouse_type,90813,4171,90815,120420,NULL)
        INTO v_reason 
        FROM dual;
      
      
        --dbms_output.put_line('Start Initiate');
        IF v_table = 'CONSIGNMENT' THEN
            -- Initiate 
            v_input_str := p_part_number || ',' || p_qty || ',,' || v_part_price || '|';
            gm_save_item_consign(v_txn_id, p_txn_type , v_reason , '01','', v_user,'Transaction missed in Grafts txn history to fill the issue gap and increase Part Qty', 1, v_user, v_input_str);
            dbms_output.put_line('Txn=' || v_txn_id || ' Initiated');
            
            -- Control
            v_input_str := p_part_number || ',1,' || p_control_number || ',,90800|';
            gm_pkg_op_item_control_txn.gm_sav_item_control_main(v_txn_id, p_txn_type, 'on', v_input_str,93343,v_user);
            dbms_output.put_line('Txn=' || v_txn_id || ' Controlled');
      
            -- Verify
            v_input_str := p_part_number || ',' || p_qty || ',' || p_control_number || ',,'|| p_target_wh || '|';
            gm_pkg_op_item_control_txn.gm_sav_item_verify_main(v_txn_id, p_txn_type, v_input_str, 93345, v_user);
            dbms_output.put_line('Txn=' || v_txn_id || ' Verified');
        ELSIF (v_table ='IN_HOUSE') THEN
          -- initiate FGIA
          --400067 From Shelf 
          v_input_str := p_part_number || ',' || p_qty || ',,,' || v_part_price || '|';
          gm_save_inhouse_item_consign(v_txn_id,p_txn_type , v_reason ,0,'','Transaction missed in Grafts txn history to fill the issue gap and increase Part Qty',v_user,v_input_str,v_out);
          dbms_output.put_line('Txn=' || v_txn_id || ' Initiated');
          
          --Control 
          BEGIN
          --93343             Pick Transacted LCTXTP , 93345  Put Transacted  LCTXTP
          v_input_str := p_part_number || ',' || p_qty || ',' || p_control_number || ',,' || p_source_wh || '|';
          gm_pkg_op_item_control_txn.gm_sav_item_control_main(v_txn_id, p_txn_type, 'on', v_input_str,93343,v_user);
          dbms_output.put_line('Txn=' || v_txn_id || ' Controlled');
          EXCEPTION WHEN OTHERS THEN
            IF ( INSTR(sqlerrm,'ORA-20403')>0) THEN
              dbms_output.put_line('v_txn_id=' || v_txn_id || ' does not need control'); 
            ELSE
              raise_application_error('-99999',sqlerrm);
            END IF;
          END;
         
          v_input_str := p_part_number || ',' || p_qty || ',' || p_control_number || ',,' || p_target_wh || '|';
          gm_pkg_op_item_control_txn.gm_sav_item_verify_main(v_txn_id, p_txn_type, v_input_str,93345,v_user);
          dbms_output.put_line('Txn=' || v_txn_id || ' Verified');
      
        END IF;
      END IF;
      
      
      --Check source Lot Qty
      v_src_lot_qty := get_lot_qty_by_wh(p_part_number, p_control_number,p_source_wh,(p_qty*-1)) ;
      
      dbms_output.put_line('v_src_lot_qty=' || v_src_lot_qty); 
          
      --If more than 1 Qty and Just Lot Adjustment only, make it as 1. When p_qty=-1 dont go inside because that is only for Lot adj.
      IF (p_qty != -1 AND v_src_lot_qty >=1 AND instr(p_issue_txn_id,'LTAJ')>0 ) THEN
        gm_pkg_op_ld_lot_track.gm_sav_control_number_inv(p_part_number,p_control_number,p_source_wh,1,'LOT_ADJ',p_bucket,gm_pkg_op_ld_lot_track.get_lot_ref_type(p_source_wh),NULL,v_user);
        
         UPDATE t5061_control_number_inv_log 
          SET c5061_new_qty =0
        WHERE c901_warehouse_type = p_source_wh
            AND c5060_control_number = p_control_number
            AND c205_part_number_id = p_part_number
            AND c5061_last_update_trans_id = p_issue_txn_id
            AND c5061_new_qty >1;
            
        dbms_output.put_line('**********p_control_number=' || p_control_number || ' p_target_wh=' || p_source_wh || 'Txn=Lot_Adj' || ' Lot qty updated as ' || 1 );
  
        v_src_lot_qty := 0;
      ELSE
        -- If WH =0, do not update source Lot Inventory
        IF (p_source_wh >0) THEN
          -- Source WH record for Lot Inventory
          gm_pkg_op_ld_lot_track.gm_sav_control_number_inv(p_part_number,p_control_number,p_source_wh,v_src_lot_qty,v_txn_id,p_bucket,gm_pkg_op_ld_lot_track.get_lot_ref_type(p_source_wh),NULL,v_user);
          dbms_output.put_line('p_control_number=' || p_control_number || ' p_source_wh=' || p_source_wh || 'Txn=' || v_txn_id || ' Lot qty updated as ' || v_src_lot_qty );
        END IF;
        
        v_trg_lot_qty := get_lot_qty_by_wh(p_part_number, p_control_number,p_target_wh, p_qty);
        
        dbms_output.put_line('v_trg_lot_qty=' || v_trg_lot_qty); 
       
        -- If WH =0, do not update target Lot Inventory
        IF (p_target_wh >0) THEN
          -- Target WH record for Lot Inventory, make the Qty =0 becasue this to correct earlier txn
          gm_pkg_op_ld_lot_track.gm_sav_control_number_inv(p_part_number,p_control_number,p_target_wh,v_trg_lot_qty,v_txn_id,p_bucket,gm_pkg_op_ld_lot_track.get_lot_ref_type(p_target_wh),NULL,v_user);
          dbms_output.put_line('p_control_number=' || p_control_number || ' p_target_wh=' || p_target_wh || 'Txn=' || v_txn_id || ' Lot qty updated as ' || v_trg_lot_qty );
        END IF;
        
        --v_src_lot_qty := get_lot_qty_by_wh(p_part_number, p_control_number,p_target_wh, p_qty);
        -- Because this is a correction txn, adjust the Lot Qty to 0
       -- gm_pkg_op_ld_lot_track.gm_sav_control_number_inv(p_part_number,p_control_number,p_target_wh,v_trg_lot_qty,'LOT_ADJ',p_bucket,gm_pkg_op_ld_lot_track.get_lot_ref_type(p_target_wh),NULL,v_user);
       -- dbms_output.put_line('p_control_number=' || p_control_number || ' p_target_wh=' || p_target_wh || 'Txn=Lot_Adj' || ' Lot qty updated as ' || v_trg_lot_qty );
  
        --One transaction per Lot number, so updating txn will not be an issue. Dont update status of the Lot, because this txn is an missing one
        UPDATE t214_transactions 
          SET c214_control_number_update_fl ='A', 
          c214_control_udpate_date=SYSDATE  
        WHERE  c214_id = v_txn_id;
        
        -- Update the issue transaction Orig Qty to 1, so nthis will not selected next time a issue record
        UPDATE t5061_control_number_inv_log 
          SET c5061_orig_qty =1
        WHERE c5061_last_update_trans_id = p_issue_txn_id
            AND c5060_control_number = p_control_number
            AND (c5061_orig_qty =0  AND c5061_txn_qty <0);
          
        -- Issue transaction created date needs to be updated to RS-XXX transaction receive date, so in Lot Code Report txn history it will come in correct order
        UPDATE t5061_control_number_inv_log 
          SET  c5061_created_date = NVL(v_rec_received_date, SYSDATE)
        WHERE c5060_control_number = p_control_number
          AND C5061_LAST_UPDATE_TRANS_ID = v_txn_id;
        
        -- Delete Adjustment rows from Lot Inventory
        DELETE t5060_control_number_inv
        WHERE c5060_control_number = p_control_number
        AND C901_warehouse_type =0 ;
        
        -- Delete Adjustment rows from Lot Inventory
        DELETE t5061_control_number_inv_log 
        WHERE c5060_control_number = p_control_number
        AND C5061_LAST_UPDATE_TRANS_ID IS NULL ;
          
      END IF;
      
      -- logs
      it_pkg_cm_user.gm_cm_sav_helpdesk_log ('3732', 'Control number corrected: ' || p_control_number || ' for Part number:' || p_part_number);

      v_rec_count:=v_rec_count+1;

    EXCEPTION
    WHEN OTHERS THEN
      --gm_procedure_log('Error:',' Part '|| v_part_number ||' Graft '|| lot.C5060_CONTROL_NUMBER || ' Details:  ' || sqlerrm);
      dbms_output.put_line(' Part '|| v_part_number ||' Graft '|| p_control_number || ' Details:  ' || sqlerrm);
    END;
    
    --Get after transaction Part data
    BEGIN
      SELECT	c205_qty_in_stock, c205_qty_in_quarantine, c205_qty_in_packaging
        INTO  fg_qty, qn_qty, pn_qty
      FROM t205_part_number   
      WHERE c205_part_number_id = p_part_number;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        fg_qty := NULL;
        qn_qty:=NULL;
        pn_qty:=NULL;
    END;
    
    BEGIN
      SELECT --Part_Number,
          nvl(fg_qty,0) fg_qty , 
          nvl(lot_fg_qty,0) lot_fg_qty, 
          CASE WHEN nvl(fg_qty,0) > nvl(lot_fg_qty,0) THEN nvl(fg_qty,0) - nvl(lot_fg_qty,0) ELSE nvl(lot_fg_qty,0) - nvl(fg_qty,0) END fg_qty_diff,
          nvl(qn_qty ,0) qn_qty,
          nvl(lot_qn_qty,0) lot_qn_qty,
          CASE WHEN nvl(qn_qty ,0) >nvl(lot_qn_qty,0) THEN nvl(qn_qty ,0) - nvl(lot_qn_qty,0) ELSE nvl(lot_qn_qty,0) -nvl(qn_qty ,0) END qn_qty_diff,
          nvl(pn_qty ,0) pn_qty,
          nvl(lot_pn_qty,0) lot_pn_qty,
          CASE WHEN nvl(pn_qty ,0) >nvl(lot_pn_qty,0) THEN nvl(pn_qty ,0) - nvl(lot_pn_qty,0) ELSE nvl(lot_pn_qty,0) -nvl(pn_qty ,0) END pn_qty_diff
          INTO fg_qty,fg_lot_qty,fg_qty_diff,qn_qty,qn_lot_qty,qn_qty_diff,pn_qty,pn_lot_qty,pn_qty_diff
        FROM
          (
          SELECT 
          t205.c205_part_number_id part_number,
          nvl(t205.c205_qty_in_stock,0) fg_qty ,
          nvl(t205.c205_qty_in_quarantine,0) qn_qty ,
          nvl(t205.c205_qty_in_packaging,0) pn_qty ,
          (SELECT sum(t5060.c5060_qty)
            FROM t5060_control_number_inv t5060
            WHERE t5060.c901_warehouse_type = 90800 --FG
            AND t5060.c205_part_number_id = t205.c205_part_number_id
            AND t205.c205_part_number_id = p_part_number
            GROUP BY t5060.c205_part_number_id
            ) lot_fg_qty,
            
            (SELECT sum(t5060.c5060_qty) qn_qty
            FROM t5060_control_number_inv t5060
            WHERE t5060.c901_warehouse_type = 90813 --Quarantine
            AND t5060.c205_part_number_id = t205.c205_part_number_id
            AND t205.c205_part_number_id = p_part_number
            GROUP BY t5060.c205_part_number_id
            ) lot_qn_qty,
            
            (SELECT sum(t5060.c5060_qty)
            FROM t5060_control_number_inv t5060
            WHERE t5060.c901_warehouse_type = 90815 --PN
            AND t5060.c205_part_number_id = t205.c205_part_number_id
            AND t205.c205_part_number_id = p_part_number
            GROUP BY t5060.c205_part_number_id
            ) lot_pn_qty
            
          FROM t205_part_number t205 
          WHERE t205.c205_active_fl!='N' 
        )
        WHERE fg_qty!=lot_fg_qty OR qn_qty !=lot_qn_qty OR pn_qty !=lot_pn_qty
        ORDER BY part_number;
        
        dbms_output.put_line('Total Records Update:'|| v_rec_count || '. Now FG_Qty=' || fg_qty ||  ' FG_Lot_Qty=' || fg_lot_qty ||', QN_Qty=' || qn_qty ||  ', QN_lot_Qty=' || qn_lot_qty || ', PN_Qty=' || pn_qty ||  ', PN_lot_Qty=' || pn_lot_qty);

      EXCEPTION WHEN NO_DATA_FOUND THEN
        dbms_output.put_line('No Data Found for p_part_number=' || p_part_number );
      END;

      
  END gm_lot_data_correct;


  /*******************************************************
  * Description : function to get Lot Qty by WH
  * Author    : Yoga
  *******************************************************/
  FUNCTION get_lot_qty_by_wh (
    p_part_number     t205_part_number.c205_part_number_id%TYPE,
    p_control_number	t5061_control_number_inv_log.c5060_control_number%TYPE,
    p_wh_type NUMBER,
    p_qty NUMBER
    )
      RETURN NUMBER
  IS
      v_curqty NUMBER;
  BEGIN
          -- Get the current Qty
        BEGIN
          SELECT c5060_qty
            INTO v_curqty
            FROM t5060_control_number_inv t5060
           WHERE t5060.c205_part_number_id = p_part_number
             AND t5060.c5060_control_number = p_control_number
             AND t5060.c901_warehouse_type = p_wh_type;
             --AND nvl(t5060.c5060_ref_id,'9999') = nvl(v_ref_id,'9999')
             --AND nvl(t5060.c901_ref_type,9999) =  nvl(v_ref_type,9999)           
          
        EXCEPTION
        WHEN no_data_found THEN
          v_curqty:=0;
        END;
            
        v_curqty := v_curqty + p_qty;

      RETURN v_curqty;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END get_lot_qty_by_wh;

/*********************************************************
	* Description : This procedure is used to correct the Inventory Vs Lot dicrepancy
	*********************************************************/
	PROCEDURE gm_sav_lot_inv_qty (
		p_help_desk_id		t914_helpdesk_log.c914_helpdesk_id%TYPE,
		p_control_number	t5061_control_number_inv_log.c5060_control_number%TYPE
	)
	AS
	v_user t101_user.c101_user_id%TYPE		:='30301';
  v_rec_count      NUMBER									:=0;
  v_msg VARCHAR2(100);
  v_input_str VARCHAR2(2000);
  v_part_number t205_part_number.c205_part_number_id%TYPE;
  v_part_price t205_part_number.c205_equity_price%TYPE;
  v_txn_id t504_consignment.c504_consignment_id%TYPE;
  fg_qty NUMBER;
  qn_qty NUMBER;
  pn_qty NUMBER;
  v_reason NUMBER;
  v_txntype NUMBER;
  
  CURSOR cur_disc_lot 
    IS
			SELECT distinct
        C5060_CONTROL_NUMBER,
        C901_WAREHOUSE_TYPE,
        get_code_name(C901_WAREHOUSE_TYPE) WAREHOUSE_TYPE
      FROM t5061_control_number_inv_log 
		WHERE c901_warehouse_type in (90813,90815)  AND-- QN and PN
        c5061_new_qty >=1
        AND NVL(c5060_control_number, '-9999') = NVL(p_control_number,c5060_control_number)
        AND c5060_control_number IN
        (
                SELECT c5060_control_number FROM
                t5061_control_number_inv_log WHERE 
                c5061_orig_qty =0 AND c5061_txn_qty<=-1 AND c5061_new_qty=0
                AND c901_last_transaction_type IN (4310,4311) -- Order, Consignment
        );
			--ORDER BY  to_char(c5061_created_date,'MM/DD/YY HH:mm:SS') DESC; 
	BEGIN
		FOR lot IN cur_disc_lot
		loop
      -- Get Part for the graft
      BEGIN
        SELECT distinct c205_part_number_id 
          INTO v_part_number
        FROM t5061_control_number_inv_log
        WHERE c5061_orig_qty =0 
          AND c5060_control_number = lot.c5060_control_number
          AND c901_last_transaction_type IN (4310,4311);
      EXCEPTION
			WHEN NO_DATA_FOUND THEN
					v_part_number:=NULL;
			END;
      
      IF v_part_number is NOT NULL THEN
        /*
        90813	Quarantine Inventory
        90800	Inventory Qty
        90815	Repackage Qty
        */
        -- Get Next id based on lot warehouse
        SELECT	decode(lot.c901_warehouse_type, 90813,get_next_consign_id('InvenIn'), 90815, get_next_consign_id('REPACK-SHELF'), NULL) 
        INTO v_txn_id
        FROM dual;
      
        --Get Part data
        BEGIN
          SELECT	c205_equity_price price, c205_qty_in_stock, c205_qty_in_quarantine, c205_qty_in_packaging
          INTO v_part_price, fg_qty, qn_qty, pn_qty
          FROM t205_part_number   
          WHERE c205_part_number_id = v_part_number;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_part_price := NULL;
            fg_qty := NULL;
            qn_qty:=NULL;
            pn_qty:=NULL;
        END;
  
        dbms_output.put_line('Txn=' || v_txn_id || ' created to move the Graft ' || lot.c5060_control_number || ' from ' || lot.warehouse_type || ' to FG. Previous FG_Qty=' || fg_qty || ', QN_Qty=' || qn_qty || ', PN_Qty=' || pn_qty);
  
        BEGIN
          -- Get Reason code
          SELECT  decode(lot.c901_warehouse_type,90813,4171,90815,120420,NULL)
          INTO v_reason 
          FROM dual;
  
          -- Get txn type
          SELECT  decode(lot.c901_warehouse_type,90813,4116,90815,400063,NULL)
          INTO v_txntype 
          FROM dual;
  
          -- Initiate 
          v_input_str := v_part_number || ',1,,' || v_part_price || '|';
          gm_save_item_consign(v_txn_id, v_txntype , v_reason , '01','', v_user,'Transaction to correct the Graft moved from wrong warehouse', 1, v_user, v_input_str);
          dbms_output.put_line('Txn=' || v_txn_id || ' Intiated');
          
          -- Control
          v_input_str := v_part_number || ',1,' || lot.c5060_control_number || ',,90800|';
          gm_pkg_op_item_control_txn.gm_sav_item_control_main(v_txn_id, v_txntype, 'on', v_input_str,93343,v_user);
          dbms_output.put_line('Txn=' || v_txn_id || ' Controlled');
  
          -- Verify
          v_input_str := v_part_number || ',1,' || lot.c5060_control_number || ',,90800|';
          gm_pkg_op_item_control_txn.gm_sav_item_verify_main(v_txn_id, v_txntype, v_input_str, 93345, v_user);
          
          
          -- Above transaction increases Lot Qty to one in FG which needs to be made 0, because this is already shipped out
          UPDATE t5060_control_number_inv
            SET c5060_qty = 0, c5060_last_updated_by = '941711', c5060_last_updated_date = SYSDATE, C5060_LAST_UPDATE_TRANS_ID='LOT_ADJ'
          WHERE C901_WAREHOUSE_TYPE=90800 
            AND C5060_CONTROL_NUMBER = lot.c5060_control_number
            AND c205_part_number_id = v_part_number  -- because redesignated part may be available
            AND c5060_qty =1; 
          
          -- Update now Order/Consignment record as Orig Qty=1, so next time it will not be selected for dicrepancy fix
          UPDATE t5061_control_number_inv_log 
            SET c5061_orig_qty =1 
          WHERE c5061_orig_qty =0 
            AND c5060_control_number = lot.c5060_control_number
            AND C901_WAREHOUSE_TYPE = 90800 --FG
            AND c901_last_transaction_type IN (4310,4311);
          
          --Get after transaction Part data
          BEGIN
            SELECT	c205_qty_in_stock, c205_qty_in_quarantine, c205_qty_in_packaging
            INTO  fg_qty, qn_qty, pn_qty
            FROM t205_part_number   
            WHERE c205_part_number_id = v_part_number;
          EXCEPTION
          WHEN NO_DATA_FOUND THEN
              fg_qty := NULL;
              qn_qty:=NULL;
              pn_qty:=NULL;
          END;

          dbms_output.put_line('Txn=' || v_txn_id || ' Verified. Now FG_Qty=' || fg_qty || ', QN_Qty=' || qn_qty || ', PN_Qty=' || pn_qty);

					-- logs
					it_pkg_cm_user.gm_cm_sav_helpdesk_log ('131', 'Control number corrected: ' || lot.c5060_control_number || ' for Part number:' || v_part_number);

          v_rec_count:=v_rec_count+1;

        EXCEPTION
        WHEN OTHERS THEN
          --gm_procedure_log('Error:',' Part '|| v_part_number ||' Graft '|| lot.C5060_CONTROL_NUMBER || ' Details:  ' || sqlerrm);
          dbms_output.put_line(' Part '|| v_part_number ||' Graft '|| lot.c5060_control_number || ' Details:  ' || sqlerrm);
        END;
      END IF;
		END loop;
			dbms_output.put_line('Total Records Update:'|| v_rec_count);
	END gm_sav_lot_inv_qty;



/*********************************************************
	* Description : This procedure is used to correct the Transaction Log Vs Lot Qty dicrepancy
	*********************************************************/
	PROCEDURE gm_sav_txn_log_missing_grafts (
		p_help_desk_id		t914_helpdesk_log.c914_helpdesk_id%TYPE
	)
	AS
	v_curqty NUMBER := 0;
	v_ref_type VARCHAR2(20):= '103921';
	v_user VARCHAR2(20):= 305037;

	-- Cursor Values
	v_part_num VARCHAR2(20) := NULL;
	v_wh_type NUMBER := 90800; --FG
	v_txn_type NUMBER := 4310; -- Order
	v_location_id VARCHAR2(20) := NULL;

	-- Get the parts that have discrepancy when compare to Txn Log and Lot Inv
	CURSOR p_num_cursor IS
	  SELECT c205_part_number_id pnum
	  FROM
		  (SELECT t214.c205_part_number_id
					, t214.txn_log_qty
					, lot_qty
					, CASE WHEN txn_log_qty > lot_qty THEN txn_log_qty - lot_qty ELSE lot_qty - txn_log_qty END diff_qty
					, t214.c214_id ttxn_id
					, txn_log_trans_qty
					, txn_log_trans_type
					, created_by
		  FROM  
				( SELECT c205_part_number_id
						, c214_new_qty txn_log_qty
						, c214_created_dt
						, t214a.c214_id
						, t214a.c214_trans_qty txn_log_trans_qty
						, t214a.c901_type txn_log_trans_type
						, t214a.c214_created_by created_by
					FROM t214_transactions t214a 
					WHERE c214_created_dt = (
										SELECT MAX(c214_created_dt)
										FROM t214_transactions t214 
										WHERE t214.c205_part_number_id = t214a.c205_part_number_id
											 AND t214.c901_transaction_type = t214a.c901_transaction_type
									)
				 AND c901_transaction_type = 90800) t214, 
			( SELECT t5060.c205_part_number_id
						 , sum(c5060_qty) lot_qty
				FROM t5060_control_number_inv t5060 
				WHERE t5060.c901_warehouse_type =90800
				GROUP BY t5060.c205_part_number_id ) t5060
		WHERE t214.c205_part_number_id = t5060.c205_part_number_id
		)
	WHERE txn_log_qty != lot_qty
	ORDER BY c205_part_number_id;

	-- For the discrepancy part, get the grafts
	CURSOR lot_disc IS
	SELECT v_part_num part_num, c5060_control_number ctrl_no , v_wh_type wh_type, txnid txn_id, v_txn_type txn_type, v_location_id loc_id      
	  FROM
	  (SELECT c5060_control_number_inv_id, c5060_control_number, txnid, cnum
	  FROM
	    (SELECT t5060.c5060_control_number_inv_id, t5060.c5060_control_number 
		   FROM t5060_control_number_inv t5060 
	     WHERE t5060.c205_part_number_id = v_part_num
		     AND t5060.c5060_qty > 0
		     AND t5060.c901_warehouse_type =90800) t5060
	,
	(SELECT t502.c501_order_id txnid, t502.c502_control_number cnum
	 FROM t502_item_order t502
	 WHERE t502.c205_part_number_id = v_part_num
	) txns
	WHERE t5060.c5060_control_number = txns.cnum (+)
	)
	WHERE cnum IS NOT NULL;

	BEGIN

	FOR v_cursor IN p_num_cursor
	LOOP

		v_part_num := v_cursor.pnum;
		 
		dbms_output.put_line('Part : ' || v_part_num );
		
		FOR lot IN lot_disc
			LOOP
				BEGIN
					gm_pkg_op_ld_lot_track.gm_sav_control_number_inv(lot.part_num,lot.ctrl_no,lot.wh_type,v_curqty,lot.txn_id,lot.txn_type,v_ref_type,lot.loc_id,v_user);
					dbms_output.put_line('Part : ' || lot.part_num || ', Graft : ' || lot.ctrl_no || ' , Order:  ' || lot.txn_id  || ' updated.');
				EXCEPTION WHEN OTHERS THEN
					dbms_output.put_line('Part : ' || lot.part_num || ', Graft : ' || lot.ctrl_no || ' , Order:  ' || lot.txn_id  || ' Details:  ' || sqlerrm);
				END;
		END LOOP;
	END LOOP;

END gm_sav_txn_log_missing_grafts;

END it_pkg_bba_lot;
/
