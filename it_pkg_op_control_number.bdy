/* Formatted on 2011/01/07 10:34 (Formatter Plus v4.8.0) */

-- @"C:\Data Correction\Script\it_pkg_op_control_number.bdy"

CREATE OR REPLACE PACKAGE BODY it_pkg_op_control_number
IS
--
   /*
    * Updating expiry date+50 for the passing control number
    */
	PROCEDURE gm_sav_add_exp_date (
	    p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_company_id		 t2550_part_control_number.c1900_company_id%TYPE 
	  , p_lot_num		 t2550_part_control_number.c2550_control_number%TYPE 
	)
	AS
		v_expiry_date	   t2550_part_control_number.c2550_expiry_date%TYPE;
		v_user_id	   NUMBER; 
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;
		
		v_expiry_date	:= SYSDATE +50;	
		
	    UPDATE t2550_part_control_number t2550
		SET t2550.c2550_expiry_date      = v_expiry_date,
			t2550.c2550_last_updated_by = v_user_id,
			t2550.c2550_last_updated_date = sysdate
		WHERE t2550.c2550_control_number = p_lot_num
		AND t2550.c1900_company_id       = p_company_id;

		-- logs 
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, p_lot_num||' New expiry date: ' || TO_CHAR(v_expiry_date, 'MM/DD/YYYY')); 
		
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_add_exp_date;
	
   /*
    * Updating expiry date for the passing control number
    */
	PROCEDURE gm_sav_exp_date (
	    p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_company_id		 t2550_part_control_number.c1900_company_id%TYPE 
	  , p_lot_num		 t2550_part_control_number.c2550_control_number%TYPE
	  , p_expiry_date	   t2550_part_control_number.c2550_expiry_date%TYPE
	)
	AS		
		v_user_id	   NUMBER; 
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;	
		
		
	    UPDATE t2550_part_control_number t2550
		SET t2550.c2550_expiry_date      = p_expiry_date,
			t2550.c2550_last_updated_by = v_user_id,
			t2550.c2550_last_updated_date = sysdate
		WHERE t2550.c2550_control_number = p_lot_num
		AND t2550.c1900_company_id       = p_company_id;

		-- logs 
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, p_lot_num||' New expiry date: ' || TO_CHAR(p_expiry_date, 'MM/DD/YYYY')); 
		
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_exp_date;

   /*
    * Updating expiry date for the passing part number and control number
    */
	PROCEDURE gm_sav_exp_date_part_num (
	    p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_company_id		 t2550_part_control_number.c1900_company_id%TYPE 
	  , p_part_num           t2550_part_control_number.C205_PART_NUMBER_ID%TYPE
	  , p_lot_num		 t2550_part_control_number.c2550_control_number%TYPE
	  , p_expiry_date	   t2550_part_control_number.c2550_expiry_date%TYPE
	)
	AS		
		v_user_id	   NUMBER; 
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;	
		
		
	    UPDATE t2550_part_control_number t2550
		SET t2550.c2550_expiry_date      = p_expiry_date,
			t2550.c2550_last_updated_by = v_user_id,
			t2550.c2550_last_updated_date = sysdate
		WHERE t2550.c2550_control_number = p_lot_num
		AND t2550.C205_PART_NUMBER_ID = p_part_num
		AND t2550.c1900_company_id       = p_company_id;

		-- logs 
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'part number '||p_part_num||' Lot number '||p_lot_num||' New expiry date: ' || TO_CHAR(p_expiry_date, 'MM/DD/YYYY')); 
		
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_exp_date_part_num;
	 /*
    * To update the lot qty TSK-9156
    */
	PROCEDURE gm_sav_lot_qty (
	    p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_qty		 t5060_control_number_inv.c5060_qty%TYPE 
	  , p_trans_type           t5060_control_number_inv.c901_last_transaction_type%TYPE
	  , p_user_id		 t5060_control_number_inv.c5060_last_updated_by%TYPE
	  , p_inv_id	   t5060_control_number_inv.c5060_control_number_inv_id%TYPE
	)
	AS		
		v_user_id	   NUMBER; 
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;	
		
		
	    UPDATE t5060_control_number_inv 
		  SET c5060_qty = p_qty
		      ,c901_last_transaction_type = p_trans_type 
		      ,c5060_last_updated_by = p_user_id 
		      ,c5060_last_updated_date = SYSDATE 
			WHERE c5060_control_number_inv_id = p_inv_id;

		-- logs 
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Lot qty updated'); 
	--
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_lot_qty;


 /*
    * LPA label error: To update missing/incorrect UDI (TSK-10670)
    */
	PROCEDURE gm_upd_UDI (
	    p_help_desk_id	 t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_part_number		 t2550_part_control_number.c205_part_number_id%TYPE 
	  , p_cntrl_no     t2550_part_control_number.c2550_control_number%TYPE
    , p_udi_hrf        t2550_part_control_number.C2550_UDI_HRF%TYPE
    , p_udi_mrf        t2550_part_control_number.C2550_UDI_MRF%TYPE
	  , p_user_id		     t2550_part_control_number.c2550_last_updated_by%TYPE
	)
	AS		
		v_user_id	   NUMBER; 
    v_count      NUMBER;
	--
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;	

    SELECT count(1) INTO v_count FROM t2550_part_control_number WHERE c205_part_number_id = p_part_number  and c2550_control_number = p_cntrl_no; 
		
    IF (v_count = 0)
		THEN
      UPDATE T2550_PART_CONTROL_NUMBER 
      SET C2550_UDI_HRF = p_udi_hrf
       ,C2550_UDI_MRF = p_udi_mrf
       ,C2550_LAST_UPDATED_BY = v_user_id  
       ,C2550_LAST_UPDATED_DATE = SYSDATE 
     WHERE C205_PART_NUMBER_ID = p_part_number 
     AND C2550_CONTROL_NUMBER = p_cntrl_no; 
     -- log
  	it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'UDI_HRF: ' || p_udi_hrf);
    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'UDI_MRF: ' || p_udi_mrf);
   END IF;

  SELECT count(1) INTO v_count FROM t408_dhr WHERE c205_part_number_id = p_part_number  and c408_control_number = p_cntrl_no; 
 
  IF (v_count = 0)
  THEN
     UPDATE T408_DHR 
     SET C408_UDI_NO = p_udi_hrf 
        ,C408_LAST_UPDATED_BY = v_user_id 
        ,C408_LAST_UPDATED_DATE = SYSDATE 
     WHERE C205_PART_NUMBER_ID =  p_part_number
     AND C408_CONTROL_NUMBER = p_cntrl_no; 
    -- log
  	it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'UDI_HRF: ' || p_udi_hrf);
  END IF;

	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_upd_UDI;

 
END it_pkg_op_control_number;
/
