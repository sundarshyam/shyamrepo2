/* Formatted on 2011/12/02 17:03 (Formatter Plus v4.8.0) */
-- @"C:\Data Correction\Script\it_pkg_ac_field_tag.bdy"

CREATE OR REPLACE PACKAGE BODY it_pkg_ac_field_tag
IS
--

	/***************************************************
		Description : This procedure Adjust Consignemnt given in the Consignemnt Adjustment request Form
	*********************************************************/
	PROCEDURE gm_ac_sav_cons_adj (
		p_help_desk_id	 IN   t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_tag_id		 IN   t5010_tag.c5010_tag_id%TYPE
	  , p_setid 		 IN   t207_set_master.c207_set_id%TYPE
	  , p_pnum			 IN   t208_set_details.c205_part_number_id%TYPE
	  , p_distid		 IN   t701_distributor.c701_distributor_id%TYPE
	  , p_qty			 IN   NUMBER   --Part/Qty should be NULL for Set write up/off.
	  , p_type			 IN   VARCHAR2
	  , p_ext_refid 	 IN   VARCHAR2	 --used for TAG-ADJ-ONLY paramenter
	  , p_comment		 IN   VARCHAR2
	)
	AS
		v_user_id	   NUMBER;
		v_cnt		   NUMBER;
		v_out_refid    VARCHAR2 (20);
		v_tag_part	   t208_set_details.c205_part_number_id%TYPE;
		v_date		   VARCHAR2 (500);
		v_out_conid    VARCHAR2 (20);
		v_def_comment  VARCHAR2 (4000)
			:= 'This TXN is created as a part of the Field Sales Audit: As per the Consignment Adjustment Request Form';
		v_comment	   VARCHAR2 (4000) := NVL (p_comment, v_def_comment);
	--
	BEGIN
		--Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		SELECT TO_CHAR (SYSDATE, 'MM/DD/YYYY	HH24:MM:SS')
		  INTO v_date
		  FROM DUAL;

		SELECT COUNT (1)
		  INTO v_cnt
		  FROM t701_distributor
		 WHERE c701_distributor_id = p_distid AND c701_void_fl IS NULL;

		IF v_cnt = 0 THEN
			raise_application_error ('-20085', 'Invalid Distributor ID...!!!');
		END IF;

		IF p_ext_refid IS NOT NULL AND p_type <> 'TAG-ADJ-ONLY' THEN
			raise_application_error ('-20085', 'Ref ID can only be used with TAG-ADJ-ONLY...!!!');
		END IF;

		IF p_type = 'WRITE-OFF-SET' OR p_type = 'WRITE-UP-SET' THEN
			IF p_pnum IS NOT NULL OR p_qty <> 0 THEN
				raise_application_error ('-20085', 'Part/Qty should be NULL for Set write up/off.');
			END IF;

			SELECT COUNT (1)
			  INTO v_cnt
			  FROM t207_set_master
			 WHERE c207_set_id = p_setid AND c207_void_fl IS NULL;

			IF v_cnt = 0 THEN
				raise_application_error ('-20085', 'Invalid Set ID...!!!');
			END IF;

			IF p_type = 'WRITE-OFF-SET' THEN
				gm_pkg_ac_audit_reconciliation.gm_ac_sav_set_writeoff (p_setid
																	 , p_distid
																	 , v_user_id
																	 , v_comment
																	 , v_out_refid
																	  );
			END IF;

			IF p_type = 'WRITE-UP-SET' THEN
				gm_pkg_ac_audit_reconciliation.gm_ac_sav_set_writeup (p_setid
																	, p_distid
																	, v_user_id
																	, v_comment
																	, v_out_refid
																	 );
			END IF;

			IF p_tag_id IS NOT NULL AND p_setid IS NOT NULL AND p_distid IS NOT NULL AND p_qty = 0 THEN
				gm_ac_sav_link_tag (p_help_desk_id
								  , p_tag_id
								  , p_setid
								  , p_pnum
								  , p_distid
								  , p_qty
								  , p_type
								  , v_out_refid
								  , v_user_id
								   );
			END IF;
		ELSIF p_type = 'WRITE-OFF-PART' OR p_type = 'WRITE-UP-PART' THEN
			IF p_setid IS NOT NULL THEN
				raise_application_error ('-20085', 'Set ID should be NULL for part write up/off.');
			END IF;

			SELECT COUNT (1)
			  INTO v_cnt
			  FROM t205_part_number
			 WHERE c205_part_number_id = p_pnum;

			IF v_cnt = 0 THEN
				raise_application_error ('-20085', 'Invalid Part #...!!!');
			END IF;

			IF p_qty <> 1 THEN
				raise_application_error ('-20085', 'Please specify Qty as 1.');
			END IF;

			IF p_type = 'WRITE-OFF-PART' THEN
				gm_pkg_ac_audit_reconciliation.gm_ac_sav_part_writeoff (p_pnum
																	  , p_distid
																	  , p_qty
																	  , v_user_id
																	  , v_comment
																	  , v_out_refid
																	   );
			END IF;

			IF p_type = 'WRITE-UP-PART' THEN
				gm_pkg_ac_audit_reconciliation.gm_ac_sav_part_writeup (p_pnum
																	 , p_distid
																	 , p_qty
																	 , v_user_id
																	 , v_comment
																	 , v_out_refid
																	  );
			END IF;

			IF p_tag_id IS NOT NULL AND p_pnum IS NOT NULL AND p_distid IS NOT NULL AND p_qty = 1 THEN
				gm_ac_sav_link_tag (p_help_desk_id
								  , p_tag_id
								  , p_setid
								  , p_pnum
								  , p_distid
								  , p_qty
								  , p_type
								  , v_out_refid
								  , v_user_id
								   );
			END IF;
		ELSIF p_type = 'TAG-ADJ-ONLY' THEN
			IF	   p_tag_id IS NOT NULL
			   AND v_out_refid IS NULL
			   AND p_setid IS NOT NULL
			   AND p_pnum IS NULL
			   AND p_distid IS NOT NULL
			   AND p_qty = 0 THEN
				v_out_refid := p_ext_refid;
				gm_ac_sav_link_tag (p_help_desk_id
								  , p_tag_id
								  , p_setid
								  , p_pnum
								  , p_distid
								  , p_qty
								  , p_type
								  , v_out_refid
								  , v_user_id
								   );
			ELSE
				raise_application_error ('-20085', 'Please specify valid parameters for linking a TAG-ADJ-ONLY tag.');
			END IF;
		ELSIF p_type = 'TAG-TRANSFER-ONLY' THEN
			IF	   p_tag_id IS NOT NULL
			   AND v_out_refid IS NULL
			   AND p_setid IS NOT NULL
			   AND p_pnum IS NULL
			   AND p_distid IS NOT NULL
			   AND p_qty = 0 THEN
				gm_ac_sav_link_tag (p_help_desk_id
								  , p_tag_id
								  , p_setid
								  , p_pnum
								  , p_distid
								  , p_qty
								  , p_type
								  , v_out_refid
								  , v_user_id
								   );
			ELSE
				raise_application_error ('-20085'
									   , 'Please specify valid parameters for linking a TAG-TRANSFER-ONLY tag.'
										);
			END IF;
		ELSIF p_type = 'WRITE-OFF-INACTIVE-DIST' THEN
			IF p_tag_id IS NULL AND p_pnum IS NULL AND p_distid IS NOT NULL AND p_setid IS NULL AND p_qty = 0 THEN
				gm_pkg_ac_audit_reconciliation.gm_sav_inactive_dist_writeoff (p_distid
																			, v_user_id
																			, v_comment
																			, v_out_refid
																			, v_out_conid
																			 );
			ELSE
				raise_application_error ('-20085', 'Please specify valid parameters for  WRITE-OFF-INACTIVE-DIST.');
			END IF;
		ELSE
			raise_application_error ('-20085', 'Please specify type of Adjustment.');
		END IF;

		--
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id
											 ,	  ' Adj Type:'
											   || p_type
											   || '	  '
											   || '	  Tag ID:'
											   || p_tag_id
											   || '	  '
											   || '	 TXN ID:'
											   || v_out_refid
											   || '	  '
											   || '	 TXN ID:'
											   || v_out_conid
											   || '	  '
											   || '	  SET ID:'
											   || p_setid
											   || '	  '
											   || '	  PART#:'
											   || p_pnum
											   || '	  '
											   || '	  DIST ID:'
											   || p_distid
											   || '	  '
											   || '	  QTY:'
											   || p_qty
											   || '	  '
											   || '	  ADJUST DATE:'
											   || v_date
											  );
		--
		DBMS_OUTPUT.put_line (	 'Helpdesk # '
							  || p_help_desk_id
							  || ' Adj Type:'
							  || p_type
							  || '		'
							  || '		Tag ID:'
							  || p_tag_id
							  || '		'
							  || '	 TXN ID:'
							  || v_out_refid
							  || '		'
							  || '	 TXN ID:'
							  || v_out_conid
							  || '		'
							  || '		SET ID:'
							  || p_setid
							  || '		'
							  || '		PART#:'
							  || p_pnum
							  || '		'
							  || '		DIST ID:'
							  || p_distid
							  || '		'
							  || '		QTY:'
							  || p_qty
							  || '		'
							  || '		ADJUST DATE:'
							  || v_date
							 );
		COMMIT;
	--
	EXCEPTION
		WHEN OTHERS THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP:' || SQLERRM);
			ROLLBACK;
	END gm_ac_sav_cons_adj;

/***************************************************
		Description : This procedure is used to link tags
*********************************************************/
	PROCEDURE gm_ac_sav_link_tag (
		p_help_desk_id	 IN   t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_tag_id		 IN   t5010_tag.c5010_tag_id%TYPE
	  , p_setid 		 IN   t207_set_master.c207_set_id%TYPE
	  , p_pnum			 IN   t208_set_details.c205_part_number_id%TYPE
	  , p_distid		 IN   t701_distributor.c701_distributor_id%TYPE
	  , p_qty			 IN   NUMBER   --Part/Qty should be NULL for Set write up/off.
	  , p_type			 IN   VARCHAR2
	  , p_refid 		 IN   VARCHAR2
	  , p_user_id		 IN   t5010_tag.c5010_last_updated_by%TYPE
	)
	AS
		v_cnt		   NUMBER;
		v_tag_part	   t208_set_details.c205_part_number_id%TYPE;
		v_cnt_tag_part NUMBER;
		v_cnt_part	   NUMBER;
		v_status	   NUMBER;
	--
	BEGIN
		IF p_tag_id IS NOT NULL THEN
			IF p_type = 'WRITE-UP-SET' OR p_type = 'TAG-ADJ-ONLY' OR p_type = 'WRITE-UP-PART' THEN
				BEGIN
					SELECT c901_status
					  INTO v_status
					  FROM t5010_tag
					 WHERE c5010_tag_id = p_tag_id;
				EXCEPTION
					WHEN NO_DATA_FOUND THEN
						UPDATE t5010_tag t5010
						   SET t5010.c901_status = 51010   -- Available
							 , t5010.c5010_last_updated_by = 303497
							 , t5010.c5010_last_updated_date = SYSDATE
						 WHERE t5010.c5010_tag_id = p_tag_id AND t5010.c901_status IS NULL
							   AND t5010.c5010_void_fl IS NULL;
				END;

				UPDATE t5010_tag t5010
				   SET t5010.c901_status = 51011   --51011 released, 51010 available
					 , t5010.c5010_last_updated_by = p_user_id
					 , t5010.c5010_last_updated_date = SYSDATE
				 WHERE t5010.c5010_tag_id = p_tag_id AND c901_status = 51010 AND t5010.c5010_void_fl IS NULL;
			END IF;

			SELECT COUNT (1)
			  INTO v_cnt
			  FROM t5010_tag
			 WHERE c5010_tag_id = p_tag_id
			   AND c5010_void_fl IS NULL
			   AND c901_status =
					   DECODE (p_type
							 , 'WRITE-OFF-SET', 51012
							 , 'WRITE-UP-SET', 51011
							 , 'TAG-ADJ-ONLY', 51011
							 , 'TAG-TRANSFER-ONLY', 51012
							 , 'WRITE-OFF-PART', 51012
							 , 'WRITE-UP-PART', 51011
							 , 0
							  );   -- Release;

			IF v_cnt = 0 THEN
				raise_application_error ('-20085', 'Invalid Tag Status...!!!');
			END IF;

			IF p_setid IS NOT NULL THEN
				BEGIN
					SELECT t208.c205_part_number_id
					  INTO v_tag_part
					  FROM t208_set_details t208, t205d_part_attribute t205d
					 WHERE t208.c207_set_id = p_setid
					   AND t208.c205_part_number_id = t205d.c205_part_number_id
					   AND t208.c208_void_fl IS NULL
					   AND t205d.c901_attribute_type = 92340
					   AND t205d.c205d_void_fl IS NULL
					   AND t208.c208_inset_fl = 'Y';
				EXCEPTION
					WHEN NO_DATA_FOUND THEN
						v_tag_part	:= NULL;
				END;

				IF v_tag_part IS NULL THEN
					raise_application_error ('-20085', 'There should be only 1 taggable part for this set.');
				END IF;
			ELSIF p_pnum IS NOT NULL THEN
				SELECT COUNT (1)
				  INTO v_cnt_tag_part
				  FROM t205d_part_attribute t205d
				 WHERE t205d.c205_part_number_id = p_pnum AND t205d.c901_attribute_type = 92340;

				IF v_cnt_tag_part = 0 THEN
					raise_application_error ('-20085', 'Invalid Taggable Part #...!!!');
				END IF;

				v_tag_part	:= p_pnum;
			END IF;

			UPDATE t5010_tag t5010
			   SET t5010.c5010_control_number = 'NOC#'
				 , t5010.c5010_last_updated_trans_id =
					   DECODE (p_type
							 , 'WRITE-OFF-SET', p_refid
							 , 'WRITE-UP-SET', p_refid
							 , 'TAG-ADJ-ONLY', p_refid
							 , 'TAG-TRANSFER-ONLY', NULL
							 , 'WRITE-OFF-PART', p_refid
							 , 'WRITE-UP-PART', p_refid
							 , NULL
							  )
				 , t5010.c5010_location_id =
					   DECODE (p_type
							 , 'WRITE-OFF-SET', '52099'
							 , 'WRITE-UP-SET', p_distid
							 , 'TAG-ADJ-ONLY', p_distid
							 , 'TAG-TRANSFER-ONLY', p_distid
							 , 'WRITE-OFF-PART', '52099'
							 , 'WRITE-UP-PART', p_distid
							 , NULL
							  )
				 , t5010.c5010_void_fl = DECODE (p_type, 'WRITE-OFF-SET', 'Y', 'WRITE-OFF-PART', 'Y', NULL)
				 , t5010.c5010_lock_fl = 'Y'
				 , t5010.c205_part_number_id = v_tag_part
				 , t5010.c901_trans_type =
					   DECODE (p_type
							 , 'WRITE-OFF-SET', '51001'
							 , 'WRITE-UP-SET', '51000'
							 , 'TAG-ADJ-ONLY', '51000'
							 , 'TAG-TRANSFER-ONLY', '51000'
							 , 'WRITE-OFF-PART', '51001'
							 , 'WRITE-UP-PART', '51000'
							 , NULL
							  )   --Consignment
				 , t5010.c207_set_id = p_setid
				 , t5010.c901_location_type =
					   DECODE (p_type
							 , 'WRITE-OFF-SET', 40033
							 , 'WRITE-UP-SET', 4120
							 , 'TAG-ADJ-ONLY', 4120
							 , 'TAG-TRANSFER-ONLY', 4120
							 , 'WRITE-OFF-PART', 40033
							 , 'WRITE-UP-PART', 4120
							 , NULL
							  )   --4120--Distributor
				 , t5010.c901_status = 51012   --Active
				 , t5010.c5010_last_updated_by = p_user_id
				 , t5010.c5010_last_updated_date = SYSDATE
			 WHERE t5010.c5010_tag_id = p_tag_id AND t5010.c5010_void_fl IS NULL;

			IF SQL%ROWCOUNT = 0 THEN
				raise_application_error ('-20085', 'Zero(0) rows updated. Please vevify the parameters.!!!');
			ELSE
				it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id
													 ,	  '  Total Tags Updated:'
													   || p_tag_id
													   || '   '
													   || '  Count:'
													   || SQL%ROWCOUNT
													  );
			END IF;
		END IF;
	END gm_ac_sav_link_tag;
END it_pkg_ac_field_tag;
/
