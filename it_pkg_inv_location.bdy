/* Formatted on 2011/05/05 14:46 (Formatter Plus v4.8.0) */
-- @"C:\Data Correction\Script\it_pkg_inv_location.bdy";

CREATE OR REPLACE PACKAGE BODY it_pkg_inv_location
IS
	PROCEDURE gm_sav_loc_curr_qty (
		p_help_desk_id	 IN	  t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_partnum		 IN   VARCHAR2
	  , p_location		 IN   VARCHAR2
	  , p_qty			 IN   NUMBER
	)
	AS
	v_user_id	   NUMBER;
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		v_user_id	:= it_pkg_cm_user.get_valid_user;

		UPDATE t5053_location_part_mapping
		   SET c5053_curr_qty = p_qty
			 , c5053_last_updated_by = v_user_id
			 , c5053_last_updated_date = SYSDATE
			 , c5053_last_update_trans_id = 'INV-LOAD'
			 , c901_last_transaction_type = 4316
		 WHERE c5052_location_id = p_location AND c205_part_number_id = p_partnum;

		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id
											 ,	  'For '
											   || p_partnum
											   || ' and for '
											   || p_location
											   || ' the updated current qty is '
											   || p_qty
											  );
		DBMS_OUTPUT.put_line ('For ' || p_partnum || ' and for ' || p_location || ' the updated current qty is '
							  || p_qty
							 );
		COMMIT;
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_loc_curr_qty;
/***********************************************************************
  * Author : Arockia Prasath
  * Description : To update Inventory pick status updated for transaction
  ************************************************************************/ 	
	PROCEDURE gm_sav_inv_pick_assignee_sts (
		p_help_desk_id	 IN	  t914_helpdesk_log.c914_helpdesk_id%TYPE
	  , p_status		 IN   t5050_invpick_assign_detail.c901_status%TYPE
	  , p_void_fl		 IN   t5050_invpick_assign_detail.c5050_void_fl%TYPE
	  , p_trans_id		 IN   t5050_invpick_assign_detail.c5050_ref_id%TYPE
	  , p_invpick_assign_id IN t5050_invpick_assign_detail.c5050_invpick_assign_id%TYPE
	  , p_user_id			 IN   t5050_invpick_assign_detail.c5050_last_updated_by%TYPE
	)
	AS	
	BEGIN
		-- Mandatory helpdesk to log executed user information
		it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, NULL);
		
		UPDATE t5050_invpick_assign_detail 
        SET c901_status =NVL(p_status,'93006'), --completed 
          c5050_last_updated_by =p_user_id,
          c5050_void_fl = NVL(p_void_fl,c5050_void_fl),
          c5050_last_updated_date=sysdate 
        WHERE c5050_ref_id = p_trans_id 
        	AND c5050_invpick_assign_id= NVL(p_invpick_assign_id,c5050_invpick_assign_id); 

		-- logs
	    it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_help_desk_id, 'Inventory pick status updated for transaction : ' || p_trans_id) ;
		COMMIT;
	EXCEPTION
		WHEN OTHERS
		THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP gm_sav_inv_pick_assignee_status: ' || SQLERRM);
			ROLLBACK;
	END gm_sav_inv_pick_assignee_sts;
END it_pkg_inv_location;
/
